export const data = [
  {
    "info": "4hv3CbjBAeo8GYBWdYvRnobP638P5nQIDAQAB",
    "dn": {
      "uid": "93sam",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "uid": "93sam",
    "sn": "McIntyre",
    "uidNumber": "964",
    "keyFingerPrint": "CEBB52301D617E910390FE16587979573442684E",
    "gidNumber": "964",
    "ircNick": "Sledge",
    "labeledURI": "http://www.einval.com/~steve/"
  },
  {
    "info": "# aardvark, users, debian.org",
    "dn": {
      "uid": "aardvark",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aardvark",
    "objectClass": "debianDeveloper",
    "uidNumber": "3310",
    "ircNick": "aardvark",
    "cn": "Lucy",
    "sn": "Wayland",
    "gidNumber": "3310"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "aaron",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aaron",
    "uid": "aaron",
    "sn": "Howell",
    "labeledURI": "http://www.kitten.net.au/~aaron",
    "uidNumber": "1135",
    "ircNick": "Angelus",
    "gidNumber": "1638"
  },
  {
    "info": "# aaronl, users, debian.org",
    "dn": {
      "uid": "aaronl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aaron",
    "uid": "aaronl",
    "sn": "Lehmann",
    "uidNumber": "2388",
    "ircNick": "aaronl",
    "gidNumber": "2388"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "aaronv",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aaron",
    "uid": "aaronv",
    "uidNumber": "1215",
    "ircNick": "Aaron",
    "sn": "Van Couwenberghe",
    "gidNumber": "1664"
  },
  {
    "info": "# aba, users, debian.org",
    "dn": {
      "uid": "aba",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "aba",
    "sn": "Barth",
    "uidNumber": "2645",
    "ircNick": "aba",
    "keyFingerPrint": "010BF4B922AC26888C4F895F49BB63F18B4CCAD5",
    "gidNumber": "2645"
  },
  {
    "info": "# abe, users, debian.org",
    "dn": {
      "uid": "abe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "abe",
    "objectClass": "debianDeveloper",
    "uidNumber": "3058",
    "cn": "Axel",
    "sn": "Beckert",
    "keyFingerPrint": "2517B724C5F6CA9953296E612FF9CD59612616B5",
    "ircNick": "XTaran",
    "jabberJID": "abe@noone.org",
    "gidNumber": "3058",
    "labeledURI": "https://axel.beckert.ch/"
  },
  {
    "info": "# abhijith, users, debian.org",
    "dn": {
      "uid": "abhijith",
      "ou": "users",
      "dc": "org"
    },
    "uid": "abhijith",
    "objectClass": "debianDeveloper",
    "uidNumber": "3549",
    "gidNumber": "3549",
    "keyFingerPrint": "EF13EA26A698FF35FD7C902E863D4DF2ED9C28EF",
    "cn": "Abhijith",
    "sn": "PA",
    "ircNick": "bhe",
    "labeledURI": "https://abhijithpa.me"
  },
  {
    "info": "# abi, users, debian.org",
    "dn": {
      "uid": "abi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "abi",
    "objectClass": "debianDeveloper",
    "uidNumber": "2772",
    "keyFingerPrint": "75D8908EBD35E31D94D102EF10557B83807CAC25",
    "cn": "Michael",
    "sn": "Ablassmeier",
    "ircNick": "abi",
    "gidNumber": "2772"
  },
  {
    "info": "# aboll, users, debian.org",
    "dn": {
      "uid": "aboll",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aboll",
    "objectClass": "debianDeveloper",
    "uidNumber": "3515",
    "gidNumber": "3515",
    "keyFingerPrint": "E390B9700582FAEA959ACAD41EEF53D38A3A9C67",
    "cn": "Andreas",
    "sn": "Boll",
    "ircNick": "aboll"
  },
  {
    "info": "# abraham, users, debian.org",
    "dn": {
      "uid": "abraham",
      "ou": "users",
      "dc": "org"
    },
    "uid": "abraham",
    "objectClass": "debianDeveloper",
    "uidNumber": "3654",
    "gidNumber": "3654",
    "keyFingerPrint": "10F57F97CE7F4500803B2EAAF67DA33EE71DFDA9",
    "cn": "Abraham",
    "sn": "Raji",
    "ircNick": "abrahamr",
    "labeledURI": "https://abrahamraji.in"
  },
  {
    "info": "# absurd, users, debian.org",
    "dn": {
      "uid": "absurd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephan",
    "uid": "absurd",
    "sn": "Suerken",
    "uidNumber": "1045",
    "keyFingerPrint": "54EB6D089FA37C19A64590B3066DE8017E2B61D9",
    "gidNumber": "1045",
    "ircNick": "absurd"
  },
  {
    "info": "# abz, users, debian.org",
    "dn": {
      "uid": "abz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Abraham",
    "uid": "abz",
    "sn": "van der Merwe",
    "labeledURI": "http://oasis.frogfoot.net/",
    "uidNumber": "2327",
    "gidNumber": "2327"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ac",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anders",
    "uid": "ac",
    "uidNumber": "816",
    "sn": "Chrigstrom",
    "gidNumber": "816"
  },
  {
    "info": "20091208] ac@gnu.org, retired 2005-01-12",
    "dn": {
      "uid": "ac212",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alistair",
    "uid": "ac212",
    "uidNumber": "1195",
    "ircNick": "Ursa",
    "sn": "Cunningham",
    "gidNumber": "1653"
  },
  {
    "info": "# achernya, users, debian.org",
    "dn": {
      "uid": "achernya",
      "ou": "users",
      "dc": "org"
    },
    "uid": "achernya",
    "objectClass": "debianDeveloper",
    "uidNumber": "3327",
    "keyFingerPrint": "3224C4469D7DF8F3D6F41A02BBC756DDBE595F6B",
    "cn": "Alexander",
    "sn": "Chernyakhovsky",
    "gidNumber": "3327",
    "ircNick": "achernya",
    "labeledURI": "https://achernya.com"
  },
  {
    "info": "# acid, users, debian.org",
    "dn": {
      "uid": "acid",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Julien",
    "uid": "acid",
    "sn": "Danjou",
    "uidNumber": "2491",
    "ircNick": "jd_",
    "jabberJID": "jd@im.naquadah.org",
    "labeledURI": "http://julien.danjou.info",
    "gidNumber": "2491"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "aclark",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://ghoti.org",
    "cn": "Ashley",
    "uid": "aclark",
    "uidNumber": "2018",
    "sn": "Clark",
    "gidNumber": "2018"
  },
  {
    "info": "# acornet, users, debian.org",
    "dn": {
      "uid": "acornet",
      "ou": "users",
      "dc": "org"
    },
    "uid": "acornet",
    "objectClass": "debianDeveloper",
    "uidNumber": "2874",
    "cn": "Arnaud",
    "sn": "Cornet",
    "ircNick": "nohar",
    "jabberJID": "arnaud.cornet@gmail.com",
    "gidNumber": "2874"
  },
  {
    "info": "# acute, users, debian.org",
    "dn": {
      "uid": "acute",
      "ou": "users",
      "dc": "org"
    },
    "uid": "acute",
    "objectClass": "debianDeveloper",
    "uidNumber": "3591",
    "gidNumber": "3591",
    "keyFingerPrint": "85E7AAB8C29DF38349405DC06F8DE44D59D7DBCC",
    "cn": "Ana",
    "sn": "Custura",
    "ircNick": "acute"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "acz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anthony",
    "uid": "acz",
    "uidNumber": "1267",
    "sn": "Zboralski",
    "gidNumber": "1604"
  },
  {
    "info": "# adam, users, debian.org",
    "dn": {
      "uid": "adam",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://flounder.net",
    "cn": "Adam",
    "uid": "adam",
    "uidNumber": "2211",
    "ircNick": "ElBarono",
    "sn": "McKenna",
    "gidNumber": "2211"
  },
  {
    "info": "# adamm, users, debian.org",
    "dn": {
      "uid": "adamm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "adamm",
    "uidNumber": "2618",
    "sn": "Majer",
    "keyFingerPrint": "90B66A695BB4557F228CE989E523F220AC8DFBD0",
    "gidNumber": "2618",
    "ircNick": "adamm"
  },
  {
    "info": "# adconrad, users, debian.org",
    "dn": {
      "uid": "adconrad",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "adconrad",
    "sn": "Conrad",
    "uidNumber": "2504",
    "gidNumber": "2504",
    "ircNick": "infinity",
    "jabberJID": "adconrad@jabber.org",
    "labeledURI": "http://www.theadam.org/"
  },
  {
    "info": "# adejong, users, debian.org",
    "dn": {
      "uid": "adejong",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Arthur",
    "uid": "adejong",
    "uidNumber": "2622",
    "sn": "de Jong",
    "labeledURI": "http://arthurdejong.org/",
    "keyFingerPrint": "452EC2CB65CF68C2A1ADBF5F2A8B746810E0AFC1",
    "gidNumber": "2622"
  },
  {
    "info": "# adeodato, users, debian.org",
    "dn": {
      "uid": "adeodato",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adeodato",
    "uid": "adeodato",
    "uidNumber": "2735",
    "gidNumber": "2735",
    "sn": ": U2ltw7M="
  },
  {
    "info": "# adn, users, debian.org",
    "dn": {
      "uid": "adn",
      "ou": "users",
      "dc": "org"
    },
    "uid": "adn",
    "objectClass": "debianDeveloper",
    "uidNumber": "2815",
    "sn": "Trojette",
    "ircNick": "adn",
    "jabberJID": "adn@jabber.org",
    "labeledURI": "http://adn.diwi.org",
    "keyFingerPrint": "3E5FF5F704DA71974CFB4E31DE9F93584D474C1B",
    "gidNumber": "2815",
    "cn": ": TW9oYW1tZWQgQWRuw6huZQ=="
  },
  {
    "info": "# adrianorg, users, debian.org",
    "dn": {
      "uid": "adrianorg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "adrianorg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3437",
    "gidNumber": "3437",
    "keyFingerPrint": "3F2C078F09E85FB34E4E1BAE083781A2D2ACE48B",
    "cn": "Adriano",
    "sn": "Gomes",
    "ircNick": "adrianorg",
    "jabberJID": "adrianorg@arg.eti.br",
    "labeledURI": "https://arg.eti.br/"
  },
  {
    "info": "# adric, users, debian.org",
    "dn": {
      "uid": "adric",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gregory",
    "jabberJID": "haphazard@jabber.org",
    "uid": "adric",
    "sn": "Norris",
    "uidNumber": "1293",
    "gidNumber": "1293"
  },
  {
    "info": "# ads, users, debian.org",
    "dn": {
      "uid": "ads",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "ads",
    "sn": "Stribblehill",
    "uidNumber": "2201",
    "ircNick": "strib",
    "labeledURI": "http://people.debian.org/~ads/",
    "gidNumber": "2201"
  },
  {
    "info": "T5QX8oCc+/rWgpA8JiTj2XnCZ4WsTzxw1wIDAQAB",
    "dn": {
      "uid": "adsb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "adsb",
    "objectClass": "debianDeveloper",
    "uidNumber": "2949",
    "keyFingerPrint": "7203630E2C8E727251684FEBC5CE5DC2C542CD59",
    "sn": "Barratt",
    "gidNumber": "2949",
    "cn": "Adam",
    "ircNick": "adsb"
  },
  {
    "info": "# aeb, users, debian.org",
    "dn": {
      "uid": "aeb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "aeb",
    "sn": "Bombe",
    "uidNumber": "2183",
    "keyFingerPrint": "25714AECDBFDACEE1CE95FE77F6022516E869F64",
    "gidNumber": "2183",
    "ircNick": "aeb",
    "labeledURI": "https://activelow.net/"
  },
  {
    "info": "# aelmahmoudy, users, debian.org",
    "dn": {
      "uid": "aelmahmoudy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aelmahmoudy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3091",
    "keyFingerPrint": "8206A19620847E6D0DF8B176BC196A94EDDDA1B7",
    "ircNick": "AnAnt",
    "jabberJID": "aelmahmoudy@jabber.org",
    "gidNumber": "3091",
    "cn": ": 2KPYrdmF2K8=",
    "sn": ": 2KfZhNmF2K3ZhdmI2K/Zig=="
  },
  {
    "info": "# aerostitch, users, debian.org",
    "dn": {
      "uid": "aerostitch",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aerostitch",
    "objectClass": "debianDeveloper",
    "uidNumber": "3539",
    "gidNumber": "3539",
    "cn": "Joseph",
    "sn": "Herlant",
    "ircNick": "aerostitch"
  },
  {
    "info": "# afayolle, users, debian.org",
    "dn": {
      "uid": "afayolle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexandre",
    "uid": "afayolle",
    "uidNumber": "2671",
    "sn": "Fayolle",
    "ircNick": "agurney",
    "jabberJID": "alf@jabber.logilab.org",
    "gidNumber": "2671"
  },
  {
    "info": "# aferraris, users, debian.org",
    "dn": {
      "uid": "aferraris",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aferraris",
    "objectClass": "debianDeveloper",
    "uidNumber": "3648",
    "gidNumber": "3648",
    "keyFingerPrint": "796DB393DC3FF40222B6EA22D3EBB5966BB99196",
    "cn": "Arnaud",
    "sn": "Ferraris",
    "ircNick": "a-wai"
  },
  {
    "info": "# afif, users, debian.org",
    "dn": {
      "uid": "afif",
      "ou": "users",
      "dc": "org"
    },
    "uid": "afif",
    "objectClass": "debianDeveloper",
    "uidNumber": "3398",
    "gidNumber": "3398",
    "keyFingerPrint": "8EBD460CB464A67530FF39FBCEAE6AD3AFE826FB",
    "cn": "Afif",
    "sn": "Elghraoui",
    "labeledURI": "http://afif.ghraoui.name"
  },
  {
    "info": "# afrb2, users, debian.org",
    "dn": {
      "uid": "afrb2",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alan",
    "uid": "afrb2",
    "uidNumber": "895",
    "sn": "Bain",
    "labeledURI": "http://www.chiark.greenend.org.uk/~alanb/",
    "gidNumber": "895"
  },
  {
    "info": "# ag, users, debian.org",
    "dn": {
      "uid": "ag",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ag",
    "objectClass": "debianDeveloper",
    "uidNumber": "2915",
    "ircNick": "gixxer",
    "labeledURI": "http://people.debian.org/~ag/",
    "gidNumber": "2915",
    "cn": ": QXVyw6lsaWVu",
    "sn": ": R8OpcsO0bWU="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "agf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.ultraviolet.org/andrew/",
    "cn": "Andrew",
    "uid": "agf",
    "uidNumber": "1326",
    "ircNick": "gnarph",
    "sn": "Feinberg",
    "gidNumber": "1326"
  },
  {
    "info": "# agi, users, debian.org",
    "dn": {
      "uid": "agi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alberto",
    "uid": "agi",
    "sn": "Gonzalez Iniesta",
    "uidNumber": "2399",
    "keyFingerPrint": "5347CBD83E30A9EB4D7D4BF2009B33756B9AAA55",
    "ircNick": "agi",
    "jabberJID": "agi@inittab.org",
    "gidNumber": "2399"
  },
  {
    "info": "# agmartin, users, debian.org",
    "dn": {
      "uid": "agmartin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "agmartin",
    "uidNumber": "2083",
    "keyFingerPrint": "ED78D97FA0065393A48AC71398B9E4BEEA6CB7B3",
    "gidNumber": "2083",
    "cn": ": QWd1c3TDrW4=",
    "sn": ": TWFydMOtbiBEb21pbmdv"
  },
  {
    "info": "# agney, users, debian.org",
    "dn": {
      "uid": "agney",
      "ou": "users",
      "dc": "org"
    },
    "uid": "agney",
    "objectClass": "debianDeveloper",
    "uidNumber": "2788",
    "cn": "Agney",
    "sn": "Lopes Roth Ferraz",
    "ircNick": "agney",
    "jabberJID": "agney@jabber.org",
    "labeledURI": "http://people.debian.org/~agney",
    "gidNumber": "2788"
  },
  {
    "info": "# agx, users, debian.org",
    "dn": {
      "uid": "agx",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guido",
    "uid": "agx",
    "sn": "Guenther",
    "uidNumber": "2038",
    "ircNick": "agx",
    "jabberJID": "agx@sigxcpu.org",
    "labeledURI": "http://honk.sigxcpu.org",
    "gidNumber": "2038",
    "keyFingerPrint": "0DB3932762F78E592F6522AFBB5A2C77584122D3"
  },
  {
    "info": "# ah, users, debian.org",
    "dn": {
      "uid": "ah",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ah",
    "objectClass": "debianDeveloper",
    "uidNumber": "2900",
    "cn": "Andreas",
    "sn": "Henriksson",
    "keyFingerPrint": "FAE1E5B64652BE798E278CC20BC47DC64D135306",
    "gidNumber": "2900",
    "ircNick": "ah"
  },
  {
    "info": "# ahaley, users, debian.org",
    "dn": {
      "uid": "ahaley",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ahaley",
    "objectClass": "debianDeveloper",
    "uidNumber": "3192",
    "keyFingerPrint": "EAC843EBD3EFDB98CC772FADA5CD6035332FA671",
    "cn": "Andrew",
    "sn": "Haley",
    "gidNumber": "3192"
  },
  {
    "info": "# ahs3, users, debian.org",
    "dn": {
      "uid": "ahs3",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Al",
    "uid": "ahs3",
    "sn": "Stone",
    "uidNumber": "2637",
    "gidNumber": "2637",
    "ircNick": "ahs3",
    "labeledURI": "http://ahs3.net"
  },
  {
    "info": "# aigarius, users, debian.org",
    "dn": {
      "uid": "aigarius",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aigars",
    "uid": "aigarius",
    "sn": "Mahinovs",
    "uidNumber": "2119",
    "ircNick": "aigarius",
    "jabberJID": "aigarius@gmail.com",
    "labeledURI": "http://www.aigarius.com",
    "keyFingerPrint": "166C2BA88956443CDD637F46FA643BA61D227AFB",
    "gidNumber": "2119"
  },
  {
    "info": "# ajk, users, debian.org",
    "dn": {
      "uid": "ajk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Antti-Juhani",
    "uid": "ajk",
    "sn": "Kaijanaho",
    "uidNumber": "1329",
    "ircNick": "ibid",
    "labeledURI": "http://antti-juhani.kaijanaho.fi/",
    "gidNumber": "1329"
  },
  {
    "info": "# ajkessel, users, debian.org",
    "dn": {
      "uid": "ajkessel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "ajkessel",
    "uidNumber": "2659",
    "sn": "Kessel",
    "labeledURI": "http://adam.rosi-kessel.org",
    "gidNumber": "2659"
  },
  {
    "info": "# ajmitch, users, debian.org",
    "dn": {
      "uid": "ajmitch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "ajmitch",
    "uidNumber": "2706",
    "sn": "Mitchell",
    "gidNumber": "2706",
    "keyFingerPrint": "69862BA807198EF9FBC50EF0731991F308F24FEA",
    "ircNick": "ajmitch",
    "jabberJID": "ajmitch@jabber.org"
  },
  {
    "info": "# ajpg, users, debian.org",
    "dn": {
      "uid": "ajpg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "ajpg",
    "sn": "Gray",
    "labeledURI": "http://www.andrewgray.uklinux.net/",
    "uidNumber": "1336",
    "gidNumber": "1336"
  },
  {
    "info": "# ajqlee, users, debian.org",
    "dn": {
      "uid": "ajqlee",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ajqlee",
    "objectClass": "debianDeveloper",
    "uidNumber": "2992",
    "cn": "Andrew",
    "sn": "Lee",
    "keyFingerPrint": "EF4DD4947F74418A5F132255E7CBE152AC17F973",
    "gidNumber": "2992",
    "ircNick": "AndrewLee"
  },
  {
    "info": "# ajt, users, debian.org",
    "dn": {
      "uid": "ajt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anthony",
    "uid": "ajt",
    "uidNumber": "1159",
    "sn": "Towns",
    "keyFingerPrint": "4E56CFC8D2036C0D69F347786177B35582A8D8A6",
    "gidNumber": "1159",
    "ircNick": "aj",
    "labeledURI": "http://azure.humbug.org.au/~aj/"
  },
  {
    "info": "# akira, users, debian.org",
    "dn": {
      "uid": "akira",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Akira",
    "uid": "akira",
    "uidNumber": "1409",
    "sn": "Yamada",
    "labeledURI": "http://arika.org",
    "keyFingerPrint": "04FA3190D777DFC49F1BD28B93B3C2434E7609B8",
    "gidNumber": "1409"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "aklein",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "aklein",
    "uidNumber": "1144",
    "ircNick": "aklein",
    "sn": "Klein",
    "gidNumber": "1144"
  },
  {
    "info": "# akumar, users, debian.org",
    "dn": {
      "uid": "akumar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "akumar",
    "objectClass": "debianDeveloper",
    "uidNumber": "2908",
    "keyFingerPrint": "466FDDBF10560F509CDE3A4C7A8F49E8B63480BE",
    "cn": "Kumar",
    "sn": "Appaiah",
    "gidNumber": "2908",
    "ircNick": "kmap",
    "jabberJID": "kumar.appaiah@gmail.com",
    "labeledURI": "https://www.ee.iitb.ac.in/~akumar"
  },
  {
    "info": "# akumria, users, debian.org",
    "dn": {
      "uid": "akumria",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anand",
    "uid": "akumria",
    "sn": "Kumria",
    "uidNumber": "1199",
    "ircNick": "wildfire",
    "gidNumber": "1685"
  },
  {
    "info": "# alain, users, debian.org",
    "dn": {
      "uid": "alain",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alain",
    "uid": "alain",
    "sn": "Schroeder",
    "uidNumber": "2171",
    "ircNick": "AlainS",
    "jabberJID": "alain@parkautomat.net",
    "labeledURI": "http://www.parkautomat.net",
    "gidNumber": "2171"
  },
  {
    "info": "# alan, users, debian.org",
    "dn": {
      "uid": "alan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alan",
    "uid": "alan",
    "uidNumber": "1137",
    "sn": "Cox",
    "gidNumber": "1137"
  },
  {
    "info": "# alee, users, debian.org",
    "dn": {
      "uid": "alee",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ho-seok",
    "uid": "alee",
    "uidNumber": "2416",
    "sn": "Lee",
    "ircNick": "alee",
    "jabberJID": "hoseok.ee@gmail.com",
    "gidNumber": "2416"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "alegre",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fernando",
    "uid": "alegre",
    "uidNumber": "818",
    "sn": "Alegre",
    "gidNumber": "818"
  },
  {
    "info": "# alejandro, users, debian.org",
    "dn": {
      "uid": "alejandro",
      "ou": "users",
      "dc": "org"
    },
    "uid": "alejandro",
    "objectClass": "debianDeveloper",
    "uidNumber": "3233",
    "cn": "Alejandro",
    "sn": "Garrido Mota",
    "keyFingerPrint": "A3CAEB0BE94B06BE8EF84EB571396D61F1C09EFB",
    "gidNumber": "3233",
    "ircNick": "mogaal",
    "jabberJID": "garridomota@gmail.com",
    "labeledURI": "http://www.mogaal.com"
  },
  {
    "info": "# alerios, users, debian.org",
    "dn": {
      "uid": "alerios",
      "ou": "users",
      "dc": "org"
    },
    "uid": "alerios",
    "objectClass": "debianDeveloper",
    "uidNumber": "2882",
    "cn": "Alejandro",
    "ircNick": "alerios",
    "labeledURI": "http://alerios.blogspot.com",
    "gidNumber": "2882",
    "sn": ": UsOtb3MgUGXDsWE="
  },
  {
    "info": "# alessio, users, debian.org",
    "dn": {
      "uid": "alessio",
      "ou": "users",
      "dc": "org"
    },
    "uid": "alessio",
    "objectClass": "debianDeveloper",
    "uidNumber": "3074",
    "cn": "Alessio",
    "sn": "Treglia",
    "keyFingerPrint": "04160004A8276E40BB9890FBE8A48AE5311D765A",
    "gidNumber": "3074",
    "ircNick": "alessio",
    "labeledURI": "http://en.alessiotreglia.com"
  },
  {
    "info": "# alex, users, debian.org",
    "dn": {
      "uid": "alex",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "alex",
    "sn": "List",
    "uidNumber": "1172",
    "keyFingerPrint": "9D289851E71F9673192A1046159767DF3B773EC1",
    "ircNick": "alexlist",
    "jabberJID": "alex@im.list.priv.at",
    "gidNumber": "1172"
  },
  {
    "info": "# alexander, users, debian.org",
    "dn": {
      "uid": "alexander",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "alexander",
    "sn": "Neumann",
    "uidNumber": "2508",
    "ircNick": "fd0",
    "gidNumber": "2508"
  },
  {
    "info": "# alexm, users, debian.org",
    "dn": {
      "uid": "alexm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "alexm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3480",
    "gidNumber": "3480",
    "keyFingerPrint": "6940702AC6DEB565B648A4EDE3AE978E834E5E7E",
    "cn": "Alex",
    "sn": "Muntada",
    "ircNick": "alexm",
    "labeledURI": "https://log.alexm.org/"
  },
  {
    "info": "# alexp, users, debian.org",
    "dn": {
      "uid": "alexp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alex",
    "uid": "alexp",
    "sn": "Pennace",
    "uidNumber": "2384",
    "ircNick": {},
    "labeledURI": "http://osiris.978.org/~alex/",
    "keyFingerPrint": "861E40A353C217CCEA4C27B2759D33F5C15F0596",
    "gidNumber": "2384"
  },
  {
    "info": "# alexrp, users, debian.org",
    "dn": {
      "uid": "alexrp",
      "ou": "users",
      "dc": "org"
    },
    "uid": "alexrp",
    "objectClass": "debianDeveloper",
    "uidNumber": "3250",
    "keyFingerPrint": "FEE876B174CD42952BDEDE30B88A513E5649427B",
    "cn": "Alex",
    "sn": "Petersen",
    "gidNumber": "3250"
  },
  {
    "info": "# alexu, users, debian.org",
    "dn": {
      "uid": "alexu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Janusz",
    "uid": "alexu",
    "uidNumber": "2522",
    "sn": "Urbanowicz",
    "jabberJID": "alex@hell.pl",
    "gidNumber": "2522"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "alexy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.AlexY.org",
    "cn": "Alexander",
    "uid": "alexy",
    "uidNumber": "1048",
    "sn": "Yukhimets",
    "gidNumber": "1048"
  },
  {
    "info": "# alfie, users, debian.org",
    "dn": {
      "uid": "alfie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gerfried",
    "uid": "alfie",
    "sn": "Fuchs",
    "uidNumber": "2040",
    "ircNick": "Rhonda",
    "jabberJID": "rhonda@deb.at",
    "labeledURI": "http://rhonda.deb.at/debian/",
    "gidNumber": "2040"
  },
  {
    "info": "# alfs, users, debian.org",
    "dn": {
      "uid": "alfs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "alfs",
    "sn": "Alfredsson",
    "uidNumber": "2186",
    "keyFingerPrint": "55D640C2AE41CF92C4DC8804B9A81DDEB19B4B16",
    "labeledURI": "http://alfredsson.org",
    "gidNumber": "2186"
  },
  {
    "info": "# algernon, users, debian.org",
    "dn": {
      "uid": "algernon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gergely",
    "uid": "algernon",
    "sn": "Nagy",
    "uidNumber": "2146",
    "gidNumber": "2146",
    "ircNick": {},
    "labeledURI": "https://asylum.madhouse-project.org/"
  },
  {
    "info": "# aloiret, users, debian.org",
    "dn": {
      "uid": "aloiret",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aloiret",
    "objectClass": "debianDeveloper",
    "uidNumber": "2958",
    "cn": "Arthur",
    "sn": "Loiret",
    "ircNick": "arthur",
    "gidNumber": "2958"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "alpha",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "alpha",
    "uidNumber": "2459",
    "ircNick": "alpha",
    "sn": "Byrtek",
    "gidNumber": "2459"
  },
  {
    "info": "# alteholz, users, debian.org",
    "dn": {
      "uid": "alteholz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "alteholz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3090",
    "cn": "Thorsten",
    "sn": "Alteholz",
    "keyFingerPrint": "C74F6AC9E933B3067F52F33FA459EC6715B0705F",
    "gidNumber": "3090",
    "ircNick": "alteholz"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "alvar",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alvar",
    "uid": "alvar",
    "uidNumber": "819",
    "sn": "Bray",
    "gidNumber": "819"
  },
  {
    "info": "# amacater, users, debian.org",
    "dn": {
      "uid": "amacater",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "amacater",
    "uidNumber": "1268",
    "sn": "Martin Adrian Cater",
    "keyFingerPrint": "55965E3993E06E2B5BA5CD844AA8FC2422EF1F0F",
    "gidNumber": "1708",
    "ircNick": "amacater"
  },
  {
    "info": "# amaya, users, debian.org",
    "dn": {
      "uid": "amaya",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Amaya",
    "uid": "amaya",
    "sn": "Rodrigo Sastre",
    "uidNumber": "2361",
    "keyFingerPrint": "FB693A748E2BF8E01C4BC8E2C3016ACB317F2731",
    "gidNumber": "2361",
    "ircNick": "amaya",
    "labeledURI": "http://www.amayita.com/"
  },
  {
    "info": "# amejia, users, debian.org",
    "dn": {
      "uid": "amejia",
      "ou": "users",
      "dc": "org"
    },
    "uid": "amejia",
    "objectClass": "debianDeveloper",
    "uidNumber": "3145",
    "cn": "Andres",
    "sn": "Mejia",
    "ircNick": "amejia",
    "gidNumber": "3145"
  },
  {
    "info": "# ametzler, users, debian.org",
    "dn": {
      "uid": "ametzler",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "ametzler",
    "sn": "Metzler",
    "uidNumber": "2571",
    "keyFingerPrint": "D2E092039EF8D496EDF4FA5EA54F018543821484",
    "gidNumber": "2571",
    "ircNick": "nutmeg",
    "labeledURI": "https://www.bebt.de/"
  },
  {
    "info": "20091208] Retired 02/04/2001 20010402093803.B29921@web1.lanscape.net",
    "dn": {
      "uid": "amos",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Amos",
    "uid": "amos",
    "uidNumber": "1030",
    "sn": "Shapira",
    "gidNumber": "1030"
  },
  {
    "info": "# amu, users, debian.org",
    "dn": {
      "uid": "amu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "amu",
    "sn": "Mueller",
    "uidNumber": "2140",
    "ircNick": "amu",
    "jabberJID": "amu@jabber.org",
    "labeledURI": "http://people.debian.org/~amu",
    "gidNumber": "2140"
  },
  {
    "info": "# ana, users, debian.org",
    "dn": {
      "uid": "ana",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ana",
    "objectClass": "debianDeveloper",
    "uidNumber": "2822",
    "keyFingerPrint": "7A33ECAA188B96F27C917288B3464F896AA15948",
    "cn": "Ana",
    "gidNumber": "2822",
    "sn": ": R3VlcnJlcm8gTMOzcGV6",
    "ircNick": "ana",
    "labeledURI": "http://ekaia.org"
  },
  {
    "info": "sKW7JMvFzX8+O5pWfSpRpdPatOt80yy0eqpm1uQIDAQAB",
    "dn": {
      "uid": "anarcat",
      "ou": "users",
      "dc": "org"
    },
    "uid": "anarcat",
    "objectClass": "debianDeveloper",
    "uidNumber": "3160",
    "cn": "Antoine",
    "gidNumber": "3160",
    "sn": ": QmVhdXByw6k=",
    "ircNick": "anarcat",
    "jabberJID": "anarcat@riseup.net",
    "labeledURI": "http://anarc.at/",
    "keyFingerPrint": "BBB6CD4C98D74E1358A752A602293A6FA4E53473"
  },
  {
    "info": "# anbe, users, debian.org",
    "dn": {
      "uid": "anbe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "anbe",
    "objectClass": "debianDeveloper",
    "uidNumber": "3236",
    "keyFingerPrint": "EBF30A30A8D9C63BDA44C6945FB33F9359E9ED08",
    "cn": "Andreas",
    "sn": "Beckmann",
    "gidNumber": "3236",
    "ircNick": "anbe"
  },
  {
    "info": "# and, users, debian.org",
    "dn": {
      "uid": "and",
      "ou": "users",
      "dc": "org"
    },
    "uid": "and",
    "objectClass": "debianDeveloper",
    "uidNumber": "3051",
    "cn": "Andrea",
    "sn": "Veri",
    "ircNick": "and`",
    "jabberJID": "av@jabber.gnome.org",
    "labeledURI": "http://people.gnome.org/~av/",
    "gidNumber": "3051"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "and1000",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Austin",
    "uid": "and1000",
    "uidNumber": "820",
    "sn": "Donnelly",
    "gidNumber": "820"
  },
  {
    "info": "# andersee, users, debian.org",
    "dn": {
      "uid": "andersee",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Erik",
    "uid": "andersee",
    "sn": "Andersen",
    "uidNumber": "956",
    "ircNick": "andersee",
    "labeledURI": "http://codepoet.org/andersen/erik/erik.html",
    "gidNumber": "956"
  },
  {
    "info": "# andersk, users, debian.org",
    "dn": {
      "uid": "andersk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andersk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3443",
    "gidNumber": "3443",
    "keyFingerPrint": "6A3C67D7F011900CC539248BDFF3AB374F16F73A",
    "cn": "Anders",
    "sn": "Kaseorg"
  },
  {
    "info": "# anderson, users, debian.org",
    "dn": {
      "uid": "anderson",
      "ou": "users",
      "dc": "org"
    },
    "uid": "anderson",
    "objectClass": "debianDeveloper",
    "uidNumber": "2855",
    "cn": "Stuart",
    "sn": "Anderson",
    "ircNick": "anderson_s",
    "labeledURI": "http://www.netsweng.com/~anderson/",
    "gidNumber": "2855"
  },
  {
    "info": "# andete, users, debian.org",
    "dn": {
      "uid": "andete",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andete",
    "objectClass": "debianDeveloper",
    "uidNumber": "2825",
    "cn": "Joost",
    "sn": "Damad",
    "ircNick": "andete",
    "jabberJID": "andete@gmail.com",
    "labeledURI": "http://damad.be/joost/",
    "gidNumber": "2825"
  },
  {
    "info": "# andi, users, debian.org",
    "dn": {
      "uid": "andi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3204",
    "keyFingerPrint": "938A5CEE1E290DE255D9AC98B01FEA84617B586D",
    "cn": "Andreas",
    "sn": "Mundt",
    "gidNumber": "3204",
    "jabberJID": "andi#"
  },
  {
    "info": "# andrade, users, debian.org",
    "dn": {
      "uid": "andrade",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andrade",
    "objectClass": "debianDeveloper",
    "uidNumber": "3595",
    "gidNumber": "3595",
    "keyFingerPrint": "1D38EE3C624F955CE1FA3C855A303591F8CDB08B",
    "cn": "Thiago",
    "sn": "Marques",
    "ircNick": "andrade",
    "labeledURI": "https://andrade.wiki.br"
  },
  {
    "info": "91208]",
    "dn": {
      "uid": "andre",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://jota.sm.luth.se/~anedah-9/",
    "uid": "andre",
    "uidNumber": "2245",
    "ircNick": "andred",
    "sn": "Dahlqvist",
    "gidNumber": "2245",
    "cn": ": QW5kcsOp"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "andrea",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.linux.it/~andrea",
    "cn": "Andrea",
    "uid": "andrea",
    "uidNumber": "1287",
    "ircNick": "andreaf",
    "sn": "Fanfani",
    "gidNumber": "1287"
  },
  {
    "info": "# andreas, users, debian.org",
    "dn": {
      "uid": "andreas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "andreas",
    "sn": "Schuldei",
    "uidNumber": "2118",
    "ircNick": "stockholm",
    "labeledURI": "http://www.schuldei.org",
    "gidNumber": "2118"
  },
  {
    "info": "# andree, users, debian.org",
    "dn": {
      "uid": "andree",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andree",
    "objectClass": "debianDeveloper",
    "uidNumber": "2783",
    "cn": "Andree",
    "sn": "Leidenfrost",
    "gidNumber": "2783",
    "keyFingerPrint": "418EE169FC4B17E2A06DBD229C0B90A8C51C3C58"
  },
  {
    "info": "# andrelop, users, debian.org",
    "dn": {
      "uid": "andrelop",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andre",
    "uid": "andrelop",
    "uidNumber": "2593",
    "sn": "Luis Lopes",
    "keyFingerPrint": "9E9A41001622548B562493E3B6F6F753E15B47E9",
    "ircNick": "andrelop",
    "jabberJID": "andrelop@jabber.org",
    "labeledURI": "http://people.debian.org/~andrelop/",
    "gidNumber": "2593"
  },
  {
    "info": "# andres, users, debian.org",
    "dn": {
      "uid": "andres",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andres",
    "objectClass": "debianDeveloper",
    "uidNumber": "3317",
    "cn": "Andres",
    "sn": "Freund",
    "gidNumber": "3317"
  },
  {
    "info": "# andressh, users, debian.org",
    "dn": {
      "uid": "andressh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andres",
    "uid": "andressh",
    "sn": "Seco Hernandez",
    "uidNumber": "2405",
    "ircNick": "andressh-sslowly",
    "jabberJID": "sslowly@jabber.guadawireless.org",
    "labeledURI": "http://www.andressh.net/",
    "gidNumber": "2405"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "andrew",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://home.it.net.au/~andrew",
    "cn": "Andrew",
    "uid": "andrew",
    "uidNumber": "821",
    "ircNick": "Avingar",
    "sn": "Howell",
    "gidNumber": "821"
  },
  {
    "info": "ail.com>",
    "dn": {
      "uid": "andrewl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.lenharth.org/~andrewl/",
    "cn": "Andrew",
    "uid": "andrewl",
    "uidNumber": "1399",
    "ircNick": "darthscsi",
    "sn": "Lenharth",
    "gidNumber": "1399"
  },
  {
    "info": "s.scw.cloud.",
    "dn": {
      "uid": "andrewsh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "andrewsh",
    "objectClass": "debianDeveloper",
    "uidNumber": "3248",
    "sn": "Shadura",
    "gidNumber": "3248",
    "ircNick": "andrewsh",
    "labeledURI": "https://shadura.me/",
    "cn": "Andrej",
    "keyFingerPrint": "83DCD17F44B22CC83656EDA1E8446B4AC8C77261"
  },
  {
    "info": "20091208] resigned (<87y9m7qmne.fsf@zetnet.co.uk>)",
    "dn": {
      "uid": "andy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andy",
    "uid": "andy",
    "uidNumber": "998",
    "sn": "Mortimer",
    "gidNumber": "998"
  },
  {
    "info": "# andyli, users, debian.org",
    "dn": {
      "uid": "andyli",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3566",
    "gidNumber": "3566",
    "keyFingerPrint": "DF01A9229726D0FA42F757066DAC3C448773381A",
    "cn": "Andy",
    "sn": "Li",
    "labeledURI": "https://blog.onthewings.net",
    "uid": "andyli"
  },
  {
    "info": "# anfra, users, debian.org",
    "dn": {
      "uid": "anfra",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "anfra",
    "uidNumber": "1145",
    "sn": "Franzen",
    "gidNumber": "1145",
    "keyFingerPrint": "AF2D747048DED4ACE5565B7BFAB5556B2E2FB6A3"
  },
  {
    "info": "# angdraug, users, debian.org",
    "dn": {
      "uid": "angdraug",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dmitry",
    "uid": "angdraug",
    "uidNumber": "2480",
    "sn": "Borodaenko",
    "keyFingerPrint": "7FD9D652BF5FD2EC5F1391B0798F1E35CB4D38A9",
    "gidNumber": "2480",
    "ircNick": "angdraug"
  },
  {
    "info": "# angel, users, debian.org",
    "dn": {
      "uid": "angel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "angel",
    "objectClass": "debianDeveloper",
    "uidNumber": "3165",
    "cn": "Angel",
    "sn": "Abad",
    "keyFingerPrint": "4A0897191621CCF7FBEEF752A6D58816010A1096",
    "gidNumber": "3165",
    "ircNick": "angelabad",
    "jabberJID": "angelabad@gmail.com",
    "labeledURI": "https://angelabad.me"
  },
  {
    "info": "# anibal, users, debian.org",
    "dn": {
      "uid": "anibal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anibal",
    "uid": "anibal",
    "sn": "Monsalve Salazar",
    "uidNumber": "2708",
    "keyFingerPrint": "C6045C813887B77C2DFF97A57C56ACFE947897D8",
    "gidNumber": "2708",
    "ircNick": "anibal",
    "labeledURI": "http://people.debian.org/~anibal/"
  },
  {
    "info": "# anle, users, debian.org",
    "dn": {
      "uid": "anle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.uiuc.edu/ph/www/anle",
    "cn": "An",
    "uid": "anle",
    "uidNumber": "2158",
    "ircNick": "Vetinari8",
    "sn": "Le",
    "gidNumber": "2158"
  },
  {
    "info": "# anoe, users, debian.org",
    "dn": {
      "uid": "anoe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "anoe",
    "objectClass": "debianDeveloper",
    "uidNumber": "3353",
    "keyFingerPrint": "EB532A2C0653E521E4B7214CAAF8AE16305D6E2C",
    "cn": "Alexandre",
    "gidNumber": "3353",
    "sn": ": RGVsYW5vw6s=",
    "ircNick": "anoe",
    "labeledURI": "http://alexandre.delanoe.org"
  },
  {
    "info": "DXTqjzRjknlJuZXKLXcpnRuwG5zywIDAQAB",
    "dn": {
      "uid": "ansgar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ansgar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3093",
    "keyFingerPrint": "80E976F14A508A48E9CA3FE9BC372252CA1CF964",
    "gidNumber": "3093",
    "cn": "-",
    "sn": "Ansgar"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "anton",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anton",
    "uid": "anton",
    "uidNumber": "2084",
    "ircNick": "anton",
    "sn": "Gyllenberg",
    "gidNumber": "2084"
  },
  {
    "info": "# antonio, users, debian.org",
    "dn": {
      "uid": "antonio",
      "ou": "users",
      "dc": "org"
    },
    "uid": "antonio",
    "objectClass": "debianDeveloper",
    "uidNumber": "3048",
    "cn": "Antonio",
    "sn": "Radici",
    "keyFingerPrint": "40E6D8AC1900D5246B7CE6B535C8C88872CB1EED",
    "gidNumber": "3048"
  },
  {
    "info": "# anupa, users, debian.org",
    "dn": {
      "uid": "anupa",
      "ou": "users",
      "dc": "org"
    },
    "uid": "anupa",
    "objectClass": "debianDeveloper",
    "uidNumber": "3659",
    "gidNumber": "3659",
    "keyFingerPrint": "773BE692F0F9121C5D002E7CEE94D766BAE97405",
    "cn": "Anupa",
    "sn": "Joseph"
  },
  {
    "info": "# anuradha, users, debian.org",
    "dn": {
      "uid": "anuradha",
      "ou": "users",
      "dc": "org"
    },
    "uid": "anuradha",
    "objectClass": "debianDeveloper",
    "uidNumber": "3603",
    "gidNumber": "3603",
    "keyFingerPrint": "F56B8F14E014CDEF5D047FC1636DB5A1D91860FD",
    "cn": "Anuradha",
    "sn": "Weeraman",
    "ircNick": "aweeraman",
    "labeledURI": "https://anuradha.dev"
  },
  {
    "info": "# apeeters, users, debian.org",
    "dn": {
      "uid": "apeeters",
      "ou": "users",
      "dc": "org"
    },
    "uid": "apeeters",
    "objectClass": "debianDeveloper",
    "uidNumber": "2933",
    "cn": "Adriaan",
    "sn": "Peeters",
    "gidNumber": "2933"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "apenwarr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://people.nit.ca/~apenwarr/",
    "cn": "Avery",
    "uid": "apenwarr",
    "uidNumber": "1131",
    "ircNick": "apenwarr",
    "sn": "Pennarun",
    "gidNumber": "1709"
  },
  {
    "info": "# aph, users, debian.org",
    "dn": {
      "uid": "aph",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "aph",
    "sn": "Di Carlo",
    "labeledURI": "http://people.debian.org/~aph/",
    "uidNumber": "1129",
    "ircNick": "aph",
    "gidNumber": "1619"
  },
  {
    "info": "# apo, users, debian.org",
    "dn": {
      "uid": "apo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "apo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3383",
    "gidNumber": "3383",
    "keyFingerPrint": "ACF3D088EF32EDEF6A1A835FD9AD14B9513B51E4",
    "cn": "Markus",
    "sn": "Koschany",
    "ircNick": "apo",
    "labeledURI": "https://gambaru.de"
  },
  {
    "info": "# apoikos, users, debian.org",
    "dn": {
      "uid": "apoikos",
      "ou": "users",
      "dc": "org"
    },
    "uid": "apoikos",
    "objectClass": "debianDeveloper",
    "uidNumber": "3290",
    "keyFingerPrint": "3E02FD6656295952110BAB99F51B18C720248224",
    "cn": "Apollon",
    "sn": "Oikonomopoulos",
    "ircNick": "apoikos",
    "jabberJID": "apoikos@dmesg.gr",
    "labeledURI": "http://dmesg.gr",
    "gidNumber": "3290"
  },
  {
    "info": "# apollock, users, debian.org",
    "dn": {
      "uid": "apollock",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "apollock",
    "sn": "Pollock",
    "uidNumber": "2588",
    "keyFingerPrint": "5B702496CFA6A507239C2D7951DFD93B3EFB79EF",
    "gidNumber": "2588",
    "ircNick": "Caesar",
    "jabberJID": "andrew@pollock.id.au",
    "labeledURI": "http://blog.andrew.net.au/"
  },
  {
    "info": "# appaji, users, debian.org",
    "dn": {
      "uid": "appaji",
      "ou": "users",
      "dc": "org"
    },
    "uid": "appaji",
    "objectClass": "debianDeveloper",
    "uidNumber": "2947",
    "cn": "Giridhar Appaji Nag",
    "sn": "Yasa",
    "ircNick": "appaji",
    "labeledURI": "http://appaji.net/",
    "gidNumber": "2947"
  },
  {
    "info": "# appro, users, debian.org",
    "dn": {
      "uid": "appro",
      "ou": "users",
      "dc": "org"
    },
    "uid": "appro",
    "objectClass": "debianDeveloper",
    "uidNumber": "3346",
    "keyFingerPrint": "B652F27F2B8D1B8DA78D7061BA6CDA461FE8E023",
    "cn": "Andy",
    "sn": "Polyakov",
    "gidNumber": "3346"
  },
  {
    "info": "# apw, users, debian.org",
    "dn": {
      "uid": "apw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "apw",
    "objectClass": "debianDeveloper",
    "uidNumber": "2894",
    "keyFingerPrint": "BB1838728F40B7E63BFCD67AE52707E90485EFCD",
    "cn": "Amos",
    "sn": "Waterland",
    "gidNumber": "2894"
  },
  {
    "info": "# aquette, users, debian.org",
    "dn": {
      "uid": "aquette",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Arnaud",
    "uid": "aquette",
    "uidNumber": "2602",
    "sn": "Quette",
    "ircNick": "Uzuul",
    "labeledURI": "http://arnaud.quette.free.fr/",
    "gidNumber": "2602"
  },
  {
    "info": "# ar, users, debian.org",
    "dn": {
      "uid": "ar",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yasuhiro",
    "keyFingerPrint": "BADAB1EEB153F414CFA505F4AEEC6E7302754958",
    "uid": "ar",
    "uidNumber": "2475",
    "sn": "Araki",
    "labeledURI": "http://www.araki.net/",
    "gidNumber": "2475"
  },
  {
    "info": "# arafune, users, debian.org",
    "dn": {
      "uid": "arafune",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ryuichi",
    "uid": "arafune",
    "uidNumber": "1420",
    "sn": "Arafune",
    "gidNumber": "1420"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "araqnid",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "uid": "araqnid",
    "sn": "Haslam",
    "labeledURI": "http://araqnid.homelinux.net/",
    "uidNumber": "1359",
    "ircNick": "araqnid",
    "gidNumber": "1359"
  },
  {
    "info": "# ardo, users, debian.org",
    "dn": {
      "uid": "ardo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ardo",
    "uid": "ardo",
    "uidNumber": "1059",
    "sn": "van Rangelrooij",
    "labeledURI": "http://people.debian.org/~ardo",
    "keyFingerPrint": "274110CCBCD0DFDF7591B3E583B4B2DC119A098F",
    "gidNumber": "1059"
  },
  {
    "info": "# ari, users, debian.org",
    "dn": {
      "uid": "ari",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ari",
    "uid": "ari",
    "uidNumber": "2515",
    "sn": "Pollak",
    "keyFingerPrint": "1998FD821B7124F525B25133888220B87E798989",
    "gidNumber": "2515",
    "ircNick": "ari",
    "labeledURI": "http://www.aripollak.com"
  },
  {
    "info": "# arjan, users, debian.org",
    "dn": {
      "uid": "arjan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arjan",
    "objectClass": "debianDeveloper",
    "uidNumber": "2823",
    "cn": "Arjan",
    "sn": "Oosting",
    "gidNumber": "2823",
    "ircNick": "shutdown_-h_now"
  },
  {
    "info": "# arnau, users, debian.org",
    "dn": {
      "uid": "arnau",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arnau",
    "objectClass": "debianDeveloper",
    "uidNumber": "2902",
    "cn": "Arnaud",
    "sn": "Fontaine",
    "keyFingerPrint": "43B51C2AAAD3E0D49ACF8F5997C2C6AB313C7B03",
    "gidNumber": "2902",
    "ircNick": "arnau"
  },
  {
    "info": "# arnaudr, users, debian.org",
    "dn": {
      "uid": "arnaudr",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arnaudr",
    "objectClass": "debianDeveloper",
    "uidNumber": "3616",
    "gidNumber": "3616",
    "keyFingerPrint": "9D3BEC87B4563C642B7BE38AC0831D1F15E0DA64",
    "cn": "Arnaud",
    "sn": "Rebillout"
  },
  {
    "info": "# arnd, users, debian.org",
    "dn": {
      "uid": "arnd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Arnd",
    "uid": "arnd",
    "uidNumber": "2652",
    "sn": "Bergmann",
    "gidNumber": "2652"
  },
  {
    "info": "# arno, users, debian.org",
    "dn": {
      "uid": "arno",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arno",
    "objectClass": "debianDeveloper",
    "uidNumber": "3130",
    "keyFingerPrint": "3BE9A67148F348F23E1E2076C72B51EE9D80F36D",
    "cn": "Arno",
    "gidNumber": "3130",
    "sn": ": VMO2bGw=",
    "ircNick": "daemonkeeper"
  },
  {
    "info": "# aroldan, users, debian.org",
    "dn": {
      "uid": "aroldan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "aroldan",
    "uidNumber": "2617",
    "ircNick": "aroldan",
    "jabberJID": "aroldan@fluidsignal.com",
    "labeledURI": "http://people.fluidsignal.com/~aroldan",
    "gidNumber": "2617",
    "cn": ": QW5kcsOpcw==",
    "sn": ": Um9sZMOhbg=="
  },
  {
    "info": "# aron, users, debian.org",
    "dn": {
      "uid": "aron",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aron",
    "objectClass": "debianDeveloper",
    "uidNumber": "3137",
    "keyFingerPrint": "3A9E7D149697510A3E37CD95C38E8160A17841FE",
    "cn": "Aron",
    "sn": "Xu",
    "ircNick": "happyaron",
    "gidNumber": "3137"
  },
  {
    "info": "# arthur, users, debian.org",
    "dn": {
      "uid": "arthur",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Arthur",
    "uid": "arthur",
    "sn": "Korn",
    "uidNumber": "2135",
    "ircNick": "_2ri",
    "gidNumber": "2135"
  },
  {
    "info": "# arthurliu, users, debian.org",
    "dn": {
      "uid": "arthurliu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arthurliu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3069",
    "keyFingerPrint": "95908AA6E4F7BAA78BD6C148F1A69BE429C0FFEE",
    "cn": "Obey",
    "sn": "Liu",
    "ircNick": "ArthurLiu",
    "labeledURI": "http://www.milliways.fr",
    "gidNumber": "3069"
  },
  {
    "info": "# arturo, users, debian.org",
    "dn": {
      "uid": "arturo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arturo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3442",
    "gidNumber": "3442",
    "keyFingerPrint": "DD9861AB23DC3333892E07A968E713981D1515F8",
    "cn": "Arturo",
    "sn": ": Qm9ycmVybyBHb256w6FsZXo=",
    "ircNick": "arturo"
  },
  {
    "info": "# arun, users, debian.org",
    "dn": {
      "uid": "arun",
      "ou": "users",
      "dc": "org"
    },
    "uid": "arun",
    "objectClass": "debianDeveloper",
    "uidNumber": "3657",
    "gidNumber": "3657",
    "keyFingerPrint": "DA53058DBE154BDFCBF16BAD4B542AF704F74516",
    "cn": "Arun",
    "sn": "Pariyar"
  },
  {
    "info": "# asac, users, debian.org",
    "dn": {
      "uid": "asac",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "asac",
    "uidNumber": "2665",
    "sn": "Sack",
    "gidNumber": "2665",
    "keyFingerPrint": "530B83B686A68A55564EF34E754191EE8F8135F6",
    "ircNick": "asac",
    "jabberJID": "asac@jabber.ccc.de",
    "labeledURI": "http://www.asac.ws"
  },
  {
    "info": "# asb, users, debian.org",
    "dn": {
      "uid": "asb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "asb",
    "objectClass": "debianDeveloper",
    "uidNumber": "3231",
    "keyFingerPrint": "6EB223D7D71E67A53C93A7DA3B56E2BBD53FDCB1",
    "cn": "Andrew",
    "sn": "Starr-Bochicchio",
    "ircNick": "asomething",
    "gidNumber": "3231"
  },
  {
    "info": "# ash, users, debian.org",
    "dn": {
      "uid": "ash",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ash",
    "objectClass": "debianDeveloper",
    "uidNumber": "3054",
    "cn": "Albert",
    "sn": "Huang",
    "keyFingerPrint": "1EC26450E831FAA295C1C5F0B85D12EC9BFA3B80",
    "gidNumber": "3054"
  },
  {
    "info": "# ashley, users, debian.org",
    "dn": {
      "uid": "ashley",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ashley",
    "objectClass": "debianDeveloper",
    "uidNumber": "2782",
    "cn": "Ashley",
    "sn": "Howes",
    "ircNick": "Ashley",
    "labeledURI": "http://www.ashleyhowes.com/",
    "gidNumber": "2782"
  },
  {
    "info": "# asias, users, debian.org",
    "dn": {
      "uid": "asias",
      "ou": "users",
      "dc": "org"
    },
    "uid": "asias",
    "objectClass": "debianDeveloper",
    "uidNumber": "3224",
    "keyFingerPrint": "E0FD6DAE7C51262FDAE6A28B7174A18FAAA7A078",
    "cn": "Asias",
    "sn": "He",
    "gidNumber": "3224"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "asuffield",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "asuffield",
    "sn": "Suffield",
    "uidNumber": "2389",
    "gidNumber": "2389"
  },
  {
    "info": "# atterer, users, debian.org",
    "dn": {
      "uid": "atterer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "atterer",
    "uidNumber": "2231",
    "sn": "Atterer",
    "labeledURI": "http://atterer.org",
    "gidNumber": "2231"
  },
  {
    "info": "20091208] retired in <AC4869B2-CA87-11D6-A027-0003930EBB9C@debian.org>",
    "dn": {
      "uid": "aubin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.punknews.org/",
    "cn": "Aubin",
    "uid": "aubin",
    "uidNumber": "2257",
    "ircNick": "outlyer",
    "sn": "Paul",
    "gidNumber": "2257"
  },
  {
    "info": "# aure, users, debian.org",
    "dn": {
      "uid": "aure",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aurelien",
    "uid": "aure",
    "uidNumber": "2498",
    "sn": "Beaujean",
    "ircNick": "Aure",
    "labeledURI": "http://dagobah.eu.org/",
    "gidNumber": "2498"
  },
  {
    "info": "6AuCMoedBo3WI3MlCauFWkdL+1f9YWbQIDAQAB",
    "dn": {
      "uid": "aurel32",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aurelien",
    "uid": "aurel32",
    "sn": "Jarno",
    "uidNumber": "2472",
    "keyFingerPrint": "77462642A9EF94FD0F77196DBA9C78061DDD8C9B",
    "gidNumber": "2472",
    "ircNick": "aurel32",
    "labeledURI": "http://www.aurel32.net"
  },
  {
    "info": "# avalentino, users, debian.org",
    "dn": {
      "uid": "avalentino",
      "ou": "users",
      "dc": "org"
    },
    "uid": "avalentino",
    "objectClass": "debianDeveloper",
    "uidNumber": "3284",
    "cn": "Antonio",
    "sn": "Valentino",
    "gidNumber": "3284",
    "keyFingerPrint": "3B70F209A5FFD68903C472C5EBF48AB2578F9812"
  },
  {
    "info": "# avdyk, users, debian.org",
    "dn": {
      "uid": "avdyk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Arnaud",
    "uid": "avdyk",
    "sn": "Vandyck",
    "uidNumber": "2635",
    "ircNick": "avdyk",
    "jabberJID": "avdyk@jabber.org",
    "labeledURI": "http://people.debian.org/~avdyk/",
    "gidNumber": "2635"
  },
  {
    "info": "# aviau, users, debian.org",
    "dn": {
      "uid": "aviau",
      "ou": "users",
      "dc": "org"
    },
    "uid": "aviau",
    "objectClass": "debianDeveloper",
    "uidNumber": "3393",
    "gidNumber": "3393",
    "keyFingerPrint": "E30154F5429FFBB9B22E49C2DA82830E3CCC3A3A",
    "cn": "Alexandre",
    "sn": "Viau",
    "ircNick": "aviau",
    "labeledURI": "https://alexandreviau.net"
  },
  {
    "info": "# avtobiff, users, debian.org",
    "dn": {
      "uid": "avtobiff",
      "ou": "users",
      "dc": "org"
    },
    "uid": "avtobiff",
    "objectClass": "debianDeveloper",
    "uidNumber": "3223",
    "keyFingerPrint": "3A2A7ED9122EA05132225996B188AE5579F492F8",
    "cn": "Per",
    "sn": "Andersson",
    "gidNumber": "3223",
    "ircNick": "avtobiff",
    "jabberJID": "avtobiff@jabber.se"
  },
  {
    "info": "# awm, users, debian.org",
    "dn": {
      "uid": "awm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrew",
    "uid": "awm",
    "sn": "McMillan",
    "uidNumber": "2267",
    "ircNick": "karora",
    "labeledURI": "http://debiana.net",
    "gidNumber": "2267",
    "keyFingerPrint": "1CC31CA5AF3E7C4D40874FAA473C343BBEEE3333"
  },
  {
    "info": "# awoodland, users, debian.org",
    "dn": {
      "uid": "awoodland",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alan",
    "uid": "awoodland",
    "uidNumber": "2743",
    "sn": "Woodland",
    "keyFingerPrint": "F00F67C10AC612F5418B01975098021C70518078",
    "gidNumber": "2743",
    "ircNick": "awoodland"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "awpguy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andy",
    "uid": "awpguy",
    "uidNumber": "822",
    "sn": "Guy",
    "gidNumber": "822"
  },
  {
    "info": "# az, users, debian.org",
    "dn": {
      "uid": "az",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "az",
    "sn": "Zangerl",
    "uidNumber": "2456",
    "ircNick": "glumpert",
    "jabberJID": "wappler@jabber.org",
    "labeledURI": "http://snafu.priv.at/",
    "keyFingerPrint": "431C3FE32450FCAC547EABDA2FCCF66BB963BD5F",
    "gidNumber": "2456"
  },
  {
    "info": "# azadi, users, debian.org",
    "dn": {
      "uid": "azadi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "azadi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3254",
    "cn": "Sukhbir",
    "sn": "Singh",
    "ircNick": "sukhe",
    "gidNumber": "3254"
  },
  {
    "info": "# azanella, users, debian.org",
    "dn": {
      "uid": "azanella",
      "ou": "users",
      "dc": "org"
    },
    "uid": "azanella",
    "objectClass": "debianDeveloper",
    "uidNumber": "3638",
    "gidNumber": "3638",
    "keyFingerPrint": "A33EB0E9CDD826781F83AA48FA5044CA2BBBE6B5",
    "cn": "Adhemerval",
    "sn": "Netto",
    "ircNick": "azanella"
  },
  {
    "info": "# azekulic, users, debian.org",
    "dn": {
      "uid": "azekulic",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alen",
    "keyFingerPrint": "091B93CF28AD65C2B76C9F23FF666668B9FD8238",
    "uid": "azekulic",
    "sn": "Zekulic",
    "uidNumber": "1036",
    "ircNick": "azekulic",
    "gidNumber": "1036"
  },
  {
    "info": "# bab, users, debian.org",
    "dn": {
      "uid": "bab",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "bab",
    "sn": "Burton",
    "uidNumber": "2268",
    "ircNick": "bab",
    "keyFingerPrint": "519A0009FB50255DDB4E889270A6BEDF542D38D9",
    "gidNumber": "2268"
  },
  {
    "info": "# babelouest, users, debian.org",
    "dn": {
      "uid": "babelouest",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3604",
    "gidNumber": "3604",
    "keyFingerPrint": "8405B02FCC28EF9744C8F253FE82139440BD22B9",
    "cn": "Nicolas",
    "sn": "Mora",
    "uid": "babelouest",
    "ircNick": "babelouest"
  },
  {
    "info": "# bage, users, debian.org",
    "dn": {
      "uid": "bage",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bage",
    "objectClass": "debianDeveloper",
    "uidNumber": "3636",
    "gidNumber": "3636",
    "keyFingerPrint": "2861257317C7AEE4F880497EC3860AC59F574E3A",
    "cn": "Bastian",
    "sn": "Germann"
  },
  {
    "info": "20091208] Retired [JT 2007-01-02]",
    "dn": {
      "uid": "bagpuss",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "bagpuss",
    "uidNumber": "2308",
    "sn": "Stafford",
    "ircNick": "Smsie or Stephen (bagpuss on OPN)",
    "gidNumber": "2308"
  },
  {
    "info": "# bahner, users, debian.org",
    "dn": {
      "uid": "bahner",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lars",
    "uid": "bahner",
    "sn": "Bahner",
    "uidNumber": "2441",
    "gidNumber": "2441",
    "keyFingerPrint": "D7F17B47AE851A1D97CE39870E2AF07048D0EC7E",
    "ircNick": "bahner",
    "labeledURI": "https://lars.bahner.com"
  },
  {
    "info": "# balasankarc, users, debian.org",
    "dn": {
      "uid": "balasankarc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "balasankarc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3486",
    "gidNumber": "3486",
    "cn": "Balasankar",
    "sn": "Chelamattathu",
    "keyFingerPrint": "DDF2B83B163533E05B696D5BB77D2E2E23735427",
    "ircNick": "balu",
    "labeledURI": "https://balasankarc.in"
  },
  {
    "info": "# bali, users, debian.org",
    "dn": {
      "uid": "bali",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andras",
    "uid": "bali",
    "uidNumber": "2328",
    "ircNick": "drewie",
    "sn": "Bali",
    "gidNumber": "2328"
  },
  {
    "info": "# ballombe, users, debian.org",
    "dn": {
      "uid": "ballombe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bill",
    "uid": "ballombe",
    "sn": "Allombert",
    "uidNumber": "2357",
    "keyFingerPrint": "42028EA404A2E9D80AC453148F0E7C2B4522E387",
    "ircNick": "yp17",
    "labeledURI": "http://wiki.debian.org/BillAllombert",
    "gidNumber": "2357"
  },
  {
    "info": "# bam, users, debian.org",
    "dn": {
      "uid": "bam",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "bam",
    "uidNumber": "1410",
    "sn": "May",
    "gidNumber": "1410",
    "jabberJID": "brian@linuxpenguins.xyz",
    "labeledURI": "https://linuxpenguins.xyz/brian/",
    "keyFingerPrint": "D6365126A92DB560C627ACED1784577F811F6EAC"
  },
  {
    "info": "# bame, users, debian.org",
    "dn": {
      "uid": "bame",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "bame",
    "sn": "Bame",
    "uidNumber": "989",
    "gidNumber": "989",
    "ircNick": "bame"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ban",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bjorn",
    "uid": "ban",
    "uidNumber": "2150",
    "ircNick": "ban",
    "sn": "Andersson",
    "gidNumber": "2150"
  },
  {
    "info": "# bandali, users, debian.org",
    "dn": {
      "uid": "bandali",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bandali",
    "objectClass": "debianDeveloper",
    "uidNumber": "3615",
    "gidNumber": "3615",
    "keyFingerPrint": "BE6273738E616D6D1B3A08E8A21A020248816103",
    "cn": "Amin",
    "sn": "Bandali"
  },
  {
    "info": "# bao, users, debian.org",
    "dn": {
      "uid": "bao",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bao",
    "uid": "bao",
    "uidNumber": "2332",
    "sn": "Ha",
    "gidNumber": "2332"
  },
  {
    "info": "# baozich, users, debian.org",
    "dn": {
      "uid": "baozich",
      "ou": "users",
      "dc": "org"
    },
    "uid": "baozich",
    "objectClass": "debianDeveloper",
    "uidNumber": "3339",
    "keyFingerPrint": "6F1D26E46B6612334228B04A5EA2BDB6883CCDE2",
    "cn": "Chen",
    "sn": "Baozi",
    "gidNumber": "3339",
    "ircNick": "baozich"
  },
  {
    "info": "# bap, users, debian.org",
    "dn": {
      "uid": "bap",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Barak",
    "uid": "bap",
    "sn": "Pearlmutter",
    "uidNumber": "2057",
    "keyFingerPrint": "64F429E36EA11CC2D966546F125B57475E190D18",
    "gidNumber": "2057",
    "ircNick": "barak",
    "labeledURI": "http://barak.pearlmutter.net/"
  },
  {
    "info": "# bar, users, debian.org",
    "dn": {
      "uid": "bar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3402",
    "gidNumber": "3402",
    "keyFingerPrint": "726CB11AA3DFA6F40B5D9225A7AF366FEDC2D462",
    "cn": "Alexander",
    "sn": "Barkov"
  },
  {
    "info": "# baran, users, debian.org",
    "dn": {
      "uid": "baran",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Miroslaw",
    "uid": "baran",
    "sn": "Baran",
    "uidNumber": "2331",
    "ircNick": "Jubal",
    "jabberJID": "baran@hell.pl",
    "labeledURI": "http://makabra.org/",
    "gidNumber": "2331"
  },
  {
    "info": "# barbier, users, debian.org",
    "dn": {
      "uid": "barbier",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Denis",
    "uid": "barbier",
    "uidNumber": "2108",
    "sn": "Barbier",
    "ircNick": "bouz",
    "keyFingerPrint": "0FABFD4B88C1E66DA3DDD021E81D75E2ADBCB60D",
    "gidNumber": "2108"
  },
  {
    "info": "# barry, users, debian.org",
    "dn": {
      "uid": "barry",
      "ou": "users",
      "dc": "org"
    },
    "uid": "barry",
    "objectClass": "debianDeveloper",
    "uidNumber": "3258",
    "cn": "Barry",
    "sn": "Warsaw",
    "gidNumber": "3258",
    "ircNick": "barry",
    "labeledURI": "http://barry.warsaw.us"
  },
  {
    "info": "# bart, users, debian.org",
    "dn": {
      "uid": "bart",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bart",
    "uid": "bart",
    "uidNumber": "2166",
    "ircNick": "bart",
    "sn": "Bunting",
    "gidNumber": "2166"
  },
  {
    "info": "# bartm, users, debian.org",
    "dn": {
      "uid": "bartm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bartm",
    "objectClass": "debianDeveloper",
    "uidNumber": "2853",
    "keyFingerPrint": "65A12DF4FE31AD6BAC4D76AE3355F4D63B5821CC",
    "cn": "Bart",
    "sn": "Martens",
    "gidNumber": "2853",
    "ircNick": "bartm",
    "jabberJID": "bartm@debian.org"
  },
  {
    "info": "# bartw, users, debian.org",
    "dn": {
      "uid": "bartw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bart",
    "uid": "bartw",
    "uidNumber": "1207",
    "ircNick": "bartw",
    "sn": "Warmerdam",
    "gidNumber": "1642"
  },
  {
    "info": "# baruch, users, debian.org",
    "dn": {
      "uid": "baruch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Baruch",
    "uid": "baruch",
    "sn": "Even",
    "uidNumber": "2393",
    "ircNick": "baruch",
    "jabberJID": "baruch.even@jabber.org",
    "labeledURI": "http://baruch.ev-en.org/",
    "gidNumber": "2393"
  },
  {
    "info": "# bas, users, debian.org",
    "dn": {
      "uid": "bas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bas",
    "uid": "bas",
    "sn": "Zoetekouw",
    "uidNumber": "2073",
    "keyFingerPrint": "8210B8A457F1C6047C969AFFD183A517BFA98DE6",
    "gidNumber": "2073",
    "ircNick": "ifvoid",
    "labeledURI": "https://zoetekouw.net/"
  },
  {
    "info": "# basilgello, users, debian.org",
    "dn": {
      "uid": "basilgello",
      "ou": "users",
      "dc": "org"
    },
    "uid": "basilgello",
    "objectClass": "debianDeveloper",
    "uidNumber": "3606",
    "gidNumber": "3606",
    "keyFingerPrint": "11C62C634CC61C0774A7EE122C5141A15285EC0A",
    "cn": "Vasyl",
    "sn": "Gello"
  },
  {
    "info": "# bates, users, debian.org",
    "dn": {
      "uid": "bates",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Douglas",
    "uid": "bates",
    "sn": "Bates",
    "labeledURI": "http://www.stat.wisc.edu/~bates/",
    "uidNumber": "1080",
    "gidNumber": "1080"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "baux",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Maurizio",
    "uid": "baux",
    "uidNumber": "2217",
    "ircNick": "baux",
    "sn": "Boriani",
    "gidNumber": "2217"
  },
  {
    "info": "# bayle, users, debian.org",
    "dn": {
      "uid": "bayle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "bayle",
    "sn": "Bayle",
    "uidNumber": "2478",
    "ircNick": "chris38",
    "labeledURI": "http://bayle.eu/",
    "keyFingerPrint": "97D0E2B125185AF7C95FEC776B982DEBBFE91C29",
    "gidNumber": "2478"
  },
  {
    "info": "# bbaren, users, debian.org",
    "dn": {
      "uid": "bbaren",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bbaren",
    "objectClass": "debianDeveloper",
    "uidNumber": "3457",
    "gidNumber": "3457",
    "cn": "Benjamin",
    "sn": "Barenblat",
    "keyFingerPrint": "A79AE2201D17C69FF020F1D54664E07DBDA485C5",
    "ircNick": "bbaren",
    "jabberJID": "benjamin@barenblat.name",
    "labeledURI": "https://benjamin.barenblat.name/"
  },
  {
    "info": "# bblough, users, debian.org",
    "dn": {
      "uid": "bblough",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bblough",
    "objectClass": "debianDeveloper",
    "uidNumber": "3511",
    "gidNumber": "3511",
    "keyFingerPrint": "5CDD0C9CF446BC1B250987911762E0227034CF84",
    "cn": "William",
    "sn": "Blough",
    "ircNick": "bblough"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "bbum",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bill",
    "uid": "bbum",
    "uidNumber": "920",
    "sn": "Bumgarner",
    "gidNumber": "920"
  },
  {
    "info": "# bcollins, users, debian.org",
    "dn": {
      "uid": "bcollins",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "bcollins",
    "sn": "Collins",
    "uidNumber": "1285",
    "ircNick": "BenC",
    "gidNumber": "1285"
  },
  {
    "info": "# bcwhite, users, debian.org",
    "dn": {
      "uid": "bcwhite",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "bcwhite",
    "sn": "White",
    "uidNumber": "823",
    "ircNick": "bcwhite",
    "labeledURI": "http://pobox.com/~bcwhite/",
    "keyFingerPrint": "D20D5E7E7BDA7633BEA14A9F67708B14E1C142B1",
    "gidNumber": "823"
  },
  {
    "info": "# bdale, users, debian.org",
    "dn": {
      "uid": "bdale",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bdale",
    "uid": "bdale",
    "sn": "Garbee",
    "uidNumber": "801",
    "ircNick": "bdale",
    "labeledURI": "http://gag.com/bdale/",
    "gidNumber": "801",
    "keyFingerPrint": "1E0BAAD85C2232A1B3CE92EBB7047105830B9FA1"
  },
  {
    "info": "# bdefreese, users, debian.org",
    "dn": {
      "uid": "bdefreese",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bdefreese",
    "objectClass": "debianDeveloper",
    "uidNumber": "2943",
    "keyFingerPrint": "6F1F40039E09BD7A8099C5F3E48B65B54B394F7E",
    "cn": "Barry",
    "sn": "deFreese",
    "ircNick": "bdefreese",
    "labeledURI": "http://www.bddebian.com",
    "gidNumber": "2943"
  },
  {
    "info": "# bdrung, users, debian.org",
    "dn": {
      "uid": "bdrung",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bdrung",
    "objectClass": "debianDeveloper",
    "uidNumber": "3113",
    "keyFingerPrint": "A62D2CFBD50B9B5BF360D54B159EB5C4EFC8774C",
    "cn": "Benjamin",
    "sn": "Drung",
    "gidNumber": "3113",
    "ircNick": "bdrung"
  },
  {
    "info": "# beckmanf, users, debian.org",
    "dn": {
      "uid": "beckmanf",
      "ou": "users",
      "dc": "org"
    },
    "uid": "beckmanf",
    "objectClass": "debianDeveloper",
    "uidNumber": "3359",
    "keyFingerPrint": "1A9F9365EF7F1142627526B1CC79D80A5BB7FC51",
    "cn": "Friedrich",
    "sn": "Beckmann",
    "gidNumber": "3359"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "beh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Benedikt",
    "uid": "beh",
    "uidNumber": "1039",
    "sn": "Heinen",
    "gidNumber": "1039"
  },
  {
    "info": "# behanw, users, debian.org",
    "dn": {
      "uid": "behanw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Behan",
    "uid": "behanw",
    "sn": "Webster",
    "labeledURI": "http://www.pobox.com/~behanw/",
    "uidNumber": "1177",
    "ircNick": "behanw",
    "gidNumber": "1668"
  },
  {
    "info": "# ben, users, debian.org",
    "dn": {
      "uid": "ben",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ben",
    "objectClass": "debianDeveloper",
    "uidNumber": "3218",
    "cn": "Benjamin",
    "sn": "Mesing",
    "keyFingerPrint": "0B4D4F3DD28ABA1465316C6EED630BD2FFA943F1",
    "gidNumber": "3218"
  },
  {
    "info": "# bencer, users, debian.org",
    "dn": {
      "uid": "bencer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bencer",
    "objectClass": "debianDeveloper",
    "uidNumber": "2909",
    "keyFingerPrint": "93AEA3727846B14E0DB38B65589A903470EB0FCE",
    "cn": "Jorge",
    "sn": "Sanz",
    "ircNick": "bencer",
    "jabberJID": "bencer@jabber.org",
    "labeledURI": "http://bq.cauterized.net/",
    "gidNumber": "2909"
  },
  {
    "info": "# bengen, users, debian.org",
    "dn": {
      "uid": "bengen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hilko",
    "uid": "bengen",
    "sn": "Bengen",
    "uidNumber": "2596",
    "keyFingerPrint": "AE731055442A1D96CF4D4C7875B710635C213A7E",
    "gidNumber": "2596",
    "ircNick": "hillu"
  },
  {
    "info": "# benh, users, debian.org",
    "dn": {
      "uid": "benh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "benh",
    "objectClass": "debianDeveloper",
    "uidNumber": "2798",
    "keyFingerPrint": "AC2B29BD34A6AFDDB3F68F35E7BFC8EC95861109",
    "cn": "Ben",
    "sn": "Hutchings",
    "gidNumber": "2798",
    "ircNick": "bwh",
    "labeledURI": "https://www.decadent.org.uk/ben/"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "benj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Benjamin",
    "uid": "benj",
    "sn": "Drieu",
    "labeledURI": "http://people.debian.org/~benj/",
    "uidNumber": "2451",
    "ircNick": "benj",
    "gidNumber": "2451"
  },
  {
    "info": "# benjamin, users, debian.org",
    "dn": {
      "uid": "benjamin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "benjamin",
    "objectClass": "debianDeveloper",
    "uidNumber": "2816",
    "cn": "Benjamin",
    "sn": "Seidenberg",
    "ircNick": "astronut",
    "gidNumber": "2816"
  },
  {
    "info": "# benoit, users, debian.org",
    "dn": {
      "uid": "benoit",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Benoit",
    "uid": "benoit",
    "sn": "Joly",
    "uidNumber": "2088",
    "ircNick": "benoitj",
    "gidNumber": "2088"
  },
  {
    "info": "# berin, users, debian.org",
    "dn": {
      "uid": "berin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Berin",
    "uid": "berin",
    "uidNumber": "2662",
    "sn": "Lautenbach",
    "gidNumber": "2662"
  },
  {
    "info": "x.",
    "dn": {
      "uid": "bernat",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bernat",
    "objectClass": "debianDeveloper",
    "uidNumber": "2907",
    "cn": "Vincent",
    "sn": "Bernat",
    "keyFingerPrint": "AEF2348766F371C689A7360095A42FE8353525F9",
    "gidNumber": "2907",
    "ircNick": "vbernat",
    "labeledURI": "https://vincent.bernat.ch"
  },
  {
    "info": "# berni, users, debian.org",
    "dn": {
      "uid": "berni",
      "ou": "users",
      "dc": "org"
    },
    "uid": "berni",
    "objectClass": "debianDeveloper",
    "uidNumber": "3386",
    "gidNumber": "3386",
    "keyFingerPrint": "D6E01EC516A5DFCEF71956D3775079E5B850BC93",
    "cn": "Bernhard",
    "sn": "Schmidt",
    "jabberJID": "berni@birkenwald.de"
  },
  {
    "info": "# berto, users, debian.org",
    "dn": {
      "uid": "berto",
      "ou": "users",
      "dc": "org"
    },
    "uid": "berto",
    "objectClass": "debianDeveloper",
    "uidNumber": "3262",
    "keyFingerPrint": "1B82AAF294792E7F55864F3EBE3219AF3ED41341",
    "cn": "Alberto",
    "sn": "Garcia",
    "gidNumber": "3262",
    "ircNick": "berto",
    "jabberJID": "berto"
  },
  {
    "info": "# bertol, users, debian.org",
    "dn": {
      "uid": "bertol",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nicolas",
    "uid": "bertol",
    "sn": "Bertolissio",
    "uidNumber": "2629",
    "gidNumber": "2629"
  },
  {
    "info": "# beuc, users, debian.org",
    "dn": {
      "uid": "beuc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "beuc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3075",
    "cn": "Sylvain",
    "sn": "Beucler",
    "gidNumber": "3075",
    "keyFingerPrint": "3CCCC613C34F3D54718A983CEE887356CD2F16A0"
  },
  {
    "info": "# bfulgham, users, debian.org",
    "dn": {
      "uid": "bfulgham",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brent",
    "uid": "bfulgham",
    "sn": "Fulgham",
    "labeledURI": "http://people.debian.org/~bfulgham/",
    "uidNumber": "1230",
    "ircNick": "bfulgham",
    "gidNumber": "1711"
  },
  {
    "info": "# bg66, users, debian.org",
    "dn": {
      "uid": "bg66",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Akira",
    "keyFingerPrint": "A8B90F6902FD86E7D339C9A6FA96A148004DA6B4",
    "uid": "bg66",
    "sn": "Ohashi",
    "uidNumber": "2391",
    "gidNumber": "2391"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "bgdarnel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://thoughtstream.org",
    "cn": "Benjamin",
    "uid": "bgdarnel",
    "uidNumber": "1416",
    "sn": "Darnell",
    "gidNumber": "1416"
  },
  {
    "info": "# bgoglin, users, debian.org",
    "dn": {
      "uid": "bgoglin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bgoglin",
    "objectClass": "debianDeveloper",
    "uidNumber": "2873",
    "cn": "Brice",
    "sn": "Goglin",
    "keyFingerPrint": "7A5A4E80E40097BAF6EAD638449190F3235ABD3B",
    "gidNumber": "2873",
    "ircNick": "bgoglin",
    "labeledURI": "http://bgoglin.free.fr"
  },
  {
    "info": "# bgupta, users, debian.org",
    "dn": {
      "uid": "bgupta",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bgupta",
    "objectClass": "debianDeveloper",
    "uidNumber": "3265",
    "keyFingerPrint": "AF113DD0EC498CD884BDCB36C0F8864CDA3D0358",
    "cn": "Brian",
    "sn": "Gupta",
    "ircNick": "bgupta",
    "gidNumber": "3265"
  },
  {
    "info": "# biebl, users, debian.org",
    "dn": {
      "uid": "biebl",
      "ou": "users",
      "dc": "org"
    },
    "uid": "biebl",
    "objectClass": "debianDeveloper",
    "uidNumber": "2820",
    "cn": "Michael",
    "sn": "Biebl",
    "keyFingerPrint": "09B3AC2ECB169C904345CC546AE1DF0D608F22DC",
    "ircNick": "mbiebl",
    "gidNumber": "2820"
  },
  {
    "info": "# bigeasy, users, debian.org",
    "dn": {
      "uid": "bigeasy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bigeasy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3390",
    "gidNumber": "3390",
    "keyFingerPrint": "64254695FFF0AA4466CC19E67B96E8162A8CF5D1",
    "cn": "Sebastian",
    "sn": "Siewior",
    "ircNick": "bigeasy"
  },
  {
    "info": "# bignose, users, debian.org",
    "dn": {
      "uid": "bignose",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bignose",
    "objectClass": "debianDeveloper",
    "uidNumber": "3431",
    "gidNumber": "3431",
    "keyFingerPrint": "517CF14BB2F398B0CB354855B8B24C06AC128405",
    "cn": "Ben",
    "sn": "Finney",
    "ircNick": "bignose"
  },
  {
    "info": "kiZnfgVZ3gEMoE0YjBJneS6aywQQIDAQAB",
    "dn": {
      "uid": "bigon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bigon",
    "objectClass": "debianDeveloper",
    "uidNumber": "2935",
    "cn": "Laurent",
    "sn": "Bigonville",
    "keyFingerPrint": "7E0ED3D2B34A03B15F9F3121C7F7F9660D82A682",
    "gidNumber": "2935",
    "ircNick": "bigon",
    "jabberJID": "bigon@jabber.belnet.be"
  },
  {
    "info": "# bigpaul, users, debian.org",
    "dn": {
      "uid": "bigpaul",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gaetano",
    "uid": "bigpaul",
    "uidNumber": "2444",
    "sn": "Paolone",
    "ircNick": "bigpaul",
    "labeledURI": "http://www.bigpaul.org",
    "gidNumber": "2444"
  },
  {
    "info": "# bilbo, users, debian.org",
    "dn": {
      "uid": "bilbo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Theodore",
    "uid": "bilbo",
    "uidNumber": "2663",
    "sn": "Karkoulis",
    "gidNumber": "2663"
  },
  {
    "info": "# birger, users, debian.org",
    "dn": {
      "uid": "birger",
      "ou": "users",
      "dc": "org"
    },
    "uid": "birger",
    "objectClass": "debianDeveloper",
    "uidNumber": "3599",
    "gidNumber": "3599",
    "keyFingerPrint": "C5BC7498F466D885188CB397CB06EA7B78DBE151",
    "cn": "Birger",
    "sn": "Schacht",
    "labeledURI": "https://bisco.org"
  },
  {
    "info": "# bjb, users, debian.org",
    "dn": {
      "uid": "bjb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "bjb",
    "uidNumber": "2075",
    "sn": "Bell",
    "jabberJID": "boze@jabber.org",
    "gidNumber": "2075"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "bjorn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://brenander.pp.se/bjorn/",
    "cn": "Bjorn",
    "uid": "bjorn",
    "uidNumber": "1182",
    "ircNick": "BeorN",
    "sn": "Brenander",
    "gidNumber": "1644"
  },
  {
    "info": "# bkerin, users, debian.org",
    "dn": {
      "uid": "bkerin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Britton",
    "uid": "bkerin",
    "sn": "Kerin",
    "uidNumber": "2092",
    "ircNick": "Agrajag",
    "labeledURI": "http://rawrec.sourceforge.net/britton_leo_kerin/",
    "gidNumber": "2092"
  },
  {
    "info": "# blackxored, users, debian.org",
    "dn": {
      "uid": "blackxored",
      "ou": "users",
      "dc": "org"
    },
    "uid": "blackxored",
    "objectClass": "debianDeveloper",
    "uidNumber": "3083",
    "cn": "Adrian",
    "sn": "Perez",
    "ircNick": "blackxored",
    "jabberJID": "adrianperez.deb@gmail.com",
    "gidNumber": "3083"
  },
  {
    "info": "# blade, users, debian.org",
    "dn": {
      "uid": "blade",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eduard",
    "uid": "blade",
    "sn": "Bloch",
    "uidNumber": "2130",
    "keyFingerPrint": "648DD98F4BC4829017CB0E3469740E5CB35FEC3C",
    "gidNumber": "2130",
    "ircNick": "Zomb"
  },
  {
    "info": "# blarson, users, debian.org",
    "dn": {
      "uid": "blarson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "blarson",
    "sn": "Larson",
    "uidNumber": "2625",
    "ircNick": "blarson",
    "keyFingerPrint": "23AAEC75ABBAC703F6593B40049B6D88E31734DB",
    "gidNumber": "2625"
  },
  {
    "info": "# blendi, users, debian.org",
    "dn": {
      "uid": "blendi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "blendi",
    "sn": "Butterweck",
    "labeledURI": "http://blendi.org",
    "uidNumber": "2429",
    "ircNick": "Blendi",
    "gidNumber": "2429"
  },
  {
    "info": "# bluca, users, debian.org",
    "dn": {
      "uid": "bluca",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bluca",
    "objectClass": "debianDeveloper",
    "uidNumber": "3481",
    "gidNumber": "3481",
    "keyFingerPrint": "A9EA9081724FFAE0484C35A1A81CEA22BC8C7E2E",
    "cn": "Luca",
    "sn": "Boccassi",
    "ircNick": "bluca"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "bma",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://tynian.net",
    "cn": "Brian",
    "uid": "bma",
    "uidNumber": "1235",
    "ircNick": "bma",
    "sn": "Almeida",
    "gidNumber": "1607"
  },
  {
    "info": "# bmarc, users, debian.org",
    "dn": {
      "uid": "bmarc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bmarc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3433",
    "gidNumber": "3433",
    "keyFingerPrint": "29452BD463CE670323D09B88A04AB3BADBE263EE",
    "cn": "Bertrand",
    "sn": "Marc"
  },
  {
    "info": "# bnelson, users, debian.org",
    "dn": {
      "uid": "bnelson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "bnelson",
    "uidNumber": "2595",
    "sn": "Nelson",
    "ircNick": "fenring",
    "gidNumber": "2595"
  },
  {
    "info": "# bod, users, debian.org",
    "dn": {
      "uid": "bod",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brendan",
    "uid": "bod",
    "sn": "O'Dea",
    "uidNumber": "2016",
    "ircNick": "bod",
    "keyFingerPrint": "52D5B1593D7FD9146A5A63071C7C41EDEBDDBB60",
    "gidNumber": "2016"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "boekhold",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Maatern",
    "uid": "boekhold",
    "uidNumber": "879",
    "sn": "Boekhold",
    "gidNumber": "879"
  },
  {
    "info": "20091208] Left the project 389EF016.BFB245F2@me.umn.edu [07 Feb 2000]",
    "dn": {
      "uid": "bofh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "bofh",
    "uidNumber": "959",
    "sn": "Kaszeta",
    "gidNumber": "959"
  },
  {
    "info": "20091208] Retired [2007-04-07 - JT]",
    "dn": {
      "uid": "boll",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "boll",
    "sn": "Overgaard",
    "uidNumber": "2488",
    "ircNick": "Boll",
    "jabberJID": "Boll@jabber.org",
    "labeledURI": "http://soren.overgaard.org/",
    "gidNumber": "2488",
    "cn": ": U8O4cmVu"
  },
  {
    "info": "# bootc, users, debian.org",
    "dn": {
      "uid": "bootc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bootc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3285",
    "cn": "Chris",
    "sn": "Boot",
    "gidNumber": "3285",
    "keyFingerPrint": "846753CB19213142C56DC918F5C83C05D9CEEEEE",
    "ircNick": "bootc"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "booth",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "booth",
    "uidNumber": "1217",
    "sn": "Booth",
    "gidNumber": "1647"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "borco",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ionutz",
    "uid": "borco",
    "sn": "Borcoman",
    "labeledURI": "http://storm.prohosting.com/borco/",
    "uidNumber": "1348",
    "ircNick": "borco",
    "gidNumber": "1348"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "borella",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "borella",
    "uidNumber": "1153",
    "sn": "Borella",
    "gidNumber": "1153"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "boriel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.geocities.com/boriel/",
    "cn": "Jose",
    "uid": "boriel",
    "uidNumber": "1201",
    "sn": "Rodriguez",
    "gidNumber": "1201"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "borik",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Boris",
    "uid": "borik",
    "uidNumber": "944",
    "sn": "Beletsky",
    "gidNumber": "944"
  },
  {
    "info": "20091208] Retired [JT 2007-02-14]",
    "dn": {
      "uid": "bortz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephane",
    "uid": "bortz",
    "sn": "Bortzmeyer",
    "labeledURI": "http://people.debian.org/~bortz",
    "uidNumber": "1265",
    "gidNumber": "1646"
  },
  {
    "info": "# bossekr, users, debian.org",
    "dn": {
      "uid": "bossekr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Raphael",
    "keyFingerPrint": "5202EA4B6946AE85149A9BB0376941AB835EB2FF",
    "uid": "bossekr",
    "sn": "Bossek",
    "uidNumber": "1387",
    "labeledURI": "http://www.solutions4linux.de/",
    "gidNumber": "1387"
  },
  {
    "info": "# bottoms, users, debian.org",
    "dn": {
      "uid": "bottoms",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Maitland",
    "uid": "bottoms",
    "sn": "Bottoms",
    "uidNumber": "982",
    "keyFingerPrint": "07CA87DDC4C2B06240B6B1745041F1891F44E090",
    "gidNumber": "982",
    "ircNick": "mait"
  },
  {
    "info": "# boud, users, debian.org",
    "dn": {
      "uid": "boud",
      "ou": "users",
      "dc": "org"
    },
    "uid": "boud",
    "objectClass": "debianDeveloper",
    "uidNumber": "3460",
    "gidNumber": "3460",
    "keyFingerPrint": "A118B1BCBF5EEC485FED494031715E3BC3F58B44",
    "cn": "Boud",
    "sn": "Roukema"
  },
  {
    "info": "kSJp/BfVpNSRcDyf/dFV1l6WBavzhKJwIDAQAB",
    "dn": {
      "uid": "boutil",
      "ou": "users",
      "dc": "org"
    },
    "uid": "boutil",
    "objectClass": "debianDeveloper",
    "uidNumber": "3222",
    "sn": "Boutillier",
    "gidNumber": "3222",
    "cn": ": Q8OpZHJpYw==",
    "keyFingerPrint": "884CC54CBA26ADC13C87E55248E160C2F1FE274E",
    "ircNick": "boutil",
    "jabberJID": "boutil@debian.org"
  },
  {
    "info": "# bpellin, users, debian.org",
    "dn": {
      "uid": "bpellin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bpellin",
    "objectClass": "debianDeveloper",
    "uidNumber": "2997",
    "cn": "Brian",
    "sn": "Pellin",
    "keyFingerPrint": "0246088A02F1FBD0A74B3EF6552AD9AA16C4267F",
    "gidNumber": "2997"
  },
  {
    "info": "# brad, users, debian.org",
    "dn": {
      "uid": "brad",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bradley",
    "uid": "brad",
    "sn": "Bosch",
    "uidNumber": "1053",
    "keyFingerPrint": "7ED48642624C08B853FA3240BFA28F8B9FC8AE73",
    "gidNumber": "1053"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "bradley",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dermot",
    "uid": "bradley",
    "uidNumber": "980",
    "sn": "Bradley",
    "gidNumber": "980"
  },
  {
    "info": "# bradm, users, debian.org",
    "dn": {
      "uid": "bradm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://quark.humbug.org.au/",
    "cn": "Bradley",
    "uid": "bradm",
    "uidNumber": "2053",
    "ircNick": "Jiko",
    "sn": "Marshall",
    "gidNumber": "2053"
  },
  {
    "info": "# bradsmith, users, debian.org",
    "dn": {
      "uid": "bradsmith",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bradsmith",
    "objectClass": "debianDeveloper",
    "uidNumber": "2951",
    "cn": "Bradley",
    "sn": "Smith",
    "ircNick": "|Brad|",
    "labeledURI": "http://brad-smith.co.uk/",
    "gidNumber": "2951"
  },
  {
    "info": "# branden, users, debian.org",
    "dn": {
      "uid": "branden",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Branden",
    "uid": "branden",
    "sn": "Robinson",
    "uidNumber": "1155",
    "gidNumber": "1155",
    "keyFingerPrint": "8773D61D68E30E072B10DC1AD19E9C7D71266DCE",
    "ircNick": "Overfiend"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "branderh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Erick",
    "uid": "branderh",
    "uidNumber": "825",
    "sn": "Branderhorst",
    "gidNumber": "825"
  },
  {
    "info": "# brandon, users, debian.org",
    "dn": {
      "uid": "brandon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brandon",
    "uid": "brandon",
    "uidNumber": "2237",
    "sn": "Griffith",
    "labeledURI": "http://people.debian.org/~brandon",
    "gidNumber": "2237"
  },
  {
    "info": "# bremner, users, debian.org",
    "dn": {
      "uid": "bremner",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bremner",
    "objectClass": "debianDeveloper",
    "uidNumber": "3104",
    "cn": "David",
    "sn": "Bremner",
    "gidNumber": "3104",
    "keyFingerPrint": "7A18807F100A4570C59684207E4E65C8720B706B",
    "ircNick": "bremner"
  },
  {
    "info": "# brian, users, debian.org",
    "dn": {
      "uid": "brian",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "brian",
    "uidNumber": "824",
    "ircNick": "brianm",
    "sn": "Mays",
    "gidNumber": "824"
  },
  {
    "info": "# brianb, users, debian.org",
    "dn": {
      "uid": "brianb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "brianb",
    "sn": "Bassett",
    "labeledURI": "http://www.bbassett.net/",
    "uidNumber": "1103",
    "ircNick": "Schnauser",
    "gidNumber": "1103"
  },
  {
    "info": "# brianr, users, debian.org",
    "dn": {
      "uid": "brianr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "keyFingerPrint": "578A87BDBE6D8A04DF7A6EB30D46FE7CD21DF495",
    "uid": "brianr",
    "sn": "Ristuccia",
    "uidNumber": "1309",
    "ircNick": "BrianR",
    "jabberJID": "brianr@jabber.978.org",
    "labeledURI": "http://osiris.978.org/~brianr/",
    "gidNumber": "1309"
  },
  {
    "info": "# bridgett, users, debian.org",
    "dn": {
      "uid": "bridgett",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adrian",
    "uid": "bridgett",
    "sn": "Bridgett",
    "labeledURI": "http://www.smop.co.uk",
    "uidNumber": "1038",
    "ircNick": "Wyvern",
    "gidNumber": "1038"
  },
  {
    "info": "# brinkmd, users, debian.org",
    "dn": {
      "uid": "brinkmd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marcus",
    "uid": "brinkmd",
    "sn": "Brinkmann",
    "labeledURI": "http://www.marcus-brinkmann.de/",
    "uidNumber": "1127",
    "ircNick": "marcus",
    "gidNumber": "1127"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "brinks",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "brinks",
    "uidNumber": "826",
    "sn": "Brinks",
    "gidNumber": "826"
  },
  {
    "info": "# brlink, users, debian.org",
    "dn": {
      "uid": "brlink",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bernhard",
    "uid": "brlink",
    "sn": "Link",
    "uidNumber": "2551",
    "ircNick": "noshadow",
    "labeledURI": "http://www.brlink.eu/",
    "keyFingerPrint": "F8AC04D50B9B064B3383C3DAAFFC96D1151DFFDC",
    "gidNumber": "2551"
  },
  {
    "info": "# broonie, users, debian.org",
    "dn": {
      "uid": "broonie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "broonie",
    "sn": "Brown",
    "uidNumber": "1301",
    "keyFingerPrint": "3F2568AAC26998F9E813A1C5C3F436CA30F5D8EB",
    "gidNumber": "1301",
    "ircNick": "broonie",
    "jabberJID": "broonie@gmail.com",
    "labeledURI": "http://www.sirena.org.uk/"
  },
  {
    "info": "# brother, users, debian.org",
    "dn": {
      "uid": "brother",
      "ou": "users",
      "dc": "org"
    },
    "uid": "brother",
    "objectClass": "debianDeveloper",
    "uidNumber": "3119",
    "cn": "Martin",
    "sn": "Bagge",
    "ircNick": "brother-",
    "labeledURI": "http://martinbagge.tumblr.com",
    "keyFingerPrint": "56522C5F712B8BA202C99C093884437B13D8FF8B",
    "gidNumber": "3119"
  },
  {
    "info": "# bruce, users, debian.org",
    "dn": {
      "uid": "bruce",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.perens.com/",
    "cn": "Bruce",
    "uid": "bruce",
    "uidNumber": "1002",
    "ircNick": "BrucePerens",
    "sn": "Perens",
    "gidNumber": "1002"
  },
  {
    "info": "# bruno, users, debian.org",
    "dn": {
      "uid": "bruno",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bruno",
    "uid": "bruno",
    "uidNumber": "2692",
    "sn": "Barrera",
    "ircNick": "bbc",
    "jabberJID": "bbarrerac@jabber.org",
    "labeledURI": "http://people.debian.org/~bruno/",
    "gidNumber": "2692"
  },
  {
    "info": "# bs, users, debian.org",
    "dn": {
      "uid": "bs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bernd",
    "uid": "bs",
    "sn": "Schumacher",
    "uidNumber": "1122",
    "labeledURI": "http://bschu.de",
    "keyFingerPrint": "C7934D35FF87D526F797DBEAC519A20D121461F7",
    "gidNumber": "1122"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "bsb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bernd",
    "uid": "bsb",
    "sn": "Brentrup",
    "uidNumber": "827",
    "ircNick": "bsb",
    "gidNumber": "827"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "bsulcer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "bsulcer",
    "uidNumber": "905",
    "sn": "Sulcer",
    "gidNumber": "905"
  },
  {
    "info": "# btb, users, debian.org",
    "dn": {
      "uid": "btb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bradley",
    "uid": "btb",
    "sn": "Bell",
    "uidNumber": "1378",
    "ircNick": "btb",
    "labeledURI": "http://www.facebook.com/bradley.bell",
    "gidNumber": "1378"
  },
  {
    "info": "# bubulle, users, debian.org",
    "dn": {
      "uid": "bubulle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "bubulle",
    "sn": "Perrier",
    "uidNumber": "2382",
    "ircNick": "bubulle",
    "labeledURI": "http://www.perrier.eu.org/",
    "gidNumber": "2382"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "buci",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "buci",
    "uidNumber": "1035",
    "sn": "Nemkin",
    "gidNumber": "1035"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "budde",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marco",
    "uid": "budde",
    "uidNumber": "1043",
    "sn": "Budde",
    "gidNumber": "1043"
  },
  {
    "info": "20091208] Retired [2007-03-31 - JT]",
    "dn": {
      "uid": "bug1",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Glenn",
    "uid": "bug1",
    "uidNumber": "2238",
    "sn": "McGrath",
    "ircNick": "bug1",
    "gidNumber": "2238"
  },
  {
    "info": "4RkgkAKZ6WUnTIIXo8QeisIKIK8WQIDAQAB",
    "dn": {
      "uid": "bunk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adrian",
    "uid": "bunk",
    "uidNumber": "2063",
    "sn": "Bunk",
    "gidNumber": "2063",
    "keyFingerPrint": "3AFA757FAC6EA11D2FF45DF088D24287A2D898B1",
    "ircNick": "bunk"
  },
  {
    "info": "# bureado, users, debian.org",
    "dn": {
      "uid": "bureado",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bureado",
    "objectClass": "debianDeveloper",
    "uidNumber": "2895",
    "cn": "Jose",
    "sn": "Parrella",
    "keyFingerPrint": "3A0BEE9EEE0E1B7ECC6424E0C03242A988D4B7DF",
    "gidNumber": "2895",
    "ircNick": "bureado",
    "labeledURI": "http://www.bureado.com/"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "bweaver",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "bweaver",
    "uidNumber": "948",
    "sn": "Morris",
    "gidNumber": "948"
  },
  {
    "info": "# byang, users, debian.org",
    "dn": {
      "uid": "byang",
      "ou": "users",
      "dc": "org"
    },
    "uid": "byang",
    "objectClass": "debianDeveloper",
    "uidNumber": "3535",
    "gidNumber": "3535",
    "keyFingerPrint": "7E7729476D87D6F11D91ACCBC293E7B461825ACE",
    "cn": "Boyuan",
    "sn": "Yang",
    "ircNick": "hosiet"
  },
  {
    "info": "# byron, users, debian.org",
    "dn": {
      "uid": "byron",
      "ou": "users",
      "dc": "org"
    },
    "uid": "byron",
    "objectClass": "debianDeveloper",
    "uidNumber": "3273",
    "keyFingerPrint": "D5A37E1EAC124CED355C45E43DF87DACD098DE49",
    "cn": "Byron",
    "sn": "Campen",
    "gidNumber": "3273"
  },
  {
    "info": "# bzed, users, debian.org",
    "dn": {
      "uid": "bzed",
      "ou": "users",
      "dc": "org"
    },
    "uid": "bzed",
    "objectClass": "debianDeveloper",
    "uidNumber": "2904",
    "cn": "Bernd",
    "sn": "Zeimetz",
    "keyFingerPrint": "ECA1E3F28E112432D485DD95EB36171A6FF9435F",
    "gidNumber": "2904",
    "ircNick": "bzed",
    "jabberJID": "bzed@debian.org",
    "labeledURI": "http://bzed.de"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "caelum",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rafael",
    "uid": "caelum",
    "uidNumber": "1349",
    "ircNick": "rkitover",
    "sn": "Kitover",
    "gidNumber": "1349"
  },
  {
    "info": "# caiqian, users, debian.org",
    "dn": {
      "uid": "caiqian",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Qian",
    "uid": "caiqian",
    "uidNumber": "2741",
    "sn": "Cai",
    "ircNick": "caiqian",
    "labeledURI": "http://people.debian.org/~caiqian/",
    "gidNumber": "2741"
  },
  {
    "info": "# cajus, users, debian.org",
    "dn": {
      "uid": "cajus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Cajus",
    "uid": "cajus",
    "uidNumber": "2543",
    "sn": "Pollmeier",
    "ircNick": "organist",
    "gidNumber": "2543"
  },
  {
    "info": "# calculus, users, debian.org",
    "dn": {
      "uid": "calculus",
      "ou": "users",
      "dc": "org"
    },
    "uid": "calculus",
    "objectClass": "debianDeveloper",
    "uidNumber": "3311",
    "cn": "Jerome",
    "sn": "Benoit",
    "gidNumber": "3311",
    "keyFingerPrint": "AE28AE15710DFF1D87E5A7623F9219A67F36C68B"
  },
  {
    "info": "# calhariz, users, debian.org",
    "dn": {
      "uid": "calhariz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "calhariz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3467",
    "gidNumber": "3467",
    "keyFingerPrint": "464BC7CD439FEE5E8B4098A0348A778D6885EF8F",
    "cn": "Jose",
    "sn": "Calhariz",
    "ircNick": "calhariz",
    "labeledURI": "http://blog.calhariz.com"
  },
  {
    "info": "# calvin, users, debian.org",
    "dn": {
      "uid": "calvin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bastian",
    "uid": "calvin",
    "uidNumber": "2222",
    "sn": "Kleineidam",
    "gidNumber": "2222"
  },
  {
    "info": "# camm, users, debian.org",
    "dn": {
      "uid": "camm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Camm",
    "uid": "camm",
    "uidNumber": "1033",
    "sn": "Maguire",
    "keyFingerPrint": "FE214F363697773242E816ECB845CE510F9B714D",
    "gidNumber": "1033"
  },
  {
    "info": "# camrdale, users, debian.org",
    "dn": {
      "uid": "camrdale",
      "ou": "users",
      "dc": "org"
    },
    "uid": "camrdale",
    "objectClass": "debianDeveloper",
    "uidNumber": "2918",
    "keyFingerPrint": "FF0784FEE439DA85CAF1EA950F1F76E20D2036AD",
    "cn": "Cameron",
    "sn": "Dale",
    "gidNumber": "2918",
    "ircNick": "camrdale",
    "jabberJID": "camrdale@gmail.com",
    "labeledURI": "http://www.camrdale.org"
  },
  {
    "info": "# capper, users, debian.org",
    "dn": {
      "uid": "capper",
      "ou": "users",
      "dc": "org"
    },
    "uid": "capper",
    "objectClass": "debianDeveloper",
    "uidNumber": "3380",
    "keyFingerPrint": "445ED9C8FB58E42351D9289CF28981739A16F250",
    "cn": "Steven",
    "sn": "Capper",
    "gidNumber": "3380"
  },
  {
    "info": "# capriott, users, debian.org",
    "dn": {
      "uid": "capriott",
      "ou": "users",
      "dc": "org"
    },
    "uid": "capriott",
    "objectClass": "debianDeveloper",
    "uidNumber": "2881",
    "cn": "Andrea",
    "sn": "Capriotti",
    "keyFingerPrint": "1AF86F8139461B6B4E4AF7B30BAFEEEFE969BD54",
    "gidNumber": "2881",
    "ircNick": "capriott",
    "jabberJID": "capriott@jabber.linux.it",
    "labeledURI": "http://www.andreacapriotti.it"
  },
  {
    "info": "# carey, users, debian.org",
    "dn": {
      "uid": "carey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Carey",
    "uid": "carey",
    "uidNumber": "1188",
    "sn": "Evans",
    "ircNick": "carey",
    "labeledURI": "http://carey.geek.nz/",
    "gidNumber": "1640"
  },
  {
    "info": "# carnil, users, debian.org",
    "dn": {
      "uid": "carnil",
      "ou": "users",
      "dc": "org"
    },
    "uid": "carnil",
    "objectClass": "debianDeveloper",
    "uidNumber": "3095",
    "keyFingerPrint": "04A4407CB9142C23030C17AE789D6F057FD863FE",
    "cn": "Salvatore",
    "sn": "Bonaccorso",
    "gidNumber": "3095",
    "ircNick": "carnil"
  },
  {
    "info": "# caroll, users, debian.org",
    "dn": {
      "uid": "caroll",
      "ou": "users",
      "dc": "org"
    },
    "uid": "caroll",
    "objectClass": "debianDeveloper",
    "uidNumber": "3186",
    "keyFingerPrint": "86233022C12B6ECFC90A400785045872DA83FD56",
    "sn": "Comandulli",
    "cn": "Ana Carolina",
    "ircNick": "Caroll",
    "labeledURI": "http://carollices.me/",
    "gidNumber": "3186"
  },
  {
    "info": "# carsten, users, debian.org",
    "dn": {
      "uid": "carsten",
      "ou": "users",
      "dc": "org"
    },
    "uid": "carsten",
    "objectClass": "debianDeveloper",
    "uidNumber": "2972",
    "keyFingerPrint": "B638FD9E5E6B184FCF9E2363329465A24F1FC85D",
    "cn": "Carsten",
    "sn": "Hey",
    "ircNick": "carstenh",
    "gidNumber": "2972"
  },
  {
    "info": "# cas, users, debian.org",
    "dn": {
      "uid": "cas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Craig",
    "uid": "cas",
    "sn": "Sanders",
    "uidNumber": "828",
    "gidNumber": "828",
    "ircNick": "cas",
    "labeledURI": "http://taz.net.au/blog/"
  },
  {
    "info": "# cascardo, users, debian.org",
    "dn": {
      "uid": "cascardo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cascardo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3214",
    "keyFingerPrint": "DF813B226DD39A2C530F6F7D0ABA650372FD9571",
    "cn": "Thadeu",
    "sn": "Cascardo",
    "gidNumber": "3214",
    "ircNick": "cascardo",
    "jabberJID": "cascardo@jabber-br.org",
    "labeledURI": "https://cascardo.eti.br/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "casi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jean",
    "uid": "casi",
    "uidNumber": "2196",
    "ircNick": "yip",
    "sn": "Bourdaret",
    "gidNumber": "2196"
  },
  {
    "info": "# cate, users, debian.org",
    "dn": {
      "uid": "cate",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Giacomo",
    "uid": "cate",
    "uidNumber": "2101",
    "sn": "Catenazzi",
    "ircNick": "cate",
    "labeledURI": "http://cateee.net",
    "keyFingerPrint": "A91751625A08C3CF9088F6AD55FF03AE59EC5558",
    "gidNumber": "2101"
  },
  {
    "info": "# cavedon, users, debian.org",
    "dn": {
      "uid": "cavedon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cavedon",
    "objectClass": "debianDeveloper",
    "uidNumber": "3043",
    "cn": "Ludovico",
    "sn": "Cavedon",
    "keyFingerPrint": "C62F5124B8950ADBF7F3AB4C13C0B5667A38B0B0",
    "gidNumber": "3043",
    "ircNick": "cavedon"
  },
  {
    "info": "# cavok, users, debian.org",
    "dn": {
      "uid": "cavok",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Domenico",
    "uid": "cavok",
    "uidNumber": "2249",
    "sn": "Andreoli",
    "gidNumber": "2249",
    "keyFingerPrint": "3B100CA18674ACBAB4FEFCD2CE5BCF179960DE13"
  },
  {
    "info": "# cbf, users, debian.org",
    "dn": {
      "uid": "cbf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Carlos",
    "uid": "cbf",
    "sn": "Barros",
    "uidNumber": "1210",
    "ircNick": "corky",
    "gidNumber": "1613"
  },
  {
    "info": "# cbiedl, users, debian.org",
    "dn": {
      "uid": "cbiedl",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cbiedl",
    "objectClass": "debianDeveloper",
    "uidNumber": "3434",
    "gidNumber": "3434",
    "keyFingerPrint": "597308FBBDBA035D8C7C95DDC42C58EB591492FD",
    "cn": "Christoph",
    "sn": "Biedl"
  },
  {
    "info": "# ccheney, users, debian.org",
    "dn": {
      "uid": "ccheney",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christopher",
    "keyFingerPrint": "2D31192787D71F249FF91BC5D1065AB38E384AF2",
    "uid": "ccheney",
    "sn": "Cheney",
    "uidNumber": "2036",
    "ircNick": "calc",
    "jabberJID": "chris.cheney@gmail.com",
    "labeledURI": "http://people.debian.org/~ccheney/",
    "gidNumber": "2036"
  },
  {
    "info": "# ccontavalli, users, debian.org",
    "dn": {
      "uid": "ccontavalli",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.commedia.it/ccontavalli/",
    "cn": "Carlo",
    "uid": "ccontavalli",
    "uidNumber": "2497",
    "ircNick": "lap",
    "sn": "Contavalli",
    "gidNumber": "2497"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "cd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cd",
    "sn": "Davis",
    "labeledURI": "http://www.lrtw.org",
    "uidNumber": "1255",
    "ircNick": "doozer",
    "gidNumber": "1601"
  },
  {
    "info": "20091208] Resigned 11/11 <20001111190035.A622@warg.uwaterloo.ca>",
    "dn": {
      "uid": "cdm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cdm",
    "uidNumber": "1404",
    "sn": "McKillop",
    "gidNumber": "1404"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "cdmorgan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Charles",
    "uid": "cdmorgan",
    "uidNumber": "829",
    "sn": "Morgan",
    "gidNumber": "829"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "cdr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Costa",
    "uid": "cdr",
    "uidNumber": "830",
    "sn": "Rasmussen",
    "gidNumber": "830"
  },
  {
    "info": "SqDqxGCb1Pdq7T0nIw",
    "dn": {
      "uid": "cech",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Petr",
    "uid": "cech",
    "sn": "Cech",
    "uidNumber": "1203",
    "keyFingerPrint": "DF330C6EE20179FFD2B2930AF682B9EEB9E99CF8",
    "gidNumber": "1632",
    "ircNick": "Peta",
    "labeledURI": "http://merlin.debian.net/"
  },
  {
    "info": "# cedric, users, debian.org",
    "dn": {
      "uid": "cedric",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "cedric",
    "uidNumber": "2496",
    "sn": "Delfosse",
    "labeledURI": "http://www.delfosse.name",
    "ircNick": "cdelfosse",
    "gidNumber": "2496",
    "cn": ": Q8OpZHJpYw=="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "cerb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Erick",
    "uid": "cerb",
    "sn": "Kinnee",
    "labeledURI": "http://www.kinnee.net",
    "uidNumber": "1299",
    "ircNick": "cerb",
    "gidNumber": "1299"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "cerebus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Allan",
    "uid": "cerebus",
    "uidNumber": "1303",
    "sn": "Anderson",
    "gidNumber": "1303"
  },
  {
    "info": "# certik, users, debian.org",
    "dn": {
      "uid": "certik",
      "ou": "users",
      "dc": "org"
    },
    "uid": "certik",
    "objectClass": "debianDeveloper",
    "uidNumber": "3328",
    "keyFingerPrint": "1B470A1C043AB115A99638E048682904DEE27C7D",
    "gidNumber": "3328",
    "cn": ": T25kxZllag==",
    "sn": ": xIxlcnTDrWs="
  },
  {
    "info": "# cesco, users, debian.org",
    "dn": {
      "uid": "cesco",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Francesco",
    "uid": "cesco",
    "uidNumber": "1054",
    "sn": "Tapparo",
    "gidNumber": "1054"
  },
  {
    "info": "# cespedes, users, debian.org",
    "dn": {
      "uid": "cespedes",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Juan",
    "uid": "cespedes",
    "sn": "Cespedes",
    "uidNumber": "1067",
    "keyFingerPrint": "3EF6FC9BDFD076F506BEA16FAFAB931FA560607C",
    "gidNumber": "1067",
    "ircNick": "cespedes",
    "labeledURI": "http://www.cespedes.org/"
  },
  {
    "info": "# cetus, users, debian.org",
    "dn": {
      "uid": "cetus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nikita",
    "uid": "cetus",
    "uidNumber": "1082",
    "sn": "Schmidt",
    "gidNumber": "1082"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "cfm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christopher",
    "uid": "cfm",
    "uidNumber": "1280",
    "sn": "Miller",
    "gidNumber": "1610"
  },
  {
    "info": "# cfry, users, debian.org",
    "dn": {
      "uid": "cfry",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cfry",
    "objectClass": "debianDeveloper",
    "uidNumber": "2803",
    "cn": "Charles",
    "sn": "Fry",
    "labeledURI": "http://frogcircus.org/",
    "gidNumber": "2803"
  },
  {
    "info": "# cgb, users, debian.org",
    "dn": {
      "uid": "cgb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christoph",
    "uid": "cgb",
    "sn": "Baumann",
    "uidNumber": "1306",
    "ircNick": "hlHSM",
    "labeledURI": "http://www.sgoc.de",
    "gidNumber": "1306"
  },
  {
    "info": "# ch, users, debian.org",
    "dn": {
      "uid": "ch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "ch",
    "uidNumber": "1197",
    "ircNick": "lathspell",
    "jabberJID": "lathspell@jabber.gondor.com",
    "labeledURI": "http://www.lathspell.de",
    "sn": "Brunotte",
    "gidNumber": "1602"
  },
  {
    "info": "# chaica, users, debian.org",
    "dn": {
      "uid": "chaica",
      "ou": "users",
      "dc": "org"
    },
    "uid": "chaica",
    "objectClass": "debianDeveloper",
    "uidNumber": "3181",
    "keyFingerPrint": "DD34BB88EFB7DE735C77597C0270A2758CD736E2",
    "cn": "Carl",
    "sn": "Chenet",
    "gidNumber": "3181"
  },
  {
    "info": "# chanop, users, debian.org",
    "dn": {
      "uid": "chanop",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chanop",
    "uid": "chanop",
    "sn": "Silpa-Anan",
    "labeledURI": "http://thai.anu.edu.au/",
    "uidNumber": "2043",
    "ircNick": "avatar",
    "gidNumber": "2043"
  },
  {
    "info": "# charon, users, debian.org",
    "dn": {
      "uid": "charon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kenneth",
    "uid": "charon",
    "uidNumber": "2259",
    "sn": "Schmidt",
    "ircNick": "charon",
    "gidNumber": "2259"
  },
  {
    "info": "# chaton, users, debian.org",
    "dn": {
      "uid": "chaton",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jean-Marc",
    "uid": "chaton",
    "uidNumber": "2270",
    "sn": "Chaton",
    "ircNick": "jcn",
    "gidNumber": "2270"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "che",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "che",
    "uidNumber": "1041",
    "ircNick": "Che_Fox",
    "sn": "Gertzfield",
    "gidNumber": "1041"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "chep",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Cyrille",
    "uid": "chep",
    "sn": "Chepelov",
    "uidNumber": "2294",
    "ircNick": "Chep",
    "gidNumber": "2294"
  },
  {
    "info": "# chewie, users, debian.org",
    "dn": {
      "uid": "chewie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chad",
    "uid": "chewie",
    "sn": "Walstrom",
    "uidNumber": "2252",
    "ircNick": "^chewie",
    "labeledURI": "http://www.wookimus.net/",
    "gidNumber": "2252"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "chip",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chip",
    "uid": "chip",
    "sn": "Salzenberg",
    "labeledURI": "http://pobox.com/~chip/",
    "uidNumber": "1381",
    "ircNick": "chip",
    "gidNumber": "1381"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "chojin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "chojin",
    "uidNumber": "2621",
    "ircNick": "ChoJin",
    "sn": "Peyrot",
    "gidNumber": "2621",
    "cn": ": UXXDtGM="
  },
  {
    "info": "# cholm, users, debian.org",
    "dn": {
      "uid": "cholm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cholm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3036",
    "cn": "Christian",
    "sn": "Christensen",
    "gidNumber": "3036"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "chomsky",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chuck",
    "uid": "chomsky",
    "uidNumber": "1325",
    "ircNick": "chomsky",
    "sn": "Thomas",
    "gidNumber": "1325"
  },
  {
    "info": "# chris, users, debian.org",
    "dn": {
      "uid": "chris",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christopher",
    "uid": "chris",
    "sn": "Chimelis",
    "uidNumber": "1110",
    "ircNick": "crack_ho",
    "gidNumber": "1110"
  },
  {
    "info": "# chris_se, users, debian.org",
    "dn": {
      "uid": "chris_se",
      "ou": "users",
      "dc": "org"
    },
    "uid": "chris_se",
    "objectClass": "debianDeveloper",
    "uidNumber": "3354",
    "cn": "Christian",
    "sn": "Seiler",
    "keyFingerPrint": "D3284E4E61A9278A511ABC9655DB1ABC3818B08C",
    "gidNumber": "3354"
  },
  {
    "info": "# chrisb, users, debian.org",
    "dn": {
      "uid": "chrisb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "chrisb",
    "sn": "Butler",
    "uidNumber": "2156",
    "ircNick": "crispy",
    "labeledURI": "http://crispygoth.livejournal.com/",
    "gidNumber": "2156"
  },
  {
    "info": "# chrisd, users, debian.org",
    "dn": {
      "uid": "chrisd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "chrisd",
    "uidNumber": "1123",
    "sn": "DiBona",
    "gidNumber": "1123"
  },
  {
    "info": "# chrish, users, debian.org",
    "dn": {
      "uid": "chrish",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "chrish",
    "uidNumber": "883",
    "sn": "Hudon",
    "gidNumber": "883"
  },
  {
    "info": "# chrism, users, debian.org",
    "dn": {
      "uid": "chrism",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christoph",
    "uid": "chrism",
    "sn": "Martin",
    "uidNumber": "911",
    "keyFingerPrint": "4C8F6B0D121EB15F076FEB1704EE131AE6D621BE",
    "gidNumber": "911",
    "ircNick": "chrism",
    "jabberJID": "martin@jabber.uni-mainz.de"
  },
  {
    "info": "# chrissie, users, debian.org",
    "dn": {
      "uid": "chrissie",
      "ou": "users",
      "dc": "org"
    },
    "uid": "chrissie",
    "objectClass": "debianDeveloper",
    "uidNumber": "3088",
    "cn": "Christine",
    "sn": "Caulfield",
    "ircNick": "chrissie",
    "labeledURI": "http://chrissie.bandcamp.com",
    "gidNumber": "3088"
  },
  {
    "info": "# christine, users, debian.org",
    "dn": {
      "uid": "christine",
      "ou": "users",
      "dc": "org"
    },
    "uid": "christine",
    "objectClass": "debianDeveloper",
    "uidNumber": "2864",
    "keyFingerPrint": "FBE01342FCEFD379D3DCE61764959FE9838DF19C",
    "sn": "Spang",
    "ircNick": "christine",
    "jabberJID": "spang@mit.edu",
    "labeledURI": "http://spang.cc/",
    "cn": "Christine",
    "gidNumber": "2864"
  },
  {
    "info": "L1leHpA4is7So7rb9aAE5KlvwIDAQAB k=rsa",
    "dn": {
      "uid": "christoph",
      "ou": "users",
      "dc": "org"
    },
    "uid": "christoph",
    "objectClass": "debianDeveloper",
    "uidNumber": "3042",
    "keyFingerPrint": "9FED5C6CE206B70A585770CA965522B9D49AE731",
    "cn": "Christoph",
    "sn": "Egger",
    "gidNumber": "3042",
    "ircNick": "christoph",
    "jabberJID": "christoph@egger.im",
    "labeledURI": "http://www.christoph-egger.org"
  },
  {
    "info": "# christophe, users, debian.org",
    "dn": {
      "uid": "christophe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christophe",
    "uid": "christophe",
    "uidNumber": "2502",
    "sn": "Barbe",
    "gidNumber": "2502"
  },
  {
    "info": "# chrisvdb, users, debian.org",
    "dn": {
      "uid": "chrisvdb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "chrisvdb",
    "objectClass": "debianDeveloper",
    "uidNumber": "2754",
    "cn": "Chris",
    "sn": "Vanden Berghe",
    "ircNick": "chrisvdb",
    "labeledURI": "http://chris.vandenberghe.org/",
    "gidNumber": "2754"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "chrisw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "chrisw",
    "uidNumber": "1025",
    "sn": "Walker",
    "gidNumber": "1025"
  },
  {
    "info": "# chronitis, users, debian.org",
    "dn": {
      "uid": "chronitis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3577",
    "gidNumber": "3577",
    "keyFingerPrint": "E8FC295C86B8D7C049F97BA7A35DAFFBAD29E8DE",
    "cn": "Gordon",
    "sn": "Ball",
    "uid": "chronitis",
    "ircNick": "chronitis"
  },
  {
    "info": "# chrsmrtn, users, debian.org",
    "dn": {
      "uid": "chrsmrtn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christopher",
    "uid": "chrsmrtn",
    "uidNumber": "2749",
    "sn": "Martin",
    "gidNumber": "2749"
  },
  {
    "info": "# chrysn, users, debian.org",
    "dn": {
      "uid": "chrysn",
      "ou": "users",
      "dc": "org"
    },
    "uid": "chrysn",
    "objectClass": "debianDeveloper",
    "uidNumber": "3201",
    "keyFingerPrint": "08CD6D1255FA3A875C1FB096398D1112D3A4BDE1",
    "cn": "Christian",
    "gidNumber": "3201",
    "ircNick": "chrysn",
    "jabberJID": "chrysn@jabber.fsfe.org",
    "labeledURI": "http://christian.amsuess.com/",
    "sn": ": QW1zw7xzcw=="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ciaccio",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "ciaccio",
    "uidNumber": "1085",
    "sn": "Ciaccio",
    "gidNumber": "1085"
  },
  {
    "info": "# cilibrar, users, debian.org",
    "dn": {
      "uid": "cilibrar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cilibrar",
    "objectClass": "debianDeveloper",
    "uidNumber": "2899",
    "cn": "Rudi",
    "sn": "Cilibrasi",
    "gidNumber": "2899"
  },
  {
    "info": "# cinemast, users, debian.org",
    "dn": {
      "uid": "cinemast",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cinemast",
    "objectClass": "debianDeveloper",
    "uidNumber": "3371",
    "keyFingerPrint": "132BF9B79BFF69295924C3983FC89867A5418556",
    "cn": "Peter",
    "sn": "Spiess-Knafl",
    "gidNumber": "3371"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "cjf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cjf",
    "sn": "Fearnley",
    "labeledURI": "http://www.CJFearnley.com",
    "uidNumber": "832",
    "gidNumber": "832"
  },
  {
    "info": "ZlBhmwgcADTzpXJMF+olBx2nx8IL5WL8DwIDAQAB",
    "dn": {
      "uid": "cjwatson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Colin",
    "uid": "cjwatson",
    "sn": "Watson",
    "uidNumber": "2235",
    "keyFingerPrint": "AC0A4FF12611B6FCCF01C111393587D97D86500B",
    "gidNumber": "2235",
    "ircNick": "cjwatson",
    "labeledURI": "https://www.chiark.greenend.org.uk/~cjwatson/"
  },
  {
    "info": "# ckesselh, users, debian.org",
    "dn": {
      "uid": "ckesselh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "ckesselh",
    "sn": "Kesselheim",
    "labeledURI": "http://www.kesselheim.com",
    "uidNumber": "2466",
    "ircNick": "RealCynic",
    "gidNumber": "2466"
  },
  {
    "info": "AVC12D2n90IBhFzhsDasV8NZQ6W53F64kCOJOq0rTosIlJCcWon5zqowqd2cpq0CAwEAAQ==",
    "dn": {
      "uid": "ckk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ckk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3367",
    "keyFingerPrint": "08F084DA146C873C361AAFA8E76004C5CEF0C94C",
    "cn": "Christian",
    "sn": "Kastner",
    "gidNumber": "3367",
    "ircNick": "ckk"
  },
  {
    "info": "# cklin, users, debian.org",
    "dn": {
      "uid": "cklin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chuan-kai",
    "uid": "cklin",
    "sn": "Lin",
    "uidNumber": "2197",
    "keyFingerPrint": "C0730C82DF731FD9E619273A167FD434C043A313",
    "gidNumber": "2197",
    "ircNick": "cklin"
  },
  {
    "info": "# clameter, users, debian.org",
    "dn": {
      "uid": "clameter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christoph",
    "uid": "clameter",
    "sn": "Lameter",
    "uidNumber": "930",
    "ircNick": "o-o",
    "labeledURI": "http://lameter.com",
    "gidNumber": "930"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "claus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Claus",
    "uid": "claus",
    "uidNumber": "1126",
    "sn": "Wickinghoff",
    "gidNumber": "1126"
  },
  {
    "info": "# claviola, users, debian.org",
    "dn": {
      "uid": "claviola",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Carlos",
    "uid": "claviola",
    "sn": "Laviola",
    "uidNumber": "2300",
    "keyFingerPrint": "86447B523F2D8F3E28E7F62517E950C257EB2134",
    "gidNumber": "2300",
    "ircNick": "claviola",
    "jabberJID": "claviola@jabber.org",
    "labeledURI": "http://laviola.org"
  },
  {
    "info": "# clebars, users, debian.org",
    "dn": {
      "uid": "clebars",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.lebars.org/",
    "cn": "Christophe",
    "uid": "clebars",
    "uidNumber": "887",
    "sn": "Le Bars",
    "gidNumber": "887"
  },
  {
    "info": "# cleto, users, debian.org",
    "dn": {
      "uid": "cleto",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cleto",
    "objectClass": "debianDeveloper",
    "uidNumber": "3217",
    "cn": "Cleto",
    "gidNumber": "3217",
    "sn": ": TWFydMOtbiBBbmdlbGluYQ==",
    "ircNick": "cleto",
    "jabberJID": "cleto.martin@gmail.com"
  },
  {
    "info": "zqG9oNXYd4mdagU/4fqcu7q6Z3F6HVwIDAQAB",
    "dn": {
      "uid": "clint",
      "ou": "users",
      "dc": "org"
    },
    "uid": "clint",
    "objectClass": "debianDeveloper",
    "uidNumber": "3122",
    "keyFingerPrint": "2100A32C46F895AF3A08783AF6D3495BB0AE9A02",
    "cn": "Clint",
    "sn": "Adams",
    "gidNumber": "3122",
    "ircNick": "Clint"
  },
  {
    "info": "# clown, users, debian.org",
    "dn": {
      "uid": "clown",
      "ou": "users",
      "dc": "org"
    },
    "uid": "clown",
    "objectClass": "debianDeveloper",
    "uidNumber": "2812",
    "cn": "Alexei",
    "sn": "Nikolov",
    "keyFingerPrint": "930BB50F68C747D2A0E70FA4566E861304468DEC",
    "gidNumber": "2812"
  },
  {
    "info": "# cmb, users, debian.org",
    "dn": {
      "uid": "cmb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cmb",
    "sn": "Boyle",
    "uidNumber": "2455",
    "keyFingerPrint": "EE6116FF05A783D6E6176B4517B1CA7D64089528",
    "gidNumber": "2455",
    "ircNick": "cmb",
    "labeledURI": "https://chris.boyle.name/"
  },
  {
    "info": "# cmc, users, debian.org",
    "dn": {
      "uid": "cmc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Claire",
    "uid": "cmc",
    "sn": "Connelly",
    "uidNumber": "2100",
    "ircNick": "cmc",
    "labeledURI": "http://www.eskimo.com/~c/",
    "gidNumber": "2100"
  },
  {
    "info": "# cmiller, users, debian.org",
    "dn": {
      "uid": "cmiller",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chad",
    "uid": "cmiller",
    "uidNumber": "2176",
    "ircNick": "funroll-loops",
    "sn": "Miller",
    "gidNumber": "2176"
  },
  {
    "info": "# cmorrone, users, debian.org",
    "dn": {
      "uid": "cmorrone",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christopher",
    "uid": "cmorrone",
    "uidNumber": "1164",
    "sn": "Morrone",
    "gidNumber": "1164"
  },
  {
    "info": "# cmot, users, debian.org",
    "dn": {
      "uid": "cmot",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adrian",
    "uid": "cmot",
    "sn": "von Bidder",
    "uidNumber": "2721",
    "ircNick": "cmot",
    "labeledURI": "http://fortytwo.ch/",
    "gidNumber": "2721"
  },
  {
    "info": "# cmr, users, debian.org",
    "dn": {
      "uid": "cmr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cmr",
    "sn": "Rutter",
    "labeledURI": "http://www.armlinux.org/~chris/",
    "uidNumber": "2074",
    "ircNick": "cmr",
    "gidNumber": "2074"
  },
  {
    "info": "# cmruffin, users, debian.org",
    "dn": {
      "uid": "cmruffin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cmruffin",
    "sn": "Ruffin",
    "uidNumber": "2126",
    "ircNick": "ruff",
    "gidNumber": "2126"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "cmwilson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "cmwilson",
    "uidNumber": "932",
    "sn": "Wilson",
    "gidNumber": "932"
  },
  {
    "info": "# cnanakos, users, debian.org",
    "dn": {
      "uid": "cnanakos",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cnanakos",
    "objectClass": "debianDeveloper",
    "uidNumber": "3357",
    "keyFingerPrint": "6583B3EBB874F4544D5D2B23873DEA309D13807F",
    "cn": "Chrysostomos",
    "sn": "Nanakos",
    "ircNick": "cnanakos",
    "gidNumber": "3357"
  },
  {
    "info": "jyQRn5vDehb9ibp2WjzB8rf3fSwQZc3i/QIDAQAB",
    "dn": {
      "uid": "codehelp",
      "ou": "users",
      "dc": "org"
    },
    "uid": "codehelp",
    "objectClass": "debianDeveloper",
    "uidNumber": "2829",
    "cn": "Neil",
    "sn": "Williams",
    "keyFingerPrint": "7F71C1E9C78E735D0360C6CCF167E43C8143B682",
    "gidNumber": "2829",
    "ircNick": "codehelp",
    "labeledURI": "http://www.linux.codehelp.co.uk/"
  },
  {
    "info": "20091208] Dead remove in favor of mkc",
    "dn": {
      "uid": "coleman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "coleman",
    "uidNumber": "902",
    "sn": "Coleman",
    "gidNumber": "902"
  },
  {
    "info": "# colint, users, debian.org",
    "dn": {
      "uid": "colint",
      "ou": "users",
      "dc": "org"
    },
    "uid": "colint",
    "objectClass": "debianDeveloper",
    "uidNumber": "2892",
    "cn": "Colin",
    "sn": "Tuckley",
    "ircNick": "Colin",
    "gidNumber": "2892"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "colol",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Colin",
    "uid": "colol",
    "uidNumber": "2448",
    "ircNick": "colol",
    "sn": "Mattson",
    "gidNumber": "2448"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "coolo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephan",
    "uid": "coolo",
    "uidNumber": "1169",
    "sn": "Kulow",
    "gidNumber": "1169"
  },
  {
    "info": "# cord, users, debian.org",
    "dn": {
      "uid": "cord",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Cord",
    "uid": "cord",
    "sn": "Beermann",
    "uidNumber": "2178",
    "keyFingerPrint": "12CF5605F7A8D8A6789B9B5398561D2D2502FE39",
    "gidNumber": "2178",
    "ircNick": {},
    "jabberJID": "cord@jabber.org",
    "labeledURI": "https://cord.de"
  },
  {
    "info": "# corsac, users, debian.org",
    "dn": {
      "uid": "corsac",
      "ou": "users",
      "dc": "org"
    },
    "uid": "corsac",
    "objectClass": "debianDeveloper",
    "uidNumber": "2884",
    "keyFingerPrint": "4510DCB57ED4704060C6647630550F7871EF0BA8",
    "cn": "Yves-Alexis",
    "sn": "Perez",
    "gidNumber": "2884",
    "ircNick": "Corsac",
    "jabberJID": "corsac@vaurien.net",
    "labeledURI": "http://www.corsac.net"
  },
  {
    "info": "# costela, users, debian.org",
    "dn": {
      "uid": "costela",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Leo",
    "uid": "costela",
    "sn": "Costela",
    "uidNumber": "2577",
    "gidNumber": "2577",
    "ircNick": "costela",
    "labeledURI": "https://costela.net",
    "keyFingerPrint": "E0D888C9518B7DE723316D93D6CE13EE353508BF"
  },
  {
    "info": "# coucouf, users, debian.org",
    "dn": {
      "uid": "coucouf",
      "ou": "users",
      "dc": "org"
    },
    "uid": "coucouf",
    "objectClass": "debianDeveloper",
    "uidNumber": "3552",
    "gidNumber": "3552",
    "keyFingerPrint": "216FFF40001A0E070E283B1F71A7E533F291A324",
    "cn": ": QXVyw6lsaWVu",
    "sn": "Couderc",
    "ircNick": "coucouf",
    "jabberJID": "coucouf@jabber.fr"
  },
  {
    "info": "# coven, users, debian.org",
    "dn": {
      "uid": "coven",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pawel",
    "uid": "coven",
    "sn": "Wiecek",
    "labeledURI": "http://www.coven.vmh.net/",
    "uidNumber": "1050",
    "ircNick": "coven",
    "gidNumber": "1050"
  },
  {
    "info": "# cowboy, users, debian.org",
    "dn": {
      "uid": "cowboy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "cowboy",
    "sn": "Nelson",
    "uidNumber": "1163",
    "ircNick": "cowboy",
    "jabberJID": "cowboy@cavein.org",
    "keyFingerPrint": "684A1F8DC9BBE0DFF3168C9F015427E3A4B47676",
    "gidNumber": "1163"
  },
  {
    "info": "# cpbotha, users, debian.org",
    "dn": {
      "uid": "cpbotha",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Charl",
    "uid": "cpbotha",
    "sn": "Botha",
    "labeledURI": "http://cpbotha.net/",
    "uidNumber": "2148",
    "ircNick": "burp",
    "gidNumber": "2148"
  },
  {
    "info": "# cpbs, users, debian.org",
    "dn": {
      "uid": "cpbs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Charles",
    "uid": "cpbs",
    "uidNumber": "1058",
    "sn": "Briscoe-Smith",
    "ircNick": "Pippin",
    "labeledURI": "http://briscoe-smith.org.uk/charles/",
    "gidNumber": "1058"
  },
  {
    "info": "# cph, users, debian.org",
    "dn": {
      "uid": "cph",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cph",
    "uidNumber": "2179",
    "sn": "Hanson",
    "labeledURI": "http://chris-hanson.org/",
    "gidNumber": "2179"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "cprados",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.geocities.com/cprados",
    "cn": "Carlos",
    "uid": "cprados",
    "uidNumber": "2290",
    "ircNick": "cpb",
    "sn": "Prados",
    "gidNumber": "2290"
  },
  {
    "info": "# cptnemo, users, debian.org",
    "dn": {
      "uid": "cptnemo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cptnemo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3468",
    "gidNumber": "3468",
    "keyFingerPrint": "DE0837244DAFAE6C5DF33AD57EAE264181E773D5",
    "cn": "Uli",
    "sn": "Scholler",
    "ircNick": "cpt_nemo"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "cr212",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "cr212",
    "uidNumber": "1214",
    "sn": "Reed",
    "gidNumber": "1656"
  },
  {
    "info": "# crafterm, users, debian.org",
    "dn": {
      "uid": "crafterm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://blogs.cocoondev.org/crafterm/",
    "cn": "Marcus",
    "uid": "crafterm",
    "uidNumber": "2339",
    "ircNick": "shoelace",
    "sn": "Crafter",
    "gidNumber": "2339"
  },
  {
    "info": "# craig, users, debian.org",
    "dn": {
      "uid": "craig",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Craig",
    "uid": "craig",
    "uidNumber": "1323",
    "sn": "Brozefsky",
    "gidNumber": "1323"
  },
  {
    "info": "# cree, users, debian.org",
    "dn": {
      "uid": "cree",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cree",
    "objectClass": "debianDeveloper",
    "uidNumber": "3132",
    "keyFingerPrint": "D409541B80A1927FCC41E0A10BE7C53FC1DE67F3",
    "cn": "Michael",
    "sn": "Cree",
    "gidNumber": "3132"
  },
  {
    "info": "# creekorful, users, debian.org",
    "dn": {
      "uid": "creekorful",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3613",
    "gidNumber": "3613",
    "keyFingerPrint": "DA4AA4369BFAE29967CDE85BF733E8710859FCD2",
    "sn": "Micard",
    "uid": "creekorful",
    "cn": ": QWxvw69z",
    "ircNick": "creekorful",
    "labeledURI": "https://www.creekorful.org"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "creis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Helios",
    "uid": "creis",
    "uidNumber": "2544",
    "ircNick": "creis",
    "sn": "de Creisquer",
    "gidNumber": "2544"
  },
  {
    "info": "# cristian, users, debian.org",
    "dn": {
      "uid": "cristian",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cristian",
    "objectClass": "debianDeveloper",
    "uidNumber": "3151",
    "cn": "Cristian",
    "sn": "Greco",
    "ircNick": "cgreco",
    "jabberJID": "cristian@regolo.cc",
    "gidNumber": "3151"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "crow",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://systematicattack.net",
    "cn": "Stephen",
    "uid": "crow",
    "uidNumber": "1269",
    "ircNick": "Crow-",
    "sn": "Crowley",
    "gidNumber": "1625"
  },
  {
    "info": "# crusoe, users, debian.org",
    "dn": {
      "uid": "crusoe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3587",
    "gidNumber": "3587",
    "keyFingerPrint": "724D609337113C710550D7473C26763F6C67E6E2",
    "cn": "Michael",
    "sn": "Crusoe",
    "uid": "crusoe",
    "ircNick": "crusoe",
    "labeledURI": "https://orcid.org/0000-0002-2961-9670"
  },
  {
    "info": "xahAB/PmvQIDAQAB",
    "dn": {
      "uid": "csmall",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Craig",
    "uid": "csmall",
    "sn": "Small",
    "uidNumber": "1010",
    "keyFingerPrint": "5D2FB320B825D93904D205193938F96BDF50FEA5",
    "gidNumber": "1679",
    "ircNick": "seeS",
    "jabberJID": "csmall@dropbear.xyz",
    "labeledURI": "https://dropbear.xyz/"
  },
  {
    "info": "# cstamas, users, debian.org",
    "dn": {
      "uid": "cstamas",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cstamas",
    "objectClass": "debianDeveloper",
    "uidNumber": "3269",
    "keyFingerPrint": "3AA80E42C72F173ABE1A6C9DA248278850A65533",
    "cn": "Tamas",
    "sn": "Csillag",
    "ircNick": "cstamas",
    "labeledURI": "http://cstamas.hu/",
    "gidNumber": "3269"
  },
  {
    "info": "# csurchi, users, debian.org",
    "dn": {
      "uid": "csurchi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "csurchi",
    "sn": "Surchi",
    "uidNumber": "2044",
    "ircNick": "Christian",
    "jabberJID": "cs@jabber.linux.it",
    "gidNumber": "2044"
  },
  {
    "info": "# ctaylor, users, debian.org",
    "dn": {
      "uid": "ctaylor",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ctaylor",
    "objectClass": "debianDeveloper",
    "uidNumber": "3046",
    "cn": "Christopher",
    "sn": "Taylor",
    "keyFingerPrint": "829679D2343D0D7A1E264A1E746B10E52F7CEC16",
    "gidNumber": "3046",
    "ircNick": "ctaylor",
    "labeledURI": "http://www.code-monkeys.org"
  },
  {
    "info": "# ctrochalakis, users, debian.org",
    "dn": {
      "uid": "ctrochalakis",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ctrochalakis",
    "objectClass": "debianDeveloper",
    "uidNumber": "3444",
    "gidNumber": "3444",
    "keyFingerPrint": "7F648F6C2123C98FB329C82B11362600A747ECD9",
    "cn": "Christos",
    "sn": "Trochalakis",
    "ircNick": "ctrochalakis",
    "jabberJID": "yatiohi@jabber.org"
  },
  {
    "info": "# cts, users, debian.org",
    "dn": {
      "uid": "cts",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "cts",
    "sn": "Steigies",
    "uidNumber": "1099",
    "keyFingerPrint": "32E1DE99463F182E167A3E04700D567871B28342",
    "gidNumber": "1099",
    "ircNick": "cts"
  },
  {
    "info": "# cuavas, users, debian.org",
    "dn": {
      "uid": "cuavas",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cuavas",
    "objectClass": "debianDeveloper",
    "uidNumber": "3427",
    "gidNumber": "3427",
    "keyFingerPrint": "0003311B496F9CF748C3065AD7CCD54DD20C7D80",
    "cn": "Vas",
    "sn": "Crabb"
  },
  {
    "info": "# cupis, users, debian.org",
    "dn": {
      "uid": "cupis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "cupis",
    "sn": "Cupis",
    "uidNumber": "2559",
    "ircNick": "cupis",
    "labeledURI": "http://www.cupis.co.uk",
    "gidNumber": "2559"
  },
  {
    "info": "# curan, users, debian.org",
    "dn": {
      "uid": "curan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "curan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3118",
    "keyFingerPrint": "060096CEF3C8E733E5B61587A309D76CE1DE59D2",
    "cn": "Kai",
    "ircNick": "Curan",
    "gidNumber": "3118",
    "sn": ": V2Fzc2VyYsOkY2g="
  },
  {
    "info": "# cworth, users, debian.org",
    "dn": {
      "uid": "cworth",
      "ou": "users",
      "dc": "org"
    },
    "uid": "cworth",
    "objectClass": "debianDeveloper",
    "uidNumber": "3021",
    "cn": "Carl",
    "sn": "Worth",
    "keyFingerPrint": "CEF8DDC8019CA66D4E3094F3600233BA9E54DC61",
    "ircNick": "cworth",
    "labeledURI": "http://cworth.org",
    "gidNumber": "3021"
  },
  {
    "info": "# cwryu, users, debian.org",
    "dn": {
      "uid": "cwryu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Changwoo",
    "uid": "cwryu",
    "sn": "Ryu",
    "uidNumber": "1418",
    "gidNumber": "1418",
    "keyFingerPrint": "A27CBEBA35256865380E0CC9EC1879D076D8AB48",
    "ircNick": "changwoo"
  },
  {
    "info": "# cyb, users, debian.org",
    "dn": {
      "uid": "cyb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Cyril",
    "keyFingerPrint": "5E045B0EA831824D9E5F58716754891DEAAC62DF",
    "uid": "cyb",
    "sn": "Bouthors",
    "uidNumber": "2476",
    "ircNick": "cyrilb",
    "labeledURI": "http://cyril.bouthors.org/",
    "jabberJID": "CyrilB@jabber.org",
    "gidNumber": "2476"
  },
  {
    "info": "# czchen, users, debian.org",
    "dn": {
      "uid": "czchen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "czchen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3378",
    "cn": "ChangZhuo",
    "sn": "Chen",
    "gidNumber": "3378",
    "keyFingerPrint": "BA04346DC2E1FE63C7908793CC65B0CDEC275D5B",
    "ircNick": "czchen",
    "labeledURI": "https://czchen.org/"
  },
  {
    "info": "# daenzer, users, debian.org",
    "dn": {
      "uid": "daenzer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michel",
    "uid": "daenzer",
    "sn": "Daenzer",
    "uidNumber": "2195",
    "ircNick": "MrCooper",
    "jabberJID": "MrCooper@swissjabber.ch",
    "labeledURI": "http://people.debian.org/~daenzer/",
    "gidNumber": "2195"
  },
  {
    "info": "# daf, users, debian.org",
    "dn": {
      "uid": "daf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dafydd",
    "uid": "daf",
    "uidNumber": "2729",
    "sn": "Harries",
    "gidNumber": "2729",
    "ircNick": "daf",
    "jabberJID": "daf@rhydd.org"
  },
  {
    "info": "# dai, users, debian.org",
    "dn": {
      "uid": "dai",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dai",
    "objectClass": "debianDeveloper",
    "uidNumber": "3167",
    "keyFingerPrint": "0B29D88E42E6B765B8D8EA507839619DD439668E",
    "cn": "Daisuke",
    "sn": "Higuchi",
    "gidNumber": "3167"
  },
  {
    "info": "# daigo, users, debian.org",
    "dn": {
      "uid": "daigo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "daigo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2774",
    "cn": "Daigo",
    "sn": "Moriwaki",
    "gidNumber": "2774",
    "labeledURI": "http://www.sgtpepper.net/hyspro/diary/"
  },
  {
    "info": "# daissi, users, debian.org",
    "dn": {
      "uid": "daissi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "daissi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3538",
    "gidNumber": "3538",
    "keyFingerPrint": "9A3C07BD06DE2F416E813A5D612EF1613E050F54",
    "cn": "Dylan",
    "sn": ": QcOvc3Np"
  },
  {
    "info": "# dajobe, users, debian.org",
    "dn": {
      "uid": "dajobe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "dajobe",
    "sn": "Beckett",
    "uidNumber": "2707",
    "ircNick": "dajobe",
    "labeledURI": "http://www.dajobe.org/",
    "gidNumber": "2707"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "dale",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dale",
    "uid": "dale",
    "uidNumber": "817",
    "sn": "Miller",
    "gidNumber": "817"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "dalla",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michele",
    "uid": "dalla",
    "uidNumber": "1149",
    "sn": "Silvestra",
    "gidNumber": "1149"
  },
  {
    "info": "# damog, users, debian.org",
    "dn": {
      "uid": "damog",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "damog",
    "sn": "Garza",
    "uidNumber": "2738",
    "ircNick": "damog",
    "labeledURI": "http://damog.net",
    "gidNumber": "2738"
  },
  {
    "info": "# dan, users, debian.org",
    "dn": {
      "uid": "dan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dan",
    "uid": "dan",
    "sn": "Jacobowitz",
    "uidNumber": "1228",
    "ircNick": "drow",
    "gidNumber": "1609"
  },
  {
    "info": "# danai, users, debian.org",
    "dn": {
      "uid": "danai",
      "ou": "users",
      "dc": "org"
    },
    "uid": "danai",
    "objectClass": "debianDeveloper",
    "uidNumber": "2995",
    "cn": "Danai",
    "sn": "Sae-Han",
    "gidNumber": "2995",
    "keyFingerPrint": "1BBC875324B7181B94C616032E31880A62EB5434",
    "ircNick": "Danai"
  },
  {
    "info": "# dancer, users, debian.org",
    "dn": {
      "uid": "dancer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Junichi",
    "uid": "dancer",
    "sn": "Uekawa",
    "labeledURI": "http://www.netfort.gr.jp/~dancer",
    "uidNumber": "2208",
    "ircNick": "dancerj",
    "keyFingerPrint": "E8F377AEEE943676599BC50C07299F687458A89E",
    "gidNumber": "2208"
  },
  {
    "info": "# danchev, users, debian.org",
    "dn": {
      "uid": "danchev",
      "ou": "users",
      "dc": "org"
    },
    "uid": "danchev",
    "objectClass": "debianDeveloper",
    "uidNumber": "2920",
    "keyFingerPrint": "1AE77C660A265BFFDF225D551C570C890E4BD0AB",
    "cn": "George",
    "sn": "Danchev",
    "ircNick": "flightplan",
    "jabberJID": "danchev@jabber.org",
    "labeledURI": "http://people.fccf.net/danchev/",
    "gidNumber": "2920"
  },
  {
    "info": "# daniel, users, debian.org",
    "dn": {
      "uid": "daniel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "daniel",
    "objectClass": "debianDeveloper",
    "uidNumber": "2804",
    "cn": "Daniel",
    "sn": "Baumann",
    "keyFingerPrint": "8136ED25C7D67E92C74A429255CF1BF986ABB9C7",
    "gidNumber": "2804"
  },
  {
    "info": "20091208] Resigned",
    "dn": {
      "uid": "daniels",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "daniels",
    "uidNumber": "2636",
    "sn": "Stone",
    "ircNick": "daniels",
    "jabberJID": "daniel@fooishbar.org",
    "labeledURI": "http://www.fooishbar.org/",
    "gidNumber": "2636"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "danish",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "danish",
    "sn": "Danis",
    "uidNumber": "2324",
    "ircNick": "danish",
    "gidNumber": "2324"
  },
  {
    "info": "# dank, users, debian.org",
    "dn": {
      "uid": "dank",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dank",
    "objectClass": "debianDeveloper",
    "uidNumber": "3644",
    "gidNumber": "3644",
    "keyFingerPrint": "9A2FFF747994E287BEC422F15F43400C21CBFACC",
    "cn": "Nick",
    "sn": "Black",
    "ircNick": "sosodank",
    "labeledURI": "https://nick-black.com/dankwiki/index.php/Hack_on"
  },
  {
    "info": "# danlutz, users, debian.org",
    "dn": {
      "uid": "danlutz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "danlutz",
    "sn": "Lutz",
    "uidNumber": "2272",
    "ircNick": "masterlu",
    "gidNumber": "2272"
  },
  {
    "info": "# dannf, users, debian.org",
    "dn": {
      "uid": "dannf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dann",
    "uid": "dannf",
    "sn": "Frazier",
    "uidNumber": "2428",
    "keyFingerPrint": "09F47DBF2D32EEDC2443EBEE1BF83C5E54FC8640",
    "gidNumber": "2428",
    "ircNick": "dannf",
    "labeledURI": "http://bloggf.dannf.org/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "danno",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Clay",
    "uid": "danno",
    "uidNumber": "2041",
    "ircNick": "TheFiend | The_Fiend",
    "sn": "Crouch",
    "gidNumber": "2041"
  },
  {
    "info": "# danpat, users, debian.org",
    "dn": {
      "uid": "danpat",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "danpat",
    "sn": "Patterson",
    "labeledURI": "http://danpat.net/",
    "uidNumber": "1347",
    "ircNick": "danpat",
    "gidNumber": "1347"
  },
  {
    "info": "# danstowell, users, debian.org",
    "dn": {
      "uid": "danstowell",
      "ou": "users",
      "dc": "org"
    },
    "uid": "danstowell",
    "objectClass": "debianDeveloper",
    "uidNumber": "3278",
    "keyFingerPrint": "60155CC5D69233FB259DDCF4D1D4C255D6420D3E",
    "cn": "Dan",
    "sn": "Stowell",
    "gidNumber": "3278"
  },
  {
    "info": "# danw, users, debian.org",
    "dn": {
      "uid": "danw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "danw",
    "objectClass": "debianDeveloper",
    "uidNumber": "3271",
    "cn": "Dan",
    "sn": "Winship",
    "gidNumber": "3271"
  },
  {
    "info": "# dap, users, debian.org",
    "dn": {
      "uid": "dap",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dap",
    "uidNumber": "2685",
    "ircNick": "dap",
    "sn": "Parker",
    "gidNumber": "2685"
  },
  {
    "info": "# dapal, users, debian.org",
    "dn": {
      "uid": "dapal",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dapal",
    "objectClass": "debianDeveloper",
    "uidNumber": "3037",
    "cn": "David",
    "sn": "Paleino",
    "keyFingerPrint": "81D96BD19A3AEE396FDBD30C0359959479467018",
    "gidNumber": "3037",
    "ircNick": {},
    "labeledURI": "http://www.hanskalabs.net"
  },
  {
    "info": "# darek, users, debian.org",
    "dn": {
      "uid": "darek",
      "ou": "users",
      "dc": "org"
    },
    "uid": "darek",
    "objectClass": "debianDeveloper",
    "uidNumber": "3401",
    "gidNumber": "3401",
    "keyFingerPrint": "C795C09263AB4009421A241D211A73C69A7B0FC7",
    "cn": "Dariusz",
    "sn": "Dwornikowski",
    "ircNick": "tdi"
  },
  {
    "info": "# dark, users, debian.org",
    "dn": {
      "uid": "dark",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "dark",
    "uidNumber": "1057",
    "ircNick": "dark",
    "sn": "Braakman",
    "gidNumber": "1057"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "darke",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "darke",
    "sn": "Crystal",
    "labeledURI": "http://cyberpunks.org/darke/homepage.phtml",
    "uidNumber": "1384",
    "ircNick": "darkewolf",
    "gidNumber": "1384"
  },
  {
    "info": "# darkxst, users, debian.org",
    "dn": {
      "uid": "darkxst",
      "ou": "users",
      "dc": "org"
    },
    "uid": "darkxst",
    "objectClass": "debianDeveloper",
    "uidNumber": "3518",
    "gidNumber": "3518",
    "keyFingerPrint": "0E0880479A6F1063372395275B39C0A1153ACABA",
    "cn": "Tim",
    "sn": "Lunn"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "data",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luis",
    "uid": "data",
    "uidNumber": "2230",
    "ircNick": "larocha",
    "sn": "Arocha",
    "gidNumber": "2230"
  },
  {
    "info": "# dato, users, debian.org",
    "dn": {
      "uid": "dato",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dato",
    "objectClass": "debianDeveloper",
    "uidNumber": "3268",
    "cn": "Dato",
    "gidNumber": "3268",
    "sn": ": U2ltw7M=",
    "ircNick": "dato"
  },
  {
    "info": "# dave, users, debian.org",
    "dn": {
      "uid": "dave",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "dave",
    "sn": "Holland",
    "uidNumber": "927",
    "labeledURI": "http://www.biff.org.uk/dave/",
    "keyFingerPrint": "D4A9BBA214CF41C95B89517502D0204508FB0EF7",
    "gidNumber": "927"
  },
  {
    "info": "# dave.anglin, users, debian.org",
    "dn": {
      "uid": "dave.anglin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dave.anglin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3374",
    "keyFingerPrint": "9D1CE5FBA7BDF834EB121C845DBFCDAE5F194DF1",
    "cn": "John",
    "sn": "Anglin",
    "gidNumber": "3374"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "daveb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.daves.net/debian",
    "cn": "David",
    "uid": "daveb",
    "uidNumber": "833",
    "ircNick": "dpb",
    "sn": "Boswell",
    "gidNumber": "833"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "davecook",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "davecook",
    "uidNumber": "1040",
    "ircNick": "Cevad",
    "sn": "Cook",
    "gidNumber": "1040"
  },
  {
    "info": "# david, users, debian.org",
    "dn": {
      "uid": "david",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "david",
    "uidNumber": "834",
    "sn": "Engel",
    "gidNumber": "834"
  },
  {
    "info": "# davidc, users, debian.org",
    "dn": {
      "uid": "davidc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "davidc",
    "sn": "Coe",
    "uidNumber": "2021",
    "ircNick": "dc0e",
    "keyFingerPrint": "1B36433A095BA85C22C473FDE434E57F7DA4AEF2",
    "gidNumber": "2021"
  },
  {
    "info": "# davidw, users, debian.org",
    "dn": {
      "uid": "davidw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "davidw",
    "uidNumber": "1047",
    "sn": "Welton",
    "ircNick": "davidw",
    "labeledURI": "http://www.dedasys.com/davidw/",
    "gidNumber": "1047"
  },
  {
    "info": "# dbharris, users, debian.org",
    "dn": {
      "uid": "dbharris",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dbharris",
    "sn": "Harris",
    "uidNumber": "2664",
    "ircNick": "ElectricElf",
    "gidNumber": "2664"
  },
  {
    "info": "# dbugger, users, debian.org",
    "dn": {
      "uid": "dbugger",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dennis",
    "keyFingerPrint": "F3A12880BCF7F8047C39B29E292A35920034C733",
    "uid": "dbugger",
    "sn": "Clark",
    "uidNumber": "1168",
    "ircNick": "DeeBugger",
    "jabberJID": "boomfish@gmail.com",
    "gidNumber": "1168"
  },
  {
    "info": "# dburrows, users, debian.org",
    "dn": {
      "uid": "dburrows",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://people.debian.org/~dburrows",
    "cn": "Daniel",
    "uid": "dburrows",
    "uidNumber": "2081",
    "ircNick": "dburrows",
    "sn": "Burrows",
    "gidNumber": "2081"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "dcole",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "dcole",
    "uidNumber": "1414",
    "sn": "Cole",
    "gidNumber": "1414"
  },
  {
    "info": "# debacle, users, debian.org",
    "dn": {
      "uid": "debacle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wolfgang",
    "uid": "debacle",
    "uidNumber": "1252",
    "sn": "Borgert",
    "keyFingerPrint": "7ED1DEA345D9A0A118D4A740E3E0A1C286B963EA",
    "gidNumber": "1696",
    "jabberJID": "debacle@debian.org",
    "labeledURI": "http://people.debian.org/~debacle/"
  },
  {
    "info": "# debalance, users, debian.org",
    "dn": {
      "uid": "debalance",
      "ou": "users",
      "dc": "org"
    },
    "uid": "debalance",
    "objectClass": "debianDeveloper",
    "uidNumber": "2986",
    "cn": "Philipp",
    "sn": "Huebner",
    "keyFingerPrint": "671925C5B8CDE74A52253DF9E5CA8C4925E4205F",
    "gidNumber": "2986",
    "ircNick": "debalance",
    "jabberJID": "philipp@debalance.de",
    "labeledURI": "http://www.debalance.de"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "decker",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sascha",
    "uid": "decker",
    "uidNumber": "1142",
    "sn": "Runschke",
    "gidNumber": "1142"
  },
  {
    "info": "# decklin, users, debian.org",
    "dn": {
      "uid": "decklin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Decklin",
    "uid": "decklin",
    "sn": "Foster",
    "uidNumber": "2047",
    "ircNick": "decklin",
    "jabberJID": "decklin@gmail.com",
    "labeledURI": "http://www.red-bean.com/decklin/",
    "gidNumber": "2047"
  },
  {
    "info": "# deek, users, debian.org",
    "dn": {
      "uid": "deek",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeff",
    "uid": "deek",
    "sn": "Teunissen",
    "uidNumber": "2277",
    "gidNumber": "2277",
    "ircNick": "Deek",
    "labeledURI": "http://www.d2dc.net/~deek/"
  },
  {
    "info": "# deepak, users, debian.org",
    "dn": {
      "uid": "deepak",
      "ou": "users",
      "dc": "org"
    },
    "uid": "deepak",
    "objectClass": "debianDeveloper",
    "uidNumber": "3097",
    "keyFingerPrint": "4BEBFC6A62FC0CB40E10F2CEC4D1681A7F217B01",
    "cn": "Deepak",
    "sn": "Tripathi",
    "gidNumber": "3097",
    "ircNick": "deepak",
    "labeledURI": "http://www.gnumonk.com"
  },
  {
    "info": "# deiv, users, debian.org",
    "dn": {
      "uid": "deiv",
      "ou": "users",
      "dc": "org"
    },
    "uid": "deiv",
    "objectClass": "debianDeveloper",
    "uidNumber": "3619",
    "gidNumber": "3619",
    "keyFingerPrint": "C450196033B24682EFE44C9D7CB6E83D8605423A",
    "cn": "David",
    "sn": ": U3XDoXJleg==",
    "ircNick": "deiv"
  },
  {
    "info": "# delaunay, users, debian.org",
    "dn": {
      "uid": "delaunay",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "delaunay",
    "uidNumber": "999",
    "sn": "Delaunay",
    "keyFingerPrint": "0A0FBFF6BCE060FC8A3B9A87CDB227FC0E691E17",
    "gidNumber": "999"
  },
  {
    "info": "# deller, users, debian.org",
    "dn": {
      "uid": "deller",
      "ou": "users",
      "dc": "org"
    },
    "uid": "deller",
    "objectClass": "debianDeveloper",
    "uidNumber": "3372",
    "cn": "Helge",
    "sn": "Deller",
    "ircNick": "deller",
    "gidNumber": "3372",
    "keyFingerPrint": "454482282CD910DBEF3D25F83E5F3D04A7A24603"
  },
  {
    "info": "# deltaone, users, debian.org",
    "dn": {
      "uid": "deltaone",
      "ou": "users",
      "dc": "org"
    },
    "uid": "deltaone",
    "objectClass": "debianDeveloper",
    "uidNumber": "3629",
    "gidNumber": "3629",
    "keyFingerPrint": "6287415C347AF1CC591EEDCA9E9F7A603077FE56",
    "cn": "Patrick",
    "sn": "Franz",
    "ircNick": "Delta-One"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "demonishi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://demonishi.org",
    "cn": "Genise",
    "uid": "demonishi",
    "uidNumber": "2306",
    "ircNick": "Demonishi",
    "sn": "Pearce",
    "gidNumber": "2306"
  },
  {
    "info": "# denis, users, debian.org",
    "dn": {
      "uid": "denis",
      "ou": "users",
      "dc": "org"
    },
    "uid": "denis",
    "objectClass": "debianDeveloper",
    "uidNumber": "3569",
    "gidNumber": "3569",
    "keyFingerPrint": "8F8438094EE7A364A4259CDA10589E8283DDA9A7",
    "cn": "Denis",
    "sn": "Briand"
  },
  {
    "info": "# dennis, users, debian.org",
    "dn": {
      "uid": "dennis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dennis",
    "uid": "dennis",
    "uidNumber": "2066",
    "sn": "Schoen",
    "ircNick": "roccoblues",
    "gidNumber": "2066"
  },
  {
    "info": "# dererk, users, debian.org",
    "dn": {
      "uid": "dererk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dererk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3078",
    "keyFingerPrint": "A6C7B88B9583046A11C5403E0B00FB6CEBE2D002",
    "cn": "Ulises",
    "sn": "Vitulli",
    "gidNumber": "3078",
    "ircNick": "dererk",
    "jabberJID": "dererk@im.org.ar"
  },
  {
    "info": "# des, users, debian.org",
    "dn": {
      "uid": "des",
      "ou": "users",
      "dc": "org"
    },
    "uid": "des",
    "objectClass": "debianDeveloper",
    "uidNumber": "2851",
    "sn": "Viano",
    "ircNick": "des",
    "jabberJID": "des@lug.fi.uba.ar",
    "labeledURI": "http://damianv.com.ar/",
    "gidNumber": "2851",
    "cn": ": RGFtacOhbg=="
  },
  {
    "info": "# desrt, users, debian.org",
    "dn": {
      "uid": "desrt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "desrt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3276",
    "keyFingerPrint": "438B192072ADC20958D65C999F7A2232AFAA6FF6",
    "cn": "Ryan",
    "sn": "Lortie",
    "gidNumber": "3276"
  },
  {
    "info": "# devin, users, debian.org",
    "dn": {
      "uid": "devin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Devin",
    "uid": "devin",
    "uidNumber": "2557",
    "sn": "Carraway",
    "ircNick": "Requiem",
    "labeledURI": "http://www.devin.com",
    "gidNumber": "2557"
  },
  {
    "info": "# dexter, users, debian.org",
    "dn": {
      "uid": "dexter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Piotr",
    "uid": "dexter",
    "sn": "Roszatycki",
    "uidNumber": "1313",
    "gidNumber": "1313",
    "keyFingerPrint": "12CCFE48BAD829BBD6BFFE3D566217F3C4395C9C",
    "ircNick": "dex4er",
    "jabberJID": "piotr.roszatycki@gmail.com"
  },
  {
    "info": "# dfrey, users, debian.org",
    "dn": {
      "uid": "dfrey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dfrey",
    "uidNumber": "812",
    "sn": "Frey",
    "gidNumber": "812"
  },
  {
    "info": "# dgoulet, users, debian.org",
    "dn": {
      "uid": "dgoulet",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dgoulet",
    "objectClass": "debianDeveloper",
    "uidNumber": "3298",
    "keyFingerPrint": "B74417EDDF22AC9F9E90F49142E86A2A11F48D36",
    "cn": "David",
    "sn": "Goulet",
    "gidNumber": "3298"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "dgregor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "D.J.",
    "uid": "dgregor",
    "uidNumber": "836",
    "sn": "Gregor",
    "gidNumber": "836"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "dhd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.pobox.com/~dhd/",
    "cn": "David",
    "uid": "dhd",
    "uidNumber": "1238",
    "ircNick": "dhd",
    "sn": "Huggins-Daines",
    "gidNumber": "1608"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "dhs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dhs",
    "uidNumber": "837",
    "sn": "Silber",
    "gidNumber": "837"
  },
  {
    "info": "# diane, users, debian.org",
    "dn": {
      "uid": "diane",
      "ou": "users",
      "dc": "org"
    },
    "uid": "diane",
    "objectClass": "debianDeveloper",
    "uidNumber": "3358",
    "keyFingerPrint": "1AFC09386700D1A8F05C65ADE5AB5F161CDD0D98",
    "cn": "Diane",
    "sn": "Trout",
    "gidNumber": "3358",
    "ircNick": "detrout"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "dichro",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.rcpt.to/",
    "cn": "Mikolaj",
    "uid": "dichro",
    "uidNumber": "1302",
    "sn": "Habryn",
    "gidNumber": "1302"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "dido",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paolo",
    "uid": "dido",
    "labeledURI": "http://superdido.com/paolo/",
    "uidNumber": "1358",
    "ircNick": "dido",
    "gidNumber": "1358",
    "sn": ": RGlkb27DqA=="
  },
  {
    "info": "# dilinger, users, debian.org",
    "dn": {
      "uid": "dilinger",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andres",
    "uid": "dilinger",
    "uidNumber": "2679",
    "sn": "Salomon",
    "ircNick": "dilinger",
    "keyFingerPrint": "500524F97D58893223B35F6A645D0247C36E7637",
    "gidNumber": "2679"
  },
  {
    "info": "# dima, users, debian.org",
    "dn": {
      "uid": "dima",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dima",
    "uid": "dima",
    "sn": "Barsky",
    "uidNumber": "1257",
    "ircNick": "dima",
    "gidNumber": "1627"
  },
  {
    "info": "# diocles, users, debian.org",
    "dn": {
      "uid": "diocles",
      "ou": "users",
      "dc": "org"
    },
    "uid": "diocles",
    "objectClass": "debianDeveloper",
    "uidNumber": "3049",
    "cn": "Tim",
    "sn": "Retout",
    "keyFingerPrint": "058051A76AEE285679C2260C7B7B40AD0F81D93B",
    "gidNumber": "3049",
    "ircNick": "diocles",
    "labeledURI": "https://retout.co.uk/"
  },
  {
    "info": "# directhex, users, debian.org",
    "dn": {
      "uid": "directhex",
      "ou": "users",
      "dc": "org"
    },
    "uid": "directhex",
    "objectClass": "debianDeveloper",
    "uidNumber": "3027",
    "keyFingerPrint": "C10B0C427181A34B1BC5FEDDC90F9CB90E1FAD0C",
    "cn": "Jo",
    "sn": "Shields",
    "ircNick": "directhex",
    "jabberJID": "directhex@jabber.earth.li",
    "labeledURI": "http://apebox.org",
    "gidNumber": "3027"
  },
  {
    "info": "# dirson, users, debian.org",
    "dn": {
      "uid": "dirson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yann",
    "uid": "dirson",
    "sn": "Dirson",
    "uidNumber": "1061",
    "ircNick": "yann",
    "labeledURI": "http://ydirson.free.fr/",
    "keyFingerPrint": "74AF05DDD92027F5F0C3CDD50D85F29625A3F9FD",
    "gidNumber": "1061"
  },
  {
    "info": "# djpig, users, debian.org",
    "dn": {
      "uid": "djpig",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frank",
    "keyFingerPrint": "EFFDC2C90B84664F14B3096E41B9F4E85B713DF0",
    "uid": "djpig",
    "sn": "Lichtenheld",
    "uidNumber": "2642",
    "gidNumber": "2642",
    "ircNick": "djpig"
  },
  {
    "info": "# dkg, users, debian.org",
    "dn": {
      "uid": "dkg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dkg",
    "objectClass": "debianDeveloper",
    "uidNumber": "2980",
    "cn": "Daniel",
    "sn": "Gillmor",
    "ircNick": "dkg",
    "jabberJID": "dkg@jabber.org",
    "gidNumber": "2980",
    "keyFingerPrint": "C29F8A0C01F35E34D816AA5CE092EB3A5CA10DBA"
  },
  {
    "info": "# dkogan, users, debian.org",
    "dn": {
      "uid": "dkogan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dkogan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3287",
    "cn": "Dima",
    "sn": "Kogan",
    "gidNumber": "3287",
    "keyFingerPrint": "751C135DC2CE0143380854F3ED63B6125A1D1561",
    "ircNick": "dima5",
    "labeledURI": "http://notes.secretsauce.net"
  },
  {
    "info": "# dktrkranz, users, debian.org",
    "dn": {
      "uid": "dktrkranz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dktrkranz",
    "objectClass": "debianDeveloper",
    "uidNumber": "2998",
    "cn": "Luca",
    "sn": "Falavigna",
    "keyFingerPrint": "DDC53E5130FD08223F98945649086AD3EBE2F31F",
    "gidNumber": "2998",
    "ircNick": "DktrKranz",
    "labeledURI": "http://dktrkranz.wordpress.com/"
  },
  {
    "info": "# dlange, users, debian.org",
    "dn": {
      "uid": "dlange",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dlange",
    "objectClass": "debianDeveloper",
    "uidNumber": "3452",
    "gidNumber": "3452",
    "keyFingerPrint": "35750B8FB6EF95FF16B8EBC0664F1238AA8F138A",
    "cn": "Daniel",
    "sn": "Lange",
    "ircNick": "DLange",
    "labeledURI": "https://daniel-lange.com"
  },
  {
    "info": "# dld, users, debian.org",
    "dn": {
      "uid": "dld",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Drake",
    "keyFingerPrint": "4AC29C6152C82353307E4BDEC39FFC6F55A410A0",
    "uid": "dld",
    "sn": "Diedrich",
    "uidNumber": "1068",
    "ircNick": "dld",
    "gidNumber": "1068"
  },
  {
    "info": "# dlehn, users, debian.org",
    "dn": {
      "uid": "dlehn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dlehn",
    "uidNumber": "2582",
    "sn": "Lehn",
    "keyFingerPrint": "86D92F9286F1276BA6AF8370DE85C199300CBB5C",
    "gidNumber": "2582",
    "ircNick": "taaz",
    "labeledURI": "http://dil.lehn.org/"
  },
  {
    "info": "# dleidert, users, debian.org",
    "dn": {
      "uid": "dleidert",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dleidert",
    "objectClass": "debianDeveloper",
    "uidNumber": "3205",
    "cn": "Daniel",
    "sn": "Leidert",
    "keyFingerPrint": "BEED4DED5544A4C03E283DC74BCD0567C296D05D",
    "gidNumber": "3205",
    "labeledURI": "https://www.wgdd.org"
  },
  {
    "info": "# dmartin, users, debian.org",
    "dn": {
      "uid": "dmartin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dale",
    "uid": "dmartin",
    "sn": "Martin",
    "uidNumber": "1118",
    "keyFingerPrint": "17EF07E6F59E9F3E16E0C0A259EC43D65246508B",
    "gidNumber": "1118",
    "jabberJID": "dmartin@jabber.org",
    "labeledURI": "http://www.the-martins.org/~dmartin"
  },
  {
    "info": "# dmaze, users, debian.org",
    "dn": {
      "uid": "dmaze",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dmaze",
    "sn": "Maze",
    "labeledURI": "http://www.mit.edu/~dmaze/",
    "uidNumber": "2234",
    "gidNumber": "2234"
  },
  {
    "info": "LuNzyalybBsIi03PlAypTd0bYlgrLp+S0wIDAQAB",
    "dn": {
      "uid": "dmn",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dmn",
    "objectClass": "debianDeveloper",
    "uidNumber": "2862",
    "cn": "Damyan",
    "sn": "Ivanov",
    "keyFingerPrint": "AEA0C44ECB056E93630D9D33DBBE9D4D99D2A004",
    "gidNumber": "2862",
    "ircNick": "dam",
    "jabberJID": "dam@ktnx.net"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "dmz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "dmz",
    "sn": "Zendzian",
    "labeledURI": "http://www.dmzs.com/~dmz/",
    "uidNumber": "1361",
    "ircNick": "dmz",
    "gidNumber": "1361"
  },
  {
    "info": "20091208] Resigned in <20020502004953.A20988@auric.debian.org>",
    "dn": {
      "uid": "dnn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dan",
    "uid": "dnn",
    "uidNumber": "1373",
    "sn": "Nguyen",
    "gidNumber": "1373"
  },
  {
    "info": "# dnusinow, users, debian.org",
    "dn": {
      "uid": "dnusinow",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "keyFingerPrint": "F2E1D6829607241FE54C12A8C8B7E935D63469DF",
    "uid": "dnusinow",
    "uidNumber": "2668",
    "sn": "Nusinow",
    "ircNick": "gravity",
    "labeledURI": "http://gravitypulls.net",
    "gidNumber": "2668"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "dobbo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.steve.dobson.org/",
    "cn": "Steve",
    "uid": "dobbo",
    "uidNumber": "2142",
    "sn": "Dobson",
    "gidNumber": "2142"
  },
  {
    "info": "# dod, users, debian.org",
    "dn": {
      "uid": "dod",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dod",
    "objectClass": "debianDeveloper",
    "uidNumber": "3143",
    "keyFingerPrint": "9F7239FCB664F10B33E9DC03C31F4FD949AB2B6C",
    "cn": "Dominique",
    "sn": "Dumont",
    "gidNumber": "3143",
    "ircNick": "dod",
    "labeledURI": "http://ddumont.wordpress.com/"
  },
  {
    "info": "# dogsleg, users, debian.org",
    "dn": {
      "uid": "dogsleg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dogsleg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3394",
    "gidNumber": "3394",
    "keyFingerPrint": "DE6BA671D57D9B009CF686505EE76EE20216D2A5",
    "cn": "Lev",
    "sn": "Lamberov",
    "ircNick": "dogsleg",
    "labeledURI": "https://www.pimentola.ru"
  },
  {
    "info": "# doko, users, debian.org",
    "dn": {
      "uid": "doko",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthias",
    "uid": "doko",
    "uidNumber": "1174",
    "sn": "Klose",
    "keyFingerPrint": "D56571B88A8BBAF140BF63D6BD7EAA60778FA6F5",
    "gidNumber": "1174",
    "ircNick": "doko"
  },
  {
    "info": "# dom, users, debian.org",
    "dn": {
      "uid": "dom",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dom",
    "objectClass": "debianDeveloper",
    "uidNumber": "2799",
    "cn": "Dominic",
    "sn": "Hargreaves",
    "keyFingerPrint": "CB496527F9009F2B1C1A76DAC0057E714D694FB2",
    "ircNick": "Dom",
    "jabberJID": "dom@jabber.earth.li",
    "labeledURI": "http://www.larted.org.uk/~dom/",
    "gidNumber": "2799"
  },
  {
    "info": "# domibel, users, debian.org",
    "dn": {
      "uid": "domibel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "domibel",
    "objectClass": "debianDeveloper",
    "uidNumber": "3076",
    "cn": "Dominique",
    "sn": "Belhachemi",
    "keyFingerPrint": "EA482D50992E936CEBDA5FD219B5E6C862191001",
    "gidNumber": "3076"
  },
  {
    "info": "# don, users, debian.org",
    "dn": {
      "uid": "don",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Don",
    "uid": "don",
    "sn": "Armstrong",
    "uidNumber": "2682",
    "keyFingerPrint": "2AD865BA26040F67F9CC3660A2D13001D98C0FBA",
    "gidNumber": "2682",
    "ircNick": "dondelelcaro",
    "jabberJID": "dondelelcaro@jabber.org",
    "labeledURI": "http://www.donarmstrong.com"
  },
  {
    "info": "# donald, users, debian.org",
    "dn": {
      "uid": "donald",
      "ou": "users",
      "dc": "org"
    },
    "uid": "donald",
    "objectClass": "debianDeveloper",
    "uidNumber": "3382",
    "keyFingerPrint": "B7A15F455B287F384174D5E9E5EC4AC9BD627B05",
    "cn": "Donald",
    "sn": "Norwood",
    "gidNumber": "3382",
    "ircNick": "cnote",
    "jabberJID": "donald@jabberes.org"
  },
  {
    "info": "# donkult, users, debian.org",
    "dn": {
      "uid": "donkult",
      "ou": "users",
      "dc": "org"
    },
    "uid": "donkult",
    "objectClass": "debianDeveloper",
    "uidNumber": "3415",
    "gidNumber": "3415",
    "keyFingerPrint": "1E09B66DED7B5875B552B142E197012676B9B739",
    "cn": "David",
    "sn": "Kalnischkies",
    "ircNick": "DonKult",
    "labeledURI": "https://david.kalnischkies.de"
  },
  {
    "info": "# doogie, users, debian.org",
    "dn": {
      "uid": "doogie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "doogie",
    "sn": "Heath",
    "uidNumber": "1128",
    "ircNick": "doogie",
    "gidNumber": "1670"
  },
  {
    "info": "# dopey, users, debian.org",
    "dn": {
      "uid": "dopey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "dopey",
    "sn": "Hope",
    "uidNumber": "2436",
    "ircNick": "dopey",
    "gidNumber": "2436"
  },
  {
    "info": "# dottedmag, users, debian.org",
    "dn": {
      "uid": "dottedmag",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dottedmag",
    "objectClass": "debianDeveloper",
    "uidNumber": "3073",
    "cn": "Mikhail",
    "sn": "Gusarov",
    "keyFingerPrint": "A8DF13269E5D9A38E57CFAC29D20F6503E338888",
    "gidNumber": "3073",
    "ircNick": "dottedmag",
    "jabberJID": "dottedmag@dottedmag.net",
    "labeledURI": "https://dottedmag.net/"
  },
  {
    "info": "XfHSxBrlmG40pGqbI+Tiojb1k+pBXh+kJIwIDAQAB",
    "dn": {
      "uid": "dparsons",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Drew",
    "uid": "dparsons",
    "uidNumber": "2097",
    "sn": "Parsons",
    "keyFingerPrint": "23C9A93E585819E9126D0A36573EF1E4BD5A01FA",
    "gidNumber": "2097",
    "ircNick": "Rizzer"
  },
  {
    "info": "# dpat, users, debian.org",
    "dn": {
      "uid": "dpat",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dpat",
    "objectClass": "debianDeveloper",
    "uidNumber": "3562",
    "gidNumber": "3562",
    "keyFingerPrint": "8DF2271871E57DA5714C8EDE9BB6983DDD81AFEB",
    "cn": "Darshaka",
    "sn": "Pathirana"
  },
  {
    "info": "# dpk, users, debian.org",
    "dn": {
      "uid": "dpk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dennis",
    "uid": "dpk",
    "uidNumber": "1417",
    "sn": "Kelly",
    "gidNumber": "1417"
  },
  {
    "info": "# drazzib, users, debian.org",
    "dn": {
      "uid": "drazzib",
      "ou": "users",
      "dc": "org"
    },
    "uid": "drazzib",
    "objectClass": "debianDeveloper",
    "uidNumber": "3026",
    "cn": "Damien",
    "sn": "Raude-Morvan",
    "gidNumber": "3026",
    "ircNick": "drazzib",
    "labeledURI": "http://www.drazzib.com/"
  },
  {
    "info": "20091208] retired 2006-01-07 <20060107231316.GA3437@tooloud.org>",
    "dn": {
      "uid": "drb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "drb",
    "sn": "Bungert",
    "uidNumber": "2392",
    "gidNumber": "2392"
  },
  {
    "info": "# dreamind, users, debian.org",
    "dn": {
      "uid": "dreamind",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "dreamind",
    "sn": "Pfetzing",
    "uidNumber": "2465",
    "ircNick": "dreamind",
    "jabberJID": "dreamind@jabber.ap-wdsl.de",
    "labeledURI": "http://www.dreamind.de/",
    "gidNumber": "2465"
  },
  {
    "info": "# dres, users, debian.org",
    "dn": {
      "uid": "dres",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "uid": "dres",
    "sn": "LewisMoss",
    "labeledURI": "http://www.lewismoss.org/~dres",
    "uidNumber": "1017",
    "ircNick": "dres",
    "gidNumber": "1017"
  },
  {
    "info": "# drew, users, debian.org",
    "dn": {
      "uid": "drew",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://me.woot.net",
    "cn": "Joe",
    "uid": "drew",
    "uidNumber": "2226",
    "ircNick": "HoserHead",
    "sn": "Drew",
    "gidNumber": "2226"
  },
  {
    "info": "# ds, users, debian.org",
    "dn": {
      "uid": "ds",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "ds",
    "uidNumber": "2107",
    "sn": "Schleef",
    "ircNick": "ds",
    "gidNumber": "2107"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "dsb3",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "dsb3",
    "sn": "Baker",
    "labeledURI": "http://dsb3.com/",
    "uidNumber": "2138",
    "ircNick": "dsb3",
    "gidNumber": "2138"
  },
  {
    "info": "# dsilvers, users, debian.org",
    "dn": {
      "uid": "dsilvers",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "keyFingerPrint": "19568523759E2A2858F4606B3CCEBABE206C3B69",
    "uid": "dsilvers",
    "sn": "Silverstone",
    "uidNumber": "2378",
    "ircNick": "Kinnison",
    "labeledURI": "http://www.digital-scurf.org/",
    "gidNumber": "2378"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "dsp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Doug",
    "uid": "dsp",
    "sn": "Porter",
    "labeledURI": "http://people.debian.org/~dsp/",
    "uidNumber": "2213",
    "ircNick": "dsp",
    "gidNumber": "2213"
  },
  {
    "info": "# dsw, users, debian.org",
    "dn": {
      "uid": "dsw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "dsw",
    "uidNumber": "1312",
    "ircNick": "Elfez",
    "sn": "Swegen",
    "gidNumber": "1312"
  },
  {
    "info": "# dswarbrick, users, debian.org",
    "dn": {
      "uid": "dswarbrick",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dswarbrick",
    "objectClass": "debianDeveloper",
    "uidNumber": "3631",
    "gidNumber": "3631",
    "keyFingerPrint": "303F687A4EFCB1AE7C1A35A3B700173FBB805A29",
    "cn": "Daniel",
    "sn": "Swarbrick",
    "ircNick": "dswarbrick"
  },
  {
    "info": "# dtorrance, users, debian.org",
    "dn": {
      "uid": "dtorrance",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dtorrance",
    "objectClass": "debianDeveloper",
    "uidNumber": "3362",
    "keyFingerPrint": "803CE41F4DC252ECB5E5F1B9D12B2BE26D3FF663",
    "cn": "Douglas",
    "sn": "Torrance",
    "gidNumber": "3362"
  },
  {
    "info": "# duck, users, debian.org",
    "dn": {
      "uid": "duck",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marc",
    "uid": "duck",
    "uidNumber": "2731",
    "keyFingerPrint": "72972A83E526453DF2885F8155E9F9F7AC1C443F",
    "gidNumber": "2731",
    "sn": ": RGVxdcOobmVz",
    "ircNick": "Duck",
    "jabberJID": "duck@milkypond.org",
    "labeledURI": "https://www.duckcorp.org/"
  },
  {
    "info": "# duncf, users, debian.org",
    "dn": {
      "uid": "duncf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Duncan",
    "uid": "duncf",
    "sn": "Findlay",
    "uidNumber": "2507",
    "ircNick": "duncf",
    "gidNumber": "2507"
  },
  {
    "info": "20091208] Retired 2006-01-14",
    "dn": {
      "uid": "dunham",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steven",
    "uid": "dunham",
    "sn": "Dunham",
    "labeledURI": "http://www.cse.msu.edu/~dunham/",
    "uidNumber": "928",
    "ircNick": "dunham",
    "gidNumber": "928"
  },
  {
    "info": "# dusmil, users, debian.org",
    "dn": {
      "uid": "dusmil",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dusmil",
    "objectClass": "debianDeveloper",
    "uidNumber": "3200",
    "cn": "Dusan",
    "sn": "Milosavljevic",
    "gidNumber": "3200"
  },
  {
    "info": "# dvdeug, users, debian.org",
    "dn": {
      "uid": "dvdeug",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "jabberJID": "prosfilaes@jabber.org",
    "uid": "dvdeug",
    "uidNumber": "2304",
    "ircNick": "dvdeug",
    "sn": "Starner",
    "gidNumber": "2304"
  },
  {
    "info": "# dwarf, users, debian.org",
    "dn": {
      "uid": "dwarf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dale",
    "uid": "dwarf",
    "uidNumber": "838",
    "sn": "Scheetz",
    "gidNumber": "838"
  },
  {
    "info": "# dwatson, users, debian.org",
    "dn": {
      "uid": "dwatson",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dwatson",
    "objectClass": "debianDeveloper",
    "uidNumber": "2828",
    "keyFingerPrint": "5FDBB1BE9B31D3A8298172850DE8799EA7D6AE79",
    "cn": "David",
    "sn": "Watson",
    "ircNick": "dwatson",
    "jabberJID": "dwatson@planetwatson.co.uk",
    "labeledURI": "http://planetwatson.co.uk/blog",
    "gidNumber": "2828"
  },
  {
    "info": "# dwhedon, users, debian.org",
    "dn": {
      "uid": "dwhedon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.kimdon.org/david",
    "cn": "David",
    "uid": "dwhedon",
    "uidNumber": "2220",
    "ircNick": "dwhedon",
    "sn": "Kimdon",
    "gidNumber": "2220"
  },
  {
    "info": "# dxld, users, debian.org",
    "dn": {
      "uid": "dxld",
      "ou": "users",
      "dc": "org"
    },
    "uid": "dxld",
    "objectClass": "debianDeveloper",
    "uidNumber": "3645",
    "gidNumber": "3645",
    "keyFingerPrint": "57A1BF15B4F6F99B89EDB29FD39481AE1E79ACF7",
    "cn": "Daniel",
    "sn": ": R3LDtmJlcg=="
  },
  {
    "info": "# dz, users, debian.org",
    "dn": {
      "uid": "dz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Massimo",
    "uid": "dz",
    "sn": "Dal Zotto",
    "uidNumber": "2164",
    "ircNick": "dizzy",
    "labeledURI": "http://people.debian.org/~dz/",
    "gidNumber": "2164"
  },
  {
    "info": "# dzu, users, debian.org",
    "dn": {
      "uid": "dzu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Detlev",
    "uid": "dzu",
    "sn": "Zundel",
    "labeledURI": "http://www.akk.org/~dzu",
    "uidNumber": "2055",
    "ircNick": "lambda",
    "gidNumber": "2055"
  },
  {
    "info": "# ean, users, debian.org",
    "dn": {
      "uid": "ean",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ean",
    "keyFingerPrint": "1C977AFDCD9B00F2BC509A088C09994F6E48A047",
    "uid": "ean",
    "sn": "Schuessler",
    "uidNumber": "1064",
    "gidNumber": "1064",
    "ircNick": "schue",
    "labeledURI": "http://www.brainfood.com/~ean"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "eberlein",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dominik",
    "uid": "eberlein",
    "uidNumber": "966",
    "sn": "Eberlein",
    "gidNumber": "966"
  },
  {
    "info": "# ebourg, users, debian.org",
    "dn": {
      "uid": "ebourg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ebourg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3325",
    "keyFingerPrint": "B8CE4DE21080DCF903E16C40F513C419E4B9D0AC",
    "cn": "Emmanuel",
    "sn": "Bourg",
    "gidNumber": "3325",
    "ircNick": "ebourg",
    "labeledURI": "https://twitter.com/smanux"
  },
  {
    "info": "# ebroder, users, debian.org",
    "dn": {
      "uid": "ebroder",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ebroder",
    "objectClass": "debianDeveloper",
    "uidNumber": "3647",
    "gidNumber": "3647",
    "keyFingerPrint": "9E1DBF474BBA314B5E0B645345D77E9E30CB1B11",
    "cn": "Evan",
    "sn": "Broder"
  },
  {
    "info": "# ecain, users, debian.org",
    "dn": {
      "uid": "ecain",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "ecain",
    "uidNumber": "2200",
    "ircNick": "ec",
    "sn": "Cain",
    "gidNumber": "2200"
  },
  {
    "info": "# ecki, users, debian.org",
    "dn": {
      "uid": "ecki",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bernd",
    "uid": "ecki",
    "sn": "Eckenfels",
    "uidNumber": "919",
    "ircNick": "eckes",
    "labeledURI": "http://www.eckes.org",
    "gidNumber": "919"
  },
  {
    "info": "# ecsv, users, debian.org",
    "dn": {
      "uid": "ecsv",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ecsv",
    "objectClass": "debianDeveloper",
    "uidNumber": "3583",
    "gidNumber": "3583",
    "keyFingerPrint": "522D7163831C73A635D12FE5EC371482956781AF",
    "cn": "Sven",
    "sn": "Eckelmann"
  },
  {
    "info": "# ed, users, debian.org",
    "dn": {
      "uid": "ed",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ed",
    "keyFingerPrint": "E959DBA0092A7AD23F41500E7649D1C5B4A4E070",
    "uid": "ed",
    "sn": "Boraas",
    "uidNumber": "1297",
    "gidNumber": "1297",
    "ircNick": "Kronos-X"
  },
  {
    "info": "# edd, users, debian.org",
    "dn": {
      "uid": "edd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dirk",
    "uid": "edd",
    "sn": "Eddelbuettel",
    "uidNumber": "840",
    "keyFingerPrint": "AE89DB0EE10E60C01100A8F2A1489FE2AB99A21A",
    "gidNumber": "840",
    "ircNick": "eddel",
    "labeledURI": "https://dirk.eddelbuettel.com"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "eddie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sam",
    "jabberJID": "sam@teknohaus.dyndns.org",
    "uid": "eddie",
    "sn": "Couter",
    "uidNumber": "2168",
    "ircNick": "EddieSam",
    "gidNumber": "2168"
  },
  {
    "info": "# edelhard, users, debian.org",
    "dn": {
      "uid": "edelhard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.edelhard.de/",
    "cn": "Edelhard",
    "uid": "edelhard",
    "uidNumber": "2592",
    "sn": "Becker",
    "gidNumber": "2592"
  },
  {
    "info": "# ediaz, users, debian.org",
    "dn": {
      "uid": "ediaz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eduardo",
    "uid": "ediaz",
    "sn": "Comellas",
    "labeledURI": "http://www.com.uvigo.es/~ediaz/",
    "uidNumber": "1114",
    "ircNick": "miedu",
    "gidNumber": "1114"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "edlang",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Edward",
    "uid": "edlang",
    "sn": "Lang",
    "labeledURI": "http://people.debian.org/~edlang/",
    "uidNumber": "1374",
    "ircNick": "woot",
    "gidNumber": "1374"
  },
  {
    "info": "# edmonds, users, debian.org",
    "dn": {
      "uid": "edmonds",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "edmonds",
    "uidNumber": "1141",
    "sn": "Edmonds",
    "keyFingerPrint": "DF3D96EEB3827820F302665C01817AB0AAF6CDAE",
    "ircNick": "edmonds",
    "gidNumber": "1141"
  },
  {
    "info": "# edward, users, debian.org",
    "dn": {
      "uid": "edward",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Edward",
    "uid": "edward",
    "sn": "Betts",
    "uidNumber": "1281",
    "gidNumber": "1660",
    "keyFingerPrint": "FB8ACFA78C726089C38AD0269605A1098C63B92A",
    "ircNick": "edward",
    "labeledURI": "https://edwardbetts.com/"
  },
  {
    "info": "# eevans, users, debian.org",
    "dn": {
      "uid": "eevans",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "eevans",
    "sn": "Evans",
    "uidNumber": "2733",
    "keyFingerPrint": "C4965EE9E3015D192CCCF2B6F758CE318D77295D",
    "ircNick": "urandom",
    "labeledURI": "http://blog.sym-link.com",
    "gidNumber": "2733"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "efraim",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "efraim",
    "uidNumber": "1256",
    "ircNick": "Efraim",
    "sn": "Koch",
    "gidNumber": "1674"
  },
  {
    "info": "Ro4hhmlP8ms1EIEIQGa4lmljhwUOj5DtwIDAQAB",
    "dn": {
      "uid": "ehashman",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ehashman",
    "objectClass": "debianDeveloper",
    "uidNumber": "3494",
    "gidNumber": "3494",
    "keyFingerPrint": "7D0B1775E2626DD899D20BEDDECA0C9D30ED9FE3",
    "cn": "Elana",
    "sn": "Hashman",
    "ircNick": "ehashman",
    "labeledURI": "https://hashman.ca"
  },
  {
    "info": "# eichin, users, debian.org",
    "dn": {
      "uid": "eichin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.thok.org/",
    "cn": "Mark",
    "uid": "eichin",
    "uidNumber": "841",
    "sn": "Eichin",
    "keyFingerPrint": "345EAE92AEFDE9FBDE0395C317E92B42206F8A04",
    "gidNumber": "841"
  },
  {
    "info": "# eighthave, users, debian.org",
    "dn": {
      "uid": "eighthave",
      "ou": "users",
      "dc": "org"
    },
    "uid": "eighthave",
    "objectClass": "debianDeveloper",
    "uidNumber": "3207",
    "cn": "Hans-Christoph",
    "sn": "Steiner",
    "gidNumber": "3207",
    "keyFingerPrint": "EE6620C7136B0D2C456C0A4DE9E28DEA00AA5556",
    "ircNick": "_hc",
    "jabberJID": "hans@guardianproject.info",
    "labeledURI": "http://at.or.at/hans/"
  },
  {
    "info": "# eike, users, debian.org",
    "dn": {
      "uid": "eike",
      "ou": "users",
      "dc": "org"
    },
    "uid": "eike",
    "objectClass": "debianDeveloper",
    "uidNumber": "2857",
    "cn": "Eike",
    "sn": "Sauer",
    "keyFingerPrint": "9D0ED0480C310B06B88F0EA298042723E7133742",
    "gidNumber": "2857",
    "ircNick": "eike"
  },
  {
    "info": "# ejad, users, debian.org",
    "dn": {
      "uid": "ejad",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://heddley.com/edd/",
    "cn": "Edd",
    "uid": "ejad",
    "uidNumber": "2520",
    "ircNick": "edd",
    "sn": "Dumbill",
    "gidNumber": "2520"
  },
  {
    "info": "# ejb, users, debian.org",
    "dn": {
      "uid": "ejb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Edward",
    "uid": "ejb",
    "sn": "Brocklesby",
    "uidNumber": "1330",
    "ircNick": "is",
    "gidNumber": "1330"
  },
  {
    "info": "# elbrus, users, debian.org",
    "dn": {
      "uid": "elbrus",
      "ou": "users",
      "dc": "org"
    },
    "uid": "elbrus",
    "objectClass": "debianDeveloper",
    "uidNumber": "3172",
    "cn": "Paul",
    "sn": "Gevers",
    "keyFingerPrint": "58B66D48736BE93B052DE6729C5C99EB05BD750A",
    "gidNumber": "3172",
    "ircNick": "Elbrus"
  },
  {
    "info": "# elf, users, debian.org",
    "dn": {
      "uid": "elf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marc",
    "uid": "elf",
    "uidNumber": "1245",
    "sn": "Singer",
    "keyFingerPrint": "A4CA03F82F1A32BD9E1741CC1ACC11FE7D39213C",
    "gidNumber": "1612"
  },
  {
    "info": "# ellert, users, debian.org",
    "dn": {
      "uid": "ellert",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ellert",
    "objectClass": "debianDeveloper",
    "uidNumber": "3156",
    "cn": "Mattias",
    "sn": "Ellert",
    "keyFingerPrint": "EA1830AFDF4D431AD9E11452E8AEC2FF3BE1A94B",
    "gidNumber": "3156"
  },
  {
    "info": "# elonen, users, debian.org",
    "dn": {
      "uid": "elonen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jarno",
    "uid": "elonen",
    "sn": "Elonen",
    "labeledURI": "http://iki.fi/elonen/",
    "uidNumber": "2641",
    "ircNick": "elonen",
    "gidNumber": "2641"
  },
  {
    "info": "# eloy, users, debian.org",
    "dn": {
      "uid": "eloy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Krzysztof",
    "uid": "eloy",
    "uidNumber": "2368",
    "sn": "Krzyzaniak",
    "keyFingerPrint": "F7D0679A87AD501B48237DB1F45E7D53CF0E01FE",
    "gidNumber": "2368",
    "ircNick": "eloy",
    "labeledURI": "http://www.kofeina.net/eloy/"
  },
  {
    "info": "# elphick, users, debian.org",
    "dn": {
      "uid": "elphick",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Oliver",
    "uid": "elphick",
    "sn": "Elphick",
    "uidNumber": "1069",
    "labeledURI": "http://www.lfix.co.uk/oliver/",
    "gidNumber": "1069"
  },
  {
    "info": "# ema, users, debian.org",
    "dn": {
      "uid": "ema",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Emanuele",
    "uid": "ema",
    "sn": "Rocca",
    "uidNumber": "2638",
    "keyFingerPrint": "9545969619281C17FFAC5891D5085A0750FDB7A3",
    "gidNumber": "2638",
    "ircNick": "ema",
    "jabberJID": "ema@jabber.linux.it",
    "labeledURI": "https://www.linux.it/~ema/"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "emanuele",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Emanuele",
    "uid": "emanuele",
    "sn": "Pucciarelli",
    "uidNumber": "915",
    "ircNick": "pino",
    "gidNumber": "915"
  },
  {
    "info": "# emfox, users, debian.org",
    "dn": {
      "uid": "emfox",
      "ou": "users",
      "dc": "org"
    },
    "uid": "emfox",
    "objectClass": "debianDeveloper",
    "uidNumber": "2835",
    "cn": "Emfox",
    "sn": "Zhou",
    "keyFingerPrint": "654E8C20440667B12D20BE8E8D83379110000DEB",
    "gidNumber": "2835",
    "ircNick": "emfox",
    "jabberJID": "emfoxzhou@gmail.com"
  },
  {
    "info": "# emolitor, users, debian.org",
    "dn": {
      "uid": "emolitor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.brainfood.com",
    "cn": "Eric",
    "uid": "emolitor",
    "uidNumber": "2014",
    "ircNick": "molitor",
    "sn": "Molitor",
    "gidNumber": "2014"
  },
  {
    "info": "# emollier, users, debian.org",
    "dn": {
      "uid": "emollier",
      "ou": "users",
      "dc": "org"
    },
    "uid": "emollier",
    "objectClass": "debianDeveloper",
    "uidNumber": "3632",
    "gidNumber": "3632",
    "keyFingerPrint": "8F91B227C7D6F2B1948C8236793CF67E8F0D11DA",
    "cn": ": w4l0aWVubmU=",
    "sn": "Mollier",
    "ircNick": "emollier"
  },
  {
    "info": "# emorrp1, users, debian.org",
    "dn": {
      "uid": "emorrp1",
      "ou": "users",
      "dc": "org"
    },
    "uid": "emorrp1",
    "objectClass": "debianDeveloper",
    "uidNumber": "3655",
    "gidNumber": "3655",
    "keyFingerPrint": "2C429F5DD6B7F023BD300993453F8D999F113609",
    "cn": "Phil",
    "sn": "Morrell"
  },
  {
    "info": "# emschwar, users, debian.org",
    "dn": {
      "uid": "emschwar",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.pobox.com/~emschwar",
    "cn": "Eric",
    "uid": "emschwar",
    "uidNumber": "2030",
    "ircNick": "skif",
    "sn": "Schwartz",
    "gidNumber": "2030"
  },
  {
    "info": "# ender, users, debian.org",
    "dn": {
      "uid": "ender",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "ender",
    "uidNumber": "2414",
    "ircNick": "Jo-Con-El",
    "gidNumber": "2414",
    "sn": ": TWFydMOtbmV6"
  },
  {
    "info": "# enrico, users, debian.org",
    "dn": {
      "uid": "enrico",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Enrico",
    "uid": "enrico",
    "sn": "Zini",
    "uidNumber": "2415",
    "keyFingerPrint": "1793D6AB75663E6BF104953A634F4BD1E7AD5568",
    "gidNumber": "2415",
    "ircNick": "enrico",
    "jabberJID": "enrico@enricozini.org",
    "labeledURI": "http://www.enricozini.org"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "epetron",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ed",
    "uid": "epetron",
    "uidNumber": "929",
    "sn": "Petron",
    "gidNumber": "929"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "epg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "jabberJID": "pretzelgod@jabber.org",
    "uid": "epg",
    "uidNumber": "2240",
    "ircNick": "pretzelgod",
    "sn": "Gillespie",
    "gidNumber": "2240"
  },
  {
    "info": "# eppesuig, users, debian.org",
    "dn": {
      "uid": "eppesuig",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Giuseppe",
    "uid": "eppesuig",
    "sn": "Sacco",
    "uidNumber": "2561",
    "keyFingerPrint": "51539D3F62E8522ED0C7C070C2AEDB1447EDD321",
    "gidNumber": "2561",
    "ircNick": "eppesuig",
    "labeledURI": "https://blog.sguazz.it/"
  },
  {
    "info": "# epsilon, users, debian.org",
    "dn": {
      "uid": "epsilon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "epsilon",
    "objectClass": "debianDeveloper",
    "uidNumber": "3546",
    "gidNumber": "3546",
    "keyFingerPrint": "D0D085B169C3BFD9404858FA21FC29504B5230DB",
    "cn": "Daniel",
    "ircNick": "eps1l0n",
    "labeledURI": "https://r2d2.icu/",
    "sn": "Echeverri"
  },
  {
    "info": "# era, users, debian.org",
    "dn": {
      "uid": "era",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Enrique",
    "uid": "era",
    "uidNumber": "2149",
    "sn": "Arnuncio",
    "ircNick": "tulkas",
    "gidNumber": "2149"
  },
  {
    "info": "# eriberto, users, debian.org",
    "dn": {
      "uid": "eriberto",
      "ou": "users",
      "dc": "org"
    },
    "uid": "eriberto",
    "objectClass": "debianDeveloper",
    "uidNumber": "3322",
    "keyFingerPrint": "357DCB0EEC95A01AEBA1F0D2DE63B9C704EBE9EF",
    "cn": "Joao Eriberto",
    "sn": "Mota Filho",
    "gidNumber": "3322",
    "ircNick": "eriberto",
    "jabberJID": "eriberto@jabber-br.org",
    "labeledURI": "https://people.debian.org/~eriberto"
  },
  {
    "info": "# eric, users, debian.org",
    "dn": {
      "uid": "eric",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "eric",
    "sn": "Dorland",
    "uidNumber": "2271",
    "keyFingerPrint": "43CF1228F726FD5B474CE962C256FBD500221E93",
    "gidNumber": "2271",
    "ircNick": "hooty",
    "labeledURI": "http://www.kuroneko.ca/"
  },
  {
    "info": "# erich, users, debian.org",
    "dn": {
      "uid": "erich",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Erich",
    "uid": "erich",
    "sn": "Schubert",
    "uidNumber": "2443",
    "ircNick": "erich",
    "labeledURI": "http://www.vitavonni.de/",
    "keyFingerPrint": "BE8321D41C42D0AC513D001B86464F59104378F1",
    "gidNumber": "2443"
  },
  {
    "info": "# ericvb, users, debian.org",
    "dn": {
      "uid": "ericvb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "ericvb",
    "sn": "Van Buggenhaut",
    "uidNumber": "2303",
    "ircNick": "eBug",
    "jabberJID": "eBug@njs.netlab.cz",
    "labeledURI": "http://www.debian.org/~ericvb",
    "gidNumber": "2303"
  },
  {
    "info": "# erijo, users, debian.org",
    "dn": {
      "uid": "erijo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "erijo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3099",
    "keyFingerPrint": "A4038E3B4C5B083FDCF8597BFA3460727E28522C",
    "cn": "Erik",
    "sn": "Johansson",
    "gidNumber": "3099"
  },
  {
    "info": "# erik, users, debian.org",
    "dn": {
      "uid": "erik",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Erik",
    "uid": "erik",
    "uidNumber": "2477",
    "sn": "Wenzel",
    "keyFingerPrint": "47A83979F90C0B9B1E3BB45C26EC67B4FE6CBC8F",
    "gidNumber": "2477",
    "ircNick": "pl0pix",
    "jabberJID": "plopix@jabber.ccc.de/home",
    "labeledURI": "https://gpl.code.de/"
  },
  {
    "info": "# erikd, users, debian.org",
    "dn": {
      "uid": "erikd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "erikd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3018",
    "keyFingerPrint": "73571E85C19F4281D8C97AA86CA41A7743B8D6C8",
    "cn": "Erik",
    "sn": "de Castro Lopo",
    "gidNumber": "3018"
  },
  {
    "info": "# eriks, users, debian.org",
    "dn": {
      "uid": "eriks",
      "ou": "users",
      "dc": "org"
    },
    "uid": "eriks",
    "objectClass": "debianDeveloper",
    "uidNumber": "2785",
    "cn": "Erik",
    "sn": "Schanze",
    "keyFingerPrint": "BA5B1C3CED82638DC3DF0DEC726746E0F0BC8365",
    "gidNumber": "2785"
  },
  {
    "info": "# erinn, users, debian.org",
    "dn": {
      "uid": "erinn",
      "ou": "users",
      "dc": "org"
    },
    "uid": "erinn",
    "objectClass": "debianDeveloper",
    "uidNumber": "2807",
    "keyFingerPrint": "8738A680B84B3031A630F2DB416F061063FEE659",
    "sn": "Clark",
    "cn": "Erinn",
    "ircNick": "helix",
    "gidNumber": "2807"
  },
  {
    "info": "# eriol, users, debian.org",
    "dn": {
      "uid": "eriol",
      "ou": "users",
      "dc": "org"
    },
    "uid": "eriol",
    "objectClass": "debianDeveloper",
    "uidNumber": "3554",
    "gidNumber": "3554",
    "keyFingerPrint": "C65AEF9FE5BF8CCBD6EDB0198BAF522C0D6CCEDD",
    "cn": "Daniele",
    "sn": "Tricoli",
    "ircNick": "eriol",
    "labeledURI": "https://mornie.org"
  },
  {
    "info": "# error, users, debian.org",
    "dn": {
      "uid": "error",
      "ou": "users",
      "dc": "org"
    },
    "uid": "error",
    "objectClass": "debianDeveloper",
    "uidNumber": "3267",
    "cn": "Jacob",
    "sn": "Appelbaum",
    "gidNumber": "3267"
  },
  {
    "info": "# espy, users, debian.org",
    "dn": {
      "uid": "espy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joel",
    "uid": "espy",
    "sn": "Klecker",
    "labeledURI": "http://web.espy.org/",
    "uidNumber": "1117",
    "ircNick": "Espy",
    "gidNumber": "1117"
  },
  {
    "info": "# etbe, users, debian.org",
    "dn": {
      "uid": "etbe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Russell",
    "uid": "etbe",
    "uidNumber": "2159",
    "sn": "Coker",
    "keyFingerPrint": "9F7D619DCC06F57C02AA66C7D141CD30FC4B8F79",
    "gidNumber": "2159"
  },
  {
    "info": "# etobi, users, debian.org",
    "dn": {
      "uid": "etobi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "etobi",
    "objectClass": "debianDeveloper",
    "uidNumber": "2938",
    "cn": "Tobias",
    "sn": "Grimm",
    "keyFingerPrint": "57A4935F9221888FDE3714A2D62E4F5B041390BC",
    "gidNumber": "2938",
    "labeledURI": "http://e-tobi.net"
  },
  {
    "info": "# eugen, users, debian.org",
    "dn": {
      "uid": "eugen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eugeniy",
    "uid": "eugen",
    "sn": "Meshcheryakov",
    "uidNumber": "2732",
    "keyFingerPrint": "AC9D68316918E4C026567767C16DA8E7F451B93C",
    "ircNick": "eugen",
    "jabberJID": "eugeniy.meshcheryakov@googlemail.com",
    "gidNumber": "2732"
  },
  {
    "info": "# eugene, users, debian.org",
    "dn": {
      "uid": "eugene",
      "ou": "users",
      "dc": "org"
    },
    "uid": "eugene",
    "objectClass": "debianDeveloper",
    "uidNumber": "3376",
    "cn": "Eugene",
    "sn": "Zhukov",
    "gidNumber": "3376"
  },
  {
    "info": "# evan, users, debian.org",
    "dn": {
      "uid": "evan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Evan",
    "uid": "evan",
    "sn": "Prodromou",
    "uidNumber": "2254",
    "ircNick": "evanpro",
    "labeledURI": "http://evan.prodromou.name/",
    "gidNumber": "2254"
  },
  {
    "info": "# evgeni, users, debian.org",
    "dn": {
      "uid": "evgeni",
      "ou": "users",
      "dc": "org"
    },
    "uid": "evgeni",
    "objectClass": "debianDeveloper",
    "uidNumber": "2975",
    "cn": "Evgeni",
    "sn": "Golov",
    "keyFingerPrint": "C575A957E819BA18BF07E766A1B09B42333961E8",
    "gidNumber": "2975",
    "ircNick": "Zhenech",
    "jabberJID": "evgeni@golov.de",
    "labeledURI": "https://www.die-welt.net"
  },
  {
    "info": "# evo, users, debian.org",
    "dn": {
      "uid": "evo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Davide",
    "uid": "evo",
    "sn": "Puricelli",
    "uidNumber": "2051",
    "ircNick": "evo",
    "labeledURI": "http://www.puricelli.info",
    "keyFingerPrint": "DA6AB28551DAB530AAD5D91BE665078E8E6AF13E",
    "gidNumber": "2051"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ewigin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kevin",
    "uid": "ewigin",
    "uidNumber": "1189",
    "sn": "Poorman",
    "gidNumber": "1663"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "ewt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Erik",
    "uid": "ewt",
    "uidNumber": "890",
    "sn": "Troan",
    "gidNumber": "890"
  },
  {
    "info": "# ezanard, users, debian.org",
    "dn": {
      "uid": "ezanard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Enrique",
    "uid": "ezanard",
    "sn": "Zanardi",
    "uidNumber": "937",
    "ircNick": "zemby",
    "gidNumber": "937"
  },
  {
    "info": "# fabbe, users, debian.org",
    "dn": {
      "uid": "fabbe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fabian",
    "uid": "fabbe",
    "uidNumber": "2720",
    "sn": "Fagerholm",
    "keyFingerPrint": "87546B51422EF780FA9C4A7A542B94E55719A67D",
    "gidNumber": "2720",
    "ircNick": "fabbe",
    "labeledURI": "http://people.paniq.net/~fabbe/"
  },
  {
    "info": "# fabbione, users, debian.org",
    "dn": {
      "uid": "fabbione",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fabio",
    "uid": "fabbione",
    "sn": "Di Nitto",
    "uidNumber": "2580",
    "ircNick": "fabbione",
    "jabberJID": "fabbione@jabber.org",
    "labeledURI": "http://www.fabbione.net/",
    "gidNumber": "2580"
  },
  {
    "info": "# fabian, users, debian.org",
    "dn": {
      "uid": "fabian",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fabian",
    "objectClass": "debianDeveloper",
    "uidNumber": "3352",
    "keyFingerPrint": "22C17648A9526B84DF191C96CBEA8E970CCD59DF",
    "cn": "Fabian",
    "sn": "Greffrath",
    "gidNumber": "3352"
  },
  {
    "info": "# fabien, users, debian.org",
    "dn": {
      "uid": "fabien",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fabien",
    "uid": "fabien",
    "sn": "Ninoles",
    "labeledURI": "http://www.tzone.org/~fabien/",
    "uidNumber": "2002",
    "ircNick": "veneur or corbeau",
    "gidNumber": "2002"
  },
  {
    "info": "# fabo, users, debian.org",
    "dn": {
      "uid": "fabo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fabo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2888",
    "cn": "Fathi",
    "sn": "Boudra",
    "ircNick": "fabo",
    "labeledURI": "http://fboudra.free.fr",
    "gidNumber": "2888",
    "keyFingerPrint": "AE05E0F23E9FFC78A6702A58FF3439A94818A98C"
  },
  {
    "info": "# falk, users, debian.org",
    "dn": {
      "uid": "falk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Falk",
    "uid": "falk",
    "sn": "Hueffner",
    "uidNumber": "1247",
    "ircNick": "mellum",
    "gidNumber": "1623"
  },
  {
    "info": "# faw, users, debian.org",
    "dn": {
      "uid": "faw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "faw",
    "objectClass": "debianDeveloper",
    "uidNumber": "2838",
    "cn": "Felipe",
    "sn": "van de Wiel",
    "keyFingerPrint": "36CCC10F61849F24F04E0CB1C6B8598485522E2D",
    "gidNumber": "2838",
    "ircNick": "faw",
    "jabberJID": "felipewiel@jabber.org",
    "labeledURI": "http://people.debian.org/~faw/"
  },
  {
    "info": "# federico, users, debian.org",
    "dn": {
      "uid": "federico",
      "ou": "users",
      "dc": "org"
    },
    "uid": "federico",
    "objectClass": "debianDeveloper",
    "uidNumber": "3344",
    "keyFingerPrint": "7CA7DDFB333921408C6F2B966F31BC44F5177DAA",
    "cn": "Federico",
    "sn": "Ceratto",
    "ircNick": "federico3",
    "labeledURI": "https://wiki.debian.org/FedericoCeratto",
    "gidNumber": "3344"
  },
  {
    "info": "# felix, users, debian.org",
    "dn": {
      "uid": "felix",
      "ou": "users",
      "dc": "org"
    },
    "uid": "felix",
    "objectClass": "debianDeveloper",
    "uidNumber": "3633",
    "gidNumber": "3633",
    "keyFingerPrint": "2E6B7C0E128B8F9B16DAA76A5857883E277DB3CC",
    "cn": ": RsOpbGl4",
    "sn": "Sipma",
    "ircNick": "gueux"
  },
  {
    "info": "# fenio, users, debian.org",
    "dn": {
      "uid": "fenio",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bartosz",
    "uid": "fenio",
    "sn": "Fenski",
    "uidNumber": "2677",
    "ircNick": "fEnIo",
    "jabberJID": "fenio@jabber.org",
    "labeledURI": "http://fenski.pl",
    "keyFingerPrint": "2BEC79D6FB49FB227872901BB0D9D4A83CD3BBC1",
    "gidNumber": "2677"
  },
  {
    "info": "# fenton, users, debian.org",
    "dn": {
      "uid": "fenton",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joel",
    "uid": "fenton",
    "sn": "Aelwyn",
    "uidNumber": "2552",
    "ircNick": "Fentonator",
    "gidNumber": "2552"
  },
  {
    "info": "# fer, users, debian.org",
    "dn": {
      "uid": "fer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.datalay.com",
    "cn": "Fernando",
    "uid": "fer",
    "uidNumber": "1343",
    "ircNick": "abso",
    "sn": "Sanchez",
    "gidNumber": "1343"
  },
  {
    "info": "# fgeyer, users, debian.org",
    "dn": {
      "uid": "fgeyer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fgeyer",
    "objectClass": "debianDeveloper",
    "uidNumber": "3213",
    "keyFingerPrint": "164C70512F7929476764AB56FE22C6FD83135D45",
    "cn": "Felix",
    "sn": "Geyer",
    "gidNumber": "3213",
    "ircNick": "debfx"
  },
  {
    "info": "# fh, users, debian.org",
    "dn": {
      "uid": "fh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Florian",
    "keyFingerPrint": "F9AB00C13E3A8125DD3FDF1CDF79A374B4071A65",
    "uid": "fh",
    "sn": "Hinzmann",
    "uidNumber": "1234",
    "gidNumber": "1600",
    "ircNick": "butch",
    "jabberJID": "butch@jabber.ccc.de"
  },
  {
    "info": "# filipe, users, debian.org",
    "dn": {
      "uid": "filipe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "filipe",
    "objectClass": "debianDeveloper",
    "uidNumber": "2913",
    "cn": "Filipe",
    "sn": "Lautert",
    "ircNick": "filipe",
    "labeledURI": "http://filipe.icewall.org",
    "gidNumber": "2913"
  },
  {
    "info": "# filippo, users, debian.org",
    "dn": {
      "uid": "filippo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Filippo",
    "uid": "filippo",
    "sn": "Giunchedi",
    "uidNumber": "2718",
    "keyFingerPrint": "BAD91EFF7DB4C1837B6D705A99D49B6B00CAD1E5",
    "gidNumber": "2718",
    "ircNick": "godog",
    "jabberJID": "godog@esaurito.net",
    "labeledURI": "https://esaurito.net"
  },
  {
    "info": "# fizbin, users, debian.org",
    "dn": {
      "uid": "fizbin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "fizbin",
    "sn": "Martin",
    "labeledURI": "http://www.snowplow.org/martin/",
    "uidNumber": "1187",
    "ircNick": "fizbin",
    "gidNumber": "1662"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "fjordan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frank",
    "uid": "fjordan",
    "sn": "Jordan",
    "labeledURI": "http://www.f-jordan.de",
    "uidNumber": "1183",
    "gidNumber": "1694"
  },
  {
    "info": "# fjp, users, debian.org",
    "dn": {
      "uid": "fjp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frans",
    "uid": "fjp",
    "uidNumber": "2734",
    "sn": "Pop",
    "ircNick": "fjp",
    "gidNumber": "2734"
  },
  {
    "info": "# fladerer, users, debian.org",
    "dn": {
      "uid": "fladerer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fladerer",
    "objectClass": "debianDeveloper",
    "uidNumber": "3316",
    "keyFingerPrint": "A56FFE735FCC4FF12E72360EACE61874EE61F443",
    "cn": "Michael",
    "sn": "Fladerer",
    "gidNumber": "3316"
  },
  {
    "info": "# fladi, users, debian.org",
    "dn": {
      "uid": "fladi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fladi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3350",
    "keyFingerPrint": "D8812F4065320B8DCA3CEF18694CADEF51C7B5B6",
    "cn": "Michael",
    "sn": "Fladischer",
    "gidNumber": "3350",
    "ircNick": "fladi",
    "jabberJID": "FladischerMichael@fladi.at",
    "labeledURI": "https://www.fladi.at"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "flashz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "flashz",
    "sn": "Cars",
    "uidNumber": "1300",
    "ircNick": "flashz",
    "gidNumber": "1300"
  },
  {
    "info": "# flatmax, users, debian.org",
    "dn": {
      "uid": "flatmax",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "flatmax",
    "uidNumber": "2632",
    "sn": "Flax",
    "labeledURI": "http://www.flatmax.org",
    "gidNumber": "2632"
  },
  {
    "info": "# flight, users, debian.org",
    "dn": {
      "uid": "flight",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gregor",
    "uid": "flight",
    "sn": "Hoffleit",
    "uidNumber": "1084",
    "gidNumber": "1084"
  },
  {
    "info": "# flimsy, users, debian.org",
    "dn": {
      "uid": "flimsy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathan",
    "uid": "flimsy",
    "uidNumber": "2175",
    "ircNick": "Flimzy",
    "sn": "Hall",
    "gidNumber": "2175"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "flinny",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nicholas",
    "uid": "flinny",
    "sn": "Flintham",
    "uidNumber": "1055",
    "ircNick": "Flinny",
    "gidNumber": "1055"
  },
  {
    "info": "# flo, users, debian.org",
    "dn": {
      "uid": "flo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Florian",
    "uid": "flo",
    "sn": "Lohoff",
    "uidNumber": "2020",
    "keyFingerPrint": "75BF68EE879B5F6A5AA50FCA90DD4120CB09F22F",
    "gidNumber": "2020",
    "ircNick": "fl0l0",
    "jabberJID": "f@zz.de"
  },
  {
    "info": "# florian, users, debian.org",
    "dn": {
      "uid": "florian",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Florian",
    "uid": "florian",
    "sn": "Ernst",
    "uidNumber": "2702",
    "keyFingerPrint": "067D375ED270572A65276EB1063741BAF5DD1ECE",
    "gidNumber": "2702",
    "ircNick": "fernst"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "fmenard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.ims-experts.com",
    "cn": "Francois",
    "uid": "fmenard",
    "uidNumber": "2333",
    "sn": "Menard",
    "gidNumber": "2333"
  },
  {
    "info": "# fmw, users, debian.org",
    "dn": {
      "uid": "fmw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Florian",
    "uid": "fmw",
    "sn": "Weps",
    "uidNumber": "2547",
    "ircNick": "fmw",
    "labeledURI": "http://people.debian.org/~fmw",
    "gidNumber": "2547"
  },
  {
    "info": "# fnord, users, debian.org",
    "dn": {
      "uid": "fnord",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Elie",
    "uid": "fnord",
    "sn": "Rosenblum",
    "labeledURI": "http://www.cosanostra.net/",
    "uidNumber": "1121",
    "ircNick": "fnord",
    "gidNumber": "1121"
  },
  {
    "info": "# fog, users, debian.org",
    "dn": {
      "uid": "fog",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Federico",
    "keyFingerPrint": "697807F613F7E0A359D1FF95BDC0A0AE06468DEB",
    "uid": "fog",
    "sn": "Di Gregorio",
    "uidNumber": "1052",
    "ircNick": "fog",
    "jabberJID": "fog@jabber.linux.it",
    "labeledURI": "http://people.initd.org/fog",
    "gidNumber": "1052"
  },
  {
    "info": "# foka, users, debian.org",
    "dn": {
      "uid": "foka",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anthony",
    "uid": "foka",
    "sn": "Fok",
    "uidNumber": "1056",
    "keyFingerPrint": "142421B19AD4A95996F95072EA2500B412C59ACF",
    "gidNumber": "1056",
    "ircNick": "foka",
    "jabberJID": "anthony.t.fok@gmail.com",
    "labeledURI": "https://anthonyfok.org/"
  },
  {
    "info": "# foleyj, users, debian.org",
    "dn": {
      "uid": "foleyj",
      "ou": "users",
      "dc": "org"
    },
    "uid": "foleyj",
    "objectClass": "debianDeveloper",
    "uidNumber": "3260",
    "keyFingerPrint": "84351E776812D95A7DA6BF61C2EDD3628852CCD5",
    "cn": "John",
    "sn": "Foley",
    "gidNumber": "3260"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "foof",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://synthcode.com/",
    "cn": "Alexander",
    "uid": "foof",
    "uidNumber": "1403",
    "ircNick": "foof",
    "sn": "Shinn",
    "gidNumber": "1403"
  },
  {
    "info": "20091208] Retired [JT 2007-02-19]",
    "dn": {
      "uid": "forcer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jorgen",
    "uid": "forcer",
    "sn": "Schaefer",
    "labeledURI": "http://www.forcix.cx/",
    "uidNumber": "2086",
    "ircNick": "forcer",
    "gidNumber": "2086"
  },
  {
    "info": "# formorer, users, debian.org",
    "dn": {
      "uid": "formorer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "formorer",
    "sn": "Wirt",
    "uidNumber": "2639",
    "keyFingerPrint": "6E3966C1E1D15DB973D05B491E45F8CA9DE23B16",
    "gidNumber": "2639",
    "ircNick": "formorer",
    "jabberJID": "formorer@formorer.de",
    "labeledURI": "http://www.formorer.de"
  },
  {
    "info": "# fourdollars, users, debian.org",
    "dn": {
      "uid": "fourdollars",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fourdollars",
    "objectClass": "debianDeveloper",
    "uidNumber": "3475",
    "gidNumber": "3475",
    "keyFingerPrint": "A8B65F1F8C9728B6D235D8D3E9EC46F5A547F31E",
    "cn": "Shih-Yuan",
    "sn": "Lee",
    "ircNick": "FourDollars",
    "labeledURI": "https://sylee.org/"
  },
  {
    "info": "# fourmond, users, debian.org",
    "dn": {
      "uid": "fourmond",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fourmond",
    "objectClass": "debianDeveloper",
    "uidNumber": "2854",
    "cn": "Vincent",
    "sn": "Fourmond",
    "labeledURI": "http://vince-debian.blogspot.com/",
    "gidNumber": "2854"
  },
  {
    "info": "1ywD8tDwIDAQAB",
    "dn": {
      "uid": "fpeters",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frederic",
    "uid": "fpeters",
    "sn": "Peters",
    "uidNumber": "1212",
    "keyFingerPrint": "7149147DF2F46AE03D556E3B2AE901E5C70218D2",
    "ircNick": "fredp",
    "labeledURI": "http://www.0d.be",
    "gidNumber": "1698"
  },
  {
    "info": "# fpolacco, users, debian.org",
    "dn": {
      "uid": "fpolacco",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fabrizio",
    "uid": "fpolacco",
    "uidNumber": "939",
    "ircNick": "fab",
    "sn": "Polacco",
    "gidNumber": "939"
  },
  {
    "info": "# franam, users, debian.org",
    "dn": {
      "uid": "franam",
      "ou": "users",
      "dc": "org"
    },
    "uid": "franam",
    "objectClass": "debianDeveloper",
    "uidNumber": "3168",
    "cn": "Francesco",
    "sn": "Namuri",
    "keyFingerPrint": "320DD86E1E256275B28F89D729CEADC9260F0067",
    "gidNumber": "3168",
    "ircNick": "hierax",
    "jabberJID": "franam@jabber.org",
    "labeledURI": "http://namuri.it/"
  },
  {
    "info": "# francisco, users, debian.org",
    "dn": {
      "uid": "francisco",
      "ou": "users",
      "dc": "org"
    },
    "uid": "francisco",
    "objectClass": "debianDeveloper",
    "uidNumber": "2981",
    "cn": "Francisco",
    "sn": "Claramonte",
    "keyFingerPrint": "B555A43FADF8D3C652C543837FF0C7423219C4E7",
    "gidNumber": "2981",
    "jabberJID": "fgclaramonte@jabberes.org",
    "labeledURI": "http://people.debian.org/~francisco/"
  },
  {
    "info": "# franck, users, debian.org",
    "dn": {
      "uid": "franck",
      "ou": "users",
      "dc": "org"
    },
    "uid": "franck",
    "objectClass": "debianDeveloper",
    "uidNumber": "3041",
    "cn": "Franck",
    "sn": "Joncourt",
    "gidNumber": "3041"
  },
  {
    "info": "# francois, users, debian.org",
    "dn": {
      "uid": "francois",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Francois",
    "keyFingerPrint": "8C470B2A0B31568E110D432516281F2E007C98D1",
    "uid": "francois",
    "uidNumber": "2608",
    "sn": "Marier",
    "gidNumber": "2608",
    "ircNick": "fmarier",
    "jabberJID": "francois@fmarier.org",
    "labeledURI": "https://fmarier.org"
  },
  {
    "info": "# frank, users, debian.org",
    "dn": {
      "uid": "frank",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frank",
    "uid": "frank",
    "uidNumber": "2660",
    "ircNick": "fant",
    "gidNumber": "2660",
    "sn": ": S8O8c3Rlcg=="
  },
  {
    "info": "# frankie, users, debian.org",
    "dn": {
      "uid": "frankie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Francesco",
    "uid": "frankie",
    "sn": "Lovergine",
    "uidNumber": "2261",
    "keyFingerPrint": "0579A97A2238EBF9BE61ED020F02A5E1163686A4",
    "gidNumber": "2261",
    "ircNick": "frankie",
    "labeledURI": "http://lovergine.com"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "frb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frank",
    "uid": "frb",
    "uidNumber": "2026",
    "sn": "Belew",
    "gidNumber": "2026"
  },
  {
    "info": "# frediz, users, debian.org",
    "dn": {
      "uid": "frediz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "frediz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3506",
    "gidNumber": "3506",
    "keyFingerPrint": "87DBD5756155202EC3885E936B7498FCB9B358AC",
    "cn": ": RnLDqWTDqXJpYw==",
    "sn": "Bonnard"
  },
  {
    "info": "# freee, users, debian.org",
    "dn": {
      "uid": "freee",
      "ou": "users",
      "dc": "org"
    },
    "uid": "freee",
    "objectClass": "debianDeveloper",
    "uidNumber": "2791",
    "cn": "Free",
    "sn": "Ekanayaka",
    "keyFingerPrint": "76E7B1471EE8D0E7A593E89620CAA0E7EA885677",
    "gidNumber": "2791",
    "ircNick": "free"
  },
  {
    "info": "# frn, users, debian.org",
    "dn": {
      "uid": "frn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Florent",
    "uid": "frn",
    "sn": "Rougon",
    "uidNumber": "2698",
    "ircNick": "frn",
    "labeledURI": "http://people.via.ecp.fr/~flo/",
    "gidNumber": "2698"
  },
  {
    "info": "# frob, users, debian.org",
    "dn": {
      "uid": "frob",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roland",
    "uid": "frob",
    "uidNumber": "1344",
    "sn": "McGrath",
    "ircNick": "rmcgrath",
    "labeledURI": "http://www.frob.com/",
    "gidNumber": "1344"
  },
  {
    "info": "# fs, users, debian.org",
    "dn": {
      "uid": "fs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fs",
    "objectClass": "debianDeveloper",
    "uidNumber": "2762",
    "cn": "Frederik",
    "ircNick": "fs",
    "gidNumber": "2762",
    "sn": ": U2Now7xsZXI="
  },
  {
    "info": "# fsateler, users, debian.org",
    "dn": {
      "uid": "fsateler",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fsateler",
    "objectClass": "debianDeveloper",
    "uidNumber": "3081",
    "keyFingerPrint": "218EE0362033C87B6C135FA4A3BABAE2408DD6CF",
    "cn": "Felipe",
    "sn": "Sateler",
    "gidNumber": "3081",
    "ircNick": "fsateler"
  },
  {
    "info": "# fsfs, users, debian.org",
    "dn": {
      "uid": "fsfs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fsfs",
    "objectClass": "debianDeveloper",
    "uidNumber": "3244",
    "keyFingerPrint": "30B23C8B4E6A3B09EAA6B65212973B6E72DC07B5",
    "cn": "Florian",
    "sn": "Schlichting",
    "gidNumber": "3244",
    "ircNick": "fsfs",
    "jabberJID": "fschlich@jabber.fu-berlin.de"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "fsmunoz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://fsmunoz.freeshell.org/index.html",
    "cn": "Frederico",
    "uid": "fsmunoz",
    "uidNumber": "2336",
    "ircNick": "fsmunoz",
    "sn": "Munoz",
    "gidNumber": "2336"
  },
  {
    "info": "# fst, users, debian.org",
    "dn": {
      "uid": "fst",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fst",
    "objectClass": "debianDeveloper",
    "uidNumber": "2901",
    "cn": "Frank",
    "sn": "Thomas",
    "gidNumber": "2901"
  },
  {
    "info": "# ftobich, users, debian.org",
    "dn": {
      "uid": "ftobich",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ftobich",
    "objectClass": "debianDeveloper",
    "uidNumber": "3628",
    "gidNumber": "3628",
    "keyFingerPrint": "97304066E5AEFAC22683D03D4FB3B4D37EF63B2E",
    "cn": "Fabio",
    "sn": "Tobich",
    "ircNick": "ftobich"
  },
  {
    "info": "# fuddl, users, debian.org",
    "dn": {
      "uid": "fuddl",
      "ou": "users",
      "dc": "org"
    },
    "uid": "fuddl",
    "objectClass": "debianDeveloper",
    "uidNumber": "2929",
    "cn": "Bruno",
    "sn": "Kleinert",
    "keyFingerPrint": "6CD068620DE13D1B938572334B0069CCF6527847",
    "gidNumber": "2929",
    "jabberJID": "fuddl@mailbox.org"
  },
  {
    "info": "# fuji, users, debian.org",
    "dn": {
      "uid": "fuji",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "keyFingerPrint": "D4024AF37488165BCC3441477F0CD922EE4C26E8",
    "uid": "fuji",
    "sn": "Kelemen",
    "uidNumber": "1337",
    "ircNick": "Fuji^",
    "jabberJID": "Peter.Kelemen@gmail.com",
    "labeledURI": "http://cern.ch/fuji/",
    "gidNumber": "1337"
  },
  {
    "info": "# fw, users, debian.org",
    "dn": {
      "uid": "fw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Florian",
    "keyFingerPrint": "C8D3D9CFFA9E70563F32FA54BF7BFF0402D524BE",
    "uid": "fw",
    "uidNumber": "2614",
    "sn": "Weimer",
    "gidNumber": "2614",
    "ircNick": "fw",
    "labeledURI": "http://www.enyo.de/fw/"
  },
  {
    "info": "# gabriel, users, debian.org",
    "dn": {
      "uid": "gabriel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gabriel",
    "objectClass": "debianDeveloper",
    "uidNumber": "3600",
    "gidNumber": "3600",
    "keyFingerPrint": "FB05091C5554035C1A76F73CAE9B1138386ECAF2",
    "cn": "Gabriel",
    "sn": "F. T. Gomes",
    "ircNick": "gabrielftg"
  },
  {
    "info": "# gadek, users, debian.org",
    "dn": {
      "uid": "gadek",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Grzegorz",
    "uid": "gadek",
    "sn": "Prokopski",
    "uidNumber": "2506",
    "ircNick": "gadek",
    "gidNumber": "2506"
  },
  {
    "info": "# gagath, users, debian.org",
    "dn": {
      "uid": "gagath",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gagath",
    "objectClass": "debianDeveloper",
    "uidNumber": "3669",
    "gidNumber": "3669",
    "keyFingerPrint": "7D2A8C42F5BB8D24E895AC5F83A395925B29CE90",
    "cn": "Agathe",
    "sn": "Porte"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "galenh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Galen",
    "uid": "galenh",
    "uidNumber": "978",
    "sn": "Hazelwood",
    "gidNumber": "978"
  },
  {
    "info": "# garabik, users, debian.org",
    "dn": {
      "uid": "garabik",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Radovan",
    "uid": "garabik",
    "uidNumber": "2056",
    "sn": "Garabik",
    "jabberJID": "garabik@jabber.juls.savba.sk",
    "labeledURI": "http://kassiopeia.juls.savba.sk/~garabik/",
    "keyFingerPrint": "C1B8D13D3DF5DF650C464EEE6856A4A640AF3FD3",
    "gidNumber": "2056"
  },
  {
    "info": "# garden, users, debian.org",
    "dn": {
      "uid": "garden",
      "ou": "users",
      "dc": "org"
    },
    "uid": "garden",
    "objectClass": "debianDeveloper",
    "uidNumber": "2967",
    "cn": "Ludovico",
    "sn": "Gardenghi",
    "ircNick": "garden",
    "jabberJID": "gardengl@gmail.com",
    "keyFingerPrint": "B0A6E03283E639BD769D3CF2DA03B32626600662",
    "gidNumber": "2967"
  },
  {
    "info": "# gareuselesinge, users, debian.org",
    "dn": {
      "uid": "gareuselesinge",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gareuselesinge",
    "objectClass": "debianDeveloper",
    "uidNumber": "2770",
    "cn": "Enrico",
    "sn": "Tassi",
    "gidNumber": "2770",
    "ircNick": "gares",
    "labeledURI": "http://www-sop.inria.fr/members/Enrico.Tassi/"
  },
  {
    "info": "# gaudenz, users, debian.org",
    "dn": {
      "uid": "gaudenz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gaudenz",
    "uid": "gaudenz",
    "sn": "Steinlin",
    "uidNumber": "2691",
    "keyFingerPrint": "836E4F81EFBBADA7085279BFA97A7702BAF91EF5",
    "gidNumber": "2691",
    "ircNick": "gaudenz",
    "jabberJID": "gaudenz@durcheinandertal.ch"
  },
  {
    "info": "20091208] resigned",
    "dn": {
      "uid": "gbechly",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.bechly.de/gbechly.htm",
    "cn": "Guenter",
    "uid": "gbechly",
    "uidNumber": "2091",
    "sn": "Bechly",
    "gidNumber": "2091"
  },
  {
    "info": "# gcs, users, debian.org",
    "dn": {
      "uid": "gcs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "gcs",
    "uidNumber": "2699",
    "keyFingerPrint": "A0DF7E0D3851E0EE45C00BC8ACE1F33CB933BBBB",
    "ircNick": "GCS",
    "labeledURI": "http://www.gcs.org.hu/",
    "gidNumber": "2699",
    "cn": ": TMOhc3psw7M=",
    "sn": ": QsO2c3rDtnJtw6lueWk="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "gecko",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Darren",
    "uid": "gecko",
    "uidNumber": "1227",
    "ircNick": "gecko",
    "sn": "Benham",
    "gidNumber": "1651"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "geddes",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bill",
    "uid": "geddes",
    "uidNumber": "1310",
    "sn": "Geddes",
    "gidNumber": "1310"
  },
  {
    "info": "# geiger, users, debian.org",
    "dn": {
      "uid": "geiger",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guenter",
    "uid": "geiger",
    "uidNumber": "1086",
    "sn": "Geiger",
    "labeledURI": "http://gige.xdv.org",
    "gidNumber": "1086"
  },
  {
    "info": "# geissert, users, debian.org",
    "dn": {
      "uid": "geissert",
      "ou": "users",
      "dc": "org"
    },
    "uid": "geissert",
    "objectClass": "debianDeveloper",
    "uidNumber": "2984",
    "cn": "Raphael",
    "sn": "Geissert",
    "keyFingerPrint": "FE96A3E9B6685DDD0CE3E63E49DDA59BA2EC1A7F",
    "gidNumber": "2984",
    "ircNick": "raphael",
    "jabberJID": "atomo64@gmail.com",
    "labeledURI": "http://rgeissert.blogspot.com"
  },
  {
    "info": "# gemorin, users, debian.org",
    "dn": {
      "uid": "gemorin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guillaume",
    "uid": "gemorin",
    "sn": "Morin",
    "uidNumber": "2037",
    "ircNick": "canard",
    "gidNumber": "2037"
  },
  {
    "info": "# genannt, users, debian.org",
    "dn": {
      "uid": "genannt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "genannt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3343",
    "keyFingerPrint": "CFC5B232C0D082CAE6B3A166F04CEFF6016CFFD0",
    "cn": "Jonas",
    "sn": "Genannt",
    "gidNumber": "3343"
  },
  {
    "info": "# georg, users, debian.org",
    "dn": {
      "uid": "georg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "georg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3564",
    "gidNumber": "3564",
    "cn": "Georg",
    "sn": "Faerber",
    "keyFingerPrint": "16C4D1D607C8716F38049F21EA2DB6963F8B2984"
  },
  {
    "info": "# georgesk, users, debian.org",
    "dn": {
      "uid": "georgesk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "georgesk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3080",
    "cn": "Georges",
    "sn": "Khaznadar",
    "keyFingerPrint": "3340B364FF67153FB7CCAE851C2816907136AE39",
    "gidNumber": "3080"
  },
  {
    "info": "# gewo, users, debian.org",
    "dn": {
      "uid": "gewo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gewo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3455",
    "gidNumber": "3455",
    "keyFingerPrint": "45877F1F6F591AB1F366BC3A02541A371530B71F",
    "cn": "Gert",
    "sn": "Wollny"
  },
  {
    "info": "# gfa, users, debian.org",
    "dn": {
      "uid": "gfa",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gfa",
    "objectClass": "debianDeveloper",
    "uidNumber": "3454",
    "gidNumber": "3454",
    "cn": "Gustavo",
    "sn": "Panizzo",
    "keyFingerPrint": "27263FA42553615F904A7EBE2A40A2ECB8DAD8D5",
    "ircNick": "gfa",
    "jabberJID": "gfa@debian.org"
  },
  {
    "info": "# ghe, users, debian.org",
    "dn": {
      "uid": "ghe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ghe",
    "objectClass": "debianDeveloper",
    "uidNumber": "2819",
    "cn": "Juan",
    "sn": "Gregorio Hernando Rivero",
    "ircNick": "GheRivero",
    "jabberJID": "ghe_rivero@jabber.org",
    "keyFingerPrint": "190473745A88BF8DFFE844A0DD0BA251BC52FA6F",
    "gidNumber": "2819"
  },
  {
    "info": "# ghedo, users, debian.org",
    "dn": {
      "uid": "ghedo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ghedo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3184",
    "keyFingerPrint": "A4F455C3414B10563FCC9244AFA51BD6CDE573CB",
    "cn": "Alessandro",
    "sn": "Ghedini",
    "gidNumber": "3184",
    "ircNick": "ghedo",
    "labeledURI": "https://www.ghedini.me"
  },
  {
    "info": "# ghostbar, users, debian.org",
    "dn": {
      "uid": "ghostbar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ghostbar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3020",
    "cn": "Jose",
    "sn": "Rivas Contreras",
    "keyFingerPrint": "D278F9C15E5461AA3C1E2FCD13EC43EEB9AC8C43",
    "gidNumber": "3020",
    "ircNick": "ghostbar",
    "labeledURI": "https://ghostbar.co"
  },
  {
    "info": "# gibmat, users, debian.org",
    "dn": {
      "uid": "gibmat",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gibmat",
    "objectClass": "debianDeveloper",
    "uidNumber": "3658",
    "gidNumber": "3658",
    "keyFingerPrint": "0A3455B7CCDE5C5343747321C0BA812C6C4DCB2E",
    "cn": "Mathias",
    "sn": "Gibbens",
    "ircNick": "gibmat"
  },
  {
    "info": "# gibreel, users, debian.org",
    "dn": {
      "uid": "gibreel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "gibreel",
    "uidNumber": "1181",
    "ircNick": "donatien",
    "sn": "Zander",
    "gidNumber": "1689"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "gilbert",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gilbert",
    "uid": "gilbert",
    "uidNumber": "1090",
    "sn": "Coville",
    "gidNumber": "1090"
  },
  {
    "info": "# gildor, users, debian.org",
    "dn": {
      "uid": "gildor",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gildor",
    "objectClass": "debianDeveloper",
    "uidNumber": "2763",
    "cn": "Sylvain",
    "sn": "Le Gall",
    "ircNick": "gildor",
    "jabberJID": "gildor478@gmail.com",
    "labeledURI": "http://le-gall.net/sylvain+violaine/",
    "gidNumber": "2763"
  },
  {
    "info": "# ginggs, users, debian.org",
    "dn": {
      "uid": "ginggs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ginggs",
    "objectClass": "debianDeveloper",
    "uidNumber": "3368",
    "keyFingerPrint": "25E3FF2D7F469DBE7D0D4E50AFCFEC8E669CE1C2",
    "cn": "Graham",
    "sn": "Inggs",
    "gidNumber": "3368",
    "ircNick": "ginggs"
  },
  {
    "info": "XyDnLAdyjqguGKevJTdrwli+IhvwIDAQAB",
    "dn": {
      "uid": "gio",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gio",
    "objectClass": "debianDeveloper",
    "uidNumber": "3077",
    "cn": "Giovanni",
    "sn": "Mascellani",
    "keyFingerPrint": "82D119A840C6EFCA6F5AF9459EDCC991D9AB457E",
    "gidNumber": "3077",
    "ircNick": "gio",
    "labeledURI": "http://poisson.phc.dm.unipi.it/~mascellani"
  },
  {
    "info": "# giovani, users, debian.org",
    "dn": {
      "uid": "giovani",
      "ou": "users",
      "dc": "org"
    },
    "uid": "giovani",
    "objectClass": "debianDeveloper",
    "uidNumber": "3458",
    "gidNumber": "3458",
    "keyFingerPrint": "B3F51A2712619C3ECD74B8E178494EF72375A66C",
    "cn": "Giovani",
    "sn": "Ferreira",
    "ircNick": "jova2",
    "jabberJID": "jova2@jabber.cz",
    "labeledURI": "http://softwarelivre.org/jova2"
  },
  {
    "info": "# giskard, users, debian.org",
    "dn": {
      "uid": "giskard",
      "ou": "users",
      "dc": "org"
    },
    "uid": "giskard",
    "objectClass": "debianDeveloper",
    "uidNumber": "2793",
    "cn": "Riccardo",
    "sn": "Setti",
    "keyFingerPrint": "81187882B05112F6627455A5ED6434406E34893C",
    "gidNumber": "2793",
    "ircNick": "giskard",
    "jabberJID": "giskard@autistici.org",
    "labeledURI": "http://giskard.noblogs.org"
  },
  {
    "info": "# gismo, users, debian.org",
    "dn": {
      "uid": "gismo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gismo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2878",
    "cn": "Luca",
    "sn": "Capello",
    "ircNick": "gismo",
    "jabberJID": "luca@pca.it",
    "labeledURI": "http://luca.pca.it",
    "gidNumber": "2878"
  },
  {
    "info": "# gistad, users, debian.org",
    "dn": {
      "uid": "gistad",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gistad",
    "objectClass": "debianDeveloper",
    "uidNumber": "3470",
    "gidNumber": "3470",
    "keyFingerPrint": "0E364951D11E572E9386516E19CB0C6B2D52B0E5",
    "cn": "Geir",
    "sn": "Istad"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "giuseppe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Giuseppe",
    "uid": "giuseppe",
    "uidNumber": "813",
    "sn": "Vacanti",
    "gidNumber": "813"
  },
  {
    "info": "# gladk, users, debian.org",
    "dn": {
      "uid": "gladk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gladk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3237",
    "keyFingerPrint": "BBBD45EA818AB86FF67E7285D3E17383CFA7FF06",
    "cn": "Anton",
    "sn": "Gladky",
    "gidNumber": "3237",
    "ircNick": "gladk"
  },
  {
    "info": "# glandium, users, debian.org",
    "dn": {
      "uid": "glandium",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "glandium",
    "uidNumber": "2704",
    "sn": "Hommey",
    "keyFingerPrint": "182E161D1130B9FCCD7DB167E42AA04FA6AA8C72",
    "gidNumber": "2704",
    "ircNick": "glandium",
    "jabberJID": "mh@jabber.glandium.org",
    "labeledURI": "https://glandium.org/"
  },
  {
    "info": "# glandrum, users, debian.org",
    "dn": {
      "uid": "glandrum",
      "ou": "users",
      "dc": "org"
    },
    "uid": "glandrum",
    "objectClass": "debianDeveloper",
    "uidNumber": "3210",
    "keyFingerPrint": "FB612EB4FE3498A33191FA60C654602274E58333",
    "cn": "Gregory",
    "sn": "Landrum",
    "gidNumber": "3210"
  },
  {
    "info": "# glaubitz, users, debian.org",
    "dn": {
      "uid": "glaubitz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "glaubitz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3232",
    "keyFingerPrint": "62FF8A7584E029569546000674263B37F5B5F913",
    "cn": "John Paul Adrian",
    "sn": "Glaubitz",
    "ircNick": "cbmuser",
    "gidNumber": "3232"
  },
  {
    "info": "# glaweh, users, debian.org",
    "dn": {
      "uid": "glaweh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Henning",
    "uid": "glaweh",
    "uidNumber": "2633",
    "sn": "Glawe",
    "keyFingerPrint": "0FF89AA2BE33C4197DF80EF2D0AAB32A23B5397C",
    "gidNumber": "2633",
    "ircNick": "eartoaster"
  },
  {
    "info": "# gleydson, users, debian.org",
    "dn": {
      "uid": "gleydson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gleydson",
    "keyFingerPrint": "BC8103B7B2891227A02E675AA56BC3DF985BA281",
    "uid": "gleydson",
    "uidNumber": "2348",
    "sn": "Mazioli da Silva",
    "gidNumber": "2348",
    "ircNick": "gleydson",
    "labeledURI": "http://www.guiafoca.org"
  },
  {
    "info": "# glondu, users, debian.org",
    "dn": {
      "uid": "glondu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "glondu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3000",
    "keyFingerPrint": "58EB0999C64E897EE894B8037853DA4D49881AD3",
    "sn": "Glondu",
    "gidNumber": "3000",
    "cn": ": U3TDqXBoYW5l",
    "ircNick": "sgnb",
    "jabberJID": "sgnb@jabber.crans.org",
    "labeledURI": "https://stephane.glondu.net/"
  },
  {
    "info": "# gniibe, users, debian.org",
    "dn": {
      "uid": "gniibe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "NIIBE",
    "uid": "gniibe",
    "sn": "Yutaka",
    "uidNumber": "2737",
    "gidNumber": "2737",
    "ircNick": "gniibe",
    "labeledURI": "https://www.gniibe.org/",
    "keyFingerPrint": "249CB3771750745D5CDD323CE267B052364F028D"
  },
  {
    "info": "# godisch, users, debian.org",
    "dn": {
      "uid": "godisch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "godisch",
    "sn": "Godisch",
    "uidNumber": "2566",
    "keyFingerPrint": "1842323B4FCF9B90995FA17FA350B991F05A4857",
    "gidNumber": "2566",
    "labeledURI": "http://people.debian.org/~godisch/"
  },
  {
    "info": "# goedson, users, debian.org",
    "dn": {
      "uid": "goedson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Goedson",
    "uid": "goedson",
    "sn": "Teixeira Paixao",
    "uidNumber": "2591",
    "gidNumber": "2591",
    "keyFingerPrint": "E8CF1075FABF169914F9121CA3B7DB93EB8A46DC",
    "ircNick": "goedson",
    "jabberJID": "goedson@jabber.org",
    "labeledURI": "http://people.debian.org/~goedson/"
  },
  {
    "info": "# goneri, users, debian.org",
    "dn": {
      "uid": "goneri",
      "ou": "users",
      "dc": "org"
    },
    "uid": "goneri",
    "objectClass": "debianDeveloper",
    "uidNumber": "2906",
    "sn": "Le Bouder",
    "keyFingerPrint": "1FF368E8019913731705B8AF049ED9B94765572E",
    "gidNumber": "2906",
    "cn": ": R29uw6lyaQ==",
    "ircNick": "goneri"
  },
  {
    "info": "# gopal, users, debian.org",
    "dn": {
      "uid": "gopal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gopal",
    "uid": "gopal",
    "sn": "Narayanan",
    "labeledURI": "http://www.astro.umass.edu/~gopal/",
    "uidNumber": "1317",
    "ircNick": "Gop_Astro",
    "gidNumber": "1317"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "gor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.dcs.napier.ac.uk/~gor/",
    "cn": "Gordon",
    "uid": "gor",
    "uidNumber": "842",
    "sn": "Russell",
    "gidNumber": "842"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "goran",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Goran",
    "uid": "goran",
    "uidNumber": "2003",
    "sn": "Andersson",
    "gidNumber": "2003"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "gord",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gordon",
    "uid": "gord",
    "uidNumber": "1286",
    "sn": "Matzigkeit",
    "gidNumber": "1286"
  },
  {
    "info": "# gordon, users, debian.org",
    "dn": {
      "uid": "gordon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gordon",
    "uid": "gordon",
    "sn": "Fraser",
    "uidNumber": "2434",
    "ircNick": "gordon",
    "gidNumber": "2434"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "gorgo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gergely",
    "uid": "gorgo",
    "uidNumber": "1006",
    "ircNick": "gorgo",
    "sn": "Madarasz",
    "gidNumber": "1006"
  },
  {
    "info": "# gotom, users, debian.org",
    "dn": {
      "uid": "gotom",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Masanori",
    "uid": "gotom",
    "uidNumber": "2082",
    "sn": "Goto",
    "keyFingerPrint": "D29E635FDF31BB875632563BC95C49F12A068813",
    "gidNumber": "2082",
    "ircNick": "gotom",
    "labeledURI": "http://sanori.org/~gotom"
  },
  {
    "info": "# gpastore, users, debian.org",
    "dn": {
      "uid": "gpastore",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gpastore",
    "objectClass": "debianDeveloper",
    "uidNumber": "2790",
    "cn": "Guilherme",
    "sn": "de S. Pastore",
    "ircNick": "fatalerror",
    "gidNumber": "2790"
  },
  {
    "info": "# gq, users, debian.org",
    "dn": {
      "uid": "gq",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gq",
    "objectClass": "debianDeveloper",
    "uidNumber": "2983",
    "keyFingerPrint": "04B59D90DF7CC2ABCD49BAEACA87E9E82AAC33F1",
    "cn": "Alexander",
    "sn": "Gerasiov",
    "gidNumber": "2983",
    "ircNick": "GQ",
    "labeledURI": "http://gerasiov.net"
  },
  {
    "info": "# graham, users, debian.org",
    "dn": {
      "uid": "graham",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Graham",
    "uid": "graham",
    "sn": "Wilson",
    "uidNumber": "2616",
    "ircNick": "gram",
    "gidNumber": "2616"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "grant",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Grant",
    "uid": "grant",
    "uidNumber": "1244",
    "sn": "Bowman",
    "gidNumber": "1639"
  },
  {
    "info": "# grantma, users, debian.org",
    "dn": {
      "uid": "grantma",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "keyFingerPrint": "C1A1C3F1135DBE0936705CE3C44E5FFC1335C3A2",
    "uid": "grantma",
    "uidNumber": "2494",
    "sn": "Grant",
    "gidNumber": "2494"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "gray",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Graydon",
    "uid": "gray",
    "uidNumber": "1146",
    "sn": "Hoare",
    "gidNumber": "1146"
  },
  {
    "info": "# gregh, users, debian.org",
    "dn": {
      "uid": "gregh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Greg",
    "uid": "gregh",
    "uidNumber": "814",
    "sn": "Hankins",
    "gidNumber": "814"
  },
  {
    "info": "N8wARAriEOpact2oScHja95lw1+jMGnXQYQIDAQAB",
    "dn": {
      "uid": "gregoa",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gregoa",
    "objectClass": "debianDeveloper",
    "uidNumber": "2924",
    "keyFingerPrint": "D1E1316E93A760A8104D85FABB3A68018649AA06",
    "cn": "Gregor",
    "sn": "Herrmann",
    "gidNumber": "2924",
    "ircNick": "gregoa",
    "jabberJID": "gregoa@jabber.colgarra.priv.at",
    "labeledURI": "https://info.comodo.priv.at/"
  },
  {
    "info": "20091208] Retired [JT - 2007-02-20]",
    "dn": {
      "uid": "grendel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marek",
    "uid": "grendel",
    "sn": "Habersack",
    "labeledURI": "http://caudium.net/",
    "uidNumber": "2316",
    "ircNick": "Grendel^",
    "gidNumber": "2316"
  },
  {
    "info": "# greuff, users, debian.org",
    "dn": {
      "uid": "greuff",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "keyFingerPrint": "01FA29EF7F5666A86596C50143225D698FC03128",
    "uid": "greuff",
    "uidNumber": "2688",
    "sn": "Wana",
    "ircNick": "greuff",
    "labeledURI": "http://www.wana.at/",
    "gidNumber": "2688"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "grimaldi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jens",
    "uid": "grimaldi",
    "uidNumber": "1154",
    "sn": "Scheidtmann",
    "gidNumber": "1154"
  },
  {
    "info": "# grin, users, debian.org",
    "dn": {
      "uid": "grin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "grin",
    "sn": "Gervai",
    "labeledURI": "http://grin.hu/",
    "uidNumber": "2133",
    "ircNick": "grin",
    "gidNumber": "2133"
  },
  {
    "info": "# gris, users, debian.org",
    "dn": {
      "uid": "gris",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gris",
    "objectClass": "debianDeveloper",
    "uidNumber": "3131",
    "keyFingerPrint": "8B94819C255570A374B62CCD26E3C875A74420EF",
    "cn": "Christoph",
    "gidNumber": "3131",
    "sn": ": R8O2aHJl"
  },
  {
    "info": "# grisu, users, debian.org",
    "dn": {
      "uid": "grisu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "keyFingerPrint": "3CAB9397E835E2E95289A0C0F85EBF46258D8781",
    "uid": "grisu",
    "sn": "Bramer",
    "uidNumber": "1162",
    "ircNick": "grisu",
    "gidNumber": "1162"
  },
  {
    "info": "# gspr, users, debian.org",
    "dn": {
      "uid": "gspr",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gspr",
    "objectClass": "debianDeveloper",
    "uidNumber": "3588",
    "gidNumber": "3588",
    "keyFingerPrint": "CFC5EF8510851E7355B55E809D11582AFD548CFA",
    "cn": "Gard",
    "sn": "Spreemann",
    "ircNick": "gspr",
    "labeledURI": "https://nonempty.org"
  },
  {
    "info": "# gt, users, debian.org",
    "dn": {
      "uid": "gt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gerhard",
    "uid": "gt",
    "uidNumber": "2352",
    "sn": "Tonn",
    "gidNumber": "2352"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "gthomas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guy",
    "uid": "gthomas",
    "uidNumber": "843",
    "sn": "Thomas",
    "gidNumber": "843"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "guido",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guido",
    "uid": "guido",
    "uidNumber": "1328",
    "sn": "Witmond",
    "gidNumber": "1328"
  },
  {
    "info": "NwFdiZ9H3KjM5MUwUJ9ifyy1n2bH/5LOVEsqzmbVJqwrTi1gIP3JQL9zEwIDAQAB",
    "dn": {
      "uid": "guilhem",
      "ou": "users",
      "dc": "org"
    },
    "uid": "guilhem",
    "objectClass": "debianDeveloper",
    "uidNumber": "3473",
    "gidNumber": "3473",
    "keyFingerPrint": "7420DF86BCE15A458DCE997639278DA8109E6244",
    "cn": "Guilhem",
    "sn": "Moulin",
    "ircNick": "guilhem"
  },
  {
    "info": "# guillem, users, debian.org",
    "dn": {
      "uid": "guillem",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guillem",
    "uid": "guillem",
    "sn": "Jover",
    "uidNumber": "2570",
    "keyFingerPrint": "4F3E74F436050C10F5696574B972BF3EA4AE57A3",
    "gidNumber": "2570",
    "ircNick": "guillem",
    "labeledURI": "https://www.hadrons.org/~guillem/"
  },
  {
    "info": "# gunnarhj, users, debian.org",
    "dn": {
      "uid": "gunnarhj",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gunnarhj",
    "objectClass": "debianDeveloper",
    "uidNumber": "3622",
    "gidNumber": "3622",
    "keyFingerPrint": "0CFE997B724580A7FA72F8CFF0B110E75A692F32",
    "cn": "Gunnar",
    "sn": "Hjalmarsson"
  },
  {
    "info": "# guoliang, users, debian.org",
    "dn": {
      "uid": "guoliang",
      "ou": "users",
      "dc": "org"
    },
    "uid": "guoliang",
    "objectClass": "debianDeveloper",
    "uidNumber": "3178",
    "cn": "Liang",
    "sn": "Guo",
    "labeledURI": "http://bluestone.cublog.cn",
    "gidNumber": "3178"
  },
  {
    "info": "# gus, users, debian.org",
    "dn": {
      "uid": "gus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Angus",
    "uid": "gus",
    "uidNumber": "2093",
    "sn": "Lees",
    "keyFingerPrint": "4CD79C1A472F03F8673725C15BE2EB392C5C4329",
    "gidNumber": "2093",
    "ircNick": "gus"
  },
  {
    "info": "# gusnan, users, debian.org",
    "dn": {
      "uid": "gusnan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "gusnan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3321",
    "keyFingerPrint": "DB306E4B10FFD98EF4DB55D7194B631AB2DA2888",
    "cn": "Andreas",
    "gidNumber": "3321",
    "sn": ": UsO2bm5xdWlzdA==",
    "ircNick": "gusnan",
    "labeledURI": "http://www.gusnan.se"
  },
  {
    "info": "# guterm, users, debian.org",
    "dn": {
      "uid": "guterm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "guterm",
    "uidNumber": "2650",
    "sn": "Gubser",
    "ircNick": "guterm",
    "gidNumber": "2650"
  },
  {
    "info": "# guus, users, debian.org",
    "dn": {
      "uid": "guus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guus",
    "uid": "guus",
    "sn": "Sliepen",
    "uidNumber": "2329",
    "keyFingerPrint": "4D1B7796C03E0831991BDD423F490DEB871EF9FA",
    "gidNumber": "2329",
    "ircNick": "guus"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "gvence",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Greg",
    "uid": "gvence",
    "uidNumber": "1031",
    "sn": "Vence",
    "gidNumber": "1031"
  },
  {
    "info": "# gwj, users, debian.org",
    "dn": {
      "uid": "gwj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gregory",
    "uid": "gwj",
    "uidNumber": "1407",
    "sn": "Johnson",
    "gidNumber": "1407"
  },
  {
    "info": "# gwolf, users, debian.org",
    "dn": {
      "uid": "gwolf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gunnar",
    "uid": "gwolf",
    "sn": "Wolf",
    "uidNumber": "2586",
    "gidNumber": "2586",
    "ircNick": "gwolf",
    "jabberJID": "gwolf@debian.org",
    "labeledURI": "https://www.gwolf.org/",
    "keyFingerPrint": "4D14050653A402D73687049D2404C9546E145360"
  },
  {
    "info": "# haas, users, debian.org",
    "dn": {
      "uid": "haas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christoph",
    "uid": "haas",
    "sn": "Haas",
    "uidNumber": "2683",
    "keyFingerPrint": "559E4BA79816CB5AC1F95DDBFE038E5950B5A343",
    "gidNumber": "2683",
    "ircNick": "Signum",
    "jabberJID": "chrish@jabber.workaround.org",
    "labeledURI": "http://workaround.org/"
  },
  {
    "info": "# hadess, users, debian.org",
    "dn": {
      "uid": "hadess",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bastien",
    "uid": "hadess",
    "sn": "Nocera",
    "labeledURI": "http://hadess.net",
    "uidNumber": "2468",
    "ircNick": "hadess",
    "gidNumber": "2468"
  },
  {
    "info": "# hafre, users, debian.org",
    "dn": {
      "uid": "hafre",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hans",
    "uid": "hafre",
    "sn": "Freitag",
    "labeledURI": "http://www.president.eu.org",
    "uidNumber": "2527",
    "ircNick": "Macrotron",
    "gidNumber": "2527"
  },
  {
    "info": "# hakan, users, debian.org",
    "dn": {
      "uid": "hakan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hakan",
    "uid": "hakan",
    "sn": "Ardo",
    "uidNumber": "943",
    "labeledURI": "http://www.maths.lth.se/matematiklth/personal/ardo/",
    "keyFingerPrint": "881C66A096E124903BEECA1CEA25F9FB06A9A7D1",
    "gidNumber": "943"
  },
  {
    "info": "# hallon, users, debian.org",
    "dn": {
      "uid": "hallon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fredrik",
    "uid": "hallon",
    "sn": "Hallenberg",
    "uidNumber": "1173",
    "labeledURI": "http://www.lysator.liu.se/~hallon",
    "keyFingerPrint": "BADCB4C43D3DED4E540771DAE337DB93426E4154",
    "gidNumber": "1173"
  },
  {
    "info": "# halls, users, debian.org",
    "dn": {
      "uid": "halls",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "halls",
    "sn": "Halls",
    "uidNumber": "2545",
    "gidNumber": "2545",
    "ircNick": "haggai",
    "jabberJID": "haggai@jabber.org",
    "keyFingerPrint": "5264561BBA17FFF462A6A8A6BC8031288D89A751"
  },
  {
    "info": "# hamish, users, debian.org",
    "dn": {
      "uid": "hamish",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hamish",
    "uid": "hamish",
    "sn": "Moffatt",
    "uidNumber": "1065",
    "ircNick": "hamish",
    "gidNumber": "1065"
  },
  {
    "info": "# harmoney, users, debian.org",
    "dn": {
      "uid": "harmoney",
      "ou": "users",
      "dc": "org"
    },
    "uid": "harmoney",
    "objectClass": "debianDeveloper",
    "uidNumber": "3274",
    "cn": "Patty",
    "sn": "Langasek",
    "keyFingerPrint": "A86FF3650C417790415A66EC28E545808596DC7C",
    "gidNumber": "3274"
  },
  {
    "info": "# harshula, users, debian.org",
    "dn": {
      "uid": "harshula",
      "ou": "users",
      "dc": "org"
    },
    "uid": "harshula",
    "objectClass": "debianDeveloper",
    "uidNumber": "3169",
    "cn": "Harshula",
    "sn": "Jayasuriya",
    "keyFingerPrint": "B6DFFF2AEEE316B030F2B4D18D55C80D3430D100",
    "gidNumber": "3169"
  },
  {
    "info": "jSU4DKsRg05FKOqLmT/6D+6W6O6xW5wIDAQAB",
    "dn": {
      "uid": "hartmans",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sam",
    "uid": "hartmans",
    "sn": "Hartman",
    "uidNumber": "2173",
    "keyFingerPrint": "332D4CE3A2FAE5439B7E25B2283681BA6FE7F41D",
    "gidNumber": "2173",
    "ircNick": "hartmans"
  },
  {
    "info": "# hattas, users, debian.org",
    "dn": {
      "uid": "hattas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shuzo",
    "uid": "hattas",
    "sn": "Hatta",
    "uidNumber": "2078",
    "ircNick": "hattas",
    "gidNumber": "2078"
  },
  {
    "info": "# hauva, users, debian.org",
    "dn": {
      "uid": "hauva",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ari",
    "uid": "hauva",
    "sn": "Makela",
    "labeledURI": "http://arska.org/hauva/",
    "uidNumber": "2499",
    "ircNick": "N/A",
    "gidNumber": "2499"
  },
  {
    "info": "# hazelsct, users, debian.org",
    "dn": {
      "uid": "hazelsct",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "keyFingerPrint": "D54D1AEEB11CCE9BA02BC5DD526F01E8564EE4B6",
    "uid": "hazelsct",
    "uidNumber": "2120",
    "sn": "Powell",
    "gidNumber": "2120"
  },
  {
    "info": "# he, users, debian.org",
    "dn": {
      "uid": "he",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marc",
    "uid": "he",
    "sn": "Brockschmidt",
    "uidNumber": "2657",
    "gidNumber": "2657",
    "ircNick": "HE",
    "jabberJID": "HE@jabber.org",
    "labeledURI": "http://marcbrockschmidt.de"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "hec",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hugo",
    "uid": "hec",
    "sn": "Espuny",
    "labeledURI": "http://people.debian.org/~hec",
    "uidNumber": "2537",
    "ircNick": "hec",
    "gidNumber": "2537"
  },
  {
    "info": "# hecker, users, debian.org",
    "dn": {
      "uid": "hecker",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "hecker",
    "sn": "Hecker",
    "uidNumber": "2338",
    "keyFingerPrint": "5919FC3BDC30CE4135F1DC070F5E1ED4664196E2",
    "gidNumber": "2338",
    "ircNick": "NoWhereMan",
    "labeledURI": "http://hecker.debian.net"
  },
  {
    "info": "# hector, users, debian.org",
    "dn": {
      "uid": "hector",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hector",
    "uid": "hector",
    "sn": "Garcia",
    "uidNumber": "2192",
    "keyFingerPrint": "70F7E757154F2A01D77C1DAEC2FE04B214E36550",
    "gidNumber": "2192",
    "ircNick": "bal00"
  },
  {
    "info": "# hefee, users, debian.org",
    "dn": {
      "uid": "hefee",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hefee",
    "objectClass": "debianDeveloper",
    "uidNumber": "3441",
    "gidNumber": "3441",
    "keyFingerPrint": "39EC11A02016B72926491A06E3ADB00850605636",
    "cn": "Sandro",
    "ircNick": "hefee",
    "sn": ": S25hdcOf"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "heiko",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Heiko",
    "uid": "heiko",
    "uidNumber": "918",
    "sn": "Schlittermann",
    "gidNumber": "918"
  },
  {
    "info": "# helen, users, debian.org",
    "dn": {
      "uid": "helen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Helen",
    "uid": "helen",
    "uidNumber": "2736",
    "sn": "Faulkner",
    "ircNick": "helen",
    "gidNumber": "2736"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "helmut",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Helmut",
    "uid": "helmut",
    "uidNumber": "950",
    "sn": "Geyer",
    "gidNumber": "950"
  },
  {
    "info": "# helmutg, users, debian.org",
    "dn": {
      "uid": "helmutg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "helmutg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3255",
    "keyFingerPrint": "4CC2D2D90A8D1654DBF873AA2D1AAACF24444442",
    "cn": "Helmut",
    "sn": "Grohne",
    "gidNumber": "3255",
    "ircNick": "helmut",
    "jabberJID": "helmut@subdivi.de"
  },
  {
    "info": "# henning, users, debian.org",
    "dn": {
      "uid": "henning",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Henning",
    "uid": "henning",
    "sn": "Makholm",
    "labeledURI": "http://henning.makholm.net/",
    "uidNumber": "2690",
    "ircNick": "hmakholm",
    "gidNumber": "2690"
  },
  {
    "info": "# henrich, users, debian.org",
    "dn": {
      "uid": "henrich",
      "ou": "users",
      "dc": "org"
    },
    "uid": "henrich",
    "objectClass": "debianDeveloper",
    "uidNumber": "3067",
    "keyFingerPrint": "58E1222F9696C885A3CD104C5D328D082AAAB140",
    "cn": "Hideki",
    "sn": "Yamane",
    "gidNumber": "3067"
  },
  {
    "info": "# henry, users, debian.org",
    "dn": {
      "uid": "henry",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jason",
    "uid": "henry",
    "sn": "Parker",
    "uidNumber": "1295",
    "gidNumber": "1295"
  },
  {
    "info": "# herbert, users, debian.org",
    "dn": {
      "uid": "herbert",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://gondor.apana.org.au/~herbert/",
    "cn": "Herbert",
    "uid": "herbert",
    "uidNumber": "981",
    "sn": "Xu",
    "gidNumber": "981"
  },
  {
    "info": "# hertzog, users, debian.org",
    "dn": {
      "uid": "hertzog",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "keyFingerPrint": "3E4FB7117877F589DBCF06D6E619045DF2AC729A",
    "uid": "hertzog",
    "sn": "Hertzog",
    "uidNumber": "1258",
    "gidNumber": "1692",
    "cn": ": UmFwaGHDq2w=",
    "ircNick": "buxy",
    "labeledURI": "https://raphaelhertzog.com"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "hgebel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Harry",
    "uid": "hgebel",
    "uidNumber": "2356",
    "sn": "Gebel",
    "gidNumber": "2356"
  },
  {
    "info": "# hille42, users, debian.org",
    "dn": {
      "uid": "hille42",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hille42",
    "objectClass": "debianDeveloper",
    "uidNumber": "3127",
    "cn": "Hilmar",
    "sn": "Preusse",
    "gidNumber": "3127",
    "keyFingerPrint": "729D8041C7AB18EE93F3A17A0C871C4C653C1F59"
  },
  {
    "info": "# hilliard, users, debian.org",
    "dn": {
      "uid": "hilliard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bob",
    "uid": "hilliard",
    "sn": "Hilliard",
    "uidNumber": "1198",
    "gidNumber": "1710"
  },
  {
    "info": "# hle, users, debian.org",
    "dn": {
      "uid": "hle",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hle",
    "objectClass": "debianDeveloper",
    "uidNumber": "3377",
    "cn": "Hugo",
    "sn": "Lefeuvre",
    "gidNumber": "3377",
    "keyFingerPrint": "360B03B3BF274F4D7A3FD5E814AA1EB8A2473DFD",
    "ircNick": "hle",
    "labeledURI": "https://www.owl.eu.com"
  },
  {
    "info": "# hlieberman, users, debian.org",
    "dn": {
      "uid": "hlieberman",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hlieberman",
    "objectClass": "debianDeveloper",
    "uidNumber": "3405",
    "gidNumber": "3405",
    "keyFingerPrint": "43DF758C18C7545A3E1A4F0A88237A6A53AB1B2E",
    "cn": "Harlan",
    "sn": "Lieberman-Berg",
    "ircNick": "hlieberman",
    "labeledURI": "https://blog.setec.io"
  },
  {
    "info": "# hmc, users, debian.org",
    "dn": {
      "uid": "hmc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hmc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3666",
    "gidNumber": "3666",
    "keyFingerPrint": "3A20813CA57946868C5152115AC61074C5E81BC4",
    "cn": "Hugh",
    "sn": "McMaster",
    "ircNick": "hmc"
  },
  {
    "info": "# hmh, users, debian.org",
    "dn": {
      "uid": "hmh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Henrique",
    "uid": "hmh",
    "uidNumber": "2160",
    "sn": "de Moraes Holschuh",
    "keyFingerPrint": "C467A717507BBAFED3C160920BD9E81139CB4807",
    "gidNumber": "2160",
    "ircNick": "hmh"
  },
  {
    "info": "# hntourne, users, debian.org",
    "dn": {
      "uid": "hntourne",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hntourne",
    "objectClass": "debianDeveloper",
    "uidNumber": "3614",
    "gidNumber": "3614",
    "keyFingerPrint": "352FBF2D3FA41D25DCAA8910DD0C9594DF926C90",
    "cn": "Henry-Nicolas",
    "sn": "Tourneur",
    "ircNick": "hntourne"
  },
  {
    "info": "# hoexter, users, debian.org",
    "dn": {
      "uid": "hoexter",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hoexter",
    "objectClass": "debianDeveloper",
    "uidNumber": "3031",
    "cn": "Sven",
    "sn": "Hoexter",
    "keyFingerPrint": "7C0717F9FA2B2B9D788B141BA6DC24D9DA2493D1",
    "gidNumber": "3031"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "holgate",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://people.debian.org/~holgate",
    "cn": "Nick",
    "uid": "holgate",
    "uidNumber": "1253",
    "sn": "Holgate",
    "gidNumber": "1690"
  },
  {
    "info": "# holger, users, debian.org",
    "dn": {
      "uid": "holger",
      "ou": "users",
      "dc": "org"
    },
    "uid": "holger",
    "objectClass": "debianDeveloper",
    "uidNumber": "2863",
    "cn": "Holger",
    "sn": "Levsen",
    "keyFingerPrint": "B8BF54137B09D35CF026FE9D091AB856069AAA1C",
    "gidNumber": "2863",
    "ircNick": "h01ger",
    "labeledURI": "http://layer-acht.org"
  },
  {
    "info": "# holgerw, users, debian.org",
    "dn": {
      "uid": "holgerw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "holgerw",
    "objectClass": "debianDeveloper",
    "uidNumber": "3426",
    "gidNumber": "3426",
    "keyFingerPrint": "496AC6E814424B348508352959F187CA156EB076",
    "cn": "Holger",
    "sn": "Wansing"
  },
  {
    "info": "# holmgren, users, debian.org",
    "dn": {
      "uid": "holmgren",
      "ou": "users",
      "dc": "org"
    },
    "uid": "holmgren",
    "objectClass": "debianDeveloper",
    "uidNumber": "2911",
    "cn": "Magnus",
    "sn": "Holmgren",
    "keyFingerPrint": "CD2A073B386154170A4082E8D4F219BFEC998489",
    "ircNick": "holmgren",
    "jabberJID": "holmgren@lysator.liu.se",
    "gidNumber": "2911"
  },
  {
    "info": "# honey, users, debian.org",
    "dn": {
      "uid": "honey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lukasz",
    "uid": "honey",
    "sn": "Jachowicz",
    "uidNumber": "2345",
    "keyFingerPrint": "EC5F68E32FD9F61286C58776992FB5D8ED881C8E",
    "gidNumber": "2345",
    "ircNick": "honej",
    "jabberJID": "honey@7thguard.net",
    "labeledURI": "http://honey.7thGuard.net"
  },
  {
    "info": "# horms, users, debian.org",
    "dn": {
      "uid": "horms",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Simon",
    "uid": "horms",
    "uidNumber": "2269",
    "sn": "Horman",
    "keyFingerPrint": "E27CD9A1F5ACC2FF4BFE7285D7CF64696A374FBE",
    "ircNick": "horms",
    "labeledURI": "http://horms.org/",
    "gidNumber": "2269"
  },
  {
    "info": "# hp, users, debian.org",
    "dn": {
      "uid": "hp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Havoc",
    "uid": "hp",
    "uidNumber": "1224",
    "sn": "Pennington",
    "gidNumber": "1603"
  },
  {
    "info": "# hpfn, users, debian.org",
    "dn": {
      "uid": "hpfn",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hpfn",
    "objectClass": "debianDeveloper",
    "uidNumber": "3411",
    "gidNumber": "3411",
    "cn": "Herbert Parentes",
    "sn": "Fortes Neto",
    "jabberJID": "herbert@xmpp.zone"
  },
  {
    "info": "# hsteoh, users, debian.org",
    "dn": {
      "uid": "hsteoh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hwei",
    "uid": "hsteoh",
    "uidNumber": "2553",
    "sn": "Teoh",
    "keyFingerPrint": "FFFF328C0D4BBCC8033DFA92D1A539B0B0C3105C",
    "gidNumber": "2553"
  },
  {
    "info": "# huber, users, debian.org",
    "dn": {
      "uid": "huber",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Josh",
    "uid": "huber",
    "sn": "Huber",
    "uidNumber": "2103",
    "jabberJID": "uberjay@gmail.com",
    "gidNumber": "2103"
  },
  {
    "info": "# hug, users, debian.org",
    "dn": {
      "uid": "hug",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hug",
    "objectClass": "debianDeveloper",
    "uidNumber": "2839",
    "cn": "Philipp",
    "sn": "Hug",
    "keyFingerPrint": "210E1CD0A6A31386914A14510953ED7BAD69A1CB",
    "gidNumber": "2839",
    "ircNick": "hug",
    "jabberJID": "philipp@hug.cx"
  },
  {
    "info": "# huggie, users, debian.org",
    "dn": {
      "uid": "huggie",
      "ou": "users",
      "dc": "org"
    },
    "uid": "huggie",
    "objectClass": "debianDeveloper",
    "uidNumber": "2758",
    "cn": "Simon",
    "sn": "Huggins",
    "ircNick": "huggie",
    "jabberJID": "huggie@jabber.earth.li",
    "labeledURI": "http://www.earth.li/~huggie/",
    "gidNumber": "2758"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "hugo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://larve.net/people/hugo/",
    "cn": "Hugo",
    "uid": "hugo",
    "uidNumber": "1096",
    "ircNick": "hugo",
    "sn": "Haas",
    "gidNumber": "1096"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "hvdm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hugo",
    "uid": "hvdm",
    "uidNumber": "2194",
    "sn": "van der Merwe",
    "gidNumber": "2194"
  },
  {
    "info": "# hvhaugwitz, users, debian.org",
    "dn": {
      "uid": "hvhaugwitz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hvhaugwitz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3261",
    "keyFingerPrint": "2BBBD30FAAB29B3253BCFBA6F6947DAB68E7B931",
    "cn": "Hannes",
    "sn": "von Haugwitz",
    "gidNumber": "3261",
    "ircNick": "hvhaugwitz"
  },
  {
    "info": "# hyperair, users, debian.org",
    "dn": {
      "uid": "hyperair",
      "ou": "users",
      "dc": "org"
    },
    "uid": "hyperair",
    "objectClass": "debianDeveloper",
    "uidNumber": "3164",
    "keyFingerPrint": "12E0D09DFB3FF7F759D36ED0FBD5225B588752A1",
    "cn": "Loong Jin",
    "sn": "Chow",
    "ircNick": "hyperair",
    "labeledURI": "http://launchpad.net/~hyperair",
    "gidNumber": "3164"
  },
  {
    "info": "# i0109, users, debian.org",
    "dn": {
      "uid": "i0109",
      "ou": "users",
      "dc": "org"
    },
    "uid": "i0109",
    "objectClass": "debianDeveloper",
    "uidNumber": "3307",
    "cn": "Christian",
    "sn": "Langer",
    "gidNumber": "3307"
  },
  {
    "info": "# iamfuzz, users, debian.org",
    "dn": {
      "uid": "iamfuzz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "iamfuzz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3238",
    "keyFingerPrint": "2A62FCF3BBE867727440369C9258A798513F86CD",
    "cn": "Brian",
    "sn": "Thomason",
    "gidNumber": "3238"
  },
  {
    "info": "# ianb, users, debian.org",
    "dn": {
      "uid": "ianb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ianb",
    "objectClass": "debianDeveloper",
    "uidNumber": "2974",
    "cn": "Ian",
    "sn": "Beckwith",
    "ircNick": "ianb",
    "labeledURI": "http://erislabs.net/ianb/",
    "gidNumber": "2974"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ians",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "ians",
    "uidNumber": "1233",
    "sn": "Setford",
    "gidNumber": "1622"
  },
  {
    "info": "# ianw, users, debian.org",
    "dn": {
      "uid": "ianw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ianw",
    "objectClass": "debianDeveloper",
    "uidNumber": "2805",
    "cn": "Ian",
    "sn": "Wienand",
    "gidNumber": "2805",
    "keyFingerPrint": "FE3366210A6D0989C247FD6EB6A6FB8B9615AEC8",
    "ircNick": "ianw",
    "labeledURI": "https://www.wienand.org"
  },
  {
    "info": "# ieure, users, debian.org",
    "dn": {
      "uid": "ieure",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "ieure",
    "sn": "Eure",
    "labeledURI": "http://www.debian.org/~ieure/",
    "uidNumber": "2155",
    "ircNick": "Blue",
    "gidNumber": "2155"
  },
  {
    "info": "20091208] resigned 2009-08-29",
    "dn": {
      "uid": "igenibel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Igor",
    "uid": "igenibel",
    "sn": "Genibel",
    "labeledURI": "http://genibel.org/",
    "uidNumber": "2184",
    "ircNick": "igor_",
    "gidNumber": "2184"
  },
  {
    "info": "# igloo, users, debian.org",
    "dn": {
      "uid": "igloo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "igloo",
    "uidNumber": "1250",
    "sn": "Lynagh",
    "ircNick": "Igloo",
    "gidNumber": "1652"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "igor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Igor",
    "uid": "igor",
    "uidNumber": "992",
    "sn": "Grobman",
    "gidNumber": "992"
  },
  {
    "info": "# ijc, users, debian.org",
    "dn": {
      "uid": "ijc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ijc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3324",
    "keyFingerPrint": "9D5CEE01334F46CE2FEF6DC6EC63699779074FA8",
    "cn": "Ian",
    "sn": "Campbell",
    "ircNick": "ijc",
    "labeledURI": "http://www.hellion.org.uk",
    "gidNumber": "3324"
  },
  {
    "info": "# ijones, users, debian.org",
    "dn": {
      "uid": "ijones",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Isaac",
    "uid": "ijones",
    "uidNumber": "2640",
    "sn": "Jones",
    "ircNick": "SyntaxNinja",
    "labeledURI": "http://www.syntaxpolice.org",
    "gidNumber": "2640"
  },
  {
    "info": "# iko, users, debian.org",
    "dn": {
      "uid": "iko",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anders",
    "uid": "iko",
    "uidNumber": "1095",
    "sn": "Hammarquist",
    "keyFingerPrint": "F70A1DFDB1964669CE1F47C2DAFD5959408B7B9B",
    "gidNumber": "1095"
  },
  {
    "info": "k2HWbvVjd1FypjXJxOxmEpqIsfcSdVsQXYLTZpQIDAQAB",
    "dn": {
      "uid": "iliastsi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "iliastsi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3419",
    "gidNumber": "3419",
    "keyFingerPrint": "AD70200113F0D159848504AB0098F6131EB86413",
    "cn": "Ilias",
    "sn": "Tsitsimpis",
    "ircNick": "iliastsi"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "imain",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "imain",
    "uidNumber": "954",
    "sn": "Main",
    "gidNumber": "954"
  },
  {
    "info": "# iml, users, debian.org",
    "dn": {
      "uid": "iml",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "iml",
    "uidNumber": "2032",
    "sn": "Maclaine-cross",
    "gidNumber": "2032"
  },
  {
    "info": "# imurdock, users, debian.org",
    "dn": {
      "uid": "imurdock",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "imurdock",
    "uidNumber": "803",
    "sn": "Murdock",
    "gidNumber": "1703"
  },
  {
    "info": "# indiebio, users, debian.org",
    "dn": {
      "uid": "indiebio",
      "ou": "users",
      "dc": "org"
    },
    "uid": "indiebio",
    "objectClass": "debianDeveloper",
    "uidNumber": "3601",
    "gidNumber": "3601",
    "cn": "Bernelle",
    "sn": "Verster",
    "keyFingerPrint": "E2E81390289F2C8FA7E9ED3F59E6FCB346D609D5"
  },
  {
    "info": "# infinity0, users, debian.org",
    "dn": {
      "uid": "infinity0",
      "ou": "users",
      "dc": "org"
    },
    "uid": "infinity0",
    "objectClass": "debianDeveloper",
    "uidNumber": "3288",
    "cn": "Ximin",
    "sn": "Luo",
    "gidNumber": "3288",
    "ircNick": "infinity0",
    "keyFingerPrint": "0152DF7147EC5E633E0057FB56034877E1F87C35"
  },
  {
    "info": "# ingo, users, debian.org",
    "dn": {
      "uid": "ingo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ingo",
    "uid": "ingo",
    "sn": "Saitz",
    "uidNumber": "2248",
    "ircNick": "Salz",
    "gidNumber": "2248"
  },
  {
    "info": "# intrigeri, users, debian.org",
    "dn": {
      "uid": "intrigeri",
      "ou": "users",
      "dc": "org"
    },
    "uid": "intrigeri",
    "objectClass": "debianDeveloper",
    "uidNumber": "3171",
    "sn": "Intrigeri",
    "cn": "-",
    "keyFingerPrint": "7C84A74CFB12BC439E81BA78C92949B8A63BB098",
    "gidNumber": "3171",
    "ircNick": "intrigeri",
    "labeledURI": "https://gaffer.boum.org/intrigeri/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ioannis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ioannis",
    "uid": "ioannis",
    "uidNumber": "994",
    "sn": "Tambouras",
    "gidNumber": "994"
  },
  {
    "info": "# irl, users, debian.org",
    "dn": {
      "uid": "irl",
      "ou": "users",
      "dc": "org"
    },
    "uid": "irl",
    "objectClass": "debianDeveloper",
    "uidNumber": "3360",
    "cn": "Iain",
    "sn": "Learmonth",
    "gidNumber": "3360",
    "ircNick": "irl",
    "jabberJID": "irl@jabber.fsfe.org",
    "labeledURI": "http://www.debiain.org/"
  },
  {
    "info": "# isaac, users, debian.org",
    "dn": {
      "uid": "isaac",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Isaac",
    "uid": "isaac",
    "sn": "Clerencia",
    "uidNumber": "2687",
    "ircNick": "isaac",
    "labeledURI": "http://people.warp.es/~isaac/blog/",
    "gidNumber": "2687"
  },
  {
    "info": "# ishikawa, users, debian.org",
    "dn": {
      "uid": "ishikawa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mutsumi",
    "uid": "ishikawa",
    "sn": "Ishikawa",
    "uidNumber": "1388",
    "ircNick": "HANZUBON",
    "labeledURI": "http://hanzubon.jp/",
    "keyFingerPrint": "B5D82EE65EC1F4704AF16D0E686DD060155490E0",
    "gidNumber": "1388"
  },
  {
    "info": "# iuculano, users, debian.org",
    "dn": {
      "uid": "iuculano",
      "ou": "users",
      "dc": "org"
    },
    "uid": "iuculano",
    "objectClass": "debianDeveloper",
    "uidNumber": "3035",
    "cn": "Giuseppe",
    "sn": "Iuculano",
    "gidNumber": "3035",
    "ircNick": "Derevko",
    "labeledURI": "http://www.iuculano.it"
  },
  {
    "info": "# iustin, users, debian.org",
    "dn": {
      "uid": "iustin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "iustin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3057",
    "keyFingerPrint": "CB94E3AA3B1755D61EBB19A5F66E3E419F84F4DE",
    "cn": "Iustin",
    "sn": "Pop",
    "gidNumber": "3057"
  },
  {
    "info": "# ivan, users, debian.org",
    "dn": {
      "uid": "ivan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ivan",
    "keyFingerPrint": "68CB5B3A48FC18F17D79A94681872F9B54C1C1C9",
    "uid": "ivan",
    "sn": "Kohler",
    "uidNumber": "2275",
    "ircNick": "_ivan",
    "labeledURI": "http://www.freeside.biz/",
    "gidNumber": "2275"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ivans",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ivan",
    "uid": "ivans",
    "uidNumber": "1290",
    "sn": "Stojic",
    "gidNumber": "1290"
  },
  {
    "info": "# ivey, users, debian.org",
    "dn": {
      "uid": "ivey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "ivey",
    "sn": "Ivey",
    "labeledURI": "http://gweezlebur.com/~ivey/",
    "uidNumber": "2071",
    "ircNick": "ivey",
    "gidNumber": "2071"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "ivo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ivo",
    "jabberJID": "zarq@jabber.sk",
    "uid": "ivo",
    "sn": "Timmermans",
    "labeledURI": "http://home.o2w.net/~ivo/",
    "uidNumber": "2102",
    "gidNumber": "2102"
  },
  {
    "info": "# ivodd, users, debian.org",
    "dn": {
      "uid": "ivodd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ivodd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3247",
    "keyFingerPrint": "F07D9E73D66E69748C1626CD824294CD0217E8D8",
    "cn": "Ivo",
    "sn": "De Decker",
    "gidNumber": "3247"
  },
  {
    "info": "# iwamatsu, users, debian.org",
    "dn": {
      "uid": "iwamatsu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "iwamatsu",
    "objectClass": "debianDeveloper",
    "uidNumber": "2994",
    "cn": "Nobuhiro",
    "sn": "Iwamatsu",
    "keyFingerPrint": "5E629EE5232197357B84CF4332247FBB40AD1FA6",
    "gidNumber": "2994",
    "ircNick": "iwamatsu",
    "labeledURI": "http://www.nigauri.org/~iwamatsu/"
  },
  {
    "info": "# iwj, users, debian.org",
    "dn": {
      "uid": "iwj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ian",
    "uid": "iwj",
    "sn": "Jackson",
    "uidNumber": "804",
    "keyFingerPrint": "559AE46C2D6B6D3265E7CBA1E3E3392348B50D39",
    "gidNumber": "1615",
    "ircNick": "Diziet",
    "labeledURI": "http://www.chiark.greenend.org.uk/~ijackson/"
  },
  {
    "info": "# jab, users, debian.org",
    "dn": {
      "uid": "jab",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.jab.org",
    "cn": "Jeff",
    "uid": "jab",
    "uidNumber": "2110",
    "sn": "Breidenbach",
    "keyFingerPrint": "7F38A2F829379B7430349D32A876AE830A49ACD3",
    "gidNumber": "2110"
  },
  {
    "info": "# jack, users, debian.org",
    "dn": {
      "uid": "jack",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://i.cantcode.com",
    "cn": "Jack",
    "uid": "jack",
    "uidNumber": "2601",
    "ircNick": "jack",
    "sn": "Moffitt",
    "gidNumber": "2601"
  },
  {
    "info": "# jackyf, users, debian.org",
    "dn": {
      "uid": "jackyf",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jackyf",
    "objectClass": "debianDeveloper",
    "uidNumber": "3017",
    "cn": "Eugene",
    "sn": "Lyubimkin",
    "keyFingerPrint": "505D0AA78F90F6B23CC296A8A7ED831F59B3A0E8",
    "ircNick": "jackyf",
    "jabberJID": "jackyf@jabber.fsfe.org",
    "gidNumber": "3017"
  },
  {
    "info": "# jaime, users, debian.org",
    "dn": {
      "uid": "jaime",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jaime",
    "uid": "jaime",
    "uidNumber": "2674",
    "sn": "Robles",
    "keyFingerPrint": "3FC15BD6AAC3FC492F1BECC97B85277384EE1F78",
    "labeledURI": "http://jaime.robles.es",
    "gidNumber": "2674"
  },
  {
    "info": "# jak, users, debian.org",
    "dn": {
      "uid": "jak",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jak",
    "objectClass": "debianDeveloper",
    "uidNumber": "2948",
    "cn": "Julian",
    "sn": "Klode",
    "keyFingerPrint": "AEE1C8AAAAF0B7684019C546021B361B6B031B00",
    "gidNumber": "2948",
    "ircNick": "juliank",
    "jabberJID": "juliank@jabber.ccc.de",
    "labeledURI": "http://jak-linux.org/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jakob",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jakob",
    "uid": "jakob",
    "sn": "Borg",
    "labeledURI": "http://jakob.borg.pp.se/",
    "uidNumber": "1276",
    "gidNumber": "1658"
  },
  {
    "info": "# jaldhar, users, debian.org",
    "dn": {
      "uid": "jaldhar",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.braincells.com/jaldhar/",
    "cn": "Jaldhar",
    "uid": "jaldhar",
    "uidNumber": "1008",
    "ircNick": "jaldhar",
    "sn": "Vyas",
    "keyFingerPrint": "37EF5B0219C30C6E47B251D0EAA638FAC4794DAF",
    "gidNumber": "1008"
  },
  {
    "info": "# jalvarez, users, debian.org",
    "dn": {
      "uid": "jalvarez",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Juan",
    "uid": "jalvarez",
    "sn": "Alvarez",
    "uidNumber": "2606",
    "ircNick": "jalvarez",
    "jabberJID": "jalvarez@fluidsignal.com",
    "labeledURI": "http://people.fluidsignal.com/~jalvarez",
    "gidNumber": "2606"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "james",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.and.org/",
    "cn": "James",
    "uid": "james",
    "uidNumber": "2293",
    "ircNick": "geppetto",
    "sn": "Antill",
    "gidNumber": "2293"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jamesjb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.tailrecursion.com/",
    "cn": "James",
    "uid": "jamesjb",
    "uidNumber": "1400",
    "ircNick": "jamesjb",
    "sn": "Bielman",
    "gidNumber": "1400"
  },
  {
    "info": "# jamespage, users, debian.org",
    "dn": {
      "uid": "jamespage",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jamespage",
    "objectClass": "debianDeveloper",
    "uidNumber": "3175",
    "keyFingerPrint": "AB23E9A98422889E08C3838CBFECAECBA0E7D8C3",
    "cn": "James",
    "sn": "Page",
    "ircNick": "jamespage",
    "gidNumber": "3175"
  },
  {
    "info": "# jamessan, users, debian.org",
    "dn": {
      "uid": "jamessan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "uid": "jamessan",
    "uidNumber": "2751",
    "sn": "McCoy",
    "keyFingerPrint": "91BFBF4D6956BD5DF7B72D23DFE691AE331BA3DB",
    "gidNumber": "2751",
    "ircNick": "jamessan",
    "labeledURI": "https://jamessan.com/~jamessan/"
  },
  {
    "info": "# jamuraa, users, debian.org",
    "dn": {
      "uid": "jamuraa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "jamuraa",
    "sn": "Janssen",
    "uidNumber": "2283",
    "keyFingerPrint": "911B70F9D760274148BD0D1CD91315B964C06556",
    "gidNumber": "2283",
    "ircNick": "jamuraa",
    "jabberJID": "jamuraa@gmail.com",
    "labeledURI": "http://base0.net",
    "cn": "Marie"
  },
  {
    "info": "# jan, users, debian.org",
    "dn": {
      "uid": "jan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jan",
    "uid": "jan",
    "sn": "Niehusmann",
    "uidNumber": "2282",
    "keyFingerPrint": "37B12F75F77FD8EE47105A455299B4990437AC41",
    "ircNick": "jannic",
    "jabberJID": "jannic@jabber.gondor.com",
    "gidNumber": "2282"
  },
  {
    "info": "# jandd, users, debian.org",
    "dn": {
      "uid": "jandd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jandd",
    "objectClass": "debianDeveloper",
    "uidNumber": "2989",
    "keyFingerPrint": "B2FF1D95CE8F7A22DF4CF09BA73E0055558FB8DD",
    "cn": "Jan",
    "sn": "Dittberner",
    "gidNumber": "2989",
    "ircNick": "jandd",
    "jabberJID": "jan@dittberner.info",
    "labeledURI": "https://jan.dittberner.info/"
  },
  {
    "info": "# janos, users, debian.org",
    "dn": {
      "uid": "janos",
      "ou": "users",
      "dc": "org"
    },
    "uid": "janos",
    "objectClass": "debianDeveloper",
    "uidNumber": "3225",
    "cn": "Janos",
    "sn": "Guljas",
    "gidNumber": "3225",
    "jabberJID": "janos@resenje.org",
    "labeledURI": "http://www.resenje.org"
  },
  {
    "info": "# jaq, users, debian.org",
    "dn": {
      "uid": "jaq",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jamie",
    "uid": "jaq",
    "sn": "Wilkinson",
    "uidNumber": "2449",
    "keyFingerPrint": "440E7691F86880B4AC9C78B45750F754ED9B2138",
    "gidNumber": "2449",
    "ircNick": "jaq",
    "labeledURI": "http://spacepants.org/"
  },
  {
    "info": "# jaqque, users, debian.org",
    "dn": {
      "uid": "jaqque",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jaqque",
    "sn": "Robinson",
    "uidNumber": "2227",
    "keyFingerPrint": "BCA6F0EF11B23F924BA392296FE413326DC4B226",
    "gidNumber": "2227",
    "ircNick": "jaqque",
    "jabberJID": "jaqque@jabber.com"
  },
  {
    "info": "# jas, users, debian.org",
    "dn": {
      "uid": "jas",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jas",
    "objectClass": "debianDeveloper",
    "uidNumber": "3199",
    "cn": "Simon",
    "sn": "Josefsson",
    "gidNumber": "3199",
    "ircNick": "jas4711",
    "jabberJID": "simon@josefsson.org",
    "labeledURI": "https://blog.josefsson.org/",
    "keyFingerPrint": "B1D2BD1375BECB784CF4F8C4D73CF638C53C06BE"
  },
  {
    "info": "# jason, users, debian.org",
    "dn": {
      "uid": "jason",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jason",
    "uid": "jason",
    "uidNumber": "2353",
    "sn": "Thomas",
    "gidNumber": "2353",
    "ircNick": "freddy"
  },
  {
    "info": "# jathan, users, debian.org",
    "dn": {
      "uid": "jathan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jathan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3589",
    "gidNumber": "3589",
    "keyFingerPrint": "3006B194262224C71607F72B52E45D29AA34EFC5",
    "cn": "Jonathan",
    "sn": "Bustillos",
    "ircNick": "jathan"
  },
  {
    "info": "# jaybonci, users, debian.org",
    "dn": {
      "uid": "jaybonci",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jay",
    "keyFingerPrint": "7DF3D4B58EAD38D84E554E3B68530A812B47DCDE",
    "uid": "jaybonci",
    "sn": "Bonci",
    "uidNumber": "2619",
    "ircNick": "jaybonci",
    "jabberJID": "jaybonci@gmail.com",
    "labeledURI": "http://people.debian.org/~jaybonci",
    "gidNumber": "2619"
  },
  {
    "info": "# jbailey, users, debian.org",
    "dn": {
      "uid": "jbailey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeff",
    "uid": "jbailey",
    "sn": "Bailey",
    "uidNumber": "2278",
    "ircNick": "jbailey",
    "jabberJID": "jbailey@raspberryginger.com",
    "labeledURI": "http://www.raspberryginger.com/jbailey",
    "gidNumber": "2278"
  },
  {
    "info": "# jbelmonte, users, debian.org",
    "dn": {
      "uid": "jbelmonte",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jbelmonte",
    "sn": "Belmonte",
    "uidNumber": "2658",
    "ircNick": "belmo",
    "keyFingerPrint": "46D87F4F70492BCFE3587852B1A88A2FD52D3AF3",
    "gidNumber": "2658"
  },
  {
    "info": "# jbernard, users, debian.org",
    "dn": {
      "uid": "jbernard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jon",
    "uid": "jbernard",
    "uidNumber": "2612",
    "sn": "Bernard",
    "keyFingerPrint": "4DB7E515795C4C73CCA668B069783A9EAD536107",
    "gidNumber": "2612",
    "ircNick": "jbernard",
    "labeledURI": "https://jbernard.io"
  },
  {
    "info": "# jbfavre, users, debian.org",
    "dn": {
      "uid": "jbfavre",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jbfavre",
    "objectClass": "debianDeveloper",
    "uidNumber": "3478",
    "gidNumber": "3478",
    "keyFingerPrint": "96122F32E770733EDEBD190FC5C329EC35C2E2F1",
    "cn": "Jean",
    "sn": "Favre"
  },
  {
    "info": "# jbicha, users, debian.org",
    "dn": {
      "uid": "jbicha",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jbicha",
    "objectClass": "debianDeveloper",
    "uidNumber": "3490",
    "gidNumber": "3490",
    "keyFingerPrint": "4D0BE12F0E4776D8AACE9696E66C775AEBFE6C7D",
    "cn": "Jeremy",
    "sn": "Bicha",
    "ircNick": "jbicha",
    "labeledURI": "https://jeremy.bicha.net/"
  },
  {
    "info": "# jblache, users, debian.org",
    "dn": {
      "uid": "jblache",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Julien",
    "uid": "jblache",
    "sn": "Blache",
    "uidNumber": "2052",
    "ircNick": "jb",
    "labeledURI": "http://people.debian.org/~jblache/",
    "gidNumber": "2052"
  },
  {
    "info": "# jbouse, users, debian.org",
    "dn": {
      "uid": "jbouse",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeremy",
    "uid": "jbouse",
    "sn": "Bouse",
    "uidNumber": "2330",
    "keyFingerPrint": "09C5AB71078F4ACD235B28E5FFCE1C9A4FADF197",
    "gidNumber": "2330",
    "ircNick": "Cyis",
    "jabberJID": "jtbouse@gmail.com",
    "labeledURI": "https://undergrid.net"
  },
  {
    "info": "# jcc, users, debian.org",
    "dn": {
      "uid": "jcc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jcc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3483",
    "gidNumber": "3483",
    "keyFingerPrint": "C7203C0A920670BF94F00BB1B01D1A72AC8DC9A1",
    "cn": "Jonathan",
    "sn": "Carter",
    "ircNick": "highvoltage",
    "jabberJID": "jonathan@jabber.org",
    "labeledURI": "https://jonathancarter.org"
  },
  {
    "info": "# jcfp, users, debian.org",
    "dn": {
      "uid": "jcfp",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jcfp",
    "objectClass": "debianDeveloper",
    "uidNumber": "3620",
    "gidNumber": "3620",
    "keyFingerPrint": "D3F0E02EC45A938E1D67229EC43496655925B604",
    "cn": "Jeroen",
    "sn": "Ploemen",
    "ircNick": "jcfp"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "jchiu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joseph",
    "uid": "jchiu",
    "uidNumber": "2000",
    "sn": "Chiu",
    "gidNumber": "2000"
  },
  {
    "info": "# jcollins, users, debian.org",
    "dn": {
      "uid": "jcollins",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jamin",
    "uid": "jcollins",
    "sn": "Collins",
    "labeledURI": "http://asgardsrealm.net/",
    "uidNumber": "2615",
    "jabberJID": "jcollins@jabber.asgardsrealm.net",
    "gidNumber": "2615"
  },
  {
    "info": "# jcorbier, users, debian.org",
    "dn": {
      "uid": "jcorbier",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jcorbier",
    "objectClass": "debianDeveloper",
    "uidNumber": "3102",
    "sn": "Corbier",
    "ircNick": "toadstool",
    "labeledURI": "http://jeremie.famille-corbier.net/",
    "gidNumber": "3102",
    "cn": ": SsOpcsOpbWll"
  },
  {
    "info": "# jcowgill, users, debian.org",
    "dn": {
      "uid": "jcowgill",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jcowgill",
    "objectClass": "debianDeveloper",
    "uidNumber": "3399",
    "gidNumber": "3399",
    "keyFingerPrint": "CA8463DD0C391573B4C9834FAEED6158020EAFFF",
    "cn": "James",
    "sn": "Cowgill",
    "ircNick": "jcowgill"
  },
  {
    "info": "# jcristau, users, debian.org",
    "dn": {
      "uid": "jcristau",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jcristau",
    "objectClass": "debianDeveloper",
    "uidNumber": "2837",
    "keyFingerPrint": "7B27A3F1A6E18CD9588B4AE8310180050905E40C",
    "cn": "Julien",
    "sn": "Cristau",
    "gidNumber": "2837",
    "ircNick": "jcristau",
    "jabberJID": "julien@cristau.org"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jcs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jorg",
    "uid": "jcs",
    "uidNumber": "1355",
    "sn": "Schuler",
    "gidNumber": "1355"
  },
  {
    "info": "1-5126681,",
    "dn": {
      "uid": "jdassen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ray",
    "uid": "jdassen",
    "sn": "Dassen",
    "labeledURI": "http://www.zensunni.demon.nl",
    "uidNumber": "805",
    "ircNick": "JHM",
    "gidNumber": "1682"
  },
  {
    "info": "# jdg, users, debian.org",
    "dn": {
      "uid": "jdg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Julian",
    "uid": "jdg",
    "sn": "Gilbey",
    "uidNumber": "1279",
    "keyFingerPrint": "725E9D6EE56FCAD6C339A7F259D03CC92BA0FEAE",
    "gidNumber": "1606",
    "labeledURI": "http://people.debian.org/~jdg"
  },
  {
    "info": "# jeans, users, debian.org",
    "dn": {
      "uid": "jeans",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jeans",
    "objectClass": "debianDeveloper",
    "uidNumber": "3174",
    "keyFingerPrint": "65C0F4E0C88A5497C8697A5A1267397E45242799",
    "cn": "Jan",
    "sn": "Struyf",
    "gidNumber": "3174"
  },
  {
    "info": "6219370add25e880439ed46.nhqijqilxf.acm-validations.aws.",
    "dn": {
      "uid": "jeb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "uid": "jeb",
    "sn": "Bromberger",
    "uidNumber": "2322",
    "keyFingerPrint": "859120FE0D9FA6A5B054C775AEC828749D85C53C",
    "gidNumber": "2322",
    "ircNick": "[JEB]",
    "labeledURI": "http://www.james.rcpt.to/"
  },
  {
    "info": "# jef, users, debian.org",
    "dn": {
      "uid": "jef",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jean-Francois",
    "uid": "jef",
    "sn": "Dive",
    "labeledURI": "http://jef.linuxbe.org/",
    "uidNumber": "2484",
    "ircNick": "jdive",
    "gidNumber": "2484"
  },
  {
    "info": "# jeff, users, debian.org",
    "dn": {
      "uid": "jeff",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jefferson",
    "uid": "jeff",
    "sn": "Noxon",
    "uidNumber": "845",
    "gidNumber": "845"
  },
  {
    "info": "# jeffity, users, debian.org",
    "dn": {
      "uid": "jeffity",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jeffity",
    "objectClass": "debianDeveloper",
    "uidNumber": "3472",
    "gidNumber": "3472",
    "keyFingerPrint": "7EA95CB5567FDDBFB119539D5AC47FD9A0F2EB10",
    "cn": "Lisa",
    "sn": "Baron"
  },
  {
    "info": "# jello, users, debian.org",
    "dn": {
      "uid": "jello",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joseph",
    "uid": "jello",
    "uidNumber": "2626",
    "sn": "Nahmias",
    "gidNumber": "2626",
    "keyFingerPrint": "73173B093B03CFB85108AD14B11BD919079A3B98",
    "ircNick": "jello"
  },
  {
    "info": "# jelmer, users, debian.org",
    "dn": {
      "uid": "jelmer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jelmer",
    "objectClass": "debianDeveloper",
    "uidNumber": "2959",
    "cn": "Jelmer",
    "sn": "Vernooij",
    "keyFingerPrint": "DC837EE14A7E37347E87061700806F2BD729A457",
    "gidNumber": "2959",
    "ircNick": "jelmer",
    "jabberJID": "jelmer@jelmer.uk",
    "labeledURI": "https://www.jelmer.uk/"
  },
  {
    "info": "# jenner, users, debian.org",
    "dn": {
      "uid": "jenner",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Igor",
    "uid": "jenner",
    "uidNumber": "2724",
    "sn": "Stroh",
    "ircNick": "jenner",
    "gidNumber": "2724"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jens",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jens",
    "uid": "jens",
    "uidNumber": "1185",
    "sn": "Rosenboom",
    "gidNumber": "1636"
  },
  {
    "info": "# jensen, users, debian.org",
    "dn": {
      "uid": "jensen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jens",
    "uid": "jensen",
    "sn": "Schmalzing",
    "labeledURI": "http://www.theorie.physik.uni-muenchen.de/~jens/",
    "uidNumber": "2266",
    "ircNick": "jensen",
    "gidNumber": "2266"
  },
  {
    "info": "# jeroen, users, debian.org",
    "dn": {
      "uid": "jeroen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeroen",
    "uid": "jeroen",
    "sn": "van Wolffelaar",
    "uidNumber": "2675",
    "ircNick": "jvw",
    "jabberJID": "jeroen@wolffelaar.nl",
    "labeledURI": "http://jeroen.a-eskwadraat.nl",
    "keyFingerPrint": "532E050C8651BC4DBC6D64FE2F68E3DB7BE9A998",
    "gidNumber": "2675"
  },
  {
    "info": "# jerome, users, debian.org",
    "dn": {
      "uid": "jerome",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jerome",
    "uid": "jerome",
    "sn": "Marant",
    "uidNumber": "2279",
    "labeledURI": "http://marant.org",
    "gidNumber": "2279"
  },
  {
    "info": "# jewel, users, debian.org",
    "dn": {
      "uid": "jewel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jewel",
    "uidNumber": "2117",
    "sn": "Leuner",
    "ircNick": "jewel",
    "labeledURI": "http://subvert-the-dominant-paradigm.net/",
    "keyFingerPrint": "1D681C858FEE81D135A12D2AF3F431B892CC23AE",
    "gidNumber": "2117"
  },
  {
    "info": "# jff, users, debian.org",
    "dn": {
      "uid": "jff",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jff",
    "objectClass": "debianDeveloper",
    "uidNumber": "3332",
    "keyFingerPrint": "63E0075FC8D43ABB35AB30EE09F89F3C8CA1D25D",
    "gidNumber": "3332",
    "cn": ": SsO2cmc=",
    "sn": ": RnJpbmdzLUbDvHJzdA=="
  },
  {
    "info": "# jfs, users, debian.org",
    "dn": {
      "uid": "jfs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Javier",
    "uid": "jfs",
    "uidNumber": "1140",
    "keyFingerPrint": "150F0AABAB6D211DC34FE00E8B1F6F48AB257F98",
    "gidNumber": "1140",
    "sn": ": UGXDsWE=",
    "ircNick": "jfs",
    "jabberJID": "javifs@gmail.com",
    "labeledURI": "http://people.debian.org/~jfs/"
  },
  {
    "info": "# jgb, users, debian.org",
    "dn": {
      "uid": "jgb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://people.debian.org/~jgb",
    "cn": "Jesus",
    "uid": "jgb",
    "uidNumber": "2289",
    "sn": "Gonzalez-Barahona",
    "gidNumber": "2289"
  },
  {
    "info": "# jgg, users, debian.org",
    "dn": {
      "uid": "jgg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jason",
    "uid": "jgg",
    "sn": "Gunthorpe",
    "uidNumber": "1083",
    "ircNick": "Culus",
    "gidNumber": "1083"
  },
  {
    "info": "# jgh, users, debian.org",
    "dn": {
      "uid": "jgh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeremey",
    "uid": "jgh",
    "uidNumber": "2015",
    "sn": "Hinton",
    "gidNumber": "2015"
  },
  {
    "info": "# jgoerzen, users, debian.org",
    "dn": {
      "uid": "jgoerzen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jgoerzen",
    "uidNumber": "991",
    "sn": "Goerzen",
    "keyFingerPrint": "276D7B77B69B756C7CB68669DD29F88442839ED3",
    "gidNumber": "991",
    "ircNick": "CosmicRay",
    "labeledURI": "http://www.complete.org/"
  },
  {
    "info": "# jh, users, debian.org",
    "dn": {
      "uid": "jh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "jh",
    "uidNumber": "2403",
    "keyFingerPrint": "2F033C55478C67FAF2A2CCC59F8B68AF86EAA7D3",
    "gidNumber": "2403",
    "cn": ": SsO2cmdlbg==",
    "sn": ": SMOkZ2c=",
    "jabberJID": "jhagg@jabber.org",
    "labeledURI": "https://www.trollhassel.com//"
  },
  {
    "info": "# jhasler, users, debian.org",
    "dn": {
      "uid": "jhasler",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jhasler",
    "uidNumber": "1134",
    "sn": "Hasler",
    "gidNumber": "1687"
  },
  {
    "info": "# jhirche, users, debian.org",
    "dn": {
      "uid": "jhirche",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Johannes",
    "uid": "jhirche",
    "uidNumber": "2287",
    "sn": "Hirche",
    "ircNick": "XWolf",
    "jabberJID": "KaffeeWolf@jabber.ccc.de",
    "gidNumber": "2287"
  },
  {
    "info": "# jhr, users, debian.org",
    "dn": {
      "uid": "jhr",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jhr",
    "objectClass": "debianDeveloper",
    "uidNumber": "3034",
    "cn": "Jan",
    "sn": "Rahm",
    "keyFingerPrint": "79A045FB6FC78F80CE0FED32E083EC151231C465",
    "gidNumber": "3034",
    "ircNick": "jhr"
  },
  {
    "info": "# jim, users, debian.org",
    "dn": {
      "uid": "jim",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jim",
    "uid": "jim",
    "sn": "Pick",
    "labeledURI": "http://jimpick.com/",
    "uidNumber": "947",
    "ircNick": "jpick",
    "gidNumber": "947"
  },
  {
    "info": "# jimmy, users, debian.org",
    "dn": {
      "uid": "jimmy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jimmy",
    "keyFingerPrint": "46B65063C267884B1282170F751AB5DDA79679CC",
    "uid": "jimmy",
    "sn": "Kaplowitz",
    "uidNumber": "2354",
    "gidNumber": "2354",
    "ircNick": "Hydroxide",
    "jabberJID": "jimmy@techhouse.org"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "jjm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.ixtab.org.uk/",
    "cn": "Jon",
    "uid": "jjm",
    "uidNumber": "2358",
    "ircNick": "jjm",
    "sn": "Middleton",
    "gidNumber": "2358"
  },
  {
    "info": "# jjr, users, debian.org",
    "dn": {
      "uid": "jjr",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jjr",
    "objectClass": "debianDeveloper",
    "uidNumber": "3096",
    "cn": "Jeffrey",
    "sn": "Ratcliffe",
    "keyFingerPrint": "463293E4AE33871846F30227B321F203110FCAF3",
    "gidNumber": "3096"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jkominek",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://miranda.org/~jkominek/",
    "cn": "Jay",
    "uid": "jkominek",
    "uidNumber": "1076",
    "ircNick": "Taliesin",
    "sn": "Kominek",
    "gidNumber": "1076"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jkr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathan",
    "uid": "jkr",
    "uidNumber": "909",
    "sn": "Rabone",
    "gidNumber": "909"
  },
  {
    "info": "# jlblanco, users, debian.org",
    "dn": {
      "uid": "jlblanco",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jlblanco",
    "objectClass": "debianDeveloper",
    "uidNumber": "3497",
    "gidNumber": "3497",
    "keyFingerPrint": "77F69DE42318A3B701AD8464D443304FBD70A641",
    "cn": ": Sm9zw6kgTHVpcw==",
    "sn": "Blanco Claraco"
  },
  {
    "info": "# jlines, users, debian.org",
    "dn": {
      "uid": "jlines",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "keyFingerPrint": "A87F09E8B4C54890B17A6DF41F126539964685E7",
    "uid": "jlines",
    "sn": "Lines",
    "uidNumber": "1200",
    "gidNumber": "1672",
    "jabberJID": "john@chat.paladyn.org",
    "labeledURI": "http://paladyn.org/john/"
  },
  {
    "info": "# jljusten, users, debian.org",
    "dn": {
      "uid": "jljusten",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jljusten",
    "objectClass": "debianDeveloper",
    "uidNumber": "3594",
    "gidNumber": "3594",
    "keyFingerPrint": "C27485217414C9DF02316C2E37F99F68CAF992EB",
    "cn": "Jordan",
    "sn": "Justen",
    "ircNick": "jljusten"
  },
  {
    "info": "# jlu, users, debian.org",
    "dn": {
      "uid": "jlu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jlu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3667",
    "gidNumber": "3667",
    "keyFingerPrint": "D5D568B2D34AB32A337944D22EC3F60DE71C0B9D",
    "cn": "James",
    "sn": "Lu",
    "ircNick": "jlu5",
    "labeledURI": "https://jlu5.com"
  },
  {
    "info": "# jluebbe, users, debian.org",
    "dn": {
      "uid": "jluebbe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jluebbe",
    "objectClass": "debianDeveloper",
    "uidNumber": "2871",
    "cn": "Jan",
    "keyFingerPrint": "2376B53EDE22A15829A63D2BBBF68F03E8F37941",
    "ircNick": "Shoragan",
    "jabberJID": "shoragan@totalueberwachung.de",
    "labeledURI": "http://sicherheitsschwankung.de",
    "gidNumber": "2871",
    "sn": ": TMO8YmJl"
  },
  {
    "info": "# jmarler, users, debian.org",
    "dn": {
      "uid": "jmarler",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathan",
    "keyFingerPrint": "15F9573925428D361188CDF49E2B9967C33F126D",
    "uid": "jmarler",
    "sn": "Marler",
    "uidNumber": "1383",
    "ircNick": "JMarler",
    "labeledURI": "http://www.marlermedia.com/",
    "gidNumber": "1383"
  },
  {
    "info": "# jmaslibre, users, debian.org",
    "dn": {
      "uid": "jmaslibre",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jmaslibre",
    "objectClass": "debianDeveloper",
    "uidNumber": "3215",
    "keyFingerPrint": "B3ED4984F65A9AE06511DAF4756BEB4B70D8FB2A",
    "sn": "Abarca",
    "ircNick": "jmaslibre",
    "gidNumber": "3215",
    "cn": ": Sm9zdcOp"
  },
  {
    "info": "# jmintha, users, debian.org",
    "dn": {
      "uid": "jmintha",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jim",
    "uid": "jmintha",
    "sn": "Mintha",
    "uidNumber": "1112",
    "labeledURI": "http://jim.mintha.com/",
    "gidNumber": "1112"
  },
  {
    "info": "# jmkim, users, debian.org",
    "dn": {
      "uid": "jmkim",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jmkim",
    "objectClass": "debianDeveloper",
    "uidNumber": "3574",
    "gidNumber": "3574",
    "keyFingerPrint": "012E4A0679E14EFCDAAE9472D39D8D29BAF36DF8",
    "cn": "Jongmin",
    "sn": "Kim",
    "ircNick": "jmkim",
    "jabberJID": "jmkim@matrix.org",
    "labeledURI": "https://wiki.debian.org/JongminKim"
  },
  {
    "info": "# jmm, users, debian.org",
    "dn": {
      "uid": "jmm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jmm",
    "objectClass": "debianDeveloper",
    "uidNumber": "2765",
    "cn": "Moritz",
    "sn": "Muehlenhoff",
    "keyFingerPrint": "B6E62F3D12AC38495C0DA90510C293B6C37C4E36",
    "gidNumber": "2765",
    "ircNick": "jmm"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jmr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joe",
    "uid": "jmr",
    "uidNumber": "898",
    "sn": "Reinhardt",
    "gidNumber": "898"
  },
  {
    "info": "# jmtd, users, debian.org",
    "dn": {
      "uid": "jmtd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jmtd",
    "objectClass": "debianDeveloper",
    "uidNumber": "2982",
    "sn": "Dowland",
    "keyFingerPrint": "E037CB2A1A0061B943363C8B0907409606AAAAAA",
    "cn": "Jonathan",
    "gidNumber": "2982",
    "ircNick": "Jon",
    "labeledURI": "http://jmtd.net/"
  },
  {
    "info": "# jmunsin, users, debian.org",
    "dn": {
      "uid": "jmunsin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonas",
    "uid": "jmunsin",
    "uidNumber": "1390",
    "sn": "Munsin",
    "ircNick": "IRCNet:EvenFlow efnet:effe",
    "gidNumber": "1390"
  },
  {
    "info": "# jmvalin, users, debian.org",
    "dn": {
      "uid": "jmvalin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jmvalin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3279",
    "keyFingerPrint": "69B29CE9177E6EA2F19900969EBFF2C22D9FDABD",
    "cn": "Jean-Marc",
    "sn": "Valin",
    "gidNumber": "3279"
  },
  {
    "info": "# jmw, users, debian.org",
    "dn": {
      "uid": "jmw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jmw",
    "objectClass": "debianDeveloper",
    "uidNumber": "3109",
    "cn": "Jonathan",
    "sn": "Wiltshire",
    "gidNumber": "3109",
    "ircNick": "jmw",
    "labeledURI": "https://www.jwiltshire.org.uk/",
    "keyFingerPrint": "CA619D65A72A7BADFC96D280196418AAEB74C8A1"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "job",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "job",
    "uidNumber": "1321",
    "sn": "Bogan",
    "gidNumber": "1321"
  },
  {
    "info": "# jochen, users, debian.org",
    "dn": {
      "uid": "jochen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jochen",
    "uid": "jochen",
    "uidNumber": "2630",
    "sn": "Friedrich",
    "gidNumber": "2630",
    "labeledURI": "http://www.bocc.de"
  },
  {
    "info": "# jodal, users, debian.org",
    "dn": {
      "uid": "jodal",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jodal",
    "objectClass": "debianDeveloper",
    "uidNumber": "3385",
    "gidNumber": "3385",
    "keyFingerPrint": "30129ED01430713835CCB5E8BC1256AA6AA6EC5E",
    "cn": "Stein Magnus",
    "sn": "Jodal",
    "ircNick": "jodal",
    "labeledURI": "https://jod.al/"
  },
  {
    "info": "# jodh, users, debian.org",
    "dn": {
      "uid": "jodh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jodh",
    "objectClass": "debianDeveloper",
    "uidNumber": "3292",
    "keyFingerPrint": "80887BA0E28203ADC125E0049D50E144E6357327",
    "cn": "James",
    "sn": "Hunt",
    "gidNumber": "3292"
  },
  {
    "info": "# joe, users, debian.org",
    "dn": {
      "uid": "joe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Johann",
    "uid": "joe",
    "sn": "Botha",
    "uidNumber": "2411",
    "labeledURI": "http://www.swimgeek.com/",
    "gidNumber": "2411"
  },
  {
    "info": "# joel, users, debian.org",
    "dn": {
      "uid": "joel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joel",
    "uid": "joel",
    "sn": "Rosdahl",
    "uidNumber": "1042",
    "keyFingerPrint": "5A939A71A46792CF57866A51996DDA075594ADB8",
    "gidNumber": "1042",
    "ircNick": "jrosdahl"
  },
  {
    "info": "zU35u2EoCkgbV3Q5HOlk57tvKJEwIDAQAB",
    "dn": {
      "uid": "joerg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joerg",
    "uid": "joerg",
    "sn": "Jaspert",
    "uidNumber": "2492",
    "keyFingerPrint": "FBFABDB541B5DC955BD9BA6EDB16CF5BB12525C4",
    "gidNumber": "2492",
    "ircNick": "Ganneff",
    "labeledURI": "http://people.debian.org/~joerg/"
  },
  {
    "info": "# joergland, users, debian.org",
    "dn": {
      "uid": "joergland",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joerg",
    "uid": "joergland",
    "uidNumber": "2421",
    "sn": "Wendland",
    "ircNick": "joergland",
    "labeledURI": "http://www.wendlandnet.de/joerg",
    "gidNumber": "2421"
  },
  {
    "info": "# joey, users, debian.org",
    "dn": {
      "uid": "joey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "joey",
    "sn": "Schulze",
    "uidNumber": "847",
    "ircNick": "Joey",
    "jabberJID": "joey@jabber.infodrom.org",
    "labeledURI": "http://people.debian.org/~joey/",
    "gidNumber": "847"
  },
  {
    "info": "# joeyh, users, debian.org",
    "dn": {
      "uid": "joeyh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joey",
    "uid": "joeyh",
    "sn": "Hess",
    "uidNumber": "933",
    "ircNick": "joeyh",
    "labeledURI": "http://joeyh.name/",
    "gidNumber": "933"
  },
  {
    "info": "# johanvdw, users, debian.org",
    "dn": {
      "uid": "johanvdw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "johanvdw",
    "objectClass": "debianDeveloper",
    "uidNumber": "3333",
    "keyFingerPrint": "CC1CA34C24B7CBD0507C1864E5F451C2A33358C5",
    "cn": "Johan",
    "sn": "Van de Wauw",
    "gidNumber": "3333"
  },
  {
    "info": "# johfel, users, debian.org",
    "dn": {
      "uid": "johfel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "johfel",
    "objectClass": "debianDeveloper",
    "uidNumber": "3182",
    "keyFingerPrint": "477C8339D36D766ED853201C8359193BFDCB09C4",
    "sn": "Soden",
    "cn": "Johann Felix",
    "gidNumber": "3182"
  },
  {
    "info": "20091208] Resigned in <20020704002408.3789202e.jnelson@jamponi.net>",
    "dn": {
      "uid": "john",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathon",
    "uid": "john",
    "uidNumber": "2004",
    "ircNick": "Slimer",
    "sn": "Nelson",
    "gidNumber": "2004"
  },
  {
    "info": "# johnf, users, debian.org",
    "dn": {
      "uid": "johnf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "johnf",
    "uidNumber": "2214",
    "sn": "Ferlito",
    "ircNick": "johnf",
    "jabberJID": "jferlito@gmail.com",
    "labeledURI": "http://www.inodes.org/blog",
    "keyFingerPrint": "A9468EB345CB92B7670DBAD04FFB060C9EC70B95",
    "gidNumber": "2214"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "johnie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Johnie",
    "uid": "johnie",
    "sn": "Ingram",
    "labeledURI": "http://netgod.net/",
    "uidNumber": "942",
    "ircNick": "netgod",
    "gidNumber": "942"
  },
  {
    "info": "# johns, users, debian.org",
    "dn": {
      "uid": "johns",
      "ou": "users",
      "dc": "org"
    },
    "uid": "johns",
    "objectClass": "debianDeveloper",
    "uidNumber": "3105",
    "keyFingerPrint": "A4626CBAFF376039D2D7554497BA9CE761A0963B",
    "cn": "John",
    "sn": "Sullivan",
    "gidNumber": "3105",
    "ircNick": "johns"
  },
  {
    "info": "# jon, users, debian.org",
    "dn": {
      "uid": "jon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathan",
    "keyFingerPrint": "B8F572F14CC831F9013F7B1E8E62E7F764011A8B",
    "uid": "jon",
    "sn": "Oxer",
    "uidNumber": "2519",
    "ircNick": "jon",
    "labeledURI": "http://jon.oxer.com.au/",
    "gidNumber": "2519"
  },
  {
    "info": "# jonas, users, debian.org",
    "dn": {
      "uid": "jonas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonas",
    "uid": "jonas",
    "uidNumber": "1202",
    "sn": "Oberg",
    "ircNick": "jonas",
    "labeledURI": "http://www.coyote.org/~jonas/",
    "gidNumber": "1641"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jonathon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathon",
    "uid": "jonathon",
    "uidNumber": "1147",
    "ircNick": "randal",
    "sn": "Ross",
    "gidNumber": "1147"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jones",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "jones",
    "uidNumber": "1011",
    "sn": "Jones",
    "gidNumber": "1011"
  },
  {
    "info": "# jonny, users, debian.org",
    "dn": {
      "uid": "jonny",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jonny",
    "objectClass": "debianDeveloper",
    "uidNumber": "2950",
    "cn": "Jonny",
    "sn": "Lamb",
    "keyFingerPrint": "A4FA7977AA06AFA87D33380781812383AD289755",
    "ircNick": "jonnylamb",
    "labeledURI": "http://jonnylamb.com/",
    "gidNumber": "2950"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "joost",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joost",
    "uid": "joost",
    "sn": "Kooij",
    "labeledURI": "http://topaz.mdcc.cx/~joost/",
    "uidNumber": "1218",
    "gidNumber": "1645"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "joostje",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://joostje.op.het.net/",
    "cn": "Joost",
    "uid": "joostje",
    "uidNumber": "848",
    "sn": "Witteveen",
    "gidNumber": "848"
  },
  {
    "info": "# joostvb, users, debian.org",
    "dn": {
      "uid": "joostvb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joost",
    "uid": "joostvb",
    "sn": "van Baal",
    "uidNumber": "2161",
    "keyFingerPrint": "B8FAC2E250475B8CE940A91957930DAB0B86B067",
    "gidNumber": "2161",
    "ircNick": "joostvb",
    "labeledURI": "http://mdcc.cx/"
  },
  {
    "info": "# jordens, users, debian.org",
    "dn": {
      "uid": "jordens",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "jordens",
    "uidNumber": "2605",
    "ircNick": "rjo",
    "jabberJID": "rjo@swissjabber.ch",
    "labeledURI": "http://people.debian.org/~jordens",
    "gidNumber": "2605",
    "sn": ": SsO2cmRlbnM="
  },
  {
    "info": "# jordi, users, debian.org",
    "dn": {
      "uid": "jordi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jordi",
    "uid": "jordi",
    "sn": "Mallach",
    "uidNumber": "2033",
    "keyFingerPrint": "E8175486C02929837C286A1625502F6FCBE3CB04",
    "gidNumber": "2033",
    "ircNick": "jordi",
    "jabberJID": "jordi@sindominio.net",
    "labeledURI": "https://oskuro.net/"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "jos",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jos",
    "uidNumber": "2025",
    "sn": "O'Sullivan",
    "ircNick": "johno",
    "labeledURI": "http://www.debian.org",
    "gidNumber": "2025"
  },
  {
    "info": "# josch, users, debian.org",
    "dn": {
      "uid": "josch",
      "ou": "users",
      "dc": "org"
    },
    "uid": "josch",
    "objectClass": "debianDeveloper",
    "uidNumber": "3341",
    "keyFingerPrint": "F83356BBE112B7462A41552F7D5D8C60CF4D3EB4",
    "cn": "Johannes",
    "gidNumber": "3341",
    "sn": "Schauer Marin Rodrigues"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "joschlec",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://vision.cs.arizona.edu/~schlecht/",
    "cn": "Joseph",
    "uid": "joschlec",
    "uidNumber": "2145",
    "sn": "Schlecht",
    "gidNumber": "2145"
  },
  {
    "info": "# josem, users, debian.org",
    "dn": {
      "uid": "josem",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jose",
    "uid": "josem",
    "uidNumber": "2711",
    "sn": "Moya",
    "gidNumber": "2711"
  },
  {
    "info": "# joshk, users, debian.org",
    "dn": {
      "uid": "joshk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joshua",
    "keyFingerPrint": "3F3787880D85019456F741CBA3882EBF78446F26",
    "uid": "joshk",
    "sn": "Kwan",
    "uidNumber": "2655",
    "ircNick": "joshk",
    "labeledURI": "http://triplehelix.org/~joshk/",
    "gidNumber": "2655"
  },
  {
    "info": "# joshua, users, debian.org",
    "dn": {
      "uid": "joshua",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.reverberate.org",
    "cn": "Joshua",
    "uid": "joshua",
    "uidNumber": "2295",
    "ircNick": "habes",
    "sn": "Haberman",
    "gidNumber": "2295"
  },
  {
    "info": "# joss, users, debian.org",
    "dn": {
      "uid": "joss",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Josselin",
    "uid": "joss",
    "sn": "Mouette",
    "uidNumber": "2486",
    "ircNick": "Np237",
    "labeledURI": "http://malsain.org/",
    "keyFingerPrint": "EC49567210C69CAB25DB34EFE78F751770E81554",
    "gidNumber": "2486"
  },
  {
    "info": "# josue, users, debian.org",
    "dn": {
      "uid": "josue",
      "ou": "users",
      "dc": "org"
    },
    "uid": "josue",
    "objectClass": "debianDeveloper",
    "uidNumber": "3418",
    "gidNumber": "3418",
    "keyFingerPrint": "7733B328D2795F5BE2325ADD01509D5CAB4AFD3F",
    "sn": "Ortega",
    "cn": ": Sm9zdcOp",
    "ircNick": "noahfx",
    "labeledURI": "http://josueortega.org"
  },
  {
    "info": "# joussen, users, debian.org",
    "dn": {
      "uid": "joussen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mario",
    "uid": "joussen",
    "uidNumber": "2460",
    "sn": "Joussen",
    "keyFingerPrint": "CD5DEC1753FC15DDCE65491259DF9475A373F5B4",
    "gidNumber": "2460"
  },
  {
    "info": "# joy, users, debian.org",
    "dn": {
      "uid": "joy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Josip",
    "uid": "joy",
    "sn": "Rodin",
    "uidNumber": "1288",
    "ircNick": "Joy",
    "jabberJID": "shallot@jabber.org",
    "labeledURI": "http://joy.debian.net/",
    "keyFingerPrint": "741B5485DB27D5CDC6E75D4D8D29AB07711AE871",
    "gidNumber": "1288"
  },
  {
    "info": "# jpakkane, users, debian.org",
    "dn": {
      "uid": "jpakkane",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jpakkane",
    "objectClass": "debianDeveloper",
    "uidNumber": "3668",
    "gidNumber": "3668",
    "keyFingerPrint": "19E2D6D9B46D8DAA6288F877C24E631BABB1FE70",
    "cn": "Jussi",
    "sn": "Pakkanen"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jpenney",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://egb.2y.net/",
    "cn": "Justin",
    "uid": "jpenney",
    "uidNumber": "2123",
    "ircNick": "DrSpankenstein",
    "sn": "Penney",
    "gidNumber": "2123"
  },
  {
    "info": "# jpenny, users, debian.org",
    "dn": {
      "uid": "jpenny",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "uid": "jpenny",
    "sn": "Penny",
    "uidNumber": "2204",
    "gidNumber": "2204"
  },
  {
    "info": "# jpleau, users, debian.org",
    "dn": {
      "uid": "jpleau",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jpleau",
    "objectClass": "debianDeveloper",
    "uidNumber": "3439",
    "gidNumber": "3439",
    "keyFingerPrint": "2AFE921964D09872D6718AD52611648AC7B0B792",
    "cn": "Jason",
    "sn": "Pleau",
    "ircNick": "jpleau"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jplejacq",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jean",
    "uid": "jplejacq",
    "uidNumber": "985",
    "sn": "LeJacq",
    "gidNumber": "985"
  },
  {
    "info": "# jpmengual, users, debian.org",
    "dn": {
      "uid": "jpmengual",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jpmengual",
    "objectClass": "debianDeveloper",
    "uidNumber": "3565",
    "gidNumber": "3565",
    "keyFingerPrint": "BE167620044D6142A8C68E9D648047B2D723891C",
    "cn": "Jean-Philippe",
    "sn": "MENGUAL",
    "ircNick": "Texou",
    "jabberJID": "jpmengual@debian.org",
    "labeledURI": "http://hypra.fr"
  },
  {
    "info": "# jps, users, debian.org",
    "dn": {
      "uid": "jps",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jens",
    "jabberJID": "jps@jabber.dk",
    "uid": "jps",
    "sn": "Secher",
    "labeledURI": "http://83.73.6.130/~jps/debian",
    "uidNumber": "2609",
    "gidNumber": "2609"
  },
  {
    "info": "# jpuydt, users, debian.org",
    "dn": {
      "uid": "jpuydt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jpuydt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3516",
    "gidNumber": "3516",
    "keyFingerPrint": "812EEFD8A3FBA4ACE4DF114B04C53BD7FE030551",
    "cn": "Julien",
    "sn": "Puydt"
  },
  {
    "info": "# jr, users, debian.org",
    "dn": {
      "uid": "jr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jochen",
    "uid": "jr",
    "uidNumber": "2383",
    "gidNumber": "2383",
    "sn": ": UsO2aHJpZw=="
  },
  {
    "info": "# jredrejo, users, debian.org",
    "dn": {
      "uid": "jredrejo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jredrejo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2843",
    "ircNick": "itais",
    "jabberJID": "jredrejo@jabber.org",
    "labeledURI": "http://www.itais.net",
    "keyFingerPrint": "C3C7AB7305C85849C4BE8BE85E08AFD2A1DE50E9",
    "gidNumber": "2843",
    "cn": ": Sm9zw6k=",
    "sn": ": TC4gUmVkcmVqbyBSb2Ryw61ndWV6"
  },
  {
    "info": "# jreyer, users, debian.org",
    "dn": {
      "uid": "jreyer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3476",
    "gidNumber": "3476",
    "keyFingerPrint": "8826EBE8FCF726EE182E23D779C43E620B039B35",
    "cn": "Jens",
    "sn": "Reyer",
    "uid": "jreyer"
  },
  {
    "info": "# jrnieder, users, debian.org",
    "dn": {
      "uid": "jrnieder",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jrnieder",
    "objectClass": "debianDeveloper",
    "uidNumber": "3234",
    "keyFingerPrint": "521E58F17E96D712AA0FF102DFC671EEB333FA25",
    "cn": "Jonathan",
    "sn": "Nieder",
    "gidNumber": "3234"
  },
  {
    "info": "KGynWKIjqG4r3TD//gwsPzlRtQ6booWABQLYpSLLnkul6wIDAQAB",
    "dn": {
      "uid": "jrtc27",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jrtc27",
    "objectClass": "debianDeveloper",
    "uidNumber": "3451",
    "gidNumber": "3451",
    "keyFingerPrint": "8F58342BEABE1EF4379551FBB193770C186A1C7D",
    "sn": "Clarke",
    "ircNick": "jrtc27",
    "cn": "Jessica"
  },
  {
    "info": "# jru, users, debian.org",
    "dn": {
      "uid": "jru",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jru",
    "objectClass": "debianDeveloper",
    "uidNumber": "3664",
    "gidNumber": "3664",
    "keyFingerPrint": "23EBCF7E8FC47556787100DFA4254072E373042C",
    "cn": "Jakub",
    "sn": ": UnXFvmnEjWth"
  },
  {
    "info": "# jrv, users, debian.org",
    "dn": {
      "uid": "jrv",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://jrv.oddones.org",
    "cn": "James",
    "uid": "jrv",
    "uidNumber": "1015",
    "sn": "Van Zandt",
    "gidNumber": "1015"
  },
  {
    "info": "# js, users, debian.org",
    "dn": {
      "uid": "js",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonas",
    "uid": "js",
    "sn": "Smedegaard",
    "uidNumber": "2301",
    "keyFingerPrint": "9FE3E9C36691A69FF53CC6842C7C3146C1A00121",
    "gidNumber": "2301",
    "ircNick": "jonas",
    "jabberJID": "jonas@jones.dk",
    "labeledURI": "http://dr.jones.dk/"
  },
  {
    "info": "# jsa, users, debian.org",
    "dn": {
      "uid": "jsa",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jsa",
    "objectClass": "debianDeveloper",
    "uidNumber": "2769",
    "cn": "Juergen",
    "sn": "Salk",
    "gidNumber": "2769"
  },
  {
    "info": "20091208] Resigned",
    "dn": {
      "uid": "jsj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Scott",
    "uid": "jsj",
    "uidNumber": "2205",
    "sn": "Jaderholm",
    "gidNumber": "2205"
  },
  {
    "info": "# jsogo, users, debian.org",
    "dn": {
      "uid": "jsogo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jose",
    "uid": "jsogo",
    "sn": "Carlos Garcia Sogo",
    "uidNumber": "2375",
    "gidNumber": "2375",
    "ircNick": "Beowulf",
    "jabberJID": "jsogo@jabber.org",
    "labeledURI": "http://www.tribulaciones.org"
  },
  {
    "info": "# jspricke, users, debian.org",
    "dn": {
      "uid": "jspricke",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jspricke",
    "objectClass": "debianDeveloper",
    "uidNumber": "3375",
    "keyFingerPrint": "EC4784C5B0859A9A9FC5FCF017FBDCBFD9928FF4",
    "cn": "Jochen",
    "sn": "Sprickerhof",
    "gidNumber": "3375",
    "ircNick": "jochensp",
    "labeledURI": "https://jochen.sprickerhof.de"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jstudt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.federated.com/~jim/",
    "cn": "Jim",
    "uid": "jstudt",
    "uidNumber": "1273",
    "sn": "Studt",
    "gidNumber": "1666"
  },
  {
    "info": "# jsw, users, debian.org",
    "dn": {
      "uid": "jsw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jsw",
    "objectClass": "debianDeveloper",
    "uidNumber": "2866",
    "cn": "John",
    "sn": "Wright",
    "keyFingerPrint": "CB3EEC7081E7F3D54D2C3F2ECC79866F99409084",
    "gidNumber": "2866",
    "ircNick": "jsw"
  },
  {
    "info": "# jtarrio, users, debian.org",
    "dn": {
      "uid": "jtarrio",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jacobo",
    "uid": "jtarrio",
    "sn": "Tarrio",
    "uidNumber": "2364",
    "ircNick": "jacobo",
    "jabberJID": "jtarrio@gmail.com",
    "labeledURI": "http://jacobo.tarrio.org/",
    "gidNumber": "2364"
  },
  {
    "info": "# jtaylor, users, debian.org",
    "dn": {
      "uid": "jtaylor",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jtaylor",
    "objectClass": "debianDeveloper",
    "uidNumber": "3179",
    "keyFingerPrint": "8B77BA48391B3CE51C22953132CC4AAC028756FF",
    "cn": "Julian",
    "sn": "Taylor",
    "gidNumber": "3179"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jthom",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jthom",
    "uidNumber": "975",
    "sn": "Thomlinson",
    "gidNumber": "975"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jtravs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "jtravs",
    "uidNumber": "1345",
    "sn": "Travers",
    "gidNumber": "1345"
  },
  {
    "info": "# juanma, users, debian.org",
    "dn": {
      "uid": "juanma",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Juan Manuel",
    "uid": "juanma",
    "sn": "Garcia Molina",
    "uidNumber": "2610",
    "labeledURI": "http://people.debian.org/~juanma/",
    "gidNumber": "2610"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "jughead",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "jughead",
    "uidNumber": "1194",
    "ircNick": "belar",
    "sn": "Leblanc",
    "gidNumber": "1695"
  },
  {
    "info": "# jule, users, debian.org",
    "dn": {
      "uid": "jule",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jule",
    "objectClass": "debianDeveloper",
    "uidNumber": "3347",
    "keyFingerPrint": "B73823B302F8A2426FCA1BBE590AE197EF123736",
    "cn": "Juliane",
    "sn": "Holzt",
    "gidNumber": "3347",
    "ircNick": "julijane"
  },
  {
    "info": "# jules, users, debian.org",
    "dn": {
      "uid": "jules",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jules",
    "uid": "jules",
    "uidNumber": "1222",
    "sn": "Bean",
    "ircNick": "quicksilver",
    "gidNumber": "1643"
  },
  {
    "info": "# julian, users, debian.org",
    "dn": {
      "uid": "julian",
      "ou": "users",
      "dc": "org"
    },
    "uid": "julian",
    "objectClass": "debianDeveloper",
    "uidNumber": "3212",
    "keyFingerPrint": "C2C8904E314CD8FA041D9B00D5FDFC156168BF60",
    "ircNick": "JuN1x",
    "gidNumber": "3212",
    "cn": ": SnVsacOhbg==",
    "sn": ": TW9yZW5vIFBhdGnDsW8="
  },
  {
    "info": "# julien, users, debian.org",
    "dn": {
      "uid": "julien",
      "ou": "users",
      "dc": "org"
    },
    "uid": "julien",
    "objectClass": "debianDeveloper",
    "uidNumber": "3103",
    "cn": "Julien",
    "sn": "Valroff",
    "gidNumber": "3103"
  },
  {
    "info": "# jurij, users, debian.org",
    "dn": {
      "uid": "jurij",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jurij",
    "objectClass": "debianDeveloper",
    "uidNumber": "2795",
    "cn": "Jurij",
    "sn": "Smakov",
    "ircNick": "trave11er",
    "labeledURI": "http://blog.wooyd.org",
    "gidNumber": "2795"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "justin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Justin",
    "uid": "justin",
    "uidNumber": "1246",
    "sn": "Maurer",
    "gidNumber": "1665"
  },
  {
    "info": "# jvalleroy, users, debian.org",
    "dn": {
      "uid": "jvalleroy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jvalleroy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3612",
    "gidNumber": "3612",
    "keyFingerPrint": "7D6ADB750F91085589484BE677C0C75E7B650808",
    "cn": "James",
    "sn": "Valleroy",
    "ircNick": "jvalleroy",
    "jabberJID": "james@jvalleroy.me",
    "labeledURI": "https://jvalleroy.me"
  },
  {
    "info": "# jwest, users, debian.org",
    "dn": {
      "uid": "jwest",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jim",
    "uid": "jwest",
    "sn": "Westveer",
    "uidNumber": "1351",
    "ircNick": "jwest",
    "labeledURI": "http://people.debian.org/~jwest",
    "gidNumber": "1351"
  },
  {
    "info": "# jwilk, users, debian.org",
    "dn": {
      "uid": "jwilk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "jwilk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3052",
    "keyFingerPrint": "CDB5A1243ACDB63009AD07212D4EB3A6015475F5",
    "cn": "Jakub",
    "sn": "Wilk",
    "gidNumber": "3052"
  },
  {
    "info": "# jwl, users, debian.org",
    "dn": {
      "uid": "jwl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://jam.sessionsnet.org",
    "cn": "Jim",
    "uid": "jwl",
    "uidNumber": "1398",
    "ircNick": "jim",
    "sn": "Lynch",
    "gidNumber": "1398"
  },
  {
    "info": "# kaare, users, debian.org",
    "dn": {
      "uid": "kaare",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kaare",
    "objectClass": "debianDeveloper",
    "uidNumber": "3112",
    "keyFingerPrint": "5E5C4B0E1AFD7325CF66500F962680C5305A9418",
    "sn": "Olsen",
    "gidNumber": "3112",
    "cn": ": S8OlcmU=",
    "ircNick": "Kaare",
    "labeledURI": "http://www.nightcall.dk/"
  },
  {
    "info": "# kabi, users, debian.org",
    "dn": {
      "uid": "kabi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Zdenek",
    "uid": "kabi",
    "sn": "Kabelac",
    "uidNumber": "1277",
    "ircNick": "kabi",
    "labeledURI": "http://avifile.sf.net/",
    "gidNumber": "1633"
  },
  {
    "info": "# kaction, users, debian.org",
    "dn": {
      "uid": "kaction",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kaction",
    "objectClass": "debianDeveloper",
    "uidNumber": "3545",
    "gidNumber": "3545",
    "cn": "Dmitry",
    "sn": "Bogatov"
  },
  {
    "info": "# kai, users, debian.org",
    "dn": {
      "uid": "kai",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.khms.westfalen.de/",
    "cn": "Kai",
    "uid": "kai",
    "uidNumber": "1007",
    "sn": "Henningsen",
    "gidNumber": "1007"
  },
  {
    "info": "# kalfa, users, debian.org",
    "dn": {
      "uid": "kalfa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Cosimo",
    "uid": "kalfa",
    "sn": "Alfarano",
    "uidNumber": "2153",
    "ircNick": "KA",
    "jabberJID": "c.alfarano@gmail.com",
    "labeledURI": "http://cosimo.alfarano.net",
    "gidNumber": "2153"
  },
  {
    "info": "# kaliko, users, debian.org",
    "dn": {
      "uid": "kaliko",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kaliko",
    "objectClass": "debianDeveloper",
    "uidNumber": "3646",
    "gidNumber": "3646",
    "keyFingerPrint": "2255310AD1A248A07B597638065FE53932DC551D",
    "cn": "Geoffroy",
    "sn": "Berret",
    "labeledURI": "https://kaliko.me"
  },
  {
    "info": "# kamal, users, debian.org",
    "dn": {
      "uid": "kamal",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kamal",
    "objectClass": "debianDeveloper",
    "uidNumber": "3117",
    "keyFingerPrint": "73EE922658C2E07340EA9613E7F710555409E422",
    "cn": "Kamal",
    "sn": "Mostafa",
    "gidNumber": "3117",
    "ircNick": "kamal"
  },
  {
    "info": "# kaminski, users, debian.org",
    "dn": {
      "uid": "kaminski",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://people.debian.org/~kaminski/",
    "cn": "Alexei",
    "uid": "kaminski",
    "uidNumber": "2435",
    "sn": "Kaminski",
    "gidNumber": "2435"
  },
  {
    "info": "# kamop, users, debian.org",
    "dn": {
      "uid": "kamop",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Atsushi",
    "keyFingerPrint": "2D2B6C13DB888AD4F69FDCD73A0A01731BAE93B9",
    "uid": "kamop",
    "uidNumber": "1032",
    "sn": "Kamoshida",
    "gidNumber": "1032"
  },
  {
    "info": "ification-code=008b7997cc685df0abb7df17433e4ffa",
    "dn": {
      "uid": "kanashiro",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kanashiro",
    "objectClass": "debianDeveloper",
    "uidNumber": "3414",
    "gidNumber": "3414",
    "keyFingerPrint": "8ED6C3F8BAC9DB7FC130A870F823A2729883C97C",
    "cn": "Lucas",
    "sn": "Kanashiro",
    "ircNick": "kanashiro"
  },
  {
    "info": "# kaol, users, debian.org",
    "dn": {
      "uid": "kaol",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kaol",
    "objectClass": "debianDeveloper",
    "uidNumber": "2800",
    "cn": "Kari",
    "sn": "Pahula",
    "keyFingerPrint": "09C397FE531118695A533445840867EE9D8F265C",
    "gidNumber": "2800",
    "ircNick": "kaol"
  },
  {
    "info": "# kapil, users, debian.org",
    "dn": {
      "uid": "kapil",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kapil",
    "objectClass": "debianDeveloper",
    "uidNumber": "2868",
    "cn": "Kapil",
    "sn": "Paranjape",
    "ircNick": "kapil",
    "labeledURI": "http://www.imsc.res.in/~kapil",
    "gidNumber": "2868"
  },
  {
    "info": "# kaplan, users, debian.org",
    "dn": {
      "uid": "kaplan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kaplan",
    "objectClass": "debianDeveloper",
    "uidNumber": "2776",
    "cn": "Lior",
    "sn": "Kaplan",
    "keyFingerPrint": "F931E47E19C27CD0A0AF361A09DA9408B4E14499",
    "gidNumber": "2776",
    "ircNick": "kaplan",
    "labeledURI": "http://liorkaplan.wordpress.com/"
  },
  {
    "info": "# kapouer, users, debian.org",
    "dn": {
      "uid": "kapouer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kapouer",
    "objectClass": "debianDeveloper",
    "uidNumber": "3158",
    "sn": "Lal",
    "keyFingerPrint": "03C4E7ABB880F524306E48156611C05EDD39F374",
    "gidNumber": "3158",
    "cn": ": SsOpcsOpbXk=",
    "ircNick": "kapouer"
  },
  {
    "info": "# karen, users, debian.org",
    "dn": {
      "uid": "karen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "karen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3447",
    "gidNumber": "3447",
    "keyFingerPrint": "3C8422AF5EF0EA21DF3423055F2B9266CCBE0A4E",
    "cn": "Karen",
    "sn": "Sandler"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "karl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Karl",
    "uid": "karl",
    "uidNumber": "849",
    "sn": "Ferguson",
    "gidNumber": "849"
  },
  {
    "info": "# karlb, users, debian.org",
    "dn": {
      "uid": "karlb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.linux-games.com",
    "cn": "Karl",
    "uid": "karlb",
    "uidNumber": "2481",
    "ircNick": "karlb",
    "sn": "Bartel",
    "gidNumber": "2481"
  },
  {
    "info": "20091208] Resigned 5/11/2000 <87lmuyzyh5.fsf@bittersweet.intra>",
    "dn": {
      "uid": "karlheg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Karl",
    "uid": "karlheg",
    "sn": "Hegbloom",
    "labeledURI": "http://people.debian.org/~karlheg/",
    "uidNumber": "1070",
    "ircNick": "karlheg",
    "gidNumber": "1070"
  },
  {
    "info": "# kartik, users, debian.org",
    "dn": {
      "uid": "kartik",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kartik",
    "objectClass": "debianDeveloper",
    "uidNumber": "2930",
    "cn": "Kartik",
    "sn": "Mistry",
    "keyFingerPrint": "6B631B5EF554AF6B1196629202C1D3F2783AA4DE",
    "gidNumber": "2930",
    "ircNick": "kart_",
    "labeledURI": "https://kartikm.wordpress.com"
  },
  {
    "info": "# kawamura, users, debian.org",
    "dn": {
      "uid": "kawamura",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Takao",
    "uid": "kawamura",
    "uidNumber": "1395",
    "sn": "Kawamura",
    "gidNumber": "1395"
  },
  {
    "info": "# kay, users, debian.org",
    "dn": {
      "uid": "kay",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Klaus",
    "uid": "kay",
    "sn": "Reimer",
    "uidNumber": "2187",
    "jabberJID": "k@ailis.de",
    "labeledURI": "http://www.ailis.de/~k/",
    "gidNumber": "2187"
  },
  {
    "info": "# kcoyner, users, debian.org",
    "dn": {
      "uid": "kcoyner",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kcoyner",
    "objectClass": "debianDeveloper",
    "uidNumber": "2875",
    "cn": "Kevin",
    "sn": "Coyner",
    "keyFingerPrint": "FA7FB8C09F87C4A0482F4A86EBB31A62C85D8F71",
    "gidNumber": "2875",
    "labeledURI": "http://rustybear.com"
  },
  {
    "info": "# kcr, users, debian.org",
    "dn": {
      "uid": "kcr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Karl",
    "uid": "kcr",
    "sn": "Ramm",
    "uidNumber": "2286",
    "ircNick": "kcr",
    "labeledURI": "http://www.1ts.org/~kcr/",
    "gidNumber": "2286"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "kdonn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kevin",
    "uid": "kdonn",
    "uidNumber": "1362",
    "sn": "Donnelly",
    "gidNumber": "1362"
  },
  {
    "info": "# kees, users, debian.org",
    "dn": {
      "uid": "kees",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kees",
    "objectClass": "debianDeveloper",
    "uidNumber": "2896",
    "cn": "Kees",
    "sn": "Cook",
    "keyFingerPrint": "A5C3F68F229DD60F723E6E138972F4DFDC6DC026",
    "ircNick": "kees",
    "labeledURI": "http://outflux.net/",
    "gidNumber": "2896"
  },
  {
    "info": "# keithp, users, debian.org",
    "dn": {
      "uid": "keithp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Keith",
    "uid": "keithp",
    "sn": "Packard",
    "uidNumber": "2672",
    "keyFingerPrint": "C383B778255613DFDB409D91DB221A6900000011",
    "ircNick": "keithp",
    "labeledURI": "http://keithp.com",
    "gidNumber": "2672"
  },
  {
    "info": "# keithw, users, debian.org",
    "dn": {
      "uid": "keithw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "keithw",
    "objectClass": "debianDeveloper",
    "uidNumber": "3190",
    "keyFingerPrint": "B1A47069121F6642BB3D7F3E20B7283AFE254C69",
    "cn": "Keith",
    "sn": "Winstein",
    "gidNumber": "3190"
  },
  {
    "info": "# kelbert, users, debian.org",
    "dn": {
      "uid": "kelbert",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jean-Michel",
    "uid": "kelbert",
    "sn": "Kelbert",
    "uidNumber": "2439",
    "gidNumber": "2439",
    "keyFingerPrint": "E11D1959BD866A8C37C7DE0D437FE24F9543CB69",
    "ircNick": "jmk"
  },
  {
    "info": "# kengyu, users, debian.org",
    "dn": {
      "uid": "kengyu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kengyu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3573",
    "gidNumber": "3573",
    "keyFingerPrint": "770B5CDBFB4B868B61434845C617869F1478504E",
    "cn": "Keng-Yu",
    "sn": "Lin",
    "ircNick": "kengyu",
    "labeledURI": "https://www.lexical.tw"
  },
  {
    "info": "# kenhys, users, debian.org",
    "dn": {
      "uid": "kenhys",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kenhys",
    "objectClass": "debianDeveloper",
    "uidNumber": "3396",
    "gidNumber": "3396",
    "keyFingerPrint": "D92025640886D27D14A9EE02D22C1A883455D448",
    "cn": "HAYASHI",
    "sn": "Kentaro",
    "ircNick": "kenhys",
    "labeledURI": "https://xdump.org"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "kenneth",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kenneth",
    "uid": "kenneth",
    "uidNumber": "1018",
    "sn": "MacDonald",
    "gidNumber": "1018"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "kenny",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kenny",
    "uid": "kenny",
    "uidNumber": "850",
    "sn": "Wickstrom",
    "gidNumber": "850"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "kevin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kevin",
    "uid": "kevin",
    "uidNumber": "851",
    "sn": "Dalley",
    "gidNumber": "851"
  },
  {
    "info": "# keving, users, debian.org",
    "dn": {
      "uid": "keving",
      "ou": "users",
      "dc": "org"
    },
    "uid": "keving",
    "objectClass": "debianDeveloper",
    "uidNumber": "2923",
    "cn": "Kevin",
    "sn": "Glynn",
    "gidNumber": "2923",
    "ircNick": "kagy",
    "jabberJID": "kevin.glynn@gmail.com"
  },
  {
    "info": "# kevko, users, debian.org",
    "dn": {
      "uid": "kevko",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kevko",
    "objectClass": "debianDeveloper",
    "uidNumber": "3571",
    "gidNumber": "3571",
    "keyFingerPrint": "E574265EAFFE3C4A40FAA18D4A0CF639427884E3",
    "cn": "Michal",
    "sn": "Arbet",
    "ircNick": "kevko"
  },
  {
    "info": "# keybuk, users, debian.org",
    "dn": {
      "uid": "keybuk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Scott James",
    "uid": "keybuk",
    "sn": "Remnant",
    "uidNumber": "2442",
    "ircNick": "Keybuk",
    "labeledURI": "http://www.netsplit.com/",
    "gidNumber": "2442"
  },
  {
    "info": "# khalid, users, debian.org",
    "dn": {
      "uid": "khalid",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Khalid",
    "uid": "khalid",
    "uidNumber": "2742",
    "sn": "Aziz",
    "keyFingerPrint": "2CD818BFF64619EADB582134CD58A1B17ED033FA",
    "gidNumber": "2742"
  },
  {
    "info": "# khkim, users, debian.org",
    "dn": {
      "uid": "khkim",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ki-Heon",
    "uid": "khkim",
    "uidNumber": "2483",
    "sn": "Kim",
    "gidNumber": "2483"
  },
  {
    "info": "# kibi, users, debian.org",
    "dn": {
      "uid": "kibi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kibi",
    "objectClass": "debianDeveloper",
    "uidNumber": "2921",
    "cn": "Cyril",
    "sn": "Brulebois",
    "keyFingerPrint": "B60EBF2984453C70D74CF478FF914AF0C2B35520",
    "gidNumber": "2921",
    "ircNick": "kibi",
    "jabberJID": "kibi@mraw.org",
    "labeledURI": "https://mraw.org/"
  },
  {
    "info": "20091208] Retired (see <1007211173.23827@mail.galaxy.net>)",
    "dn": {
      "uid": "kikutani",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kikutani",
    "uid": "kikutani",
    "uidNumber": "1324",
    "sn": "Makoto",
    "gidNumber": "1324"
  },
  {
    "info": "# kilian, users, debian.org",
    "dn": {
      "uid": "kilian",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kilian",
    "uid": "kilian",
    "uidNumber": "2740",
    "sn": "Krause",
    "keyFingerPrint": "8381EF07EF163D3A9754A6E9EF758EAF49FA435C",
    "gidNumber": "2740",
    "ircNick": "kilian",
    "jabberJID": "sonnenfinsternis@jabber.sk"
  },
  {
    "info": "# killer, users, debian.org",
    "dn": {
      "uid": "killer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kalle",
    "uid": "killer",
    "uidNumber": "2627",
    "sn": "Kivimaa",
    "ircNick": "Killeri",
    "labeledURI": "http://kivimaa.fi/kalle",
    "gidNumber": "2627"
  },
  {
    "info": "# kilobyte, users, debian.org",
    "dn": {
      "uid": "kilobyte",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kilobyte",
    "objectClass": "debianDeveloper",
    "uidNumber": "3395",
    "gidNumber": "3395",
    "keyFingerPrint": "FD9CE2D8D7754B78AB279BBD2C3B436FEAC68101",
    "cn": "Adam",
    "sn": "Borowski",
    "ircNick": "kilobyte"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "kims",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kim",
    "uid": "kims",
    "uidNumber": "2447",
    "ircNick": "snaund",
    "sn": "Saunders",
    "gidNumber": "2447"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "kirby",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joe",
    "uid": "kirby",
    "uidNumber": "852",
    "sn": "Kirby",
    "gidNumber": "852"
  },
  {
    "info": "# kirfrank, users, debian.org",
    "dn": {
      "uid": "kirfrank",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frank",
    "uid": "kirfrank",
    "uidNumber": "2402",
    "sn": "Kirschner",
    "ircNick": "Freaky",
    "labeledURI": "http://www.kirschnet.de/",
    "gidNumber": "2402"
  },
  {
    "info": "# kirk, users, debian.org",
    "dn": {
      "uid": "kirk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kirk",
    "uid": "kirk",
    "sn": "Hilliard",
    "uidNumber": "1138",
    "gidNumber": "1138"
  },
  {
    "info": "# kitame, users, debian.org",
    "dn": {
      "uid": "kitame",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Takuo",
    "keyFingerPrint": "DC1A298CE5C95ECE9A0882263D6903DFE1D316DC",
    "uid": "kitame",
    "sn": "KITAME",
    "uidNumber": "1240",
    "ircNick": "takuo",
    "labeledURI": "http://www.google.com/profiles/kitame",
    "gidNumber": "1667"
  },
  {
    "info": "OTBagSznsT2gdz/kPUJUdpqw6eUZ7Tjx8SGafsMCGjwIDAQAB",
    "dn": {
      "uid": "kitterman",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kitterman",
    "objectClass": "debianDeveloper",
    "uidNumber": "3079",
    "sn": "Kitterman",
    "cn": "Scott",
    "keyFingerPrint": "E7729BFFBE85400FEEEE23B178D7DEFB9AD59AF1",
    "gidNumber": "3079",
    "ircNick": "ScottK",
    "labeledURI": "http://www.kitterman.com/"
  },
  {
    "info": "# kiwamu, users, debian.org",
    "dn": {
      "uid": "kiwamu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kiwamu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3060",
    "cn": "Kiwamu",
    "sn": "Okabe",
    "gidNumber": "3060"
  },
  {
    "info": "# kix, users, debian.org",
    "dn": {
      "uid": "kix",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kix",
    "objectClass": "debianDeveloper",
    "uidNumber": "3259",
    "cn": "Rodolfo",
    "ircNick": "kix",
    "gidNumber": "3259",
    "sn": ": UGXDsWFz"
  },
  {
    "info": "# kju, users, debian.org",
    "dn": {
      "uid": "kju",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "kju",
    "sn": "Holzt",
    "uidNumber": "1371",
    "ircNick": "kju (mostly on ircnet)",
    "gidNumber": "1371"
  },
  {
    "info": "# kk, users, debian.org",
    "dn": {
      "uid": "kk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Klaus",
    "uid": "kk",
    "sn": "Kettner",
    "uidNumber": "1385",
    "ircNick": "sesom",
    "labeledURI": "http://www.die-kettners.de",
    "gidNumber": "1385"
  },
  {
    "info": "# kkremitzki, users, debian.org",
    "dn": {
      "uid": "kkremitzki",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kkremitzki",
    "objectClass": "debianDeveloper",
    "uidNumber": "3602",
    "gidNumber": "3602",
    "keyFingerPrint": "87C454EC79A26130F91C47EB2948210746DF73C0",
    "cn": "Kurt",
    "sn": "Kremitzki",
    "ircNick": "kkremitzki",
    "labeledURI": "https://www.kwk.systems"
  },
  {
    "info": "# kleptog, users, debian.org",
    "dn": {
      "uid": "kleptog",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kleptog",
    "objectClass": "debianDeveloper",
    "uidNumber": "3203",
    "keyFingerPrint": "AF6FC0D63B8AAF7D942BD2BC4BEDFBE74BE62D44",
    "cn": "Martijn",
    "sn": "van Oosterhout",
    "gidNumber": "3203"
  },
  {
    "info": "# klindsay, users, debian.org",
    "dn": {
      "uid": "klindsay",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kevin",
    "uid": "klindsay",
    "sn": "Lindsay",
    "uidNumber": "2218",
    "ircNick": "Trakker",
    "labeledURI": "http://www.trakker.ca/",
    "gidNumber": "2218"
  },
  {
    "info": "# kmccarty, users, debian.org",
    "dn": {
      "uid": "kmccarty",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kmccarty",
    "objectClass": "debianDeveloper",
    "uidNumber": "2756",
    "cn": "Kevin",
    "sn": "McCarty",
    "labeledURI": "http://people.debian.org/~kmccarty/",
    "gidNumber": "2756"
  },
  {
    "info": "# kmr, users, debian.org",
    "dn": {
      "uid": "kmr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kevin",
    "uid": "kmr",
    "uidNumber": "2510",
    "sn": "Rosenberg",
    "ircNick": "kmr",
    "labeledURI": "http://b9.com/",
    "keyFingerPrint": "B660160C4D7814C6A9FE2A5DE7A48877D686C505",
    "gidNumber": "2510"
  },
  {
    "info": "# kmuto, users, debian.org",
    "dn": {
      "uid": "kmuto",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kenshi",
    "uid": "kmuto",
    "sn": "Muto",
    "uidNumber": "1397",
    "gidNumber": "1397",
    "ircNick": "kmuto",
    "labeledURI": "http://kmuto.jp/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "kneecaps",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "kneecaps",
    "uidNumber": "2360",
    "ircNick": "Kneecaps",
    "sn": "Joseph",
    "gidNumber": "2360"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "knghtbrd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joseph",
    "uid": "knghtbrd",
    "uidNumber": "1242",
    "sn": "Carter",
    "gidNumber": "1705"
  },
  {
    "info": "# knok, users, debian.org",
    "dn": {
      "uid": "knok",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Takatsugu",
    "uid": "knok",
    "uidNumber": "2077",
    "sn": "Nokubi",
    "keyFingerPrint": "99452B4DD28BEEAC8AB5C931B06662EC9C0C1404",
    "gidNumber": "2077",
    "ircNick": "knok",
    "labeledURI": "http://www.daionet.gr.jp/~knok/"
  },
  {
    "info": "# koala, users, debian.org",
    "dn": {
      "uid": "koala",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.geekhavoc.com",
    "cn": "John",
    "uid": "koala",
    "uidNumber": "2423",
    "ircNick": "koala",
    "sn": "Daily",
    "gidNumber": "2423"
  },
  {
    "info": "# kobla, users, debian.org",
    "dn": {
      "uid": "kobla",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3579",
    "gidNumber": "3579",
    "keyFingerPrint": "746CDD186186342860EB9EB563FE10EAD55D0FDB",
    "cn": ": T25kxZllag==",
    "sn": ": S29ibGnFvmVr",
    "uid": "kobla"
  },
  {
    "info": "# kobold, users, debian.org",
    "dn": {
      "uid": "kobold",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fabio",
    "uid": "kobold",
    "sn": "Tranchitella",
    "uidNumber": "2728",
    "ircNick": "kobold",
    "jabberJID": "kobold@kobold.it",
    "labeledURI": "http://kobold.it",
    "keyFingerPrint": "46050CB27C6F231B00B148A68473062B7CF094BA",
    "gidNumber": "2728"
  },
  {
    "info": "# kobras, users, debian.org",
    "dn": {
      "uid": "kobras",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "kobras",
    "sn": "Kobras",
    "labeledURI": "http://www.tat.physik.uni-tuebingen.de/~kobras/",
    "uidNumber": "2111",
    "ircNick": "belbo",
    "gidNumber": "2111"
  },
  {
    "info": "# kohda, users, debian.org",
    "dn": {
      "uid": "kohda",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Atsuhito",
    "uid": "kohda",
    "uidNumber": "2207",
    "sn": "Kohda",
    "gidNumber": "2207"
  },
  {
    "info": "20091208] 2006-01-21 Retired",
    "dn": {
      "uid": "kokids",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chu-yeon",
    "uid": "kokids",
    "sn": "Park",
    "uidNumber": "1148",
    "ircNick": "KoKIDS",
    "labeledURI": "http://people.debian.org/~kokids",
    "gidNumber": "1148"
  },
  {
    "info": "# kolter, users, debian.org",
    "dn": {
      "uid": "kolter",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kolter",
    "objectClass": "debianDeveloper",
    "uidNumber": "3039",
    "keyFingerPrint": "4D0D537E8C37BC99DFF10B874B077723929D42C3",
    "cn": "Emmanuel",
    "sn": "Bouthenot",
    "ircNick": "kolter",
    "jabberJID": "kolter@im.openics.org",
    "labeledURI": "http://kolter.openics.org/",
    "gidNumber": "3039"
  },
  {
    "info": "# koptein, users, debian.org",
    "dn": {
      "uid": "koptein",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hartmut",
    "uid": "koptein",
    "sn": "Koptein",
    "uidNumber": "882",
    "ircNick": "haggie",
    "gidNumber": "882"
  },
  {
    "info": "# koster, users, debian.org",
    "dn": {
      "uid": "koster",
      "ou": "users",
      "dc": "org"
    },
    "uid": "koster",
    "objectClass": "debianDeveloper",
    "uidNumber": "3072",
    "cn": "Kanru",
    "sn": "Chen",
    "keyFingerPrint": "374FF2AD0A12935FD0B0C84F1B132E01CEC6AD46",
    "gidNumber": "3072",
    "ircNick": "kanru",
    "labeledURI": "https://kanru.info"
  },
  {
    "info": "# kov, users, debian.org",
    "dn": {
      "uid": "kov",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gustavo",
    "uid": "kov",
    "sn": "Noronha",
    "uidNumber": "2223",
    "ircNick": "kov",
    "jabberJID": "kov@jabber.org",
    "labeledURI": "http://kov.eti.br/",
    "keyFingerPrint": "9AE55601A23412FF423FA999D200EB30A0FB5DA6",
    "gidNumber": "2223"
  },
  {
    "info": "# kraai, users, debian.org",
    "dn": {
      "uid": "kraai",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "kraai",
    "sn": "Kraai",
    "uidNumber": "2105",
    "gidNumber": "2105",
    "ircNick": "kraai",
    "labeledURI": "https://ftbfs.org/~kraai/"
  },
  {
    "info": "# krait, users, debian.org",
    "dn": {
      "uid": "krait",
      "ou": "users",
      "dc": "org"
    },
    "uid": "krait",
    "objectClass": "debianDeveloper",
    "uidNumber": "3509",
    "gidNumber": "3509",
    "keyFingerPrint": "885AEADC783E1F842E97B82F1E759A726A9FDD74",
    "cn": "Christopher",
    "sn": "Knadle",
    "ircNick": "krait"
  },
  {
    "info": "# krala, users, debian.org",
    "dn": {
      "uid": "krala",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Antonin",
    "uid": "krala",
    "sn": "Kral",
    "uidNumber": "2309",
    "keyFingerPrint": "CDED1582A8BE02B483D5B0B9F53E93AF3DED42EB",
    "gidNumber": "2309",
    "ircNick": "bobek",
    "labeledURI": "http://www.bobek.cz"
  },
  {
    "info": "# kraxel, users, debian.org",
    "dn": {
      "uid": "kraxel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://bytesex.org/",
    "cn": "Gerd",
    "uid": "kraxel",
    "uidNumber": "2058",
    "sn": "Knorr",
    "gidNumber": "2058"
  },
  {
    "info": "# kreckel, users, debian.org",
    "dn": {
      "uid": "kreckel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.ginac.de/~kreckel/",
    "cn": "Richard",
    "uid": "kreckel",
    "uidNumber": "2216",
    "sn": "Kreckel",
    "keyFingerPrint": "2F1C04A3697FDE6E22FECE95F040BD4C93C85AD2",
    "gidNumber": "2216"
  },
  {
    "info": "# krisrose, users, debian.org",
    "dn": {
      "uid": "krisrose",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kristoffer",
    "uid": "krisrose",
    "uidNumber": "1264",
    "sn": "Rose",
    "ircNick": "krisrose",
    "labeledURI": "http://www.krisrose.net",
    "gidNumber": "1707"
  },
  {
    "info": "70kavxvvefdFWsz9WMEdwWhlNkeLbvi6OlEzblSqTQIDAQAB",
    "dn": {
      "uid": "kritzefitz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kritzefitz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3406",
    "gidNumber": "3406",
    "keyFingerPrint": "1A6F3E639A4467E8C3476525DF6D76C44D696F6B",
    "cn": "Sven",
    "sn": "Bartscher",
    "ircNick": "kritzefitz",
    "jabberJID": "kritzefitz@weltraumschlangen.de"
  },
  {
    "info": "# kroeckx, users, debian.org",
    "dn": {
      "uid": "kroeckx",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kurt",
    "uid": "kroeckx",
    "sn": "Roeckx",
    "uidNumber": "2747",
    "keyFingerPrint": "E5E52560DD91C556DDBDA5D02064C53641C25E5D",
    "gidNumber": "2747",
    "ircNick": "Q"
  },
  {
    "info": "91208]",
    "dn": {
      "uid": "krooger",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathan",
    "uid": "krooger",
    "sn": "Walther",
    "uidNumber": "1192",
    "ircNick": "Dinosaur",
    "labeledURI": "http://reactor-core.org/",
    "gidNumber": "1691"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "ks",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Karl",
    "uid": "ks",
    "sn": "Soderstrom",
    "labeledURI": "http://www.xanadunet.net",
    "uidNumber": "2180",
    "ircNick": "ks",
    "gidNumber": "2180"
  },
  {
    "info": "# kubota, users, debian.org",
    "dn": {
      "uid": "kubota",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tomohiro",
    "uid": "kubota",
    "sn": "KUBOTA",
    "uidNumber": "2059",
    "ircNick": "kubota",
    "labeledURI": "http://www.debian.or.jp/~kubota/",
    "gidNumber": "2059"
  },
  {
    "info": "# kula, users, debian.org",
    "dn": {
      "uid": "kula",
      "ou": "users",
      "dc": "org"
    },
    "uid": "kula",
    "objectClass": "debianDeveloper",
    "uidNumber": "3413",
    "gidNumber": "3413",
    "keyFingerPrint": "3DF1A4DFC732468838BCF121686930DD58C338B3",
    "cn": "Marcin",
    "sn": "Kulisz",
    "ircNick": "kuLa",
    "jabberJID": "vorlock@jabber.org"
  },
  {
    "info": "z4=",
    "dn": {
      "uid": "kyle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kyle",
    "uid": "kyle",
    "sn": "McMartin",
    "uidNumber": "2395",
    "ircNick": "kyle (OFTC) / kylem (freenode)",
    "jabberJID": "jkkm@jabber.org",
    "labeledURI": "http://kyle.mcmartin.ca/",
    "gidNumber": "2395"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "labisso",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "labisso",
    "sn": "LaBissoniere",
    "labeledURI": "http://mantic.hn.org",
    "uidNumber": "2342",
    "ircNick": "labisso",
    "gidNumber": "2342"
  },
  {
    "info": "# lakeland, users, debian.org",
    "dn": {
      "uid": "lakeland",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.cs.otago.ac.nz/postgrads/lakeland",
    "cn": "Corrin",
    "uid": "lakeland",
    "uidNumber": "2244",
    "sn": "Lakeland",
    "gidNumber": "2244"
  },
  {
    "info": "20091208] Resigned",
    "dn": {
      "uid": "lalo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lalo",
    "uid": "lalo",
    "sn": "Martins",
    "labeledURI": "http://zope.gf.com.br/lalo/",
    "uidNumber": "1180",
    "ircNick": "Bastet",
    "gidNumber": "1629"
  },
  {
    "info": "# lamby, users, debian.org",
    "dn": {
      "uid": "lamby",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lamby",
    "objectClass": "debianDeveloper",
    "uidNumber": "2942",
    "cn": "Chris",
    "sn": "Lamb",
    "keyFingerPrint": "C2FE4BD271C139B86C533E461E953E27D4311E58",
    "gidNumber": "2942",
    "ircNick": "lamby",
    "labeledURI": "https://chris-lamb.co.uk"
  },
  {
    "info": "# lamont, users, debian.org",
    "dn": {
      "uid": "lamont",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lamont",
    "uid": "lamont",
    "sn": "Jones",
    "uidNumber": "1231",
    "keyFingerPrint": "BD430448BD8CF1EE1340BBD5D547946327003A3F",
    "gidNumber": "1678",
    "ircNick": "lamont"
  },
  {
    "info": "# lamy, users, debian.org",
    "dn": {
      "uid": "lamy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lamy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3630",
    "gidNumber": "3630",
    "keyFingerPrint": "8BDD013080E9A0E8C0D6A1573B365DE93EBD1CD4",
    "cn": "Julien",
    "sn": "Lamy"
  },
  {
    "info": "kPnljdvKNHViUeawTG51R+4g8DoSFnq0wW8vQIDAQAB",
    "dn": {
      "uid": "laney",
      "ou": "users",
      "dc": "org"
    },
    "uid": "laney",
    "objectClass": "debianDeveloper",
    "uidNumber": "3135",
    "keyFingerPrint": "3D0EFB95E7B5237F16E82258E352D5C51C5041D4",
    "cn": "Iain",
    "sn": "Lane",
    "gidNumber": "3135",
    "ircNick": "laney",
    "jabberJID": "iain@orangesquash.org.uk",
    "labeledURI": "https://orangesquash.org.uk"
  },
  {
    "info": "# lange, users, debian.org",
    "dn": {
      "uid": "lange",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "lange",
    "uidNumber": "2104",
    "sn": "Lange",
    "keyFingerPrint": "B11EE3273F6B2DEB528C93DA2BF8D9FE074BCDE4",
    "gidNumber": "2104",
    "ircNick": "Mrfai",
    "labeledURI": "http://www.informatik.uni-koeln.de/ls-juenger/people/lange"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "lapeyre",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "www.physics.arizona.edu/~lapeyre/",
    "cn": "John",
    "uid": "lapeyre",
    "uidNumber": "1078",
    "sn": "Lapeyre",
    "gidNumber": "1078"
  },
  {
    "info": "# larjona, users, debian.org",
    "dn": {
      "uid": "larjona",
      "ou": "users",
      "dc": "org"
    },
    "uid": "larjona",
    "objectClass": "debianDeveloper",
    "uidNumber": "3365",
    "keyFingerPrint": "445E3AD036903F47E19B37B2F22674467E4AF4A3",
    "cn": "Laura",
    "sn": "Arjona Reina",
    "gidNumber": "3365",
    "ircNick": "larjona",
    "jabberJID": "larjona@jabber.org",
    "labeledURI": "https://wiki.debian.org/LauraArjona"
  },
  {
    "info": "# larryg, users, debian.org",
    "dn": {
      "uid": "larryg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://pobox.com/~irving/",
    "cn": "Larry",
    "uid": "larryg",
    "uidNumber": "1066",
    "sn": "Gilbert",
    "gidNumber": "1066"
  },
  {
    "info": "# lars, users, debian.org",
    "dn": {
      "uid": "lars",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lars",
    "uid": "lars",
    "uidNumber": "808",
    "sn": "Wirzenius",
    "gidNumber": "808",
    "ircNick": "liw",
    "labeledURI": "http://liw.fi/"
  },
  {
    "info": "# lavamind, users, debian.org",
    "dn": {
      "uid": "lavamind",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lavamind",
    "objectClass": "debianDeveloper",
    "uidNumber": "3507",
    "gidNumber": "3507",
    "cn": "Jerome",
    "sn": "Charaoui",
    "ircNick": "lavamind",
    "keyFingerPrint": "95F341D746CF1FC8B05A0ED5D3F900749268E55E"
  },
  {
    "info": "# lawrencc, users, debian.org",
    "dn": {
      "uid": "lawrencc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christopher",
    "uid": "lawrencc",
    "sn": "Lawrence",
    "uidNumber": "2005",
    "ircNick": "lordsutch",
    "jabberJID": "lordsutch@gmail.com",
    "labeledURI": "http://blog.lordsutch.com/",
    "keyFingerPrint": "6958A4100D1830978B3DACB6C9FC41B8F98C66CF",
    "gidNumber": "2005"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "laz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fredrik",
    "uid": "laz",
    "uidNumber": "1331",
    "sn": "Juhlin",
    "gidNumber": "1331"
  },
  {
    "info": "# lazyfrosch, users, debian.org",
    "dn": {
      "uid": "lazyfrosch",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lazyfrosch",
    "objectClass": "debianDeveloper",
    "uidNumber": "3320",
    "keyFingerPrint": "6FBDB41587919A0F4BA96EBA9A358BAB04A4ED27",
    "cn": "Markus",
    "sn": "Frosch",
    "gidNumber": "3320",
    "ircNick": "lazyfrosch",
    "labeledURI": "https://lazyfrosch.de"
  },
  {
    "info": "# lbrenta, users, debian.org",
    "dn": {
      "uid": "lbrenta",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lbrenta",
    "objectClass": "debianDeveloper",
    "uidNumber": "2806",
    "cn": "Ludovic",
    "sn": "Brenta",
    "keyFingerPrint": "55D962BB3022E32672E82C28F1C9A83C08AA3E9C",
    "gidNumber": "2806"
  },
  {
    "info": "# ldrolez, users, debian.org",
    "dn": {
      "uid": "ldrolez",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ludovic",
    "uid": "ldrolez",
    "uidNumber": "2394",
    "sn": "Drolez",
    "keyFingerPrint": "8944F03B2DFCE015306ACF363EABB1CB540A7E68",
    "gidNumber": "2394",
    "labeledURI": "https://drolez.com/blog"
  },
  {
    "info": "# lear, users, debian.org",
    "dn": {
      "uid": "lear",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://trap.mtview.ca.us/~tom/",
    "cn": "Tom",
    "uid": "lear",
    "uidNumber": "1152",
    "sn": "Lear",
    "gidNumber": "1152"
  },
  {
    "info": "# leatherface, users, debian.org",
    "dn": {
      "uid": "leatherface",
      "ou": "users",
      "dc": "org"
    },
    "uid": "leatherface",
    "objectClass": "debianDeveloper",
    "uidNumber": "3230",
    "keyFingerPrint": "34C2252964508E0BCC4BDE66397F3DB1C97A628E",
    "cn": "Julien",
    "sn": "Patriarca",
    "gidNumber": "3230",
    "ircNick": "moulip",
    "labeledURI": "http://www.linuxaddict.fr"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "leblanc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Owen",
    "uid": "leblanc",
    "uidNumber": "891",
    "sn": "LeBlanc",
    "gidNumber": "891"
  },
  {
    "info": "=ed25519",
    "dn": {
      "uid": "lechner",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lechner",
    "objectClass": "debianDeveloper",
    "uidNumber": "3495",
    "gidNumber": "3495",
    "keyFingerPrint": "F04D1C2202E2FA0D018854DAAC58A94F11618EE0",
    "cn": "Felix",
    "sn": "Lechner",
    "ircNick": "lechner"
  },
  {
    "info": "# lee, users, debian.org",
    "dn": {
      "uid": "lee",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lee",
    "objectClass": "debianDeveloper",
    "uidNumber": "3639",
    "gidNumber": "3639",
    "keyFingerPrint": "2FE2163F611C80BE2BF5EE6248D19F46BC99C9B7",
    "cn": "Mark",
    "sn": "Garrett"
  },
  {
    "info": "# leepen, users, debian.org",
    "dn": {
      "uid": "leepen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "leepen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3624",
    "gidNumber": "3624",
    "keyFingerPrint": "506C15A42B0AF5A0A85423EED28A45BF3287D649",
    "cn": "Mark",
    "sn": "Hindley",
    "ircNick": "leepen"
  },
  {
    "info": "# legoktm, users, debian.org",
    "dn": {
      "uid": "legoktm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "legoktm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3540",
    "gidNumber": "3540",
    "keyFingerPrint": "FA1E9F9A41E7F43502CA5D6352FC8E7BEDB7FCA2",
    "cn": "Kunal",
    "sn": "Mehta",
    "ircNick": "legoktm",
    "labeledURI": "https://legoktm.com/"
  },
  {
    "info": "# leist, users, debian.org",
    "dn": {
      "uid": "leist",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yven",
    "uid": "leist",
    "sn": "Leist",
    "uidNumber": "2426",
    "ircNick": "yvjohns",
    "labeledURI": "http://leist.beldesign.de",
    "gidNumber": "2426"
  },
  {
    "info": "# leitao, users, debian.org",
    "dn": {
      "uid": "leitao",
      "ou": "users",
      "dc": "org"
    },
    "uid": "leitao",
    "objectClass": "debianDeveloper",
    "uidNumber": "3430",
    "gidNumber": "3430",
    "keyFingerPrint": "AC8539A6E8F46702CA4A439B35A3939FFC78776D",
    "cn": "Breno",
    "sn": "Leitao",
    "ircNick": "leitao"
  },
  {
    "info": "# lele, users, debian.org",
    "dn": {
      "uid": "lele",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lele",
    "objectClass": "debianDeveloper",
    "uidNumber": "3115",
    "cn": "Gabriele",
    "sn": "Giacone",
    "ircNick": "gg0",
    "keyFingerPrint": "5BD067A29DFCA64B19CC7AD9D4330926497D44FE",
    "gidNumber": "3115"
  },
  {
    "info": "# lenharo, users, debian.org",
    "dn": {
      "uid": "lenharo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lenharo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3479",
    "gidNumber": "3479",
    "keyFingerPrint": "31D80509460EFB31DF4B9629FB0E132DDB0AA5B1",
    "cn": "Daniel",
    "sn": "Souza",
    "ircNick": "lenharo",
    "labeledURI": "http://www.sombra.eti.br"
  },
  {
    "info": "# leo, users, debian.org",
    "dn": {
      "uid": "leo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Carsten",
    "uid": "leo",
    "uidNumber": "1266",
    "sn": "Leonhardt",
    "gidNumber": "1617",
    "keyFingerPrint": "959F775F26A82760E9A6D3B460A68FF308C2BFDB",
    "ircNick": "leo"
  },
  {
    "info": "# lepied, users, debian.org",
    "dn": {
      "uid": "lepied",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frederic",
    "uid": "lepied",
    "uidNumber": "957",
    "sn": "Lepied",
    "ircNick": "Fred",
    "labeledURI": "http://moustix.dyndns.org/fred/",
    "gidNumber": "957"
  },
  {
    "info": "# leutloff, users, debian.org",
    "dn": {
      "uid": "leutloff",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "leutloff",
    "sn": "Leutloff",
    "labeledURI": "http://www.oche.de/~leutloff/",
    "uidNumber": "973",
    "gidNumber": "973"
  },
  {
    "info": "# lewurm, users, debian.org",
    "dn": {
      "uid": "lewurm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lewurm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3555",
    "gidNumber": "3555",
    "keyFingerPrint": "7387F7B9073E5AD91DB205F44E4A159E17A2F564",
    "cn": "Bernhard",
    "sn": "Urban"
  },
  {
    "info": "i5PUkc+",
    "dn": {
      "uid": "lex",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.cc.gatech.edu/~lex/",
    "cn": "Lex",
    "uid": "lex",
    "uidNumber": "2412",
    "sn": "Spoon",
    "gidNumber": "2412"
  },
  {
    "info": "# ley, users, debian.org",
    "dn": {
      "uid": "ley",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sebastian",
    "uid": "ley",
    "uidNumber": "2600",
    "sn": "Ley",
    "ircNick": "Wile_E",
    "gidNumber": "2600"
  },
  {
    "info": "# lfaraone, users, debian.org",
    "dn": {
      "uid": "lfaraone",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lfaraone",
    "objectClass": "debianDeveloper",
    "uidNumber": "3085",
    "cn": "Luke",
    "sn": "Faraone",
    "gidNumber": "3085",
    "keyFingerPrint": "8C823DED10AA8041639E12105ACE8D6E0C14A470",
    "ircNick": "lfaraone",
    "jabberJID": "luke@faraone.cc",
    "labeledURI": "https://luke.wf/"
  },
  {
    "info": "# lfilipoz, users, debian.org",
    "dn": {
      "uid": "lfilipoz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luca",
    "uid": "lfilipoz",
    "uidNumber": "2027",
    "sn": "Filipozzi",
    "keyFingerPrint": "CE98F258BB344F29F4F5AF45B5754A69B6BE608C",
    "gidNumber": "2027",
    "ircNick": "luca",
    "labeledURI": "https://bearded.dev/"
  },
  {
    "info": "# lfousse, users, debian.org",
    "dn": {
      "uid": "lfousse",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Laurent",
    "uid": "lfousse",
    "sn": "Fousse",
    "uidNumber": "2648",
    "ircNick": "yog",
    "jabberJID": "laurent@im.lateralis.org",
    "labeledURI": "http://komite.net/laurent/",
    "gidNumber": "2648"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "lhoeg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lars",
    "uid": "lhoeg",
    "uidNumber": "958",
    "sn": "Hog",
    "gidNumber": "958"
  },
  {
    "info": "# licquia, users, debian.org",
    "dn": {
      "uid": "licquia",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeff",
    "uid": "licquia",
    "sn": "Licquia",
    "uidNumber": "1401",
    "ircNick": "licquia",
    "gidNumber": "1401"
  },
  {
    "info": "# lidaobing, users, debian.org",
    "dn": {
      "uid": "lidaobing",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lidaobing",
    "objectClass": "debianDeveloper",
    "uidNumber": "2971",
    "cn": "LI",
    "sn": "Daobing",
    "ircNick": "lidaobing",
    "jabberJID": "lidaobing@gmail.com",
    "labeledURI": "http://blog.lidaobing.info/",
    "gidNumber": "2971"
  },
  {
    "info": "# lightsey, users, debian.org",
    "dn": {
      "uid": "lightsey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "John",
    "uid": "lightsey",
    "uidNumber": "2684",
    "sn": "Lightsey",
    "keyFingerPrint": "18D805F88178E8A9BD0C097DE44F8016D3630F89",
    "gidNumber": "2684",
    "ircNick": "lightsey",
    "labeledURI": "http://www.nixnuts.net/"
  },
  {
    "info": "# liiwi, users, debian.org",
    "dn": {
      "uid": "liiwi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jaakko",
    "uid": "liiwi",
    "uidNumber": "2317",
    "sn": "Niemi",
    "ircNick": "liiwi",
    "keyFingerPrint": "8B112EB026D4BCBAACB886A245DB6B56EC2D523D",
    "gidNumber": "2317"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "lincoln",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lincoln",
    "uid": "lincoln",
    "uidNumber": "1311",
    "sn": "Myers",
    "gidNumber": "1311"
  },
  {
    "info": "# lindi, users, debian.org",
    "dn": {
      "uid": "lindi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lindi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3139",
    "keyFingerPrint": "9A5C6B0937685F8E11878BD84D249D9B23E6FC3A",
    "cn": "Timo",
    "sn": "Lindfors",
    "ircNick": "lindi-",
    "gidNumber": "3139"
  },
  {
    "info": "# lingnau, users, debian.org",
    "dn": {
      "uid": "lingnau",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anselm",
    "uid": "lingnau",
    "sn": "Lingnau",
    "uidNumber": "1093",
    "labeledURI": "http://www.anselms.net/",
    "keyFingerPrint": "7381CB06184C5C490FE59CED0EEF2AE4AF8165FC",
    "gidNumber": "1093"
  },
  {
    "info": "# lintux, users, debian.org",
    "dn": {
      "uid": "lintux",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wilmer",
    "uid": "lintux",
    "sn": "van der Gaast",
    "uidNumber": "2430",
    "keyFingerPrint": "F4B472B56AC66F1A6ADF9F0E2770D9ADFD6C2A7E",
    "gidNumber": "2430",
    "ircNick": "wilmer",
    "labeledURI": "http://wilmer.gaa.st/"
  },
  {
    "info": "# lisandro, users, debian.org",
    "dn": {
      "uid": "lisandro",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lisandro",
    "objectClass": "debianDeveloper",
    "uidNumber": "3154",
    "keyFingerPrint": "12DDFA84AC23B2BBF04B313CAB645F406286A7D0",
    "gidNumber": "3154",
    "cn": ": TGlzYW5kcm8gRGFtacOhbiBOaWNhbm9y",
    "sn": ": UMOpcmV6IE1leWVy",
    "ircNick": "lisandro",
    "labeledURI": "http://perezmeyer.com.ar/"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "liuk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luca",
    "uid": "liuk",
    "uidNumber": "1021",
    "sn": "Maranzano",
    "gidNumber": "1021"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ljb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://neverborn.org",
    "cn": "Leon",
    "uid": "ljb",
    "uidNumber": "1350",
    "ircNick": "isildur",
    "sn": "Breedt",
    "gidNumber": "1350"
  },
  {
    "info": "# ljlane, users, debian.org",
    "dn": {
      "uid": "ljlane",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Laurence",
    "uid": "ljlane",
    "uidNumber": "1370",
    "sn": "Lane",
    "gidNumber": "1370"
  },
  {
    "info": "# lkajan, users, debian.org",
    "dn": {
      "uid": "lkajan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lkajan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3239",
    "keyFingerPrint": "91B46ECD90312D7C86F9469A9BD2D6409A0C52FA",
    "cn": "Laszlo",
    "sn": "Kajan",
    "gidNumber": "3239"
  },
  {
    "info": "# lmamane, users, debian.org",
    "dn": {
      "uid": "lmamane",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lionel",
    "uid": "lmamane",
    "sn": "Mamane",
    "uidNumber": "2697",
    "keyFingerPrint": "3ED52FEE3D7080106D89D734EED5716B078E43D4",
    "gidNumber": "2697",
    "ircNick": "schoinobates",
    "jabberJID": "master@jabber.mamane.lu",
    "labeledURI": "http://www.mamane.lu/"
  },
  {
    "info": "# lmoore, users, debian.org",
    "dn": {
      "uid": "lmoore",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lantz",
    "uid": "lmoore",
    "uidNumber": "1223",
    "sn": "Moore",
    "gidNumber": "1659"
  },
  {
    "info": "# locutusofborg, users, debian.org",
    "dn": {
      "uid": "locutusofborg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "locutusofborg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3361",
    "keyFingerPrint": "92978A6E195E4921825F7FF0F34F09744E9F5DD9",
    "cn": "Gianfranco",
    "sn": "Costamagna",
    "gidNumber": "3361",
    "ircNick": "LocutusOfBorg"
  },
  {
    "info": "# lohner, users, debian.org",
    "dn": {
      "uid": "lohner",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nils",
    "uid": "lohner",
    "uidNumber": "1193",
    "keyFingerPrint": "D7C225344F275A390F0F96A4DE6EE692F2D89614",
    "sn": "Boeffel",
    "gidNumber": "1673",
    "ircNick": "CQ",
    "labeledURI": "http://people.debian.org/~lohner/"
  },
  {
    "info": "# loic, users, debian.org",
    "dn": {
      "uid": "loic",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Loic",
    "uid": "loic",
    "sn": "Dachary",
    "uidNumber": "2381",
    "keyFingerPrint": "E99FFE83DF73E72FB6B264ED992D23B392F9E4F2",
    "gidNumber": "2381",
    "ircNick": "loic",
    "jabberJID": "dachary@jabber.org",
    "labeledURI": "http://www.dachary.org/loic/"
  },
  {
    "info": "HxoXwoERTVQIDAQAB",
    "dn": {
      "uid": "lolando",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roland",
    "uid": "lolando",
    "sn": "Mas",
    "uidNumber": "2203",
    "ircNick": "Lo-lan-do",
    "jabberJID": "lolando@jabber.org",
    "keyFingerPrint": "B415360F591EB6DD73AFFB83D30DECD2599322FC",
    "gidNumber": "2203"
  },
  {
    "info": "# lool, users, debian.org",
    "dn": {
      "uid": "lool",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "lool",
    "uidNumber": "2750",
    "sn": "Minier",
    "ircNick": "lool",
    "jabberJID": "lool@jabber.org",
    "keyFingerPrint": "EA721EE999D3135F0860E545F114084C08611111",
    "gidNumber": "2750",
    "cn": ": TG/Dr2M="
  },
  {
    "info": "# lopippo, users, debian.org",
    "dn": {
      "uid": "lopippo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lopippo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3134",
    "cn": "Filippo",
    "sn": "Rusconi",
    "keyFingerPrint": "B053304E17D6D419DD9B465141AB484D7694CF42",
    "gidNumber": "3134",
    "ircNick": "lopippo",
    "jabberJID": "filippo.rusconi@universite-paris-saclay.fr",
    "labeledURI": "http://www.msxpertsuite.org"
  },
  {
    "info": "# lowe, users, debian.org",
    "dn": {
      "uid": "lowe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Will",
    "uid": "lowe",
    "sn": "Lowe",
    "uidNumber": "1073",
    "ircNick": "harpo",
    "labeledURI": "http://www.linkedin.com/in/willowe/",
    "gidNumber": "1073"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "lprylli",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Loic",
    "uid": "lprylli",
    "uidNumber": "976",
    "sn": "Prylli",
    "gidNumber": "976"
  },
  {
    "info": "# lss, users, debian.org",
    "dn": {
      "uid": "lss",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lars",
    "uid": "lss",
    "sn": "Steinke",
    "uidNumber": "1248",
    "labeledURI": "http://www.lsweb.de",
    "gidNumber": "1611"
  },
  {
    "info": "# ltd, users, debian.org",
    "dn": {
      "uid": "ltd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "ltd",
    "sn": "Cole",
    "labeledURI": "http://amused.net/z/",
    "uidNumber": "1412",
    "ircNick": "ltd",
    "gidNumber": "1412"
  },
  {
    "info": "# luca, users, debian.org",
    "dn": {
      "uid": "luca",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luca",
    "uid": "luca",
    "uidNumber": "2255",
    "ircNick": "DeWhiskeys",
    "sn": "De Vitis",
    "gidNumber": "2255"
  },
  {
    "info": "# lucab, users, debian.org",
    "dn": {
      "uid": "lucab",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lucab",
    "objectClass": "debianDeveloper",
    "uidNumber": "2891",
    "cn": "Luca",
    "sn": "Bruno",
    "keyFingerPrint": "4C8413AA38176150A8906994BB1A3A854F3BBEBF",
    "gidNumber": "2891",
    "ircNick": "kaeso",
    "jabberJID": "lucab@debian.org"
  },
  {
    "info": "ws.com.",
    "dn": {
      "uid": "lucas",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lucas",
    "objectClass": "debianDeveloper",
    "uidNumber": "2833",
    "cn": "Lucas",
    "sn": "Nussbaum",
    "keyFingerPrint": "FEDEC1CB337BCF509F43C2243914B532F4DFBE99",
    "ircNick": "lucas",
    "jabberJID": "lucas@nussbaum.fr",
    "labeledURI": "http://www.lucas-nussbaum.net/",
    "gidNumber": "2833"
  },
  {
    "info": "# luciano, users, debian.org",
    "dn": {
      "uid": "luciano",
      "ou": "users",
      "dc": "org"
    },
    "uid": "luciano",
    "objectClass": "debianDeveloper",
    "uidNumber": "2877",
    "cn": "Luciano",
    "sn": "Bello",
    "keyFingerPrint": "6B2CC5967BD1BDEA9E589B296EC2DEF68FFE3774",
    "gidNumber": "2877",
    "ircNick": "luciano",
    "jabberJID": "lbello@jabber.org",
    "labeledURI": "http://www.lucianobello.com.ar"
  },
  {
    "info": "# luferbu, users, debian.org",
    "dn": {
      "uid": "luferbu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luis",
    "uid": "luferbu",
    "uidNumber": "2493",
    "sn": "Bustamante",
    "ircNick": "luferbu",
    "gidNumber": "2493"
  },
  {
    "info": "# luigi, users, debian.org",
    "dn": {
      "uid": "luigi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luigi",
    "uid": "luigi",
    "sn": "Gangitano",
    "uidNumber": "2513",
    "keyFingerPrint": "8D485A35FF1E6EB790E50F6D0284F20C2BA97CED",
    "gidNumber": "2513",
    "ircNick": "lg"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "luisgh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luis",
    "uid": "luisgh",
    "uidNumber": "1071",
    "ircNick": "adnix",
    "sn": "Gonzalez",
    "gidNumber": "1071"
  },
  {
    "info": "# luk, users, debian.org",
    "dn": {
      "uid": "luk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Luk",
    "uid": "luk",
    "sn": "Claes",
    "uidNumber": "2695",
    "gidNumber": "2695",
    "ircNick": "luk",
    "labeledURI": "http://people.debian.org/~luk"
  },
  {
    "info": "# lukas, users, debian.org",
    "dn": {
      "uid": "lukas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lukas",
    "uid": "lukas",
    "sn": "Geyer",
    "uidNumber": "2532",
    "ircNick": "JimButton",
    "gidNumber": "2532"
  },
  {
    "info": "# lumag, users, debian.org",
    "dn": {
      "uid": "lumag",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lumag",
    "objectClass": "debianDeveloper",
    "uidNumber": "3544",
    "gidNumber": "3544",
    "keyFingerPrint": "8F88381DD5C873E4AE487DA5199BF1243632046A",
    "cn": "Dmitry",
    "sn": "Eremin-Solenikov"
  },
  {
    "info": "# lumin, users, debian.org",
    "dn": {
      "uid": "lumin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3525",
    "gidNumber": "3525",
    "keyFingerPrint": "638BC75EC1E5C589067E35DE62645EB35F686A8A",
    "cn": "Mo",
    "sn": "Zhou",
    "uid": "lumin",
    "ircNick": "lumin",
    "jabberJID": "@cdluminate (telegram)",
    "labeledURI": "https://people.debian.org/~lumin/"
  },
  {
    "info": "# lunar, users, debian.org",
    "dn": {
      "uid": "lunar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lunar",
    "objectClass": "debianDeveloper",
    "uidNumber": "2856",
    "sn": "Bobbio",
    "ircNick": "Lunar^",
    "jabberJID": "lunar@poivron.org",
    "gidNumber": "2856",
    "cn": ": SsOpcsOpbXk="
  },
  {
    "info": "# lupus, users, debian.org",
    "dn": {
      "uid": "lupus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paolo",
    "uid": "lupus",
    "uidNumber": "1156",
    "ircNick": "lupus",
    "sn": "Molaro",
    "gidNumber": "1156"
  },
  {
    "info": "# lurdan, users, debian.org",
    "dn": {
      "uid": "lurdan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lurdan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3220",
    "keyFingerPrint": "C11CA58C0175C240D58C30C8D27DDE1140A2F113",
    "cn": "Satoru",
    "sn": "KURASHIKI",
    "gidNumber": "3220"
  },
  {
    "info": "# luther, users, debian.org",
    "dn": {
      "uid": "luther",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sven",
    "uid": "luther",
    "uidNumber": "1263",
    "sn": "Luther",
    "gidNumber": "1675"
  },
  {
    "info": "# lutin, users, debian.org",
    "dn": {
      "uid": "lutin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lutin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3040",
    "keyFingerPrint": "5732F0C3999089EEC643F0651106F2005BB6E4A5",
    "cn": "Albin",
    "sn": "Tonnerre",
    "ircNick": "Lutin",
    "jabberJID": "albin@im.apinc.org",
    "gidNumber": "3040"
  },
  {
    "info": "# lwall, users, debian.org",
    "dn": {
      "uid": "lwall",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lucas",
    "uid": "lwall",
    "sn": "Wall",
    "uidNumber": "2696",
    "ircNick": "lwall",
    "labeledURI": "http://www.kadath.com.ar",
    "gidNumber": "2696"
  },
  {
    "info": "erWEBAjio30XM64RU00q3g5Hy2uY+wIDAQAB",
    "dn": {
      "uid": "lyknode",
      "ou": "users",
      "dc": "org"
    },
    "uid": "lyknode",
    "objectClass": "debianDeveloper",
    "uidNumber": "3607",
    "gidNumber": "3607",
    "keyFingerPrint": "EB3091F834711EF432572B201EDBAA3C6926AF92",
    "cn": "Baptiste",
    "sn": "Beauplat",
    "ircNick": "lyknode"
  },
  {
    "info": "# lynbech, users, debian.org",
    "dn": {
      "uid": "lynbech",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "lynbech",
    "uidNumber": "1124",
    "sn": "Lynbech",
    "gidNumber": "1124"
  },
  {
    "info": "# ma, users, debian.org",
    "dn": {
      "uid": "ma",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "keyFingerPrint": "7DDF975E73435ACD725E62159A6027DB1A944AD7",
    "uid": "ma",
    "sn": "Albert",
    "uidNumber": "2312",
    "ircNick": "ma",
    "gidNumber": "2312"
  },
  {
    "info": "# maarten, users, debian.org",
    "dn": {
      "uid": "maarten",
      "ou": "users",
      "dc": "org"
    },
    "uid": "maarten",
    "objectClass": "debianDeveloper",
    "uidNumber": "3318",
    "keyFingerPrint": "B97BD6A80CAC4981091AE547FE558C72A67013C3",
    "cn": "Maarten",
    "sn": "Lankhorst",
    "ircNick": "mlankhorst",
    "labeledURI": "https://mblankhorst.nl",
    "gidNumber": "3318"
  },
  {
    "info": "# macan, users, debian.org",
    "dn": {
      "uid": "macan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eduardo",
    "uid": "macan",
    "sn": "Macan",
    "uidNumber": "1075",
    "ircNick": "macan",
    "labeledURI": "http://eduardo.macan.eng.br",
    "gidNumber": "1075"
  },
  {
    "info": "# mace, users, debian.org",
    "dn": {
      "uid": "mace",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Miah",
    "keyFingerPrint": "7345F2F751F07700FEB7FE2087D5E190EBC73D63",
    "uid": "mace",
    "uidNumber": "2584",
    "sn": "Gregory",
    "gidNumber": "2584",
    "ircNick": "mace"
  },
  {
    "info": "# madamezou, users, debian.org",
    "dn": {
      "uid": "madamezou",
      "ou": "users",
      "dc": "org"
    },
    "uid": "madamezou",
    "objectClass": "debianDeveloper",
    "uidNumber": "3138",
    "cn": "Francesca",
    "sn": "Ciceri",
    "gidNumber": "3138",
    "ircNick": "MadameZou",
    "jabberJID": "madamezou@jabber.org"
  },
  {
    "info": "# madcoder, users, debian.org",
    "dn": {
      "uid": "madcoder",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pierre",
    "uid": "madcoder",
    "uidNumber": "2723",
    "sn": "Habouzit",
    "ircNick": "MadCoder",
    "jabberJID": "madcoder@jabber.org",
    "labeledURI": "http://www.madism.org/",
    "gidNumber": "2723"
  },
  {
    "info": "# madduck, users, debian.org",
    "dn": {
      "uid": "madduck",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "madduck",
    "sn": "Krafft",
    "uidNumber": "2500",
    "keyFingerPrint": "2CCB26BC5C49BC221F20794255C9882D999BBCC4",
    "gidNumber": "2500",
    "ircNick": "madduck",
    "jabberJID": "madduck@madduck.net",
    "labeledURI": "http://people.debian.org/~madduck"
  },
  {
    "info": "# madhack, users, debian.org",
    "dn": {
      "uid": "madhack",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "keyFingerPrint": "7FC70DC0EF31DF837313FE2B77A8F36A3B047084",
    "uid": "madhack",
    "sn": "Markley",
    "uidNumber": "2023",
    "gidNumber": "2023",
    "ircNick": "MadHack"
  },
  {
    "info": "# madkiss, users, debian.org",
    "dn": {
      "uid": "madkiss",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "madkiss",
    "uidNumber": "2574",
    "sn": "Loschwitz",
    "ircNick": "Madkiss",
    "keyFingerPrint": "D7A1478078B370D047A11FB31E08BF280CB9A31A",
    "gidNumber": "2574"
  },
  {
    "info": "# maehara, users, debian.org",
    "dn": {
      "uid": "maehara",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Keita",
    "keyFingerPrint": "6A2E6A5438F1D1D815136CDF27122D42A674A359",
    "uid": "maehara",
    "uidNumber": "1170",
    "sn": "Maehara",
    "ircNick": "maehara",
    "labeledURI": "http://people.debian.org/~maehara/",
    "gidNumber": "1170"
  },
  {
    "info": "# mafm, users, debian.org",
    "dn": {
      "uid": "mafm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mafm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3242",
    "keyFingerPrint": "2A8E80505C486298430DD0937F7606A445DCA80E",
    "cn": "Manuel A.",
    "sn": "Fernandez Montecelo",
    "gidNumber": "3242"
  },
  {
    "info": "# mag, users, debian.org",
    "dn": {
      "uid": "mag",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Magossa'nyi",
    "uid": "mag",
    "uidNumber": "1089",
    "ircNick": "maghu",
    "sn": "A'rpa",
    "gidNumber": "1089"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "magnus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Magnus",
    "uid": "magnus",
    "uidNumber": "2427",
    "sn": "Ekdahl",
    "gidNumber": "2427"
  },
  {
    "info": "# mak, users, debian.org",
    "dn": {
      "uid": "mak",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mak",
    "objectClass": "debianDeveloper",
    "uidNumber": "3243",
    "keyFingerPrint": "D33A3F0CA16B0ACC51A60738494C8A5FBF4DECEB",
    "cn": "Matthias",
    "sn": "Klumpp",
    "gidNumber": "3243",
    "ircNick": "ximion",
    "labeledURI": "https://blog.tenstral.net"
  },
  {
    "info": "# makholm, users, debian.org",
    "dn": {
      "uid": "makholm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "makholm",
    "sn": "Makholm",
    "uidNumber": "1342",
    "ircNick": "brother",
    "labeledURI": "http://peter.makholm.net/",
    "gidNumber": "1342"
  },
  {
    "info": "# mako, users, debian.org",
    "dn": {
      "uid": "mako",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Benjamin",
    "uid": "mako",
    "sn": "Hill",
    "uidNumber": "2098",
    "keyFingerPrint": "E9A271C071964891AA57663D9EA33414F5852F4E",
    "ircNick": "mako",
    "jabberJID": "mako@member.fsf.org",
    "labeledURI": "http://mako.cc/",
    "gidNumber": "2098"
  },
  {
    "info": "# maks, users, debian.org",
    "dn": {
      "uid": "maks",
      "ou": "users",
      "dc": "org"
    },
    "uid": "maks",
    "objectClass": "debianDeveloper",
    "uidNumber": "2847",
    "cn": "Maximilian",
    "sn": "Attems",
    "ircNick": "maks",
    "keyFingerPrint": "B62B5DA4A31446F014F8D66410252E653A91E527",
    "gidNumber": "2847"
  },
  {
    "info": "# malat, users, debian.org",
    "dn": {
      "uid": "malat",
      "ou": "users",
      "dc": "org"
    },
    "uid": "malat",
    "objectClass": "debianDeveloper",
    "uidNumber": "3180",
    "keyFingerPrint": "693367FFAECD8EAACD1F063B0171E1828AE09345",
    "cn": "Mathieu",
    "sn": "Malaterre",
    "gidNumber": "3180",
    "ircNick": "malat",
    "labeledURI": "http://mathieumalaterre.com/"
  },
  {
    "info": "# malattia, users, debian.org",
    "dn": {
      "uid": "malattia",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mattia",
    "uid": "malattia",
    "uidNumber": "2700",
    "sn": "Dongili",
    "keyFingerPrint": "F962EF7FBF67B18C96108BFAE7E6F782283D6300",
    "gidNumber": "2700",
    "ircNick": "malattia"
  },
  {
    "info": "# malc, users, debian.org",
    "dn": {
      "uid": "malc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Malc",
    "uid": "malc",
    "uidNumber": "2001",
    "sn": "Arnold",
    "gidNumber": "2001"
  },
  {
    "info": "# malex, users, debian.org",
    "dn": {
      "uid": "malex",
      "ou": "users",
      "dc": "org"
    },
    "uid": "malex",
    "objectClass": "debianDeveloper",
    "uidNumber": "2767",
    "cn": "Oleksandr",
    "sn": "Moskalenko",
    "ircNick": "malex",
    "labeledURI": "http://debian.tagancha.org",
    "gidNumber": "2767"
  },
  {
    "info": "# mans0954, users, debian.org",
    "dn": {
      "uid": "mans0954",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mans0954",
    "objectClass": "debianDeveloper",
    "uidNumber": "3456",
    "gidNumber": "3456",
    "keyFingerPrint": "217EDE4F32B8C1190C906A653631D4FB7FF2B8B8",
    "cn": "Christopher",
    "sn": "Hoskin"
  },
  {
    "info": "ydbUYW3buvzW2ZmIoHadrRMcW4bhlLsnjwIDAQAB",
    "dn": {
      "uid": "manty",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Santiago",
    "uid": "manty",
    "sn": "Mantinan",
    "uidNumber": "2124",
    "keyFingerPrint": "06A3E5760F611B4BB1A90E68B8688CA3D876D5A3",
    "gidNumber": "2124",
    "ircNick": "manty",
    "labeledURI": "http://manty.net"
  },
  {
    "info": "# manu, users, debian.org",
    "dn": {
      "uid": "manu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "manu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3446",
    "gidNumber": "3446",
    "keyFingerPrint": "F6258DF2BFF9A73F5B1E15C306456DD791E95791",
    "cn": "Emmanuel",
    "sn": "Kasprzyk",
    "ircNick": "marcello^"
  },
  {
    "info": "# manuel, users, debian.org",
    "dn": {
      "uid": "manuel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "manuel",
    "objectClass": "debianDeveloper",
    "uidNumber": "2941",
    "keyFingerPrint": "802EB746852F21A39239C111F16961C8CC8D7957",
    "cn": "Manuel",
    "sn": "Prinz",
    "ircNick": "manuel",
    "gidNumber": "2941"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "maor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guy",
    "uid": "maor",
    "uidNumber": "878",
    "sn": "Maor",
    "gidNumber": "878"
  },
  {
    "info": "# mapache, users, debian.org",
    "dn": {
      "uid": "mapache",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eduardo",
    "uid": "mapache",
    "uidNumber": "2437",
    "keyFingerPrint": "E506FDDEA891E826E7137B4A1E198A792782AF55",
    "gidNumber": "2437",
    "sn": ": VHLDoXBhbmk="
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "marcel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marcel",
    "uid": "marcel",
    "sn": "Harkema",
    "labeledURI": "http://people.debian.org/~marcel/",
    "uidNumber": "1393",
    "ircNick": "marcel",
    "gidNumber": "1393"
  },
  {
    "info": "# marciosouza, users, debian.org",
    "dn": {
      "uid": "marciosouza",
      "ou": "users",
      "dc": "org"
    },
    "uid": "marciosouza",
    "objectClass": "debianDeveloper",
    "uidNumber": "3558",
    "gidNumber": "3558",
    "keyFingerPrint": "B8BF6B0CCCA27CB17427AEE6476DF16D280CDAD3",
    "cn": "Marcio de",
    "sn": "Souza Oliveira",
    "jabberJID": "marciosouza@jabb.im",
    "labeledURI": "http://www.debianbrasilia.org"
  },
  {
    "info": "# marcos, users, debian.org",
    "dn": {
      "uid": "marcos",
      "ou": "users",
      "dc": "org"
    },
    "uid": "marcos",
    "objectClass": "debianDeveloper",
    "uidNumber": "3610",
    "gidNumber": "3610",
    "keyFingerPrint": "7CB8AFFD56032FE35A347D2E6ACCBD0FA3B7447C",
    "cn": "Marcos",
    "sn": "Fouces"
  },
  {
    "info": "# marcot, users, debian.org",
    "dn": {
      "uid": "marcot",
      "ou": "users",
      "dc": "org"
    },
    "uid": "marcot",
    "objectClass": "debianDeveloper",
    "uidNumber": "3053",
    "sn": "Gontijo e Silva",
    "ircNick": "marcot",
    "jabberJID": "marcot@jabber-br.org",
    "labeledURI": "http://wiki.debian.org/MarcoSilva",
    "gidNumber": "3053",
    "cn": ": TWFyY28gVMO6bGlv"
  },
  {
    "info": "# marga, users, debian.org",
    "dn": {
      "uid": "marga",
      "ou": "users",
      "dc": "org"
    },
    "uid": "marga",
    "objectClass": "debianDeveloper",
    "uidNumber": "2759",
    "cn": "Margarita",
    "sn": "Manterola",
    "keyFingerPrint": "00C3E184E8AF12711856DFD280D0A42FF2C850CA",
    "gidNumber": "2759",
    "ircNick": "marga",
    "jabberJID": "margamanterola@gmail.com",
    "labeledURI": "http://www.marga.com.ar/blog/"
  },
  {
    "info": "# marillat, users, debian.org",
    "dn": {
      "uid": "marillat",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "marillat",
    "uidNumber": "2042",
    "sn": "Marillat",
    "keyFingerPrint": "A401FF99368FA1F98152DE755C808C2B65558117",
    "gidNumber": "2042",
    "labeledURI": "https://www.deb-multimedia.org"
  },
  {
    "info": "# mario, users, debian.org",
    "dn": {
      "uid": "mario",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mario",
    "objectClass": "debianDeveloper",
    "uidNumber": "2865",
    "cn": "Mario",
    "sn": "Iseli",
    "gidNumber": "2865"
  },
  {
    "info": "# mark, users, debian.org",
    "dn": {
      "uid": "mark",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "mark",
    "uidNumber": "1004",
    "sn": "Garey",
    "gidNumber": "1004"
  },
  {
    "info": "# markos, users, debian.org",
    "dn": {
      "uid": "markos",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Konstantinos",
    "uid": "markos",
    "sn": "Margaritis",
    "uidNumber": "1357",
    "keyFingerPrint": "3FC37391FECDD3CB550171216606027F6437124C",
    "gidNumber": "1357",
    "ircNick": "markos",
    "labeledURI": "http://freevec.org"
  },
  {
    "info": "# marks, users, debian.org",
    "dn": {
      "uid": "marks",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "marks",
    "uidNumber": "855",
    "sn": "Shuttleworth",
    "gidNumber": "855"
  },
  {
    "info": "# markus, users, debian.org",
    "dn": {
      "uid": "markus",
      "ou": "users",
      "dc": "org"
    },
    "uid": "markus",
    "objectClass": "debianDeveloper",
    "uidNumber": "3291",
    "keyFingerPrint": "ED6762360784E331E25303D6025AFE95AC9DF31B",
    "cn": "Markus",
    "sn": "Wanner",
    "gidNumber": "3291",
    "ircNick": "markus_wanner",
    "labeledURI": "http://www.bluegap.ch/"
  },
  {
    "info": "# martignlo, users, debian.org",
    "dn": {
      "uid": "martignlo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lorenzo",
    "uid": "martignlo",
    "sn": "Martignoni",
    "uidNumber": "2727",
    "ircNick": "martignlo",
    "jabberJID": "martignlo@gmail.com",
    "gidNumber": "2727"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "martijn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.foodfight.org/",
    "cn": "Martijn",
    "uid": "martijn",
    "uidNumber": "2113",
    "ircNick": "Treenaks",
    "sn": "van de Streek",
    "gidNumber": "2113"
  },
  {
    "info": "# martin, users, debian.org",
    "dn": {
      "uid": "martin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "martin",
    "uidNumber": "1051",
    "sn": "Mitchell",
    "gidNumber": "1051"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "martinb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "martinb",
    "sn": "Bialasinski",
    "uidNumber": "1236",
    "ircNick": "surf",
    "gidNumber": "1683"
  },
  {
    "info": "# masklin, users, debian.org",
    "dn": {
      "uid": "masklin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "masklin",
    "uidNumber": "1278",
    "ircNick": "masklin",
    "sn": "Leishman",
    "gidNumber": "1700"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "masoto",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "masoto",
    "uidNumber": "1060",
    "sn": "Soto",
    "gidNumber": "1060"
  },
  {
    "info": "# maswan, users, debian.org",
    "dn": {
      "uid": "maswan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mattias",
    "uid": "maswan",
    "uidNumber": "2719",
    "sn": "Wadenstein",
    "gidNumber": "2719",
    "keyFingerPrint": "6819727735B57C0B8284031FA77F88FDCA2B915D",
    "ircNick": "maswan"
  },
  {
    "info": "# matrix, users, debian.org",
    "dn": {
      "uid": "matrix",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Francois",
    "uid": "matrix",
    "uidNumber": "2167",
    "ircNick": "matrix",
    "sn": "Gurin",
    "gidNumber": "2167"
  },
  {
    "info": "# mats, users, debian.org",
    "dn": {
      "uid": "mats",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mats",
    "uid": "mats",
    "uidNumber": "2563",
    "sn": "Rynge",
    "labeledURI": "http://mats.rynge.net",
    "gidNumber": "2563"
  },
  {
    "info": "20091208] Retired on 2005-05-01",
    "dn": {
      "uid": "matt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.undue.org/",
    "cn": "Matt",
    "uid": "matt",
    "uidNumber": "1365",
    "ircNick": "viper",
    "sn": "Kern",
    "gidNumber": "1365"
  },
  {
    "info": "# mattb, users, debian.org",
    "dn": {
      "uid": "mattb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mattb",
    "objectClass": "debianDeveloper",
    "uidNumber": "2818",
    "cn": "Matthew",
    "sn": "Brown",
    "ircNick": "mattb",
    "jabberJID": "mattb@jabber.meta.net.nz",
    "labeledURI": "http://www.mattb.net.nz/",
    "keyFingerPrint": "DBB764A1797D2E21C7993CD7A628CB5FA48F065A",
    "gidNumber": "2818"
  },
  {
    "info": "# matthew, users, debian.org",
    "dn": {
      "uid": "matthew",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "matthew",
    "uidNumber": "1333",
    "sn": "Vernon",
    "keyFingerPrint": "BA4EF9C84DF96D37D8A1E2D412F4D21C8F6A63C8",
    "gidNumber": "1333",
    "ircNick": "Emperor",
    "labeledURI": "http://www.principate.org/"
  },
  {
    "info": "# matthieu, users, debian.org",
    "dn": {
      "uid": "matthieu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthieu",
    "uid": "matthieu",
    "uidNumber": "2432",
    "ircNick": "matthieu",
    "sn": "Delahaye",
    "gidNumber": "2432"
  },
  {
    "info": "# matthieucan, users, debian.org",
    "dn": {
      "uid": "matthieucan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "matthieucan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3336",
    "keyFingerPrint": "721D82EF4102FBEE4EC0164F83E7EA669A19CB94",
    "cn": "Matthieu",
    "sn": "Caneill",
    "gidNumber": "3336",
    "ircNick": "matthieucan"
  },
  {
    "info": "# matthijs, users, debian.org",
    "dn": {
      "uid": "matthijs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "matthijs",
    "objectClass": "debianDeveloper",
    "uidNumber": "2810",
    "cn": "Matthijs",
    "ircNick": "Active2",
    "jabberJID": "Active2@jabber.workaround.org",
    "labeledURI": "http://www.cacholong.nl/",
    "gidNumber": "2810",
    "sn": ": TcO2aGxtYW5u"
  },
  {
    "info": "# matthijskooijman, users, debian.org",
    "dn": {
      "uid": "matthijskooijman",
      "ou": "users",
      "dc": "org"
    },
    "uid": "matthijskooijman",
    "objectClass": "debianDeveloper",
    "uidNumber": "3665",
    "gidNumber": "3665",
    "keyFingerPrint": "E7D0C6A75BEE6D84D638F60A3798AF15A1565658",
    "cn": "Matthijs",
    "sn": "Kooijman"
  },
  {
    "info": "ll",
    "dn": {
      "uid": "mattia",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mattia",
    "objectClass": "debianDeveloper",
    "uidNumber": "3340",
    "keyFingerPrint": "66AE2B4AFCCF3F52DA184D184B043FCDB9444540",
    "cn": "Mattia",
    "sn": "Rizzolo",
    "gidNumber": "3340",
    "ircNick": "mapreri",
    "labeledURI": "https://mapreri.org"
  },
  {
    "info": "# mattice, users, debian.org",
    "dn": {
      "uid": "mattice",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "keyFingerPrint": "050C2D2FA95A25BBB61FCD16EC3BCAC4137A955E",
    "uid": "mattice",
    "sn": "Mattice",
    "uidNumber": "967",
    "gidNumber": "967",
    "ircNick": "LoRez"
  },
  {
    "info": "# mauro, users, debian.org",
    "dn": {
      "uid": "mauro",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mauro",
    "objectClass": "debianDeveloper",
    "uidNumber": "3025",
    "cn": "Mauro",
    "sn": "Lizaur",
    "ircNick": "lvm__",
    "jabberJID": "mauro@debian.org.ar",
    "gidNumber": "3025"
  },
  {
    "info": "# max, users, debian.org",
    "dn": {
      "uid": "max",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://arachni.kiwi.uni-hamburg.de/~harlekin",
    "cn": "Maximilian",
    "uid": "max",
    "uidNumber": "2482",
    "ircNick": "harlekin",
    "sn": "Reiss",
    "gidNumber": "2482"
  },
  {
    "info": "# maxx, users, debian.org",
    "dn": {
      "uid": "maxx",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "maxx",
    "sn": "Wuertele",
    "uidNumber": "2530",
    "keyFingerPrint": "E188D95736CBB351B20404D2BEBAD1FA1B874544",
    "ircNick": "maxx",
    "jabberJID": "maxx@wuertele.net",
    "labeledURI": "http://martin.wuertele.net",
    "gidNumber": "2530"
  },
  {
    "info": "# maxy, users, debian.org",
    "dn": {
      "uid": "maxy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "maxy",
    "objectClass": "debianDeveloper",
    "uidNumber": "2927",
    "cn": "Maximiliano",
    "sn": "Curia",
    "keyFingerPrint": "F8921D3A7404C86E11352215C7197699B29B232A",
    "gidNumber": "2927",
    "ircNick": "maxy",
    "jabberJID": "maxy@gnuservers.com.ar",
    "labeledURI": "http://freak.gnuservers.com.ar/~maxy"
  },
  {
    "info": "# mazen, users, debian.org",
    "dn": {
      "uid": "mazen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mazen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3253",
    "cn": "Mazen",
    "sn": "Neifer",
    "keyFingerPrint": "BAF6C64436107850D4227106B3255C6D55878D8C",
    "gidNumber": "3253"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "mbailey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "mbailey",
    "uidNumber": "811",
    "sn": "Bailey",
    "gidNumber": "811"
  },
  {
    "info": "# mbaker, users, debian.org",
    "dn": {
      "uid": "mbaker",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "mbaker",
    "sn": "Baker",
    "uidNumber": "1044",
    "ircNick": "mark",
    "gidNumber": "1044"
  },
  {
    "info": "# mbanck, users, debian.org",
    "dn": {
      "uid": "mbanck",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mbanck",
    "sn": "Banck",
    "uidNumber": "2380",
    "keyFingerPrint": "9CA877749FAB2E4FA96862ECDC686A27B43481B0",
    "gidNumber": "2380",
    "ircNick": "azeem"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mbc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mbc",
    "sn": "Cardenas",
    "labeledURI": "http://www.hyperpoem.net",
    "uidNumber": "2538",
    "ircNick": "mbc",
    "gidNumber": "2538"
  },
  {
    "info": "# mbehrle, users, debian.org",
    "dn": {
      "uid": "mbehrle",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mbehrle",
    "objectClass": "debianDeveloper",
    "uidNumber": "3417",
    "gidNumber": "3417",
    "keyFingerPrint": "AC297E5C46B9D0B61C717681D6D09BE48405BBF6",
    "cn": "Mathias",
    "sn": "Behrle",
    "ircNick": "yangoon"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mblevin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mitch",
    "uid": "mblevin",
    "uidNumber": "1292",
    "sn": "Blevins",
    "gidNumber": "1292"
  },
  {
    "info": "# mbr, users, debian.org",
    "dn": {
      "uid": "mbr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Markus",
    "uid": "mbr",
    "uidNumber": "2651",
    "sn": "Braun",
    "gidNumber": "2651"
  },
  {
    "info": "# mbrubeck, users, debian.org",
    "dn": {
      "uid": "mbrubeck",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mbrubeck",
    "objectClass": "debianDeveloper",
    "uidNumber": "2764",
    "cn": "Matt",
    "sn": "Brubeck",
    "ircNick": "mbrubeck",
    "labeledURI": "http://advogato.org/person/mbrubeck/",
    "gidNumber": "2764"
  },
  {
    "info": "# mbuck, users, debian.org",
    "dn": {
      "uid": "mbuck",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "mbuck",
    "sn": "Buck",
    "uidNumber": "1003",
    "labeledURI": "http://gromit.dyndns.org/~mb/",
    "keyFingerPrint": "91FF5256A30AE6DF2D44F53BFF55C8F4DAE92422",
    "gidNumber": "1648"
  },
  {
    "info": "# mcasadevall, users, debian.org",
    "dn": {
      "uid": "mcasadevall",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mcasadevall",
    "objectClass": "debianDeveloper",
    "uidNumber": "2957",
    "cn": "Michael",
    "sn": "Casadevall",
    "ircNick": "NCommander",
    "jabberJID": "ncommander@jabber.org",
    "gidNumber": "2957"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mcculley",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gene",
    "uid": "mcculley",
    "sn": "McCulley",
    "labeledURI": "http://enki.org/~mcculley/",
    "uidNumber": "1034",
    "ircNick": "mcculley",
    "gidNumber": "1034"
  },
  {
    "info": "# mckinstry, users, debian.org",
    "dn": {
      "uid": "mckinstry",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alastair",
    "uid": "mckinstry",
    "sn": "McKinstry",
    "uidNumber": "2542",
    "keyFingerPrint": "82383CE9165B347C787081A2CBE6BB4E5D9AD3A5",
    "gidNumber": "2542",
    "ircNick": "amck",
    "jabberJID": "mckinstry@debian.org",
    "labeledURI": "https://diaspora.mckinstry.ie/u/amckinstry"
  },
  {
    "info": "# md, users, debian.org",
    "dn": {
      "uid": "md",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marco",
    "uid": "md",
    "sn": "d'Itri",
    "uidNumber": "1074",
    "keyFingerPrint": "6791403B68AE2690517C42EAE6FFF1E38DC968B0",
    "gidNumber": "1074",
    "ircNick": "Md",
    "labeledURI": "https://www.linux.it/~md/"
  },
  {
    "info": "# mdj, users, debian.org",
    "dn": {
      "uid": "mdj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mikael",
    "uid": "mdj",
    "uidNumber": "2048",
    "sn": "Djurfeldt",
    "labeledURI": "http://www.nada.kth.se/~mdj/",
    "keyFingerPrint": "8FC758345E24684B67E523CEAFF1D4BDB69135D0",
    "gidNumber": "2048"
  },
  {
    "info": "# mdorman, users, debian.org",
    "dn": {
      "uid": "mdorman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mdorman",
    "sn": "Dorman",
    "uidNumber": "856",
    "ircNick": "mdorman",
    "jabberJID": "mdorman@ironicdesign.com",
    "labeledURI": "http://tendentious.org/",
    "gidNumber": "856"
  },
  {
    "info": "# mdz, users, debian.org",
    "dn": {
      "uid": "mdz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "mdz",
    "sn": "Zimmerman",
    "uidNumber": "2109",
    "ircNick": "mdz",
    "jabberJID": "mdz@jabber.org",
    "labeledURI": "http://mdzlog.alcor.net/",
    "keyFingerPrint": "A24331921670F0060A97500A8A111B5CAE426944",
    "gidNumber": "2109"
  },
  {
    "info": "# mechanix, users, debian.org",
    "dn": {
      "uid": "mechanix",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Filip",
    "uid": "mechanix",
    "sn": "Van Raemdonck",
    "uidNumber": "2182",
    "gidNumber": "2182",
    "ircNick": "mechanix",
    "labeledURI": "http://www.sysfs.be/"
  },
  {
    "info": "# mechtilde, users, debian.org",
    "dn": {
      "uid": "mechtilde",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mechtilde",
    "objectClass": "debianDeveloper",
    "uidNumber": "3459",
    "gidNumber": "3459",
    "keyFingerPrint": "F0E37F3DC87A4998289939E7F2877BBA141AAD7F",
    "cn": "Mechtilde",
    "sn": "Stehmann",
    "ircNick": "Mechtilde",
    "jabberJID": "Mechtilde",
    "labeledURI": "https://mechtilde.de"
  },
  {
    "info": "# meder, users, debian.org",
    "dn": {
      "uid": "meder",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "meder",
    "uidNumber": "1019",
    "sn": "Meder",
    "gidNumber": "1019"
  },
  {
    "info": "# meebey, users, debian.org",
    "dn": {
      "uid": "meebey",
      "ou": "users",
      "dc": "org"
    },
    "uid": "meebey",
    "objectClass": "debianDeveloper",
    "uidNumber": "2842",
    "keyFingerPrint": "ABE195E150A8DBE7809D3F427127E5ABEEF946C8",
    "cn": "Mirco",
    "sn": "Bauer",
    "gidNumber": "2842",
    "ircNick": "meebey",
    "jabberJID": "meebey@debian.org",
    "labeledURI": "https://www.meebey.net"
  },
  {
    "info": "# mehdi, users, debian.org",
    "dn": {
      "uid": "mehdi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mehdi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3001",
    "cn": "Mehdi",
    "sn": "Dogguy",
    "gidNumber": "3001",
    "keyFingerPrint": "FC87859B0E39574DD7294EC63DF4600039A140C4",
    "ircNick": "mehdi@OFTC; mehdid@Freenode",
    "jabberJID": "mehdi@dogguy.org",
    "labeledURI": "http://dogguy.org"
  },
  {
    "info": "# meike, users, debian.org",
    "dn": {
      "uid": "meike",
      "ou": "users",
      "dc": "org"
    },
    "uid": "meike",
    "objectClass": "debianDeveloper",
    "uidNumber": "2910",
    "cn": "Meike",
    "sn": "Reichle",
    "ircNick": "alphascorpii",
    "jabberJID": "alphascorpii@jabber.netzgehirn.de",
    "labeledURI": "http://www.alphascorpii.net/",
    "gidNumber": "2910"
  },
  {
    "info": "# mejo, users, debian.org",
    "dn": {
      "uid": "mejo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonas",
    "keyFingerPrint": "2C8CD283D3EFDF433890E7835262E7FF491049FE",
    "uid": "mejo",
    "sn": "Meurer",
    "uidNumber": "2550",
    "gidNumber": "2550",
    "ircNick": "mejo"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "menden",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Juergen",
    "uid": "menden",
    "uidNumber": "1109",
    "sn": "Menden",
    "gidNumber": "1109"
  },
  {
    "info": "# mendoza, users, debian.org",
    "dn": {
      "uid": "mendoza",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Cesar",
    "uid": "mendoza",
    "sn": "Mendoza",
    "uidNumber": "2061",
    "ircNick": "Paks",
    "labeledURI": "http://www.kitiara.org",
    "gidNumber": "2061"
  },
  {
    "info": "# mennucc1, users, debian.org",
    "dn": {
      "uid": "mennucc1",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrea",
    "uid": "mennucc1",
    "sn": "Mennucci",
    "uidNumber": "2241",
    "keyFingerPrint": "372D9DAC08EB27B8B38EE96F57CCF4596A1353C2",
    "gidNumber": "2241",
    "labeledURI": "http://mennucc1.debian.net"
  },
  {
    "info": "# merker, users, debian.org",
    "dn": {
      "uid": "merker",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Karsten",
    "uid": "merker",
    "uidNumber": "2458",
    "sn": "Merker",
    "keyFingerPrint": "BA62C2D7EE4490884FD6E7B8B22C1F17F1E89CE2",
    "gidNumber": "2458",
    "ircNick": "m4r"
  },
  {
    "info": "# merkys, users, debian.org",
    "dn": {
      "uid": "merkys",
      "ou": "users",
      "dc": "org"
    },
    "uid": "merkys",
    "objectClass": "debianDeveloper",
    "uidNumber": "3560",
    "gidNumber": "3560",
    "keyFingerPrint": "772292F6F7AC85FAE041D41EE5F43F9C2734F287",
    "cn": "Andrius",
    "sn": "Merkys"
  },
  {
    "info": "# meskes, users, debian.org",
    "dn": {
      "uid": "meskes",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "meskes",
    "sn": "Meskes",
    "uidNumber": "857",
    "keyFingerPrint": "BBBC58B45994CF9CCC56BCDADF23DA3396978EB3",
    "gidNumber": "857",
    "ircNick": "feivel",
    "jabberJID": "mme@jabber.credativ.com"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mester",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "mester",
    "uidNumber": "2035",
    "ircNick": "Roacer",
    "sn": "Mester",
    "gidNumber": "2035"
  },
  {
    "info": "# mestia, users, debian.org",
    "dn": {
      "uid": "mestia",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mestia",
    "objectClass": "debianDeveloper",
    "uidNumber": "3513",
    "gidNumber": "3513",
    "keyFingerPrint": "B1A51EB2779DD01743CC19BA1CF792111B5228B0",
    "cn": "Alexandre",
    "sn": "Mestiashvili",
    "jabberJID": "mailatgoogl@gmail.com"
  },
  {
    "info": "# metal, users, debian.org",
    "dn": {
      "uid": "metal",
      "ou": "users",
      "dc": "org"
    },
    "uid": "metal",
    "objectClass": "debianDeveloper",
    "uidNumber": "3059",
    "cn": "Marcelo",
    "sn": "Vieira",
    "ircNick": "metal",
    "jabberJID": "metal@jabber-br.org",
    "labeledURI": "http://metaldot.alucinados.com",
    "keyFingerPrint": "5FAE7D03CCD22E1C8DA9C5095B924EE310055CD3",
    "gidNumber": "3059"
  },
  {
    "info": "# meyering, users, debian.org",
    "dn": {
      "uid": "meyering",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jim",
    "uid": "meyering",
    "uidNumber": "2673",
    "sn": "Meyering",
    "keyFingerPrint": "155D3FC500C834486D1EEA677FD9FCCB000BEEEE",
    "gidNumber": "2673"
  },
  {
    "info": "# mez, users, debian.org",
    "dn": {
      "uid": "mez",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mez",
    "objectClass": "debianDeveloper",
    "uidNumber": "2960",
    "cn": "Martin",
    "sn": "Meredith",
    "keyFingerPrint": "065BFD16E4BFB184D73F768C2AFBD67FD133AC6E",
    "gidNumber": "2960",
    "ircNick": "Mez"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "mf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mika",
    "uid": "mf",
    "uidNumber": "1360",
    "ircNick": "zoop_",
    "sn": "Fischer",
    "gidNumber": "1360"
  },
  {
    "info": "# mfournier, users, debian.org",
    "dn": {
      "uid": "mfournier",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mfournier",
    "objectClass": "debianDeveloper",
    "uidNumber": "3438",
    "gidNumber": "3438",
    "keyFingerPrint": "1726090DD276E81507A92B18116F5E3AB368A4EB",
    "cn": "Marc",
    "sn": "Fournier"
  },
  {
    "info": "# mfurr, users, debian.org",
    "dn": {
      "uid": "mfurr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "mfurr",
    "uidNumber": "2523",
    "sn": "Furr",
    "ircNick": "mfurr",
    "gidNumber": "2523"
  },
  {
    "info": "Zrrjh+4CIPqgJMHuAIjtk585IdVB0BD4eGgGpkn8jW5OLmB+hFerp",
    "dn": {
      "uid": "mfv",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mfv",
    "objectClass": "debianDeveloper",
    "uidNumber": "3196",
    "keyFingerPrint": "4E8E810A6B445FDE68DAD0258062398983B2CF7A",
    "cn": "Matteo",
    "sn": "Vescovi",
    "gidNumber": "3196",
    "ircNick": "mfv",
    "jabberJID": "mfv@debian.org"
  },
  {
    "info": "# mgilbert, users, debian.org",
    "dn": {
      "uid": "mgilbert",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mgilbert",
    "objectClass": "debianDeveloper",
    "uidNumber": "3187",
    "keyFingerPrint": "1FBB8F049963FA032C3DAD88A784FF770F0F4FFA",
    "cn": "Michael",
    "sn": "Gilbert",
    "gidNumber": "3187",
    "ircNick": "gilbert"
  },
  {
    "info": "# mgrimm, users, debian.org",
    "dn": {
      "uid": "mgrimm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mgrimm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3306",
    "cn": "Martin",
    "sn": "Grimm",
    "gidNumber": "3306"
  },
  {
    "info": "# mh, users, debian.org",
    "dn": {
      "uid": "mh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "mh",
    "sn": "Howard",
    "uidNumber": "2548",
    "ircNick": "`mh",
    "labeledURI": "http://www.tildemh.com",
    "gidNumber": "2548"
  },
  {
    "info": "# mhatta, users, debian.org",
    "dn": {
      "uid": "mhatta",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Masayuki",
    "uid": "mhatta",
    "sn": "Hatta",
    "uidNumber": "2080",
    "ircNick": "mhatta",
    "jabberJID": "mhatta@gmail.com",
    "labeledURI": "http://www.mhatta.org/",
    "keyFingerPrint": "643CD2E8DDCB00F8F158121BDDAE12626E141035",
    "gidNumber": "2080"
  },
  {
    "info": "# mhummel, users, debian.org",
    "dn": {
      "uid": "mhummel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.migwien.com",
    "cn": "Michael",
    "uid": "mhummel",
    "uidNumber": "2302",
    "sn": "Hummel",
    "gidNumber": "2302"
  },
  {
    "info": "# mhy, users, debian.org",
    "dn": {
      "uid": "mhy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mhy",
    "objectClass": "debianDeveloper",
    "uidNumber": "2848",
    "cn": "Mark",
    "sn": "Hymers",
    "keyFingerPrint": "309911BEA966D0613053045711B4E5FF15B0FD82",
    "ircNick": "mhy",
    "gidNumber": "2848"
  },
  {
    "info": "# mia, users, debian.org",
    "dn": {
      "uid": "mia",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://pweb.de.uu.net/vanacken.do/pgp/fingerprint",
    "cn": "Michael",
    "uid": "mia",
    "uidNumber": "2013",
    "sn": "van Acken",
    "gidNumber": "2013"
  },
  {
    "info": "# micah, users, debian.org",
    "dn": {
      "uid": "micah",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Micah",
    "uid": "micah",
    "uidNumber": "2701",
    "sn": "Anderson",
    "gidNumber": "2701",
    "ircNick": "micah",
    "jabberJID": "micah@riseup.net",
    "keyFingerPrint": "8A6A8AC321EE5CB64E0219B52D2C65DDB27446E5"
  },
  {
    "info": "# micce, users, debian.org",
    "dn": {
      "uid": "micce",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mikael",
    "uid": "micce",
    "sn": "Hedin",
    "labeledURI": "http://people.debian.org/~micce/",
    "uidNumber": "2276",
    "gidNumber": "2276"
  },
  {
    "info": "# micha, users, debian.org",
    "dn": {
      "uid": "micha",
      "ou": "users",
      "dc": "org"
    },
    "uid": "micha",
    "objectClass": "debianDeveloper",
    "uidNumber": "3019",
    "cn": "Micha",
    "sn": "Lenk",
    "keyFingerPrint": "A3EBB41FC5ABD675CEE41C45EA6CA6B951B85139",
    "gidNumber": "3019",
    "ircNick": "Bombadil",
    "jabberJID": "micha@lenk.info"
  },
  {
    "info": "20091208] 2006-01-22 Retired",
    "dn": {
      "uid": "michaelf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "michaelf",
    "sn": "Fedrowitz",
    "uidNumber": "2341",
    "gidNumber": "2341"
  },
  {
    "info": "# michaelw, users, debian.org",
    "dn": {
      "uid": "michaelw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "michaelw",
    "uidNumber": "2188",
    "sn": "Weber",
    "ircNick": "michaelw",
    "labeledURI": "http://www.foldr.org/~michaelw/",
    "gidNumber": "2188"
  },
  {
    "info": "# michalis, users, debian.org",
    "dn": {
      "uid": "michalis",
      "ou": "users",
      "dc": "org"
    },
    "uid": "michalis",
    "objectClass": "debianDeveloper",
    "uidNumber": "3392",
    "gidNumber": "3392",
    "cn": "Michalis",
    "sn": "Kamburelis"
  },
  {
    "info": "# michi, users, debian.org",
    "dn": {
      "uid": "michi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "michi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3094",
    "keyFingerPrint": "16FB517BA866C3F68F111485F3E4122F1D8C261A",
    "cn": "Michael",
    "sn": "Schutte",
    "ircNick": "michi",
    "labeledURI": "http://uiae.at/~michi/",
    "gidNumber": "3094"
  },
  {
    "info": "# midget, users, debian.org",
    "dn": {
      "uid": "midget",
      "ou": "users",
      "dc": "org"
    },
    "uid": "midget",
    "objectClass": "debianDeveloper",
    "uidNumber": "3050",
    "cn": "Dario",
    "sn": "Minnucci",
    "keyFingerPrint": "BAA17AAFB21D6567D457D67DA82FBB83F3D57033",
    "gidNumber": "3050",
    "ircNick": "midget",
    "jabberJID": "midget@jabber.org"
  },
  {
    "info": "# miguel, users, debian.org",
    "dn": {
      "uid": "miguel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Miguel",
    "uid": "miguel",
    "uidNumber": "1105",
    "sn": "de Icaza",
    "gidNumber": "1105"
  },
  {
    "info": "# mih, users, debian.org",
    "dn": {
      "uid": "mih",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mih",
    "objectClass": "debianDeveloper",
    "uidNumber": "3110",
    "cn": "Michael",
    "sn": "Hanke",
    "keyFingerPrint": "5701F1D0D18B87E61AA31F3DC073D2287FFB9E9B",
    "ircNick": "eknahm",
    "jabberJID": "michael.hanke@gmail.com",
    "labeledURI": "http://mih.voxindeserto.de/",
    "gidNumber": "3110"
  },
  {
    "info": "# mihtjel, users, debian.org",
    "dn": {
      "uid": "mihtjel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rune",
    "uid": "mihtjel",
    "sn": "Broberg",
    "labeledURI": "http://home.mihtjel.dk/",
    "uidNumber": "2541",
    "ircNick": "mihtjel",
    "gidNumber": "2541"
  },
  {
    "info": "# mika, users, debian.org",
    "dn": {
      "uid": "mika",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mika",
    "objectClass": "debianDeveloper",
    "uidNumber": "2979",
    "cn": "Michael",
    "sn": "Prokop",
    "keyFingerPrint": "33CCB136401AFEC843A3876396A87872B7EA3737",
    "gidNumber": "2979",
    "ircNick": "mika (freenode) / mikap (oftc)",
    "jabberJID": "mika@jabber.grml.org",
    "labeledURI": "https://michael-prokop.at/"
  },
  {
    "info": "# mikan, users, debian.org",
    "dn": {
      "uid": "mikan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mikael",
    "uid": "mikan",
    "sn": "Sennerholm",
    "uidNumber": "2379",
    "ircNick": "mikan",
    "gidNumber": "2379"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "mike",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "mike",
    "uidNumber": "1062",
    "sn": "Neuffer",
    "gidNumber": "1062"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mikem",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mikem",
    "uidNumber": "2350",
    "ircNick": "natoka",
    "sn": "Moerz",
    "gidNumber": "2350"
  },
  {
    "info": "# milan, users, debian.org",
    "dn": {
      "uid": "milan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "milan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3363",
    "keyFingerPrint": "2C5CF8210CDF553B9521DE76223AE055BD94E154",
    "cn": "Milan",
    "sn": "Kupcevic",
    "gidNumber": "3363"
  },
  {
    "info": "# miquels, users, debian.org",
    "dn": {
      "uid": "miquels",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Miquel",
    "uid": "miquels",
    "uidNumber": "858",
    "sn": "van Smoorenburg",
    "keyFingerPrint": "AEE3CFA76134028A2E290DA1E42E41CAFD50FF4F",
    "gidNumber": "858",
    "ircNick": "_mike_",
    "labeledURI": "https://github.com/miquels"
  },
  {
    "info": "# miriam, users, debian.org",
    "dn": {
      "uid": "miriam",
      "ou": "users",
      "dc": "org"
    },
    "uid": "miriam",
    "objectClass": "debianDeveloper",
    "uidNumber": "2917",
    "cn": "Miriam",
    "sn": "Ruiz",
    "keyFingerPrint": "75EFC41387105FFAF4392E025F8E6BE0928FFAFA",
    "gidNumber": "2917",
    "ircNick": "Baby",
    "labeledURI": "http://www.miriamruiz.es"
  },
  {
    "info": "# misha, users, debian.org",
    "dn": {
      "uid": "misha",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Misha",
    "uid": "misha",
    "sn": "Nasledov",
    "labeledURI": "http://people.debian.org/~misha/",
    "uidNumber": "2373",
    "ircNick": "xepsilon",
    "gidNumber": "2373"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "mitchell",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bill",
    "uid": "mitchell",
    "uidNumber": "859",
    "sn": "Mitchell",
    "gidNumber": "859"
  },
  {
    "info": "# mithrandi, users, debian.org",
    "dn": {
      "uid": "mithrandi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mithrandi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3216",
    "keyFingerPrint": "DB24E315EDAAAC13D646709958C0FB1CDE3B7600",
    "cn": "Tristan",
    "sn": "Seligmann",
    "ircNick": "idnar",
    "jabberJID": "mithrandi@mithrandi.net",
    "labeledURI": "http://mithrandi.net/",
    "gidNumber": "3216"
  },
  {
    "info": "# mitya57, users, debian.org",
    "dn": {
      "uid": "mitya57",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mitya57",
    "objectClass": "debianDeveloper",
    "uidNumber": "3286",
    "keyFingerPrint": "F24299FF1BBC9018B906A4CB6026936D2F1C8AE0",
    "cn": "Dmitry",
    "sn": "Shachnev",
    "gidNumber": "3286",
    "ircNick": "mitya57",
    "labeledURI": "https://mitya57.me"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.edrc.uct.ac.za/~mj/",
    "cn": "Michael-John",
    "uid": "mj",
    "uidNumber": "1377",
    "ircNick": "MrKen",
    "sn": "Turner",
    "gidNumber": "1377"
  },
  {
    "info": "# mjb, users, debian.org",
    "dn": {
      "uid": "mjb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mjb",
    "sn": "Beattie",
    "uidNumber": "1406",
    "keyFingerPrint": "9091CDDB68CC34006335F0543B6212915022936C",
    "gidNumber": "1406",
    "ircNick": "mjb"
  },
  {
    "info": "# mjeanson, users, debian.org",
    "dn": {
      "uid": "mjeanson",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mjeanson",
    "objectClass": "debianDeveloper",
    "uidNumber": "3508",
    "gidNumber": "3508",
    "keyFingerPrint": "9866243242B6422779E20B6286561F452180FCFE",
    "cn": "Michael",
    "sn": "Jeanson",
    "ircNick": "mjeanson"
  },
  {
    "info": "# mjg59, users, debian.org",
    "dn": {
      "uid": "mjg59",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "mjg59",
    "uidNumber": "2535",
    "sn": "Garrett",
    "gidNumber": "2535",
    "keyFingerPrint": "3453B5A2F40603D7FF9FFAF07E0A399DB24B3B15",
    "ircNick": "mjg59"
  },
  {
    "info": "# mjj29, users, debian.org",
    "dn": {
      "uid": "mjj29",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mjj29",
    "objectClass": "debianDeveloper",
    "uidNumber": "2890",
    "cn": "Matthew",
    "sn": "Johnson",
    "ircNick": "mjj29",
    "jabberJID": "mattj@bluelinux.co.uk",
    "labeledURI": "http://www.matthew.ath.cx/",
    "gidNumber": "2890"
  },
  {
    "info": "# mjr, users, debian.org",
    "dn": {
      "uid": "mjr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "mjr",
    "sn": "Ray",
    "uidNumber": "2569",
    "cn": "M",
    "gidNumber": "2569",
    "keyFingerPrint": "146A3D6DBF5301C981CBA8EB78D398FBBA26E7B8",
    "ircNick": "slef",
    "jabberJID": "mjr@serene.software.coop",
    "labeledURI": "http://mjr.towers.org.uk/"
  },
  {
    "info": "# mjradwin, users, debian.org",
    "dn": {
      "uid": "mjradwin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mjradwin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3582",
    "gidNumber": "3582",
    "keyFingerPrint": "D6ACF09042B33C5882F6CFA35C4F2FEAA3D22D05",
    "cn": "Michael",
    "sn": "Radwin"
  },
  {
    "info": "# mjt, users, debian.org",
    "dn": {
      "uid": "mjt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mjt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3144",
    "cn": "Michael",
    "sn": "Tokarev",
    "ircNick": "mjt",
    "gidNumber": "3144",
    "keyFingerPrint": "6EE195D1886E8FFB810D4324457CE0A0804465C5"
  },
  {
    "info": "# mjw, users, debian.org",
    "dn": {
      "uid": "mjw",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mjw",
    "objectClass": "debianDeveloper",
    "uidNumber": "3309",
    "keyFingerPrint": "47CC0331081B8BC6D0FD4DA08370665B57816A6A",
    "cn": "Mark",
    "sn": "Wielaard",
    "gidNumber": "3309"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "mk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marco",
    "uid": "mk",
    "uidNumber": "2377",
    "sn": "Kuhlmann",
    "gidNumber": "2377"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "mkabel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthias",
    "uid": "mkabel",
    "uidNumber": "1391",
    "sn": "Kabel",
    "gidNumber": "1391"
  },
  {
    "info": "# mkc, users, debian.org",
    "dn": {
      "uid": "mkc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.mathdogs.com/people/mkc",
    "cn": "Mike",
    "uid": "mkc",
    "uidNumber": "2162",
    "ircNick": "mkc",
    "sn": "Coleman",
    "gidNumber": "2162"
  },
  {
    "info": "# mkd, users, debian.org",
    "dn": {
      "uid": "mkd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mkd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3116",
    "cn": "Mark",
    "sn": "Dieterich",
    "gidNumber": "3116"
  },
  {
    "info": ".de>",
    "dn": {
      "uid": "mkh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mkh",
    "uidNumber": "1392",
    "sn": "Herrmann",
    "gidNumber": "1392"
  },
  {
    "info": "# mkoch, users, debian.org",
    "dn": {
      "uid": "mkoch",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mkoch",
    "objectClass": "debianDeveloper",
    "uidNumber": "2761",
    "cn": "Michael",
    "sn": "Koch",
    "ircNick": "man-di",
    "jabberJID": "enoughofmichael2@jabber.de",
    "gidNumber": "2761"
  },
  {
    "info": "# mlang, users, debian.org",
    "dn": {
      "uid": "mlang",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mario",
    "uid": "mlang",
    "sn": "Lang",
    "uidNumber": "2512",
    "ircNick": "delYsid",
    "labeledURI": "http://people.debian.org/~mlang/",
    "gidNumber": "2512"
  },
  {
    "info": "# mlt, users, debian.org",
    "dn": {
      "uid": "mlt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mlt",
    "objectClass": "debianDeveloper",
    "uidNumber": "2801",
    "cn": "Marcela",
    "sn": "Tiznado",
    "ircNick": "ASCIIGirl",
    "jabberJID": "asciigirlzita@jabber.org",
    "keyFingerPrint": "EC63D02697EDFFBE375BBC185CB9D8493D29D438",
    "gidNumber": "2801"
  },
  {
    "info": "# mmagallo, users, debian.org",
    "dn": {
      "uid": "mmagallo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marcelo",
    "uid": "mmagallo",
    "sn": "Magallon",
    "uidNumber": "892",
    "ircNick": "m2-",
    "jabberJID": "marcelo.magallon@gmail.com",
    "labeledURI": "http://www.vis.uni-stuttgart.de/~magallon/",
    "gidNumber": "892"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mmickan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "mmickan",
    "uidNumber": "1072",
    "sn": "Mickan",
    "gidNumber": "1072"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mmiller",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.debian.org/~mmiller",
    "cn": "Michael",
    "uid": "mmiller",
    "uidNumber": "1319",
    "sn": "Miller",
    "gidNumber": "1319"
  },
  {
    "info": "# mmind, users, debian.org",
    "dn": {
      "uid": "mmind",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mmind",
    "objectClass": "debianDeveloper",
    "uidNumber": "3101",
    "keyFingerPrint": "795D0851095CAACA16CA79259AFB7B8C9A5F5BBC",
    "cn": "Heiko",
    "gidNumber": "3101",
    "sn": ": U3TDvGJuZXI="
  },
  {
    "info": "# mms, users, debian.org",
    "dn": {
      "uid": "mms",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Emmanuel",
    "uid": "mms",
    "uidNumber": "2568",
    "sn": "le Chevoir",
    "ircNick": "mms",
    "labeledURI": "http://perso.all-3rd.net/manu/",
    "gidNumber": "2568"
  },
  {
    "info": "# mnencia, users, debian.org",
    "dn": {
      "uid": "mnencia",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marco",
    "uid": "mnencia",
    "sn": "Nenciarini",
    "uidNumber": "2319",
    "keyFingerPrint": "7C23B8043E65D2980A21B6E2589F03F01BA55038",
    "gidNumber": "2319",
    "ircNick": "mnencia"
  },
  {
    "info": "# mnordstr, users, debian.org",
    "dn": {
      "uid": "mnordstr",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mnordstr",
    "objectClass": "debianDeveloper",
    "uidNumber": "2826",
    "keyFingerPrint": "737357552CC0B16C4339258FC0A4F11DE06B3F97",
    "cn": "Mattias",
    "sn": "Nordstrom",
    "gidNumber": "2826"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "moda",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dave",
    "uid": "moda",
    "uidNumber": "1382",
    "sn": "Tolson",
    "gidNumber": "1382"
  },
  {
    "info": "# modax, users, debian.org",
    "dn": {
      "uid": "modax",
      "ou": "users",
      "dc": "org"
    },
    "uid": "modax",
    "objectClass": "debianDeveloper",
    "uidNumber": "3045",
    "keyFingerPrint": "9E8CCC49180D6A1500B400881CEF494673EAE214",
    "cn": "Modestas",
    "sn": "Vainius",
    "ircNick": "MoDaX",
    "gidNumber": "3045"
  },
  {
    "info": "# moeller, users, debian.org",
    "dn": {
      "uid": "moeller",
      "ou": "users",
      "dc": "org"
    },
    "uid": "moeller",
    "objectClass": "debianDeveloper",
    "uidNumber": "2867",
    "cn": "Steffen",
    "gidNumber": "2867",
    "sn": ": TcO2bGxlcg==",
    "keyFingerPrint": "DF3E2E424A7E279C2E4B023AB9C29AEEB2F450A7"
  },
  {
    "info": "# mohawk2, users, debian.org",
    "dn": {
      "uid": "mohawk2",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mohawk2",
    "objectClass": "debianDeveloper",
    "uidNumber": "3642",
    "gidNumber": "3642",
    "keyFingerPrint": "5B71AA817AC26D60C3295CFC7FD0600D0343F42E",
    "cn": "Ed",
    "sn": "J"
  },
  {
    "info": "# mollydb, users, debian.org",
    "dn": {
      "uid": "mollydb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mollydb",
    "objectClass": "debianDeveloper",
    "uidNumber": "3553",
    "gidNumber": "3553",
    "keyFingerPrint": "ECE5B5BF952A3AEA92C137F9C9230A4849ACE0DB",
    "cn": "Molly",
    "sn": "de Blanc",
    "ircNick": "mollydb"
  },
  {
    "info": "# mom040267, users, debian.org",
    "dn": {
      "uid": "mom040267",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mom040267",
    "objectClass": "debianDeveloper",
    "uidNumber": "3252",
    "cn": "Oleg",
    "sn": "Moskalenko",
    "gidNumber": "3252"
  },
  {
    "info": "# mones, users, debian.org",
    "dn": {
      "uid": "mones",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mones",
    "objectClass": "debianDeveloper",
    "uidNumber": "2778",
    "cn": "Ricardo",
    "keyFingerPrint": "43BC364B16DF0C205EBD75921F0F0A88DE5BCCA6",
    "ircNick": "mones",
    "labeledURI": "http://mones.org",
    "sn": "Mones Lastra",
    "gidNumber": "2778"
  },
  {
    "info": "# monga, users, debian.org",
    "dn": {
      "uid": "monga",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mattia",
    "uid": "monga",
    "sn": "Monga",
    "uidNumber": "2318",
    "ircNick": "eMMe",
    "jabberJID": "eMMe@jabber.linux.it",
    "labeledURI": "http://homes.di.unimi.it/~monga",
    "gidNumber": "2318"
  },
  {
    "info": "# monica, users, debian.org",
    "dn": {
      "uid": "monica",
      "ou": "users",
      "dc": "org"
    },
    "uid": "monica",
    "objectClass": "debianDeveloper",
    "uidNumber": "3188",
    "gidNumber": "3188",
    "cn": ": TcOybmljYQ==",
    "sn": ": UmFtw61yZXogQXJjZWRh",
    "labeledURI": "http://dunetna.probeta.net"
  },
  {
    "info": "# mooch, users, debian.org",
    "dn": {
      "uid": "mooch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jesus",
    "uid": "mooch",
    "sn": "Climent",
    "labeledURI": "http://www.pumuki.org",
    "uidNumber": "2611",
    "ircNick": "mooch",
    "gidNumber": "2611"
  },
  {
    "info": "# moray, users, debian.org",
    "dn": {
      "uid": "moray",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Moray",
    "uid": "moray",
    "sn": "Allan",
    "uidNumber": "2644",
    "keyFingerPrint": "EB32D67638C4E3D8AA2403F0BEBD933335FC140B",
    "ircNick": "moray",
    "labeledURI": "http://www.morayallan.com/",
    "gidNumber": "2644"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "moreda",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roberto",
    "uid": "moreda",
    "uidNumber": "2370",
    "sn": "Moreda",
    "gidNumber": "2370"
  },
  {
    "info": "# morimura, users, debian.org",
    "dn": {
      "uid": "morimura",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Teruyuki",
    "keyFingerPrint": "B3AA8A8AC95EEA2877AA140D6F0B4F8857D68A06",
    "uid": "morimura",
    "uidNumber": "2065",
    "sn": "Morimura",
    "gidNumber": "2065"
  },
  {
    "info": "# mornfall, users, debian.org",
    "dn": {
      "uid": "mornfall",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mornfall",
    "objectClass": "debianDeveloper",
    "uidNumber": "2859",
    "cn": "Petr",
    "sn": "Rockai",
    "gidNumber": "2859"
  },
  {
    "info": "# moronito, users, debian.org",
    "dn": {
      "uid": "moronito",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tommaso",
    "uid": "moronito",
    "sn": "Moroni",
    "uidNumber": "2590",
    "ircNick": "moronito",
    "jabberJID": "moronito@jabber.org",
    "gidNumber": "2590"
  },
  {
    "info": "# morph, users, debian.org",
    "dn": {
      "uid": "morph",
      "ou": "users",
      "dc": "org"
    },
    "uid": "morph",
    "objectClass": "debianDeveloper",
    "uidNumber": "2940",
    "cn": "Sandro",
    "sn": "Tosi",
    "keyFingerPrint": "8C455876FA072D53EEC3CA81FE8D78F9FE4F4E10",
    "gidNumber": "2940",
    "ircNick": "morph",
    "labeledURI": "http://sandrotosi.me/"
  },
  {
    "info": "# morten, users, debian.org",
    "dn": {
      "uid": "morten",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Morten",
    "uid": "morten",
    "sn": "Hustveit",
    "uidNumber": "2518",
    "ircNick": "mortehu",
    "labeledURI": "http://www.junoplay.com/",
    "gidNumber": "2518"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "moshez",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Moshe",
    "uid": "moshez",
    "sn": "Zadka",
    "labeledURI": "http://moshez.org",
    "uidNumber": "2228",
    "ircNick": "moshez",
    "gidNumber": "2228"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "moss",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.acmeunix.org/",
    "cn": "James",
    "uid": "moss",
    "uidNumber": "2374",
    "ircNick": "moss",
    "sn": "Moss",
    "gidNumber": "2374"
  },
  {
    "info": "# moth, users, debian.org",
    "dn": {
      "uid": "moth",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Raul",
    "uid": "moth",
    "uidNumber": "860",
    "sn": "Miller",
    "gidNumber": "860"
  },
  {
    "info": "# moulard, users, debian.org",
    "dn": {
      "uid": "moulard",
      "ou": "users",
      "dc": "org"
    },
    "uid": "moulard",
    "objectClass": "debianDeveloper",
    "uidNumber": "3270",
    "keyFingerPrint": "04159C24FAA21BCD25EEBFB3516FC68F31496D58",
    "cn": "Thomas",
    "sn": "Moulard",
    "jabberJID": "thomas.moulard@gmail.com",
    "labeledURI": "http://thomas.moulard.net/",
    "gidNumber": "3270"
  },
  {
    "info": "# mpalmer, users, debian.org",
    "dn": {
      "uid": "mpalmer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "mpalmer",
    "sn": "Palmer",
    "uidNumber": "2505",
    "ircNick": "womble",
    "keyFingerPrint": "DCE76C9A36785CE022EFCC271654965A49F7FC9B",
    "gidNumber": "2505"
  },
  {
    "info": "# mpav, users, debian.org",
    "dn": {
      "uid": "mpav",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "mpav",
    "uidNumber": "1274",
    "ircNick": "mpav",
    "sn": "Pavlovich",
    "gidNumber": "1630"
  },
  {
    "info": "# mpitt, users, debian.org",
    "dn": {
      "uid": "mpitt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "mpitt",
    "sn": "Pitt",
    "uidNumber": "2631",
    "keyFingerPrint": "3DB46B55EFA59D40E6232148D14EF15DAFE11347",
    "gidNumber": "2631",
    "ircNick": "pitti",
    "labeledURI": "https://www.piware.de"
  },
  {
    "info": "# mquinson, users, debian.org",
    "dn": {
      "uid": "mquinson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://graal.ens-lyon.fr/~mquinson",
    "cn": "Martin",
    "uid": "mquinson",
    "uidNumber": "2669",
    "ircNick": "emptty",
    "sn": "Quinson",
    "keyFingerPrint": "4FBE9C4EEA52EF13D541661298BD97244F73BEA7",
    "gidNumber": "2669"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "mrbump",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "mrbump",
    "uidNumber": "1413",
    "ircNick": "MrBump",
    "sn": "Triggs",
    "gidNumber": "1413"
  },
  {
    "info": "# mrd, users, debian.org",
    "dn": {
      "uid": "mrd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "mrd",
    "sn": "Danish",
    "uidNumber": "2310",
    "keyFingerPrint": "BC8506510E9CE91D49EC2C0BBC10BD0F7B8BF08B",
    "gidNumber": "2310",
    "ircNick": "mrd",
    "labeledURI": "https://www.cl.cam.ac.uk/~mrd45/"
  },
  {
    "info": "20091208] DSA disabled",
    "dn": {
      "uid": "mrj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "mrj",
    "sn": "Johnson",
    "uidNumber": "2258",
    "ircNick": "mrj",
    "labeledURI": "http://people.debian.org/~mrj",
    "gidNumber": "2258"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "mrn20",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mrn20",
    "uidNumber": "897",
    "sn": "Nonweiler",
    "gidNumber": "897"
  },
  {
    "info": "# mryan, users, debian.org",
    "dn": {
      "uid": "mryan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "mryan",
    "uidNumber": "1081",
    "sn": "Ryan",
    "gidNumber": "1081"
  },
  {
    "info": "# ms, users, debian.org",
    "dn": {
      "uid": "ms",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "ms",
    "sn": "Schiansky",
    "labeledURI": "http://www.dummdoof.de",
    "uidNumber": "2620",
    "ircNick": "mrdata",
    "gidNumber": "2620"
  },
  {
    "info": "# msameer, users, debian.org",
    "dn": {
      "uid": "msameer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "msameer",
    "objectClass": "debianDeveloper",
    "uidNumber": "2861",
    "keyFingerPrint": "9F73032EEAC9F7AD951F280ECB668E29A3FD0DF7",
    "cn": "Mohammed",
    "sn": "Sameer",
    "ircNick": "MSameer",
    "labeledURI": "http://foolab.org",
    "gidNumber": "2861"
  },
  {
    "info": "# msp, users, debian.org",
    "dn": {
      "uid": "msp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "msp",
    "sn": "Purcell",
    "uidNumber": "2288",
    "ircNick": "pos",
    "labeledURI": "http://qa.debian.org/developer.php/developer.php?login=msp",
    "gidNumber": "2288",
    "keyFingerPrint": "DEE724FD54FA0B5BB0B29D6A2C0E8031F29C4A30"
  },
  {
    "info": "# mstone, users, debian.org",
    "dn": {
      "uid": "mstone",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mstone",
    "uidNumber": "1389",
    "sn": "Stone",
    "keyFingerPrint": "02D5315FF11F1861860E1E02F61AACDCFA11FFDE",
    "ircNick": "mstone",
    "gidNumber": "1389"
  },
  {
    "info": "# mt, users, debian.org",
    "dn": {
      "uid": "mt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mt",
    "objectClass": "debianDeveloper",
    "uidNumber": "2898",
    "cn": "Michael",
    "sn": "Tautschnig",
    "keyFingerPrint": "ACA6C3F4E1003986F35388043BBF839336ECA931",
    "gidNumber": "2898"
  },
  {
    "info": "# mtecknology, users, debian.org",
    "dn": {
      "uid": "mtecknology",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mtecknology",
    "objectClass": "debianDeveloper",
    "uidNumber": "3450",
    "gidNumber": "3450",
    "keyFingerPrint": "06CD63D74D598C4A47E42C9603A8891A765AD085",
    "cn": "Michael",
    "sn": "Lustfield",
    "ircNick": "MTecknology",
    "labeledURI": "http://michael.lustfield.net/"
  },
  {
    "info": "# mtmiller, users, debian.org",
    "dn": {
      "uid": "mtmiller",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mtmiller",
    "objectClass": "debianDeveloper",
    "uidNumber": "3282",
    "keyFingerPrint": "930750035A15E27C3830740728FA801A43BDD637",
    "cn": "Mike",
    "sn": "Miller",
    "gidNumber": "3282",
    "ircNick": "mtmiller",
    "labeledURI": "https://mtmxr.com"
  },
  {
    "info": "# muammar, users, debian.org",
    "dn": {
      "uid": "muammar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "muammar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3100",
    "cn": "Muammar",
    "sn": "El Khatib",
    "keyFingerPrint": "1C9A2574B1A48D8AC243CAE55C29137271246E4A",
    "gidNumber": "3100",
    "ircNick": "muammar",
    "labeledURI": "http://muammar.me"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "munro",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "munro",
    "uidNumber": "993",
    "sn": "Munro",
    "gidNumber": "993"
  },
  {
    "info": "# murat, users, debian.org",
    "dn": {
      "uid": "murat",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Murat",
    "uid": "murat",
    "sn": "Demirten",
    "uidNumber": "2129",
    "gidNumber": "2129",
    "keyFingerPrint": "A9BB2D83740934F01787E87C2E6B85F6CE51BFF3",
    "ircNick": "omniheurist",
    "labeledURI": "https://linux-tips.com"
  },
  {
    "info": "# mvb, users, debian.org",
    "dn": {
      "uid": "mvb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mvb",
    "objectClass": "debianDeveloper",
    "uidNumber": "3425",
    "gidNumber": "3425",
    "keyFingerPrint": "98E0D178DCB90C1945C50DB1ED0DD3368DE40924",
    "cn": "Martijn",
    "sn": "van Brummelen",
    "ircNick": "the-dude"
  },
  {
    "info": "# mvo, users, debian.org",
    "dn": {
      "uid": "mvo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "mvo",
    "uidNumber": "2031",
    "sn": "Vogt",
    "ircNick": "mvo",
    "labeledURI": "http://people.debian.org/~mvo",
    "keyFingerPrint": "DA6C6754D8887626CD06A12898CABB3ABD4CA59E",
    "gidNumber": "2031"
  },
  {
    "info": "# mwei, users, debian.org",
    "dn": {
      "uid": "mwei",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mwei",
    "objectClass": "debianDeveloper",
    "uidNumber": "3537",
    "gidNumber": "3537",
    "keyFingerPrint": "FED5434845287DF262931489CFBBFCE0B74F1B14",
    "cn": "Ming-ting",
    "sn": "Wei",
    "ircNick": "medicalwei",
    "labeledURI": "https://m-wei.net"
  },
  {
    "info": "# mwhudson, users, debian.org",
    "dn": {
      "uid": "mwhudson",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mwhudson",
    "objectClass": "debianDeveloper",
    "uidNumber": "3491",
    "gidNumber": "3491",
    "keyFingerPrint": "37CB9DC76812E6FDB09EEADA80E627A0AB757E23",
    "cn": "Michael",
    "sn": "Hudson-Doyle",
    "ircNick": "mwhudson"
  },
  {
    "info": "# mwild1, users, debian.org",
    "dn": {
      "uid": "mwild1",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mwild1",
    "objectClass": "debianDeveloper",
    "uidNumber": "3389",
    "gidNumber": "3389",
    "keyFingerPrint": "32A9EDDE3609931EB98CEAC315907E8E7BDD6BFE",
    "cn": "Matthew",
    "sn": "Wild"
  },
  {
    "info": "# mymedia, users, debian.org",
    "dn": {
      "uid": "mymedia",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mymedia",
    "objectClass": "debianDeveloper",
    "uidNumber": "3576",
    "gidNumber": "3576",
    "keyFingerPrint": "4680CB78E8ADF7723F8862CAD9B5E9377A62C02B",
    "cn": "Nicholas",
    "sn": "Guriev"
  },
  {
    "info": "# myon, users, debian.org",
    "dn": {
      "uid": "myon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christoph",
    "uid": "myon",
    "sn": "Berg",
    "uidNumber": "2748",
    "keyFingerPrint": "5C48FE6157F49179597087C64C5A6BAB12D2A7AE",
    "gidNumber": "2748",
    "ircNick": "Myon",
    "labeledURI": "https://www.df7cb.de/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "myxie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Giuliano",
    "uid": "myxie",
    "uidNumber": "1079",
    "ircNick": "Myxie",
    "sn": "Procida",
    "gidNumber": "1079"
  },
  {
    "info": "# mzf, users, debian.org",
    "dn": {
      "uid": "mzf",
      "ou": "users",
      "dc": "org"
    },
    "uid": "mzf",
    "objectClass": "debianDeveloper",
    "uidNumber": "3635",
    "gidNumber": "3635",
    "keyFingerPrint": "86A5ABD6FFDB0A0C7F5057D34797FA721C351C9E",
    "cn": "Francois",
    "sn": "Mazen"
  },
  {
    "info": ".",
    "dn": {
      "uid": "nacho",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nacho",
    "objectClass": "debianDeveloper",
    "uidNumber": "2841",
    "cn": "Nacho",
    "sn": "Arias",
    "ircNick": "chipi",
    "jabberJID": "chipi@jabber.org",
    "labeledURI": "http://criptonita.com/~nacho",
    "gidNumber": "2841"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "nakahara",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hayao",
    "uid": "nakahara",
    "uidNumber": "1419",
    "sn": "Nakahara",
    "gidNumber": "1419"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "namhas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tommi",
    "uid": "namhas",
    "uidNumber": "1151",
    "sn": "Leino",
    "gidNumber": "1151"
  },
  {
    "info": "# naoliv, users, debian.org",
    "dn": {
      "uid": "naoliv",
      "ou": "users",
      "dc": "org"
    },
    "uid": "naoliv",
    "objectClass": "debianDeveloper",
    "uidNumber": "2831",
    "cn": "Nelson",
    "sn": "de Oliveira",
    "gidNumber": "2831",
    "ircNick": "naoliv",
    "jabberJID": "naoliv@gmail.com",
    "labeledURI": "http://people.debian.org/~naoliv/"
  },
  {
    "info": "# nas, users, debian.org",
    "dn": {
      "uid": "nas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Neil",
    "uid": "nas",
    "uidNumber": "2116",
    "sn": "Schemenauer",
    "keyFingerPrint": "9B3B987BF40E932006190A17A366302EB1DE86CE",
    "ircNick": "nas",
    "labeledURI": "http://arctrix.com/nas/",
    "gidNumber": "2116"
  },
  {
    "info": "# nathans, users, debian.org",
    "dn": {
      "uid": "nathans",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nathan",
    "uid": "nathans",
    "uidNumber": "2172",
    "sn": "Scott",
    "ircNick": "nathans",
    "keyFingerPrint": "F7ABE8761E6FE68638E6283AFE0842EE36DD8C0C",
    "gidNumber": "2172"
  },
  {
    "info": "# nattie, users, debian.org",
    "dn": {
      "uid": "nattie",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nattie",
    "objectClass": "debianDeveloper",
    "uidNumber": "3342",
    "keyFingerPrint": "3A2C76F9E5B25DBE1FCD751AA1F751F1A55357E8",
    "cn": "Nattie",
    "sn": "Mayer-Hutchings",
    "gidNumber": "3342"
  },
  {
    "info": "# natureshadow, users, debian.org",
    "dn": {
      "uid": "natureshadow",
      "ou": "users",
      "dc": "org"
    },
    "uid": "natureshadow",
    "objectClass": "debianDeveloper",
    "uidNumber": "3477",
    "gidNumber": "3477",
    "cn": "Dominik",
    "sn": "George",
    "ircNick": "Natureshadow",
    "jabberJID": "nik@mercurius.teckids.org",
    "labeledURI": "https://www.dominik-george.de",
    "keyFingerPrint": "A4EB3C5160961C85E80191310AE554E5460E1BDD"
  },
  {
    "info": "# nboullis, users, debian.org",
    "dn": {
      "uid": "nboullis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nicolas",
    "uid": "nboullis",
    "uidNumber": "2490",
    "sn": "Boullis",
    "keyFingerPrint": "13B582E2E0673A311DDF1B08D0E94F8D882D4468",
    "gidNumber": "2490",
    "ircNick": "z80a"
  },
  {
    "info": "# nbreen, users, debian.org",
    "dn": {
      "uid": "nbreen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nbreen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3162",
    "keyFingerPrint": "843E5FA61E6063389876D2E5EE4AFD69EC65108F",
    "cn": "Nicholas",
    "sn": "Breen",
    "gidNumber": "3162",
    "ircNick": "nbreen"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ncm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.cantrip.org/",
    "cn": "Nathan",
    "uid": "ncm",
    "uidNumber": "2147",
    "sn": "Myers",
    "gidNumber": "2147"
  },
  {
    "info": "# nduboc, users, debian.org",
    "dn": {
      "uid": "nduboc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nicolas",
    "uid": "nduboc",
    "uidNumber": "2670",
    "sn": "Duboc",
    "ircNick": "nduboc",
    "gidNumber": "2670"
  },
  {
    "info": "# neal, users, debian.org",
    "dn": {
      "uid": "neal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Neal",
    "uid": "neal",
    "sn": "Walfield",
    "uidNumber": "2181",
    "ircNick": "neal",
    "gidNumber": "2181"
  },
  {
    "info": "# neale, users, debian.org",
    "dn": {
      "uid": "neale",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Neale",
    "uid": "neale",
    "uidNumber": "1339",
    "sn": "Pickett",
    "ircNick": "neale",
    "labeledURI": "http://woozle.org/~neale/",
    "gidNumber": "1339"
  },
  {
    "info": "# nefsall, users, debian.org",
    "dn": {
      "uid": "nefsall",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "KiHyein",
    "uid": "nefsall",
    "uidNumber": "2419",
    "ircNick": "garam",
    "sn": "Seo",
    "gidNumber": "2419"
  },
  {
    "info": "# negm, users, debian.org",
    "dn": {
      "uid": "negm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ayman",
    "uid": "negm",
    "sn": "Negm",
    "uidNumber": "2594",
    "ircNick": "XSnack",
    "gidNumber": "2594"
  },
  {
    "info": "# neil, users, debian.org",
    "dn": {
      "uid": "neil",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Neil",
    "uid": "neil",
    "uidNumber": "2464",
    "sn": "Roeth",
    "keyFingerPrint": "61D47BA1FFE7AA02EFED88CA365C1409A4B3A640",
    "gidNumber": "2464",
    "labeledURI": "http://www.occamsrazor.net"
  },
  {
    "info": "# neilm, users, debian.org",
    "dn": {
      "uid": "neilm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Neil",
    "keyFingerPrint": "8217A2055E57043B2883054E7F55BB12A40F862E",
    "uid": "neilm",
    "sn": "McGovern",
    "uidNumber": "2715",
    "gidNumber": "2715",
    "ircNick": "Maulkin",
    "jabberJID": "maulkin@jabber.earth.li",
    "labeledURI": "http://www.halon.org.uk"
  },
  {
    "info": "# nekral, users, debian.org",
    "dn": {
      "uid": "nekral",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nekral",
    "objectClass": "debianDeveloper",
    "uidNumber": "2889",
    "cn": "Nicolas",
    "ircNick": "nekral",
    "gidNumber": "2889",
    "sn": ": RnJhbsOnb2lz"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "neotron",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "neotron",
    "uidNumber": "962",
    "sn": "Hedbor",
    "gidNumber": "962"
  },
  {
    "info": "# netzwurm, users, debian.org",
    "dn": {
      "uid": "netzwurm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "netzwurm",
    "sn": "Spreen",
    "uidNumber": "2323",
    "gidNumber": "2323",
    "ircNick": "netzwurm"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "neumann",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Frank",
    "uid": "neumann",
    "uidNumber": "881",
    "ircNick": "Franky",
    "sn": "Neumann",
    "gidNumber": "881"
  },
  {
    "info": "# neutrons, users, debian.org",
    "dn": {
      "uid": "neutrons",
      "ou": "users",
      "dc": "org"
    },
    "uid": "neutrons",
    "objectClass": "debianDeveloper",
    "uidNumber": "3420",
    "gidNumber": "3420",
    "keyFingerPrint": "F532DA10E563EE84440977A19D0470BDA6CDC457",
    "cn": "Neutron",
    "sn": "Soutmun"
  },
  {
    "info": "# nevyn, users, debian.org",
    "dn": {
      "uid": "nevyn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alwyn",
    "uid": "nevyn",
    "uidNumber": "2501",
    "sn": "Schoeman",
    "gidNumber": "2501"
  },
  {
    "info": "# ng, users, debian.org",
    "dn": {
      "uid": "ng",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mark",
    "uid": "ng",
    "uidNumber": "1327",
    "sn": "Ng",
    "gidNumber": "1327"
  },
  {
    "info": "# nhandler, users, debian.org",
    "dn": {
      "uid": "nhandler",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nhandler",
    "objectClass": "debianDeveloper",
    "uidNumber": "3229",
    "keyFingerPrint": "31DCC3B33F2179883DA014C7CAFB3BAD0A75C877",
    "cn": "Nathaniel",
    "sn": "Handler",
    "gidNumber": "3229",
    "ircNick": "nhandler"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "nic",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nick",
    "uid": "nic",
    "uidNumber": "1284",
    "sn": "Cropper",
    "gidNumber": "1284"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "nick",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nicolas",
    "uid": "nick",
    "sn": "Lichtmaier",
    "labeledURI": "http://people.debian.org/~nick/",
    "uidNumber": "996",
    "ircNick": "nick",
    "gidNumber": "996"
  },
  {
    "info": "# nickm, users, debian.org",
    "dn": {
      "uid": "nickm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3580",
    "gidNumber": "3580",
    "keyFingerPrint": "22B3D97BE9E898C90FF6523B3E9B59AC6EC2D359",
    "cn": "Nick",
    "sn": "Morrott",
    "uid": "nickm",
    "ircNick": "knowledgejunkie"
  },
  {
    "info": "# nickrusnov, users, debian.org",
    "dn": {
      "uid": "nickrusnov",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nick",
    "uid": "nickrusnov",
    "sn": "Rusnov",
    "uidNumber": "2407",
    "ircNick": "nickr",
    "labeledURI": "http://rusnov.net/",
    "gidNumber": "2407"
  },
  {
    "info": "# nico, users, debian.org",
    "dn": {
      "uid": "nico",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nicolas",
    "uid": "nico",
    "sn": "Sabouret",
    "uidNumber": "2397",
    "ircNick": "nico_sa",
    "labeledURI": "http://www-poleia.lip6.fr/~sabouret/debian",
    "gidNumber": "2397"
  },
  {
    "info": "# nicolas, users, debian.org",
    "dn": {
      "uid": "nicolas",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nicolas",
    "objectClass": "debianDeveloper",
    "uidNumber": "3226",
    "cn": "Nicolas",
    "sn": "Boulenguez",
    "gidNumber": "3226",
    "keyFingerPrint": "6B09BFEB3621AA47D300A1ED14729AC9980F09D2"
  },
  {
    "info": "# nicoo, users, debian.org",
    "dn": {
      "uid": "nicoo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nicoo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3543",
    "gidNumber": "3543",
    "keyFingerPrint": "E44E9EA54B8E256AFB7349D3EC9D370872BC7A8C",
    "cn": ": IA==",
    "sn": "Nicoo",
    "ircNick": "nicoo",
    "labeledURI": "https://nicolas.braud-santoni.eu"
  },
  {
    "info": "# nidd, users, debian.org",
    "dn": {
      "uid": "nidd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "jabberJID": "nidd@altlinux.org",
    "uid": "nidd",
    "sn": "Novodvorsky",
    "labeledURI": "http://people.debian.org/~nidd",
    "uidNumber": "2076",
    "ircNick": "nidd",
    "gidNumber": "2076"
  },
  {
    "info": "# nijel, users, debian.org",
    "dn": {
      "uid": "nijel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nijel",
    "objectClass": "debianDeveloper",
    "uidNumber": "2858",
    "cn": "Michal",
    "sn": "Cihar",
    "keyFingerPrint": "63CB1DF1EF12CF2AC0EE5A329C27B31342B7511D",
    "gidNumber": "2858",
    "ircNick": "nijel",
    "labeledURI": "https://cihar.com/"
  },
  {
    "info": "# niklas, users, debian.org",
    "dn": {
      "uid": "niklas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Niklas",
    "uid": "niklas",
    "uidNumber": "1291",
    "sn": "Hoglund",
    "gidNumber": "1291"
  },
  {
    "info": "# nikratio, users, debian.org",
    "dn": {
      "uid": "nikratio",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nikratio",
    "objectClass": "debianDeveloper",
    "uidNumber": "3381",
    "keyFingerPrint": "ED31791B2C5C1613AF388B8AD113FCAC3C4E599F",
    "cn": "Nikolaus",
    "sn": "Rath",
    "gidNumber": "3381"
  },
  {
    "info": "oAmze1cu4KZJhvko096H2PrwNuSfF1TQIDAQAB",
    "dn": {
      "uid": "nilesh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nilesh",
    "objectClass": "debianDeveloper",
    "uidNumber": "3608",
    "gidNumber": "3608",
    "cn": "Nilesh",
    "sn": "Patra",
    "keyFingerPrint": "A095B66EE09024BEE6A2F0722A27904BD7243EDA",
    "ircNick": "gargantua_kerr",
    "labeledURI": "https://nileshpatra.info/"
  },
  {
    "info": "# nils, users, debian.org",
    "dn": {
      "uid": "nils",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nils",
    "uid": "nils",
    "sn": "Rennebarth",
    "uidNumber": "861",
    "gidNumber": "861"
  },
  {
    "info": "# nion, users, debian.org",
    "dn": {
      "uid": "nion",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nion",
    "objectClass": "debianDeveloper",
    "uidNumber": "2860",
    "cn": "Nico",
    "sn": "Golde",
    "gidNumber": "2860",
    "ircNick": "nion",
    "jabberJID": "nion@jabber.ccc.de",
    "labeledURI": "http://www.ngolde.de"
  },
  {
    "info": "# nirgal, users, debian.org",
    "dn": {
      "uid": "nirgal",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nirgal",
    "objectClass": "debianDeveloper",
    "uidNumber": "3351",
    "keyFingerPrint": "408303E7B34974006565532B3B5C2C71A218D83C",
    "cn": "Jean-Michel",
    "gidNumber": "3351",
    "sn": ": Vm91cmfDqHJl",
    "ircNick": "Nirgal"
  },
  {
    "info": "# njordan, users, debian.org",
    "dn": {
      "uid": "njordan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Norman",
    "uid": "njordan",
    "uidNumber": "2193",
    "ircNick": "Apocalypse",
    "sn": "Jordan",
    "gidNumber": "2193"
  },
  {
    "info": "# nmav, users, debian.org",
    "dn": {
      "uid": "nmav",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nmav",
    "objectClass": "debianDeveloper",
    "uidNumber": "3297",
    "keyFingerPrint": "1F42418905D8206AA754CCDC29EE58B996865171",
    "cn": "Nikos",
    "sn": "Mavrogiannopoulos",
    "gidNumber": "3297"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "nnair",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nikhil",
    "uid": "nnair",
    "uidNumber": "862",
    "sn": "Nair",
    "gidNumber": "862"
  },
  {
    "info": "k8sJXohjLSsdTZNkP3/ic0xAZIcOsX2QIDAQAB",
    "dn": {
      "uid": "noahm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Noah",
    "uid": "noahm",
    "sn": "Meyerhans",
    "uidNumber": "2313",
    "keyFingerPrint": "A4D19E6ED3C1331EF253EA251CD8D854FE4252C1",
    "gidNumber": "2313",
    "ircNick": "noahm",
    "labeledURI": "https://noah.meyerhans.us/"
  },
  {
    "info": "# nobse, users, debian.org",
    "dn": {
      "uid": "nobse",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Norbert",
    "uid": "nobse",
    "sn": "Tretkowski",
    "uidNumber": "2209",
    "gidNumber": "2209",
    "labeledURI": "https://tretkowski.de/"
  },
  {
    "info": "# nodens, users, debian.org",
    "dn": {
      "uid": "nodens",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nodens",
    "objectClass": "debianDeveloper",
    "uidNumber": "3578",
    "gidNumber": "3578",
    "keyFingerPrint": "06639AFAA76D6B480169708830E674676859C8AD",
    "cn": ": Q2zDqW1lbnQ=",
    "sn": "Hermann"
  },
  {
    "info": "# noel, users, debian.org",
    "dn": {
      "uid": "noel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "noel",
    "uidNumber": "2440",
    "keyFingerPrint": "A45E405C0C6C80F13FF1521768C078BE88F80CDA",
    "ircNick": "noel",
    "jabberJID": "noelkoethe@jabber.org",
    "labeledURI": "http://people.debian.org/~noel/",
    "gidNumber": "2440",
    "cn": ": Tm/DqGw=",
    "sn": ": S8O2dGhl"
  },
  {
    "info": "# nomadium, users, debian.org",
    "dn": {
      "uid": "nomadium",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nomadium",
    "objectClass": "debianDeveloper",
    "uidNumber": "3280",
    "keyFingerPrint": "4CB7FE1E280ECC90F29A597E6E608B637D8967E9",
    "cn": "Miguel",
    "sn": "Landaeta",
    "ircNick": "nomadium",
    "labeledURI": "http://miguel.cc/",
    "gidNumber": "3280"
  },
  {
    "info": "# nomeata, users, debian.org",
    "dn": {
      "uid": "nomeata",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joachim",
    "uid": "nomeata",
    "sn": "Breitner",
    "uidNumber": "2634",
    "gidNumber": "2634",
    "ircNick": "nomeata",
    "jabberJID": "nomeata@joachim-breitner.de",
    "labeledURI": "http://www.joachim-breitner.de/"
  },
  {
    "info": "# nonaka, users, debian.org",
    "dn": {
      "uid": "nonaka",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ken",
    "uid": "nonaka",
    "uidNumber": "2062",
    "sn": "Nonaka",
    "gidNumber": "2062"
  },
  {
    "info": "# noodles, users, debian.org",
    "dn": {
      "uid": "noodles",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jonathan",
    "keyFingerPrint": "0E3A94C3E83002DAB88CCA1694FA372B2DA8B985",
    "uid": "noodles",
    "sn": "McDowell",
    "uidNumber": "2144",
    "gidNumber": "2144",
    "ircNick": "Noodles",
    "labeledURI": "https://www.earth.li/~noodles/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "noop",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Greg",
    "uid": "noop",
    "uidNumber": "2232",
    "ircNick": "noop",
    "sn": "Olszewski",
    "gidNumber": "2232"
  },
  {
    "info": "# noshiro, users, debian.org",
    "dn": {
      "uid": "noshiro",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shigeo",
    "uid": "noshiro",
    "uidNumber": "2366",
    "sn": "Noshiro",
    "ircNick": "nnn",
    "keyFingerPrint": "7951D6DDD2E12A6C47520A89B6D3FC68BA25360D",
    "gidNumber": "2366"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "nrubin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Neil",
    "uid": "nrubin",
    "uidNumber": "903",
    "sn": "Rubin",
    "gidNumber": "903"
  },
  {
    "info": "# nsandver, users, debian.org",
    "dn": {
      "uid": "nsandver",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nathan",
    "uid": "nsandver",
    "uidNumber": "1282",
    "ircNick": "nsandver",
    "sn": "Sandver",
    "gidNumber": "1282"
  },
  {
    "info": "# nthykier, users, debian.org",
    "dn": {
      "uid": "nthykier",
      "ou": "users",
      "dc": "org"
    },
    "uid": "nthykier",
    "objectClass": "debianDeveloper",
    "uidNumber": "3106",
    "keyFingerPrint": "B3131A451DBFDF7CA05B4197054BBB9F7D806442",
    "cn": "Niels",
    "sn": "Thykier",
    "ircNick": "nthykier",
    "gidNumber": "3106"
  },
  {
    "info": "# ntyni, users, debian.org",
    "dn": {
      "uid": "ntyni",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ntyni",
    "objectClass": "debianDeveloper",
    "uidNumber": "2887",
    "cn": "Niko",
    "sn": "Tyni",
    "keyFingerPrint": "76A28E42C9811D91E88FBA5E2EC0FFB3B7301B1F",
    "gidNumber": "2887"
  },
  {
    "info": "# nveber, users, debian.org",
    "dn": {
      "uid": "nveber",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Norbert",
    "keyFingerPrint": "CF677AECDC3E11621E32AF6AA217C4C35E2EB5B4",
    "uid": "nveber",
    "sn": "Veber",
    "uidNumber": "1213",
    "ircNick": "Primus",
    "labeledURI": "http://www.tamariongroup.com",
    "gidNumber": "1671"
  },
  {
    "info": "# nwp, users, debian.org",
    "dn": {
      "uid": "nwp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nick",
    "keyFingerPrint": "3A6D1E36E58A91CCC372A68E7592489BCC983928",
    "uid": "nwp",
    "sn": "Phillips",
    "uidNumber": "2487",
    "ircNick": "nwp",
    "gidNumber": "2487"
  },
  {
    "info": "# obergix, users, debian.org",
    "dn": {
      "uid": "obergix",
      "ou": "users",
      "dc": "org"
    },
    "uid": "obergix",
    "objectClass": "debianDeveloper",
    "uidNumber": "3133",
    "cn": "Olivier",
    "sn": "Berger",
    "gidNumber": "3133",
    "ircNick": "obergix",
    "labeledURI": "http://www.olivierberger.org/"
  },
  {
    "info": "# ockman, users, debian.org",
    "dn": {
      "uid": "ockman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Samuel",
    "uid": "ockman",
    "uidNumber": "1125",
    "sn": "Ockman",
    "gidNumber": "1125"
  },
  {
    "info": "# ocsi, users, debian.org",
    "dn": {
      "uid": "ocsi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lenart",
    "uid": "ocsi",
    "sn": "Janos",
    "uidNumber": "2219",
    "keyFingerPrint": "83CF22A155BD99BBC084B13FA0A9766CDB362222",
    "gidNumber": "2219",
    "ircNick": "ocsi",
    "labeledURI": "https://lenart.io/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "octavian",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Holger",
    "uid": "octavian",
    "uidNumber": "1322",
    "sn": "Eitzenberger",
    "gidNumber": "1322"
  },
  {
    "info": "cciE7yFCLDmYQIDAQAB k=rsa",
    "dn": {
      "uid": "odyx",
      "ou": "users",
      "dc": "org"
    },
    "uid": "odyx",
    "objectClass": "debianDeveloper",
    "uidNumber": "3114",
    "keyFingerPrint": "5D3E052646729E4E85F05B3FD929F2992BEF0A33",
    "cn": "Didier",
    "sn": "Raboud",
    "gidNumber": "3114",
    "ircNick": "OdyX",
    "labeledURI": "https://odyx.org"
  },
  {
    "info": "# ogi, users, debian.org",
    "dn": {
      "uid": "ogi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ogi",
    "objectClass": "debianDeveloper",
    "uidNumber": "2781",
    "cn": "Ognyan",
    "sn": "Kulev",
    "keyFingerPrint": "5A4375C9244E9554A975F2F6AB98288E36D33D07",
    "ircNick": "ogi",
    "jabberJID": "ognyan@ognyankulev.com",
    "labeledURI": "http://www.ognyankulev.com/",
    "gidNumber": "2781"
  },
  {
    "info": "# ohura, users, debian.org",
    "dn": {
      "uid": "ohura",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Makoto",
    "uid": "ohura",
    "uidNumber": "2624",
    "sn": "OHURA",
    "keyFingerPrint": "CE5B4C3A973A37E0F096BE2831B8DEDB8C6DF5CD",
    "gidNumber": "2624",
    "labeledURI": "http://www.netfort.gr.jp/~ohura/"
  },
  {
    "info": "# okoli, users, debian.org",
    "dn": {
      "uid": "okoli",
      "ou": "users",
      "dc": "org"
    },
    "uid": "okoli",
    "objectClass": "debianDeveloper",
    "uidNumber": "3038",
    "cn": "Oliver",
    "sn": "Korff",
    "labeledURI": "http://www.linuxchess.org",
    "gidNumber": "3038"
  },
  {
    "info": "# oku, users, debian.org",
    "dn": {
      "uid": "oku",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Oliver",
    "uid": "oku",
    "uidNumber": "2567",
    "sn": "Kurth",
    "gidNumber": "2567"
  },
  {
    "info": "# olasd, users, debian.org",
    "dn": {
      "uid": "olasd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "olasd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3264",
    "cn": "Nicolas",
    "sn": "Dandrimont",
    "gidNumber": "3264",
    "keyFingerPrint": "6F339C5E1725D5E379100F096F31F7545A885252",
    "ircNick": "olasd",
    "jabberJID": "olasd@debian.org",
    "labeledURI": "https://blog.olasd.eu"
  },
  {
    "info": "# oldw, users, debian.org",
    "dn": {
      "uid": "oldw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tibor",
    "uid": "oldw",
    "sn": "Koleszar",
    "uidNumber": "1335",
    "gidNumber": "1335"
  },
  {
    "info": "# olebole, users, debian.org",
    "dn": {
      "uid": "olebole",
      "ou": "users",
      "dc": "org"
    },
    "uid": "olebole",
    "objectClass": "debianDeveloper",
    "uidNumber": "3326",
    "keyFingerPrint": "BAFC6C85F7CB143FEEB6FB157115AFD07710DCF7",
    "cn": "Ole",
    "sn": "Streicher",
    "gidNumber": "3326",
    "ircNick": "olebole"
  },
  {
    "info": "# olek, users, debian.org",
    "dn": {
      "uid": "olek",
      "ou": "users",
      "dc": "org"
    },
    "uid": "olek",
    "objectClass": "debianDeveloper",
    "uidNumber": "3440",
    "gidNumber": "3440",
    "keyFingerPrint": "BE44E7CCB558C248832035C8EBE4E3EE94176FC2",
    "cn": "Olek",
    "sn": "Wojnar",
    "ircNick": "olek"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "olet",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ole",
    "uid": "olet",
    "uidNumber": "1108",
    "sn": "Tetlie",
    "gidNumber": "1108"
  },
  {
    "info": "# oliva, users, debian.org",
    "dn": {
      "uid": "oliva",
      "ou": "users",
      "dc": "org"
    },
    "uid": "oliva",
    "objectClass": "debianDeveloper",
    "uidNumber": "3136",
    "keyFingerPrint": "EB3345F56441B8B81A7798767DFA41AD961985D7",
    "cn": "Gennaro",
    "sn": "Oliva",
    "gidNumber": "3136",
    "ircNick": "oliva"
  },
  {
    "info": "# oliver, users, debian.org",
    "dn": {
      "uid": "oliver",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Oliver",
    "uid": "oliver",
    "sn": "Bolzer",
    "labeledURI": "http://www.fakeroot.net/",
    "uidNumber": "2054",
    "ircNick": "OliB",
    "gidNumber": "2054"
  },
  {
    "info": "# olly, users, debian.org",
    "dn": {
      "uid": "olly",
      "ou": "users",
      "dc": "org"
    },
    "uid": "olly",
    "objectClass": "debianDeveloper",
    "uidNumber": "3029",
    "keyFingerPrint": "08E2400FF7FE8FEDE3ACB52818147B073BAD2B07",
    "cn": "Olly",
    "sn": "Betts",
    "gidNumber": "3029",
    "ircNick": "olly"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "oly",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Oliver",
    "uid": "oly",
    "uidNumber": "894",
    "sn": "Oberdorf",
    "gidNumber": "894"
  },
  {
    "info": "# omote, users, debian.org",
    "dn": {
      "uid": "omote",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Masahito",
    "uid": "omote",
    "sn": "Omote",
    "uidNumber": "2452",
    "keyFingerPrint": "E4D1B12B5F8854BF014D4FBE60C8350B6C88DA32",
    "gidNumber": "2452",
    "ircNick": "omote"
  },
  {
    "info": "# ondrej, users, debian.org",
    "dn": {
      "uid": "ondrej",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ondrej",
    "uid": "ondrej",
    "sn": "Sury",
    "uidNumber": "2094",
    "keyFingerPrint": "30B933D80FCE3D981A2D38FB0C99B70EF4FCBB07",
    "gidNumber": "2094",
    "ircNick": "ondrej",
    "labeledURI": "https://deb.sury.org/"
  },
  {
    "info": "# onlyjob, users, debian.org",
    "dn": {
      "uid": "onlyjob",
      "ou": "users",
      "dc": "org"
    },
    "uid": "onlyjob",
    "objectClass": "debianDeveloper",
    "uidNumber": "3241",
    "keyFingerPrint": "50BC7CF939D20C272A6B065652B6BBD953968D1B",
    "cn": "Dmitry",
    "sn": "Smirnov",
    "gidNumber": "3241",
    "ircNick": "onlyjob",
    "jabberJID": "onlyjob@member.fsf.org"
  },
  {
    "info": "# onovy, users, debian.org",
    "dn": {
      "uid": "onovy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "onovy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3410",
    "gidNumber": "3410",
    "keyFingerPrint": "3D983C52EB85980C46A56090357312559D1E064B",
    "cn": ": T25kxZllag==",
    "sn": ": Tm92w70=",
    "ircNick": "onovy",
    "labeledURI": "https://www.ondrej.org/"
  },
  {
    "info": "# oohara, users, debian.org",
    "dn": {
      "uid": "oohara",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yuuma",
    "keyFingerPrint": "61428D079C5B159BC1701F4A40D6F42EF464A695",
    "uid": "oohara",
    "uidNumber": "2462",
    "sn": "Oohara",
    "gidNumber": "2462"
  },
  {
    "info": "# opal, users, debian.org",
    "dn": {
      "uid": "opal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ola",
    "uid": "opal",
    "sn": "Lundqvist",
    "uidNumber": "2090",
    "keyFingerPrint": "22F232C6B1E0F4BF2B260A6A5E90DCFA9426876F",
    "gidNumber": "2090",
    "labeledURI": "http://inguza.com/"
  },
  {
    "info": "# orestis, users, debian.org",
    "dn": {
      "uid": "orestis",
      "ou": "users",
      "dc": "org"
    },
    "uid": "orestis",
    "objectClass": "debianDeveloper",
    "uidNumber": "3469",
    "gidNumber": "3469",
    "keyFingerPrint": "BE164328ADB828DAA8DC0628F3801F5B10D7F7EC",
    "cn": "Orestis",
    "sn": "Ioannou"
  },
  {
    "info": "# orv, users, debian.org",
    "dn": {
      "uid": "orv",
      "ou": "users",
      "dc": "org"
    },
    "uid": "orv",
    "objectClass": "debianDeveloper",
    "uidNumber": "3611",
    "gidNumber": "3611",
    "keyFingerPrint": "C3A504840B678260DA12766AD25D611C8E192076",
    "cn": "Benda",
    "sn": "Xu",
    "ircNick": "heroxbd",
    "labeledURI": "http://hep.tsinghua.edu.cn/~orv/"
  },
  {
    "info": "# osallou, users, debian.org",
    "dn": {
      "uid": "osallou",
      "ou": "users",
      "dc": "org"
    },
    "uid": "osallou",
    "objectClass": "debianDeveloper",
    "uidNumber": "3177",
    "keyFingerPrint": "5FB46F83D3B952046335D26D78DC68DB326D8438",
    "cn": "Olivier",
    "sn": "Sallou",
    "gidNumber": "3177"
  },
  {
    "info": "# osamu, users, debian.org",
    "dn": {
      "uid": "osamu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Osamu",
    "uid": "osamu",
    "sn": "Aoki",
    "uidNumber": "2539",
    "keyFingerPrint": "3133724D6207881579E95D621E1356881DD8D791",
    "gidNumber": "2539",
    "ircNick": "osamu",
    "labeledURI": "http://people.debian.org/~osamu"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "osd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Owen",
    "uid": "osd",
    "uidNumber": "914",
    "sn": "Dunn",
    "gidNumber": "914"
  },
  {
    "info": "# ossama, users, debian.org",
    "dn": {
      "uid": "ossama",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.dre.vanderbilt.edu/~ossama/",
    "cn": "Ossama",
    "uid": "ossama",
    "uidNumber": "1318",
    "ircNick": "Void",
    "sn": "Othman",
    "gidNumber": "1318"
  },
  {
    "info": "# ossk, users, debian.org",
    "dn": {
      "uid": "ossk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Volker",
    "uid": "ossk",
    "uidNumber": "955",
    "sn": "Ossenkopf",
    "labeledURI": "http://www.ph1.uni-koeln.de/~ossk/",
    "gidNumber": "955"
  },
  {
    "info": "# otavio, users, debian.org",
    "dn": {
      "uid": "otavio",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Otavio",
    "uid": "otavio",
    "sn": "Salvador",
    "uidNumber": "2471",
    "gidNumber": "2471",
    "ircNick": "otavio",
    "jabberJID": "otavio.salvador@gmail.com"
  },
  {
    "info": "# othmar, users, debian.org",
    "dn": {
      "uid": "othmar",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Othmar",
    "uid": "othmar",
    "sn": "Pasteka",
    "uidNumber": "2024",
    "ircNick": "BlindMan",
    "jabberJID": "othmar@tron.at",
    "gidNumber": "2024"
  },
  {
    "info": "# otto, users, debian.org",
    "dn": {
      "uid": "otto",
      "ou": "users",
      "dc": "org"
    },
    "uid": "otto",
    "objectClass": "debianDeveloper",
    "uidNumber": "3348",
    "keyFingerPrint": "99B452B11F3C74C3B453E46FBED8449FCEE8DA88",
    "cn": "Otto",
    "gidNumber": "3348",
    "sn": ": S2Vrw6Rsw6RpbmVu",
    "labeledURI": "http://otto.kekalainen.net/"
  },
  {
    "info": "# ovek, users, debian.org",
    "dn": {
      "uid": "ovek",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ove",
    "uid": "ovek",
    "uidNumber": "2191",
    "gidNumber": "2191",
    "ircNick": "villager",
    "labeledURI": "http://www.ovekaaven.com/",
    "sn": ": S8OldmVu",
    "keyFingerPrint": "0CA048866B0885360308456B15C0E9E382F72CBE"
  },
  {
    "info": "# p2, users, debian.org",
    "dn": {
      "uid": "p2",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "p2",
    "uidNumber": "2712",
    "sn": "De Schrijver",
    "ircNick": "p2-mate",
    "keyFingerPrint": "E9BC51F6D467A31687DD3F61515BD7CAEEAE3C13",
    "gidNumber": "2712"
  },
  {
    "info": "# pa, users, debian.org",
    "dn": {
      "uid": "pa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.edu.lahti.fi/~zur/",
    "cn": "Pekka",
    "uid": "pa",
    "uidNumber": "1394",
    "ircNick": "zur",
    "sn": "Knuutila",
    "gidNumber": "1394"
  },
  {
    "info": "# pa3aba, users, debian.org",
    "dn": {
      "uid": "pa3aba",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Joop",
    "uid": "pa3aba",
    "sn": "Stakenborg",
    "uidNumber": "1132",
    "gidNumber": "1676"
  },
  {
    "info": "# pabigot, users, debian.org",
    "dn": {
      "uid": "pabigot",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pabigot",
    "objectClass": "debianDeveloper",
    "uidNumber": "3173",
    "cn": "Peter",
    "sn": "Bigot",
    "gidNumber": "3173"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "pablo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://pablo.averbuj.com",
    "cn": "Pablo",
    "uid": "pablo",
    "uidNumber": "1402",
    "ircNick": "Tombstone",
    "sn": "Averbuj",
    "gidNumber": "1402"
  },
  {
    "info": "# pabs, users, debian.org",
    "dn": {
      "uid": "pabs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pabs",
    "objectClass": "debianDeveloper",
    "uidNumber": "2813",
    "cn": "Paul",
    "sn": "Wise",
    "keyFingerPrint": "610B28B55CFCFE45EA1B563B3116BA5E9FFA69A3",
    "gidNumber": "2813",
    "ircNick": "pabs",
    "jabberJID": "pabs@debian.org",
    "labeledURI": "https://wiki.debian.org/PaulWise"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "paci",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.prosa.it/~paci",
    "cn": "Davide",
    "uid": "paci",
    "uidNumber": "987",
    "ircNick": "paci",
    "sn": "Barbieri",
    "gidNumber": "987"
  },
  {
    "info": "# paco, users, debian.org",
    "dn": {
      "uid": "paco",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paco",
    "objectClass": "debianDeveloper",
    "uidNumber": "2885",
    "cn": "Francisco",
    "sn": "Moya",
    "gidNumber": "2885"
  },
  {
    "info": "mbIB8sMY",
    "dn": {
      "uid": "paddatrapper",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paddatrapper",
    "objectClass": "debianDeveloper",
    "uidNumber": "3536",
    "gidNumber": "3536",
    "cn": "Kyle",
    "sn": "Robbertze",
    "keyFingerPrint": "96A58F182BB8F9B81C5F2733854F07BE082DF14F",
    "ircNick": "paddatrapper"
  },
  {
    "info": "# paelzer, users, debian.org",
    "dn": {
      "uid": "paelzer",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paelzer",
    "objectClass": "debianDeveloper",
    "uidNumber": "3627",
    "gidNumber": "3627",
    "keyFingerPrint": "92D618F668F22F8ED80BEEF5BA3E29338280B242",
    "cn": "Christian",
    "sn": "Ehrhardt",
    "ircNick": "cpaelzer",
    "labeledURI": "https://cpaelzer.github.io/"
  },
  {
    "info": "# pape, users, debian.org",
    "dn": {
      "uid": "pape",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gerrit",
    "uid": "pape",
    "uidNumber": "2215",
    "sn": "Pape",
    "gidNumber": "2215",
    "keyFingerPrint": "DAC43860630556B6DBF0898FA5DAAEFCB14D13CC",
    "labeledURI": "http://smarden.org/pape/"
  },
  {
    "info": "# paravoid, users, debian.org",
    "dn": {
      "uid": "paravoid",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paravoid",
    "objectClass": "debianDeveloper",
    "uidNumber": "2797",
    "cn": "Faidon",
    "sn": "Liambotis",
    "keyFingerPrint": "A9592C521CB904077D6598009D0B5E5B1EEC8F0E",
    "gidNumber": "2797",
    "ircNick": "paravoid"
  },
  {
    "info": "# paride, users, debian.org",
    "dn": {
      "uid": "paride",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paride",
    "objectClass": "debianDeveloper",
    "uidNumber": "3471",
    "gidNumber": "3471",
    "keyFingerPrint": "1BD886F246FD490879D4E1505A09B4576DE8080E",
    "cn": "Paride",
    "sn": "Legovini",
    "ircNick": "paride",
    "labeledURI": "https://paride.legovini.net/"
  },
  {
    "info": "# pasc, users, debian.org",
    "dn": {
      "uid": "pasc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pascal",
    "uid": "pasc",
    "sn": "Hakim",
    "uidNumber": "2509",
    "ircNick": "pasc",
    "labeledURI": "http://www.redellipse.net/",
    "gidNumber": "2509"
  },
  {
    "info": "# pascal, users, debian.org",
    "dn": {
      "uid": "pascal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pascal",
    "uid": "pascal",
    "sn": "Giard",
    "uidNumber": "2694",
    "gidNumber": "2694",
    "ircNick": "evilynux",
    "labeledURI": {}
  },
  {
    "info": "# pat, users, debian.org",
    "dn": {
      "uid": "pat",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "pat",
    "uidNumber": "1275",
    "ircNick": "petert",
    "sn": "Teichman",
    "gidNumber": "1275"
  },
  {
    "info": "# patrick, users, debian.org",
    "dn": {
      "uid": "patrick",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "patrick",
    "sn": "Caulfield",
    "uidNumber": "2221",
    "ircNick": "chrissie",
    "gidNumber": "2221"
  },
  {
    "info": "# patryk, users, debian.org",
    "dn": {
      "uid": "patryk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "patryk",
    "objectClass": "debianDeveloper",
    "uidNumber": "2985",
    "cn": "Patryk",
    "sn": "Cisek",
    "gidNumber": "2985",
    "keyFingerPrint": "41726E33A5844A77D2EF1DAF1ABFA401CCAA707A"
  },
  {
    "info": "# paul, users, debian.org",
    "dn": {
      "uid": "paul",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "paul",
    "sn": "Slootman",
    "uidNumber": "1115",
    "keyFingerPrint": "C9F34FD3C01971BC5BF5586932F78DCEAD113B8B",
    "gidNumber": "1115",
    "ircNick": "wurtel (freenode.net)"
  },
  {
    "info": "# paulliu, users, debian.org",
    "dn": {
      "uid": "paulliu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paulliu",
    "objectClass": "debianDeveloper",
    "uidNumber": "2988",
    "cn": "Ying-Chun",
    "sn": "Liu",
    "keyFingerPrint": "A36878F464108681600CB64844173FA13D058888",
    "gidNumber": "2988",
    "ircNick": "paulliu",
    "jabberJID": "grandpaul@gmail.com",
    "labeledURI": "https://about.me/grandpaul"
  },
  {
    "info": "# paulnovo, users, debian.org",
    "dn": {
      "uid": "paulnovo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paulnovo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3249",
    "cn": "Paul",
    "sn": "Novotny",
    "keyFingerPrint": "8AD6A0674EA6BA29D3C822D9FEAA132F983191EA",
    "gidNumber": "3249"
  },
  {
    "info": "# paulproteus, users, debian.org",
    "dn": {
      "uid": "paulproteus",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paulproteus",
    "objectClass": "debianDeveloper",
    "uidNumber": "2987",
    "cn": "Asheesh",
    "sn": "Laroia",
    "keyFingerPrint": "97225E589D6B9E01533C485EEC4B033C70096AD1",
    "gidNumber": "2987",
    "ircNick": "paulproteus",
    "labeledURI": "http://www.asheesh.org/"
  },
  {
    "info": "# paultag, users, debian.org",
    "dn": {
      "uid": "paultag",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paultag",
    "objectClass": "debianDeveloper",
    "uidNumber": "3209",
    "cn": "Paul",
    "sn": "Tagliamonte",
    "gidNumber": "3209",
    "keyFingerPrint": "FEF2EB2016E6A856B98CE8202DCD6B5DE858ADF3",
    "ircNick": "paultag",
    "labeledURI": "https://pault.ag/"
  },
  {
    "info": "# paulvt, users, debian.org",
    "dn": {
      "uid": "paulvt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "paulvt",
    "sn": "van Tilburg",
    "uidNumber": "2121",
    "ircNick": "paulvt",
    "jabberJID": "paul@luon.net",
    "labeledURI": "http://paul.luon.net/",
    "keyFingerPrint": "9122D77A6212860737F36FA5C6DE073EDA9EEC4D",
    "gidNumber": "2121"
  },
  {
    "info": "# paulwaite, users, debian.org",
    "dn": {
      "uid": "paulwaite",
      "ou": "users",
      "dc": "org"
    },
    "uid": "paulwaite",
    "objectClass": "debianDeveloper",
    "uidNumber": "2796",
    "cn": "Paul",
    "sn": "Waite",
    "keyFingerPrint": "493A66484BB79570D642FA3089EADCE39B80E069",
    "gidNumber": "2796",
    "ircNick": "paulw"
  },
  {
    "info": "# pavel, users, debian.org",
    "dn": {
      "uid": "pavel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pavel",
    "uid": "pavel",
    "uidNumber": "2265",
    "ircNick": "kay",
    "sn": "Tcholakov",
    "gidNumber": "2265"
  },
  {
    "info": "# pb, users, debian.org",
    "dn": {
      "uid": "pb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philip",
    "uid": "pb",
    "sn": "Blundell",
    "uidNumber": "2095",
    "ircNick": "pb_",
    "gidNumber": "2095"
  },
  {
    "info": "# pbrown, users, debian.org",
    "dn": {
      "uid": "pbrown",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philip",
    "uid": "pbrown",
    "uidNumber": "2096",
    "sn": "Brown",
    "gidNumber": "2096"
  },
  {
    "info": "# pcolberg, users, debian.org",
    "dn": {
      "uid": "pcolberg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pcolberg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3428",
    "gidNumber": "3428",
    "keyFingerPrint": "29976EC22BD1EBBCCC77F3A6183BD5ED35278611",
    "cn": "Peter",
    "sn": "Colberg"
  },
  {
    "info": "# pda, users, debian.org",
    "dn": {
      "uid": "pda",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "pda",
    "uidNumber": "2408",
    "sn": "Ashmore",
    "gidNumber": "2408"
  },
  {
    "info": "# pdm, users, debian.org",
    "dn": {
      "uid": "pdm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Milan",
    "uid": "pdm",
    "sn": "Zamazal",
    "uidNumber": "965",
    "labeledURI": "http://www.zamazal.org",
    "keyFingerPrint": "3EFE0F3DF0C98E28B8A81C4714D01A08AFD89BE3",
    "gidNumber": "965"
  },
  {
    "info": "wwJzyZz7CQ+zKkNCK5Z1RpnxkldQIDAQAB",
    "dn": {
      "uid": "peb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "peb",
    "objectClass": "debianDeveloper",
    "uidNumber": "3542",
    "gidNumber": "3542",
    "keyFingerPrint": "9AE04D986400E3B67528F4930D442664194974E2",
    "cn": "Pierre-Elliott",
    "sn": ": QsOpY3Vl",
    "ircNick": "peb"
  },
  {
    "info": "# pelle, users, debian.org",
    "dn": {
      "uid": "pelle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Per",
    "uid": "pelle",
    "sn": "Olofsson",
    "uidNumber": "2689",
    "ircNick": "pelle",
    "labeledURI": "http://people.dsv.su.se/~pelle/",
    "gidNumber": "2689"
  },
  {
    "info": "# peloy, users, debian.org",
    "dn": {
      "uid": "peloy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eloy",
    "keyFingerPrint": "31A2171EFA6B051885809BD86A08D37C0B4D63D8",
    "uid": "peloy",
    "sn": "Paris",
    "uidNumber": "1091",
    "ircNick": "peloy",
    "gidNumber": "1091"
  },
  {
    "info": "# pere, users, debian.org",
    "dn": {
      "uid": "pere",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Petter",
    "uid": "pere",
    "sn": "Reinholdtsen",
    "uidNumber": "2556",
    "ircNick": "pere",
    "labeledURI": "http://www.hungry.com/~pere/",
    "gidNumber": "2556",
    "keyFingerPrint": "3AC7B2E3ACA5DF8778F1D827111D6B29EE4E02F9"
  },
  {
    "info": "# periapt, users, debian.org",
    "dn": {
      "uid": "periapt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "periapt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3141",
    "cn": "Nicholas",
    "sn": "Bamber",
    "ircNick": "periapt",
    "labeledURI": "http://www.periapt.co.uk",
    "gidNumber": "3141"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "pesch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fred",
    "uid": "pesch",
    "uidNumber": "960",
    "sn": "eXode",
    "gidNumber": "960"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "peter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "peter",
    "uidNumber": "1024",
    "sn": "Irannelli",
    "gidNumber": "1024"
  },
  {
    "info": "# petere, users, debian.org",
    "dn": {
      "uid": "petere",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "petere",
    "uidNumber": "2703",
    "sn": "Eisentraut",
    "keyFingerPrint": "EEA24D48558A79103E80EE29A19F1243EC41269F",
    "gidNumber": "2703",
    "ircNick": "petere",
    "jabberJID": "peter.eisentraut@jabber.ccc.de",
    "labeledURI": "http://peter.eisentraut.org/"
  },
  {
    "info": "# peterh, users, debian.org",
    "dn": {
      "uid": "peterh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "peterh",
    "sn": "Hawkins",
    "labeledURI": "http://www.hawkins.emu.id.au/",
    "uidNumber": "2511",
    "ircNick": "fishbowl",
    "gidNumber": "2511"
  },
  {
    "info": "# peterk, users, debian.org",
    "dn": {
      "uid": "peterk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "peterk",
    "uidNumber": "2029",
    "ircNick": "nafmo",
    "labeledURI": "http://www.softwolves.pp.se/",
    "sn": "Krefting",
    "gidNumber": "2029"
  },
  {
    "info": "# peterm, users, debian.org",
    "dn": {
      "uid": "peterm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.mathiasson.nu",
    "cn": "Peter",
    "uid": "peterm",
    "uidNumber": "2495",
    "sn": "Mathiasson",
    "gidNumber": "2495"
  },
  {
    "info": "# peters, users, debian.org",
    "dn": {
      "uid": "peters",
      "ou": "users",
      "dc": "org"
    },
    "uid": "peters",
    "objectClass": "debianDeveloper",
    "uidNumber": "2869",
    "cn": "Peter",
    "sn": "Samuelson",
    "ircNick": "peterS",
    "gidNumber": "2869"
  },
  {
    "info": "# petervr, users, debian.org",
    "dn": {
      "uid": "petervr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://emmy.nmsu.edu/~petervr",
    "cn": "Peter",
    "uid": "petervr",
    "uidNumber": "2281",
    "ircNick": "Magdirag",
    "sn": "van Rossum",
    "gidNumber": "2281"
  },
  {
    "info": "# petra, users, debian.org",
    "dn": {
      "uid": "petra",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Petra",
    "uid": "petra",
    "uidNumber": "2647",
    "sn": "Malik",
    "gidNumber": "2647"
  },
  {
    "info": "# pfaffben, users, debian.org",
    "dn": {
      "uid": "pfaffben",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "pfaffben",
    "uidNumber": "984",
    "sn": "Pfaff",
    "gidNumber": "984",
    "labeledURI": "https://benpfaff.org/"
  },
  {
    "info": "# pfrauenf, users, debian.org",
    "dn": {
      "uid": "pfrauenf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philipp",
    "uid": "pfrauenf",
    "sn": "Frauenfelder",
    "uidNumber": "1260",
    "ircNick": "pfr",
    "labeledURI": "http://www.frauenfelder-kuerner.ch/",
    "gidNumber": "1706"
  },
  {
    "info": "# pgt, users, debian.org",
    "dn": {
      "uid": "pgt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pgt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3618",
    "gidNumber": "3618",
    "keyFingerPrint": "C63DBBEF21427CE249DBD96B061212944647A411",
    "cn": "Pierre",
    "sn": "Gruet"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "phaggart",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.bsfh.org/",
    "cn": "Paul",
    "uid": "phaggart",
    "uidNumber": "961",
    "ircNick": "kibo",
    "sn": "Haggart",
    "gidNumber": "961"
  },
  {
    "info": "# phil, users, debian.org",
    "dn": {
      "uid": "phil",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.fifi.org/~phil/",
    "cn": "Philippe",
    "uid": "phil",
    "uidNumber": "935",
    "sn": "Troin",
    "gidNumber": "935"
  },
  {
    "info": "# philh, users, debian.org",
    "dn": {
      "uid": "philh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philip",
    "keyFingerPrint": "DFF1415ACE3227FCF20707D6D04BA3A00125D5C0",
    "uid": "philh",
    "sn": "Hands",
    "uidNumber": "952",
    "gidNumber": "952",
    "ircNick": "fil",
    "labeledURI": "http://www.hands.com/~phil/"
  },
  {
    "info": "# philipc, users, debian.org",
    "dn": {
      "uid": "philipc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philip",
    "uid": "philipc",
    "sn": "Charles",
    "uidNumber": "2243",
    "labeledURI": "http://www.copyleft.co.nz",
    "gidNumber": "2243"
  },
  {
    "info": "# philipp, users, debian.org",
    "dn": {
      "uid": "philipp",
      "ou": "users",
      "dc": "org"
    },
    "uid": "philipp",
    "objectClass": "debianDeveloper",
    "uidNumber": "2990",
    "cn": "Philipp",
    "sn": "Benner",
    "gidNumber": "2990"
  },
  {
    "info": "# philou, users, debian.org",
    "dn": {
      "uid": "philou",
      "ou": "users",
      "dc": "org"
    },
    "uid": "philou",
    "objectClass": "debianDeveloper",
    "uidNumber": "3548",
    "gidNumber": "3548",
    "keyFingerPrint": "FCDDF6D727B44BCD06FF1A32C89D5712DF945F6F",
    "cn": "Philippe",
    "sn": "Thierry",
    "ircNick": "philou",
    "labeledURI": "https://www.reseau-libre.net"
  },
  {
    "info": "rTTjWwQIDAQAB",
    "dn": {
      "uid": "phls",
      "ou": "users",
      "dc": "org"
    },
    "uid": "phls",
    "objectClass": "debianDeveloper",
    "uidNumber": "3556",
    "gidNumber": "3556",
    "keyFingerPrint": "4324A92B2E7B85DA7F9C9C40C66D06B40443C450",
    "cn": "Paulo",
    "sn": "Santana",
    "ircNick": "phls",
    "jabberJID": "phls",
    "labeledURI": "http://phls.com.br"
  },
  {
    "info": "# phreed, users, debian.org",
    "dn": {
      "uid": "phreed",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fredrick",
    "uid": "phreed",
    "uidNumber": "1372",
    "sn": "Eisele",
    "gidNumber": "1372"
  },
  {
    "info": "# phython, users, debian.org",
    "dn": {
      "uid": "phython",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "jabberJID": "montyphython@jabber.org",
    "uid": "phython",
    "sn": "Morrison",
    "labeledURI": "http://www.csclub.uwaterloo.ca/~ja2morri",
    "uidNumber": "2474",
    "ircNick": "Phython",
    "gidNumber": "2474"
  },
  {
    "info": "# picca, users, debian.org",
    "dn": {
      "uid": "picca",
      "ou": "users",
      "dc": "org"
    },
    "uid": "picca",
    "objectClass": "debianDeveloper",
    "uidNumber": "3125",
    "sn": "Picca",
    "keyFingerPrint": "E92E7E6E9E9DA6B1AA3139DC5632906F4696E015",
    "gidNumber": "3125",
    "cn": ": RnLDqWTDqXJpYy1FbW1hbnVlbA=="
  },
  {
    "info": "# piefel, users, debian.org",
    "dn": {
      "uid": "piefel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "piefel",
    "sn": "Piefel",
    "uidNumber": "2236",
    "gidNumber": "2236",
    "ircNick": "Typhon",
    "labeledURI": "http://www2.informatik.hu-berlin.de/~piefel"
  },
  {
    "info": "# piem, users, debian.org",
    "dn": {
      "uid": "piem",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "piem",
    "uidNumber": "2725",
    "sn": "Brossier",
    "gidNumber": "2725",
    "keyFingerPrint": "B88A5072D4915AECF81A24346A49B19728ABDD92",
    "ircNick": "piem",
    "labeledURI": "https://piem.org"
  },
  {
    "info": "# pierre, users, debian.org",
    "dn": {
      "uid": "pierre",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pierre",
    "objectClass": "debianDeveloper",
    "uidNumber": "3529",
    "gidNumber": "3529",
    "keyFingerPrint": "01AE96543A081B0983765B83116F1E6A260C809C",
    "cn": "Pierre",
    "sn": "Muller"
  },
  {
    "info": "# pik, users, debian.org",
    "dn": {
      "uid": "pik",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "pik",
    "sn": "Cannon",
    "uidNumber": "2473",
    "keyFingerPrint": "29E003C86C6AC548A5E9D4E8136E9CF17B5D342D",
    "gidNumber": "2473",
    "ircNick": "thepaul"
  },
  {
    "info": "# piman, users, debian.org",
    "dn": {
      "uid": "piman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.sacredchao.net",
    "cn": "Joe",
    "uid": "piman",
    "uidNumber": "2555",
    "ircNick": "piman",
    "sn": "Wreschnig",
    "gidNumber": "2555"
  },
  {
    "info": "# pini, users, debian.org",
    "dn": {
      "uid": "pini",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pini",
    "objectClass": "debianDeveloper",
    "uidNumber": "3044",
    "keyFingerPrint": "A0939BCC0AC3134E56B48C91EFE86C6C7FFECF83",
    "cn": "Gilles",
    "sn": "Filippini",
    "gidNumber": "3044",
    "ircNick": "pini"
  },
  {
    "info": "# pino, users, debian.org",
    "dn": {
      "uid": "pino",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pino",
    "objectClass": "debianDeveloper",
    "uidNumber": "3140",
    "cn": "Pino",
    "sn": "Toscano",
    "gidNumber": "3140",
    "keyFingerPrint": "5F2A9FB82FA6C1E1077007072D191C8843B13F4D",
    "ircNick": "pinotree"
  },
  {
    "info": "# piotr, users, debian.org",
    "dn": {
      "uid": "piotr",
      "ou": "users",
      "dc": "org"
    },
    "uid": "piotr",
    "objectClass": "debianDeveloper",
    "uidNumber": "2846",
    "cn": "Piotr",
    "keyFingerPrint": "1D2FA89858DAAF6217862DF7AEF6F1A2A7457645",
    "ircNick": "p1otr",
    "jabberJID": "piotr@ozarowski.pl",
    "labeledURI": "http://www.ozarowski.pl/",
    "gidNumber": "2846",
    "sn": ": T8W8YXJvd3NraQ=="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "pistore",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marco",
    "uid": "pistore",
    "uidNumber": "1087",
    "sn": "Pistore",
    "gidNumber": "1087"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "pitts2",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "pitts2",
    "uidNumber": "963",
    "sn": "Pitts",
    "gidNumber": "963"
  },
  {
    "info": "# pjb, users, debian.org",
    "dn": {
      "uid": "pjb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Phil",
    "uid": "pjb",
    "uidNumber": "2489",
    "sn": "Brooke",
    "keyFingerPrint": "2823000E3FA98FF8EB7C100A819D278A0E6F992A",
    "gidNumber": "2489",
    "labeledURI": "https://green-pike.co.uk/about.html"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "pje",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "pje",
    "uidNumber": "941",
    "sn": "Edwards",
    "gidNumber": "941"
  },
  {
    "info": "qejWu8YFn/KKrSGSnz/1jzCiJI3wIDAQAB",
    "dn": {
      "uid": "pkern",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philipp",
    "uid": "pkern",
    "sn": "Kern",
    "uidNumber": "2709",
    "keyFingerPrint": "F75FBFCD771DEB5E9C86050550C3634D3A291CF9",
    "gidNumber": "2709",
    "ircNick": "phil"
  },
  {
    "info": "# plessy, users, debian.org",
    "dn": {
      "uid": "plessy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "plessy",
    "objectClass": "debianDeveloper",
    "uidNumber": "2922",
    "cn": "Charles",
    "sn": "Plessy",
    "keyFingerPrint": "73471499CC60ED9EEE805946C5BD6C8F2295D502",
    "gidNumber": "2922",
    "jabberJID": "charles@jabber.dunkklar.org",
    "labeledURI": "http://charles.plessy.org"
  },
  {
    "info": "# plugwash, users, debian.org",
    "dn": {
      "uid": "plugwash",
      "ou": "users",
      "dc": "org"
    },
    "uid": "plugwash",
    "objectClass": "debianDeveloper",
    "uidNumber": "3193",
    "keyFingerPrint": "5340D001360CA656E3497EB70C48EA2A7A8FFD7B",
    "cn": "Peter",
    "sn": "Green",
    "gidNumber": "3193"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "plundis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.chaosdev.org/~plundis/",
    "cn": "Per",
    "uid": "plundis",
    "uidNumber": "1338",
    "ircNick": "Plundis",
    "sn": "Lundberg",
    "gidNumber": "1338"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "plypkie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pete",
    "uid": "plypkie",
    "uidNumber": "2089",
    "ircNick": "Doviende",
    "sn": "Lypkie",
    "gidNumber": "2089"
  },
  {
    "info": "# pm, users, debian.org",
    "dn": {
      "uid": "pm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "keyFingerPrint": "556054EA47665DE00263CB41603B832661F9CA53",
    "uid": "pm",
    "sn": "Martin",
    "uidNumber": "2112",
    "gidNumber": "2112",
    "ircNick": "nowster"
  },
  {
    "info": "# pm215, users, debian.org",
    "dn": {
      "uid": "pm215",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pm215",
    "objectClass": "debianDeveloper",
    "uidNumber": "3530",
    "gidNumber": "3530",
    "keyFingerPrint": "E1A5C593CD419DE28E8315CF3C2525ED14360CDE",
    "cn": "Peter",
    "sn": "Maydell"
  },
  {
    "info": "# pmachard, users, debian.org",
    "dn": {
      "uid": "pmachard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pierre",
    "uid": "pmachard",
    "sn": "Machard",
    "labeledURI": "http://www.machard.org/",
    "uidNumber": "2598",
    "ircNick": "migus",
    "gidNumber": "2598"
  },
  {
    "info": "# pmatos, users, debian.org",
    "dn": {
      "uid": "pmatos",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pmatos",
    "objectClass": "debianDeveloper",
    "uidNumber": "3561",
    "gidNumber": "3561",
    "cn": "Paulo",
    "sn": "Matos",
    "keyFingerPrint": "AB74C94EE0B4D7DAE3409850D045D82978AC5330"
  },
  {
    "info": "# pmatthaei, users, debian.org",
    "dn": {
      "uid": "pmatthaei",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pmatthaei",
    "objectClass": "debianDeveloper",
    "uidNumber": "2973",
    "cn": "Patrick",
    "keyFingerPrint": "58A03DC582425A4DC8B90E1312D9B04A90CBD8E4",
    "gidNumber": "2973",
    "sn": ": TWF0dGjDpGk=",
    "ircNick": "the-me",
    "labeledURI": "https://www.linux-dev.org"
  },
  {
    "info": "# pmhahn, users, debian.org",
    "dn": {
      "uid": "pmhahn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Philipp",
    "uid": "pmhahn",
    "sn": "Hahn",
    "uidNumber": "2516",
    "keyFingerPrint": "58AF7C2E007CDBE62C59E078F50EFDCF8AD04B1A",
    "gidNumber": "2516",
    "ircNick": "pmhahn",
    "jabberJID": "pmhahn@jabber.ccc.de",
    "labeledURI": "https://pmhahn.de/"
  },
  {
    "info": "# pmiller, users, debian.org",
    "dn": {
      "uid": "pmiller",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pmiller",
    "objectClass": "debianDeveloper",
    "uidNumber": "3086",
    "cn": "Peter",
    "sn": "Miller",
    "gidNumber": "3086"
  },
  {
    "info": "# pochu, users, debian.org",
    "dn": {
      "uid": "pochu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pochu",
    "objectClass": "debianDeveloper",
    "uidNumber": "2999",
    "cn": "Emilio",
    "sn": "Pozuelo Monfort",
    "keyFingerPrint": "709CA6C7EBE6259C5DF7643E9D46C488E4368302",
    "gidNumber": "2999",
    "ircNick": "pochu",
    "jabberJID": "pochu27@gmail.com",
    "labeledURI": "http://emilio.pozuelo.org/"
  },
  {
    "info": "# pocock, users, debian.org",
    "dn": {
      "uid": "pocock",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pocock",
    "objectClass": "debianDeveloper",
    "uidNumber": "3228",
    "cn": "Daniel",
    "sn": "Pocock",
    "ircNick": "pocock",
    "labeledURI": "http://danielpocock.com",
    "gidNumber": "3228"
  },
  {
    "info": "# pollo, users, debian.org",
    "dn": {
      "uid": "pollo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pollo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3514",
    "gidNumber": "3514",
    "cn": "Louis-Philippe",
    "sn": ": VsOpcm9ubmVhdQ==",
    "keyFingerPrint": "F64D61D321F3CB489156753DE1E5457C8BAD4113",
    "ircNick": "pollo",
    "labeledURI": "https://veronneau.org"
  },
  {
    "info": "# pollux, users, debian.org",
    "dn": {
      "uid": "pollux",
      "ou": "users",
      "dc": "org"
    },
    "uid": "pollux",
    "objectClass": "debianDeveloper",
    "uidNumber": "2876",
    "cn": "Pierre",
    "sn": "Chifflier",
    "keyFingerPrint": "BF18695797741DB24E59188FC61A64DCF1393998",
    "ircNick": "pollux",
    "gidNumber": "2876"
  },
  {
    "info": "# polverari, users, debian.org",
    "dn": {
      "uid": "polverari",
      "ou": "users",
      "dc": "org"
    },
    "uid": "polverari",
    "objectClass": "debianDeveloper",
    "uidNumber": "3641",
    "gidNumber": "3641",
    "keyFingerPrint": "B7C9AEA2C97B22A36C8B76E684E624545A27D942",
    "cn": "David",
    "sn": "Polverari"
  },
  {
    "info": "# porridge, users, debian.org",
    "dn": {
      "uid": "porridge",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marcin",
    "uid": "porridge",
    "sn": "Owsiany",
    "uidNumber": "2206",
    "gidNumber": "2206",
    "keyFingerPrint": "59F4A7DED37D95C58939FDDBEFDED44BCDFB68E9",
    "ircNick": "porridge",
    "labeledURI": "http://marcin.owsiany.pl/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "porter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "porter",
    "uidNumber": "1304",
    "ircNick": "Rattler",
    "sn": "Porter",
    "gidNumber": "1304"
  },
  {
    "info": "# portnoy, users, debian.org",
    "dn": {
      "uid": "portnoy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "portnoy",
    "uidNumber": "2524",
    "sn": "Peters",
    "labeledURI": "http://www.portnoy.org/",
    "gidNumber": "2524"
  },
  {
    "info": "# pouelle, users, debian.org",
    "dn": {
      "uid": "pouelle",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "pouelle",
    "sn": "Ouellette",
    "uidNumber": "1229",
    "keyFingerPrint": "6FB77BB89C41C8C8D2D2B59A19B267802103329F",
    "gidNumber": "1693",
    "ircNick": "spotteddog"
  },
  {
    "info": "# ppatters, users, debian.org",
    "dn": {
      "uid": "ppatters",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "ppatters",
    "uidNumber": "2467",
    "sn": "Patterson",
    "ircNick": "McAlister",
    "jabberJID": "ppatters@gmail.com",
    "labeledURI": "http://www.carillon.ca/",
    "gidNumber": "2467"
  },
  {
    "info": "# prach, users, debian.org",
    "dn": {
      "uid": "prach",
      "ou": "users",
      "dc": "org"
    },
    "uid": "prach",
    "objectClass": "debianDeveloper",
    "uidNumber": "3384",
    "gidNumber": "3384",
    "keyFingerPrint": "0CA75D987B8ECF6EA9443AD839091E8123CE1C09",
    "cn": "Prach",
    "sn": "Pongpanich",
    "ircNick": "prach"
  },
  {
    "info": "# praveen, users, debian.org",
    "dn": {
      "uid": "praveen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "praveen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3219",
    "cn": "Praveen",
    "sn": "Arimbrathodiyil",
    "gidNumber": "3219",
    "keyFingerPrint": "D30863E26020E543F4719A838F53E0193B294B75",
    "ircNick": "j4v4m4n",
    "jabberJID": "praveen@poddery.com",
    "labeledURI": "http://j4v4m4n.in"
  },
  {
    "info": "# preining, users, debian.org",
    "dn": {
      "uid": "preining",
      "ou": "users",
      "dc": "org"
    },
    "uid": "preining",
    "objectClass": "debianDeveloper",
    "uidNumber": "2808",
    "cn": "Norbert",
    "sn": "Preining",
    "gidNumber": "2808",
    "ircNick": "norbusan",
    "labeledURI": "http://www.preining.info/"
  },
  {
    "info": "# prh, users, debian.org",
    "dn": {
      "uid": "prh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "prh",
    "sn": "Hedderly",
    "uidNumber": "2169",
    "ircNick": "prh",
    "jabberJID": "prh@mjr.org",
    "gidNumber": "2169"
  },
  {
    "info": "# profeta, users, debian.org",
    "dn": {
      "uid": "profeta",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mickael",
    "uid": "profeta",
    "uidNumber": "2528",
    "sn": "Profeta",
    "labeledURI": "http://www.alezan.org",
    "gidNumber": "2528"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "promera",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michel",
    "uid": "promera",
    "sn": "Onstein",
    "labeledURI": "http://www.promera.nl",
    "uidNumber": "1261",
    "ircNick": "phobe",
    "gidNumber": "1697"
  },
  {
    "info": "# pronovic, users, debian.org",
    "dn": {
      "uid": "pronovic",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Kenneth",
    "uid": "pronovic",
    "sn": "Pronovici",
    "uidNumber": "2573",
    "keyFingerPrint": "0A463F5CE07D0979B5C5C90711192892EFD75934",
    "gidNumber": "2573",
    "labeledURI": "http://cedar-solutions.com/"
  },
  {
    "info": "# prudhomm, users, debian.org",
    "dn": {
      "uid": "prudhomm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christophe",
    "uid": "prudhomm",
    "uidNumber": "863",
    "sn": "Prudhomme",
    "jabberJID": "christophe.prudhomme@feelpp.org",
    "labeledURI": "http://www.prudhomm.org",
    "gidNumber": "863"
  },
  {
    "info": "# prussell, users, debian.org",
    "dn": {
      "uid": "prussell",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "prussell",
    "uidNumber": "2418",
    "sn": "Russell",
    "gidNumber": "2418"
  },
  {
    "info": "# pseelig, users, debian.org",
    "dn": {
      "uid": "pseelig",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "pseelig",
    "uidNumber": "1097",
    "sn": "Seelig",
    "ircNick": "pseelig",
    "jabberJID": "pseelig@jabber.org",
    "labeledURI": "http://rumbero.es",
    "gidNumber": "1097"
  },
  {
    "info": "# psg, users, debian.org",
    "dn": {
      "uid": "psg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "psg",
    "sn": "Galbraith",
    "uidNumber": "1289",
    "keyFingerPrint": "630928AE8EB3AB5722F303BC17DC3CC470D4A979",
    "gidNumber": "1289",
    "labeledURI": "http://people.debian.org/~psg"
  },
  {
    "info": "# psmith, users, debian.org",
    "dn": {
      "uid": "psmith",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Preston",
    "uid": "psmith",
    "uidNumber": "1308",
    "sn": "Smith",
    "jabberJID": "psmith@jabber.rcac.purdue.edu",
    "gidNumber": "1308"
  },
  {
    "info": "# pstorralba, users, debian.org",
    "dn": {
      "uid": "pstorralba",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pablo",
    "uid": "pstorralba",
    "sn": "Torralba",
    "uidNumber": "2579",
    "ircNick": "pianist",
    "gidNumber": "2579"
  },
  {
    "info": "# psychon, users, debian.org",
    "dn": {
      "uid": "psychon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "psychon",
    "objectClass": "debianDeveloper",
    "uidNumber": "3505",
    "gidNumber": "3505",
    "keyFingerPrint": "2BB32F88FF3D1E76E682303F22E428EBCB8FCB06",
    "cn": "Uli",
    "sn": "Schlachter"
  },
  {
    "info": "# punit, users, debian.org",
    "dn": {
      "uid": "punit",
      "ou": "users",
      "dc": "org"
    },
    "uid": "punit",
    "objectClass": "debianDeveloper",
    "uidNumber": "3474",
    "gidNumber": "3474",
    "keyFingerPrint": "BD9E4CEC13FC34495682074EF5392FD213FBFEDD",
    "cn": "Punit",
    "sn": "Agrawal",
    "ircNick": "punit"
  },
  {
    "info": "# pvaneynd, users, debian.org",
    "dn": {
      "uid": "pvaneynd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "pvaneynd",
    "sn": "Van Eynde",
    "uidNumber": "1186",
    "keyFingerPrint": "177A79EBA658DBCE9854073EB59EB72202D1BC65",
    "gidNumber": "1713",
    "ircNick": "pvaneynd",
    "labeledURI": "http://people.debian.org/~pvaneynd/"
  },
  {
    "info": "# pxt, users, debian.org",
    "dn": {
      "uid": "pxt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "pxt",
    "uidNumber": "2578",
    "sn": "Telford",
    "ircNick": "PxT",
    "labeledURI": "http://people.debian.org/~pxt/",
    "gidNumber": "2578"
  },
  {
    "info": "# pyro, users, debian.org",
    "dn": {
      "uid": "pyro",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "pyro",
    "sn": "Nelson",
    "uidNumber": "2560",
    "ircNick": "bignachos",
    "labeledURI": "http://bignachos.net",
    "gidNumber": "2560"
  },
  {
    "info": "# pzn, users, debian.org",
    "dn": {
      "uid": "pzn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pedro",
    "uid": "pzn",
    "sn": "Zorzenon Neto",
    "uidNumber": "2386",
    "ircNick": "pzn",
    "gidNumber": "2386"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "qc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Quentin",
    "uid": "qc",
    "uidNumber": "1294",
    "sn": "Cregan",
    "gidNumber": "1294"
  },
  {
    "info": "# qhuo, users, debian.org",
    "dn": {
      "uid": "qhuo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "qhuo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2779",
    "cn": "Qingning",
    "sn": "Huo",
    "gidNumber": "2779"
  },
  {
    "info": "# qjb, users, debian.org",
    "dn": {
      "uid": "qjb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jay",
    "uid": "qjb",
    "sn": "Berkenbilt",
    "uidNumber": "2716",
    "keyFingerPrint": "C2C96B10011FE009E6D1DF828A75D10998012C7E",
    "gidNumber": "2716",
    "ircNick": "qjb",
    "labeledURI": "http://q.ql.org"
  },
  {
    "info": "# racke, users, debian.org",
    "dn": {
      "uid": "racke",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "racke",
    "uidNumber": "2250",
    "sn": "Hornburg",
    "keyFingerPrint": "D6814975A277774C98D0DFEF5B93015BFA2720F8",
    "gidNumber": "2250",
    "ircNick": "racke",
    "labeledURI": "https://www.linuxia.de/"
  },
  {
    "info": "# radu, users, debian.org",
    "dn": {
      "uid": "radu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "radu",
    "objectClass": "debianDeveloper",
    "uidNumber": "2753",
    "cn": "Radu",
    "sn": "Spineanu",
    "ircNick": "sradu",
    "gidNumber": "2753"
  },
  {
    "info": "6ql6iTwIDAQAB",
    "dn": {
      "uid": "rafael",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rafael",
    "uid": "rafael",
    "sn": "Laboissiere",
    "uidNumber": "1219",
    "gidNumber": "1661",
    "keyFingerPrint": "3F464391498FE874BDB5D98F2124AA1983785C90"
  },
  {
    "info": "# rafl, users, debian.org",
    "dn": {
      "uid": "rafl",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rafl",
    "objectClass": "debianDeveloper",
    "uidNumber": "2752",
    "cn": "Florian",
    "sn": "Ragwitz",
    "gidNumber": "2752",
    "ircNick": "rafl",
    "jabberJID": "rafl@jabber.ccc.de",
    "labeledURI": "http://perldition.org/"
  },
  {
    "info": "# rafou, users, debian.org",
    "dn": {
      "uid": "rafou",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Raphael",
    "uid": "rafou",
    "sn": "Goulais",
    "labeledURI": "http://www.nicedays.net/",
    "uidNumber": "2653",
    "ircNick": "Rafou",
    "gidNumber": "2653"
  },
  {
    "info": "# rak, users, debian.org",
    "dn": {
      "uid": "rak",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rak",
    "objectClass": "debianDeveloper",
    "uidNumber": "3166",
    "keyFingerPrint": "4E469519ED677734268FBD958F7BF8FC4A11C97A",
    "cn": "Ryan",
    "sn": "Kavanagh",
    "gidNumber": "3166",
    "ircNick": "rak",
    "labeledURI": "https://rak.ac gopher://rak.ac"
  },
  {
    "info": "91208]",
    "dn": {
      "uid": "ranty",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Manuel",
    "uid": "ranty",
    "sn": "Sainz",
    "labeledURI": "http://ranty.ddts.net/~ranty",
    "uidNumber": "2274",
    "ircNick": "ranty",
    "gidNumber": "2274"
  },
  {
    "info": "# ras, users, debian.org",
    "dn": {
      "uid": "ras",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ras",
    "objectClass": "debianDeveloper",
    "uidNumber": "3257",
    "keyFingerPrint": "D25DD87DF8EA9ED1D1465023F5231C62E7843A8C",
    "cn": "Russell",
    "sn": "Stuart",
    "ircNick": "ras",
    "jabberJID": "russell@stuart.id.au",
    "labeledURI": "http://www.humbug.org.au/RussellStuart",
    "gidNumber": "3257"
  },
  {
    "info": "RFltPptwBZ8MAMacb+SxIwPNNe78JugxS8Lr2QIDAQAB",
    "dn": {
      "uid": "rattusrattus",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rattusrattus",
    "objectClass": "debianDeveloper",
    "uidNumber": "3379",
    "keyFingerPrint": "C0E6366A2050781B72A82ED5BB41D45770EF06F7",
    "cn": "Andy",
    "sn": "Simpkins",
    "gidNumber": "3379",
    "ircNick": "rattusrattus",
    "labeledURI": "https://koipond.org.uk"
  },
  {
    "info": "# rb, users, debian.org",
    "dn": {
      "uid": "rb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roland",
    "uid": "rb",
    "sn": "Bauerschmidt",
    "labeledURI": "http://people.debian.org/~rb/",
    "uidNumber": "2028",
    "ircNick": "roland / roland_ / _roland",
    "gidNumber": "2028"
  },
  {
    "info": "# rbalint, users, debian.org",
    "dn": {
      "uid": "rbalint",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rbalint",
    "objectClass": "debianDeveloper",
    "uidNumber": "3240",
    "cn": "Balint",
    "sn": "Reczey",
    "ircNick": "rbalint",
    "labeledURI": "http://balintreczey.hu",
    "gidNumber": "3240",
    "keyFingerPrint": "B38AC6FC7B734CFDA9CA8982CD3CEA34F42AA45D"
  },
  {
    "info": "# rbasak, users, debian.org",
    "dn": {
      "uid": "rbasak",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rbasak",
    "objectClass": "debianDeveloper",
    "uidNumber": "3523",
    "gidNumber": "3523",
    "keyFingerPrint": "D98F377EB47CC6DD257A9A07E564B9C275BDD52E",
    "cn": "Robie",
    "sn": "Basak"
  },
  {
    "info": "# rbd, users, debian.org",
    "dn": {
      "uid": "rbd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rbd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3183",
    "keyFingerPrint": "3779F472BB84DFE1A650C304DDC7ECFBB4F83169",
    "cn": "Roland",
    "sn": "Dreier",
    "gidNumber": "3183"
  },
  {
    "info": "20091208] Retired, 2006-09-21",
    "dn": {
      "uid": "rbf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://farrer.net/~rbf/",
    "cn": "Ron",
    "uid": "rbf",
    "uidNumber": "2122",
    "sn": "Farrer",
    "gidNumber": "2122"
  },
  {
    "info": "# rcardenes, users, debian.org",
    "dn": {
      "uid": "rcardenes",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ricardo",
    "uid": "rcardenes",
    "sn": "Cardenes",
    "uidNumber": "2298",
    "ircNick": "Heimy",
    "jabberJID": "rcardenes@jabber.org",
    "labeledURI": "http://people.debian.org/~rcardenes",
    "gidNumber": "2298"
  },
  {
    "info": "20091208] Resigned",
    "dn": {
      "uid": "rch",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ricardas",
    "uid": "rch",
    "uidNumber": "1012",
    "sn": "Cepas",
    "gidNumber": "1012"
  },
  {
    "info": "# rcp, users, debian.org",
    "dn": {
      "uid": "rcp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ross",
    "keyFingerPrint": "32F23DB43D709382E58BD19204D147A595E0A1A1",
    "uid": "rcp",
    "uidNumber": "2260",
    "sn": "Peachey",
    "jabberJID": "ross.peachey@gmail.com",
    "gidNumber": "2260"
  },
  {
    "info": "# rcw, users, debian.org",
    "dn": {
      "uid": "rcw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rcw",
    "sn": "Woodcock",
    "uidNumber": "1208",
    "keyFingerPrint": "61F695A34BB6EB702DC3F5C1829CA1942F06D925",
    "ircNick": "rcw",
    "labeledURI": "http://lly.org",
    "gidNumber": "1614"
  },
  {
    "info": "# rdonald, users, debian.org",
    "dn": {
      "uid": "rdonald",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Randall",
    "uid": "rdonald",
    "sn": "Donald",
    "uidNumber": "2170",
    "ircNick": "KhensU",
    "labeledURI": "http://www.khensu.org",
    "gidNumber": "2170"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "rduta",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Radu",
    "uid": "rduta",
    "uidNumber": "1104",
    "sn": "Duta",
    "gidNumber": "1104"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "redwards",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Randy",
    "uid": "redwards",
    "uidNumber": "1346",
    "sn": "Edwards",
    "gidNumber": "1346"
  },
  {
    "info": "# reg, users, debian.org",
    "dn": {
      "uid": "reg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "reg",
    "objectClass": "debianDeveloper",
    "uidNumber": "2934",
    "cn": "Gregory",
    "sn": "Colpart",
    "keyFingerPrint": "0C016D3BD1195D30105837CC44975278B8612B5D",
    "gidNumber": "2934",
    "ircNick": "reg",
    "labeledURI": "http://www.gcolpart.com/"
  },
  {
    "info": "# regis, users, debian.org",
    "dn": {
      "uid": "regis",
      "ou": "users",
      "dc": "org"
    },
    "uid": "regis",
    "objectClass": "debianDeveloper",
    "uidNumber": "2880",
    "cn": "Regis",
    "sn": "Boudin",
    "gidNumber": "2880"
  },
  {
    "info": "# reichel, users, debian.org",
    "dn": {
      "uid": "reichel",
      "ou": "users",
      "dc": "org"
    },
    "uid": "reichel",
    "objectClass": "debianDeveloper",
    "uidNumber": "2946",
    "cn": "Joachim",
    "sn": "Reichel",
    "keyFingerPrint": "AD7E8DCCBC30B2BE318EC115B8FAFF1C43886771",
    "gidNumber": "2946",
    "ircNick": "Col_Kernel",
    "labeledURI": "http://www.joachim-reichel.de/"
  },
  {
    "info": "# remco, users, debian.org",
    "dn": {
      "uid": "remco",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Remco",
    "uid": "remco",
    "sn": "van de Meent",
    "uidNumber": "1220",
    "labeledURI": "http://rvdmeent.googlepages.com/",
    "gidNumber": "1649"
  },
  {
    "info": "# remi, users, debian.org",
    "dn": {
      "uid": "remi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Remi",
    "uid": "remi",
    "sn": "Lefebvre",
    "uidNumber": "1375",
    "ircNick": "remi",
    "gidNumber": "1375"
  },
  {
    "info": "# rene, users, debian.org",
    "dn": {
      "uid": "rene",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rene",
    "keyFingerPrint": "E12DEA46750670CFA960801D0AA04571D03E3E70",
    "uid": "rene",
    "sn": "Engelhard",
    "uidNumber": "2514",
    "gidNumber": "2514",
    "ircNick": "_rene_"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "resendes",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "resendes",
    "uidNumber": "1088",
    "sn": "Resendes",
    "gidNumber": "1088"
  },
  {
    "info": "# ressu, users, debian.org",
    "dn": {
      "uid": "ressu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sami",
    "uid": "ressu",
    "sn": "Haahtinen",
    "uidNumber": "1209",
    "ircNick": "zanaga",
    "jabberJID": "ressu@ressukka.net",
    "labeledURI": "http://www.ressukka.net",
    "gidNumber": "1637"
  },
  {
    "info": "# restless, users, debian.org",
    "dn": {
      "uid": "restless",
      "ou": "users",
      "dc": "org"
    },
    "uid": "restless",
    "objectClass": "debianDeveloper",
    "uidNumber": "3197",
    "keyFingerPrint": "2875F6B1C2D27A4F0C8AF60B2A714497E37363AE",
    "cn": "Aleksey",
    "sn": "Kravchenko",
    "gidNumber": "3197"
  },
  {
    "info": "# rfehren, users, debian.org",
    "dn": {
      "uid": "rfehren",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rfehren",
    "objectClass": "debianDeveloper",
    "uidNumber": "3487",
    "gidNumber": "3487",
    "keyFingerPrint": "D8939339419CA3B869CFDD95070202585B812EC1",
    "cn": "Roland",
    "sn": "Fehrenbacher"
  },
  {
    "info": "# rfinnie, users, debian.org",
    "dn": {
      "uid": "rfinnie",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rfinnie",
    "objectClass": "debianDeveloper",
    "uidNumber": "3391",
    "gidNumber": "3391",
    "keyFingerPrint": "42E2C8DE8C173AB102F52C6E7E60A3A686AE8D98",
    "cn": "Ryan",
    "sn": "Finnie"
  },
  {
    "info": "# rfrancoise, users, debian.org",
    "dn": {
      "uid": "rfrancoise",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Romain",
    "uid": "rfrancoise",
    "sn": "Francoise",
    "uidNumber": "2628",
    "keyFingerPrint": "BE349742CA987ECD7E77E42DAD15F435F05F962D",
    "gidNumber": "2628",
    "ircNick": "ore"
  },
  {
    "info": "# rganesan, users, debian.org",
    "dn": {
      "uid": "rganesan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ganesan",
    "uid": "rganesan",
    "sn": "Rajagopal",
    "uidNumber": "2398",
    "ircNick": "Argon",
    "labeledURI": "http://people.debian.org/~rganesan",
    "gidNumber": "2398"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "rgmerk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rgmerk",
    "uidNumber": "2141",
    "sn": "Merkel",
    "gidNumber": "2141"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "rgwood",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://people.debian.org/~rgwood/",
    "cn": "R.",
    "uid": "rgwood",
    "uidNumber": "1379",
    "ircNick": "MrNovembr",
    "sn": "Wood",
    "gidNumber": "1379"
  },
  {
    "info": "# rha, users, debian.org",
    "dn": {
      "uid": "rha",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rha",
    "objectClass": "debianDeveloper",
    "uidNumber": "3585",
    "gidNumber": "3585",
    "keyFingerPrint": "2BC83F55A4007468864C680E1B7CC8D4D4E914AA",
    "cn": "Robert",
    "sn": "Haist"
  },
  {
    "info": "# rhonda, users, debian.org",
    "dn": {
      "uid": "rhonda",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rhonda",
    "objectClass": "debianDeveloper",
    "uidNumber": "3087",
    "cn": "Rhonda",
    "sn": "D'Vine",
    "keyFingerPrint": "2C72F328ED1707204FB34994DEE8043EE17EBB30",
    "gidNumber": "3087",
    "ircNick": "Rhonda",
    "jabberJID": "rhonda@debian.org",
    "labeledURI": "https://rhonda.deb.at/blog/"
  },
  {
    "info": "# ribalda, users, debian.org",
    "dn": {
      "uid": "ribalda",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ribalda",
    "objectClass": "debianDeveloper",
    "uidNumber": "3605",
    "gidNumber": "3605",
    "keyFingerPrint": "9EC3BB66E2FC129A6F90B39556A0D81F9F782DA9",
    "cn": "Ricardo",
    "sn": "Ribalda Delgado",
    "ircNick": "ribalda",
    "labeledURI": "http://ribalda.com"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "richard",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "richard",
    "uidNumber": "864",
    "sn": "Kettlewell",
    "gidNumber": "864"
  },
  {
    "info": "# richih, users, debian.org",
    "dn": {
      "uid": "richih",
      "ou": "users",
      "dc": "org"
    },
    "uid": "richih",
    "objectClass": "debianDeveloper",
    "uidNumber": "3266",
    "keyFingerPrint": "DF0BFDFF4A4DDA0179441B8F69064B0195206DD3",
    "cn": "Richard",
    "sn": "Hartmann",
    "ircNick": "RichiH",
    "labeledURI": "http://richardhartmann.de",
    "gidNumber": "3266"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "richr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "richr",
    "uidNumber": "971",
    "sn": "Roberto",
    "gidNumber": "971"
  },
  {
    "info": "# riku, users, debian.org",
    "dn": {
      "uid": "riku",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Riku",
    "uid": "riku",
    "sn": "Voipio",
    "uidNumber": "983",
    "gidNumber": "983",
    "ircNick": "suihkulokki",
    "labeledURI": "http://suihkulokki.blogspot.com/"
  },
  {
    "info": "# rinni, users, debian.org",
    "dn": {
      "uid": "rinni",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rinni",
    "objectClass": "debianDeveloper",
    "uidNumber": "3651",
    "gidNumber": "3651",
    "keyFingerPrint": "2BD8D4E397955F7746DB3B89AD6916967393982B",
    "cn": "Philip",
    "sn": "Rinn",
    "ircNick": "rinni"
  },
  {
    "info": "# risko, users, debian.org",
    "dn": {
      "uid": "risko",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "RISKO",
    "uid": "risko",
    "sn": "Gergely",
    "uidNumber": "2132",
    "keyFingerPrint": "BD1DDA696803C41A564A62072930100100003344",
    "gidNumber": "2132",
    "ircNick": "ErrGe",
    "labeledURI": "http://www.gergely.risko.hu/"
  },
  {
    "info": "# rjs, users, debian.org",
    "dn": {
      "uid": "rjs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roderick",
    "uid": "rjs",
    "sn": "Schertler",
    "uidNumber": "1136",
    "ircNick": "Roderick",
    "labeledURI": "http://www.argon.org/~roderick/",
    "gidNumber": "1616"
  },
  {
    "info": "# rkrishnan, users, debian.org",
    "dn": {
      "uid": "rkrishnan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ramakrishnan",
    "uid": "rkrishnan",
    "sn": "Muthukrishnan",
    "uidNumber": "2385",
    "ircNick": "vu3rdd",
    "labeledURI": "https://rkrishnan.org",
    "gidNumber": "2385"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "rkrusty",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ivan",
    "uid": "rkrusty",
    "sn": "Moore",
    "uidNumber": "1356",
    "ircNick": "RevKrusty",
    "gidNumber": "1356"
  },
  {
    "info": "# rla, users, debian.org",
    "dn": {
      "uid": "rla",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ryszard",
    "uid": "rla",
    "uidNumber": "2365",
    "sn": "Lach",
    "ircNick": "siaco",
    "jabberJID": "ryszard.lach@jabber.contium.pl",
    "gidNumber": "2365"
  },
  {
    "info": "# rlaager, users, debian.org",
    "dn": {
      "uid": "rlaager",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rlaager",
    "objectClass": "debianDeveloper",
    "uidNumber": "3596",
    "gidNumber": "3596",
    "keyFingerPrint": "D4EB7D94E78E4EE8ECE07F94F8796199C04586CE",
    "cn": "Richard",
    "sn": "Laager",
    "ircNick": "rlaager",
    "jabberJID": "rlaager@wiktel.com",
    "labeledURI": "https://coderich.net"
  },
  {
    "info": "# rlb, users, debian.org",
    "dn": {
      "uid": "rlb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rob",
    "uid": "rlb",
    "uidNumber": "810",
    "sn": "Browning",
    "keyFingerPrint": "E6A9DA3CC9FD1FF8C676D2C4C0F039E9ED1B597A",
    "gidNumber": "810",
    "ircNick": "rlb"
  },
  {
    "info": "# rleigh, users, debian.org",
    "dn": {
      "uid": "rleigh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roger",
    "uid": "rleigh",
    "sn": "Leigh",
    "uidNumber": "2572",
    "ircNick": "rleigh",
    "labeledURI": "http://people.debian.org/~rleigh/",
    "gidNumber": "2572"
  },
  {
    "info": "# rmayorga, users, debian.org",
    "dn": {
      "uid": "rmayorga",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rmayorga",
    "objectClass": "debianDeveloper",
    "uidNumber": "2962",
    "keyFingerPrint": "3CD593CFAF0908BE7C74A8F5BB20AC20E0B7D6BE",
    "cn": "Rene",
    "sn": "Mayorga",
    "gidNumber": "2962",
    "ircNick": "rmayorga",
    "jabberJID": "rmayorga@jabber.org",
    "labeledURI": "http://rmayorga.org"
  },
  {
    "info": "# rmayr, users, debian.org",
    "dn": {
      "uid": "rmayr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rene",
    "uid": "rmayr",
    "uidNumber": "2239",
    "sn": "Mayrhofer",
    "gidNumber": "2239",
    "ircNick": "rmayr",
    "labeledURI": "http://www.mayrhofer.eu.org"
  },
  {
    "info": "# rmb, users, debian.org",
    "dn": {
      "uid": "rmb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rmb",
    "objectClass": "debianDeveloper",
    "uidNumber": "3649",
    "gidNumber": "3649",
    "keyFingerPrint": "2D65BC1EB9665A6E97F9730AB3F5945285219E1F",
    "cn": "Mohammed",
    "sn": "Bilal"
  },
  {
    "info": "# rmgolbeck, users, debian.org",
    "dn": {
      "uid": "rmgolbeck",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ryan",
    "uid": "rmgolbeck",
    "sn": "Golbeck",
    "uidNumber": "2424",
    "ircNick": "gowlin",
    "jabberJID": "gowlin@jabber.debian.net",
    "labeledURI": "http://people.debian.org/~rmgolbeck",
    "keyFingerPrint": "738D57F5B656ECBA6757EB227EA7A51C29C2BDB6",
    "gidNumber": "2424"
  },
  {
    "info": "# rmh, users, debian.org",
    "dn": {
      "uid": "rmh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rmh",
    "uidNumber": "2562",
    "sn": "Millan",
    "gidNumber": "2562",
    "ircNick": "nyu",
    "keyFingerPrint": "A537F029AAAE0E9C39A7C22CBB9D98D9DEA2C38E"
  },
  {
    "info": "# rmurray, users, debian.org",
    "dn": {
      "uid": "rmurray",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ryan",
    "uid": "rmurray",
    "uidNumber": "2019",
    "sn": "Murray",
    "ircNick": "neuro",
    "gidNumber": "2019"
  },
  {
    "info": "# roam, users, debian.org",
    "dn": {
      "uid": "roam",
      "ou": "users",
      "dc": "org"
    },
    "uid": "roam",
    "objectClass": "debianDeveloper",
    "uidNumber": "3521",
    "gidNumber": "3521",
    "keyFingerPrint": "2EE7A7A517FC124CF115C354651EEFB02527DF13",
    "cn": "Peter",
    "sn": "Pentchev",
    "jabberJID": "tkzintar@jabber.org",
    "labeledURI": "https://www.ringlet.net/roam/"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "rob",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rob",
    "uidNumber": "865",
    "sn": "Leslie",
    "gidNumber": "865"
  },
  {
    "info": "# robbe, users, debian.org",
    "dn": {
      "uid": "robbe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "robbe",
    "uidNumber": "2246",
    "ircNick": "robbe",
    "sn": "Bihlmeyer",
    "gidNumber": "2246"
  },
  {
    "info": "# robert, users, debian.org",
    "dn": {
      "uid": "robert",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "robert",
    "uidNumber": "2143",
    "sn": "Luberda",
    "keyFingerPrint": "35E876FAB4D3732E93B4D237631DE7553BE8AFD4",
    "gidNumber": "2143"
  },
  {
    "info": "# robertc, users, debian.org",
    "dn": {
      "uid": "robertc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.robertcollins.net",
    "cn": "Robert",
    "uid": "robertc",
    "uidNumber": "2745",
    "ircNick": "lifeless",
    "sn": "Collins",
    "gidNumber": "2745"
  },
  {
    "info": "# robertle, users, debian.org",
    "dn": {
      "uid": "robertle",
      "ou": "users",
      "dc": "org"
    },
    "uid": "robertle",
    "objectClass": "debianDeveloper",
    "uidNumber": "2883",
    "cn": "Robert",
    "sn": "Lemmen",
    "gidNumber": "2883",
    "ircNick": "robertle",
    "labeledURI": "http://www.semistable.com"
  },
  {
    "info": "# roberto, users, debian.org",
    "dn": {
      "uid": "roberto",
      "ou": "users",
      "dc": "org"
    },
    "uid": "roberto",
    "objectClass": "debianDeveloper",
    "uidNumber": "2850",
    "cn": "Roberto",
    "sn": "Sanchez",
    "keyFingerPrint": "2AB8EB163CA562E182A13AAE7731FCCC63E4E277",
    "gidNumber": "2850",
    "ircNick": "el_cubano",
    "labeledURI": "http://people.connexer.com/~roberto"
  },
  {
    "info": "# robin, users, debian.org",
    "dn": {
      "uid": "robin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robin",
    "uid": "robin",
    "uidNumber": "2367",
    "sn": "Verduijn",
    "gidNumber": "2367"
  },
  {
    "info": "# robot101, users, debian.org",
    "dn": {
      "uid": "robot101",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "robot101",
    "sn": "McQueen",
    "uidNumber": "2099",
    "ircNick": "Robot101",
    "jabberJID": "robert.mcqueen@collabora.co.uk",
    "labeledURI": "http://robot101.net",
    "gidNumber": "2099"
  },
  {
    "info": "# robotux, users, debian.org",
    "dn": {
      "uid": "robotux",
      "ou": "users",
      "dc": "org"
    },
    "uid": "robotux",
    "objectClass": "debianDeveloper",
    "uidNumber": "3211",
    "keyFingerPrint": "5F35F67BAFA8B6D581CA08EBD003852FBD52529E",
    "cn": "Thomas",
    "sn": "Preud'homme",
    "gidNumber": "3211",
    "ircNick": "RoboTux",
    "jabberJID": "robotux@celest.fr"
  },
  {
    "info": "# robster, users, debian.org",
    "dn": {
      "uid": "robster",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rob",
    "uid": "robster",
    "sn": "Bradford",
    "uidNumber": "2404",
    "ircNick": "robster",
    "gidNumber": "2404"
  },
  {
    "info": "20091208] retired (<878ze2m5dy.fsf@mally.pyrite.org>)",
    "dn": {
      "uid": "robt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rob",
    "uid": "robt",
    "sn": "Tillotson",
    "labeledURI": "http://www.pyrite.org/",
    "uidNumber": "1205",
    "ircNick": "n9mtb",
    "gidNumber": "1626"
  },
  {
    "info": "# rodrigo, users, debian.org",
    "dn": {
      "uid": "rodrigo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rodrigo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2897",
    "cn": "Luis",
    "sn": "Rodrigo Gallardo Cruz",
    "ircNick": "gigio",
    "jabberJID": "rodrigo.gallardo@gmail.com",
    "gidNumber": "2897"
  },
  {
    "info": "qLUSmqRcOKpjzer6axuAHVHadZkhdbQIDAQAB",
    "dn": {
      "uid": "roehling",
      "ou": "users",
      "dc": "org"
    },
    "uid": "roehling",
    "objectClass": "debianDeveloper",
    "uidNumber": "3626",
    "gidNumber": "3626",
    "keyFingerPrint": "9B03EBB98300DF97C2B123BFCC8C6BDD1403F4CA",
    "cn": "Timo",
    "sn": ": UsO2aGxpbmc=",
    "ircNick": "roehling"
  },
  {
    "info": "# rogerso, users, debian.org",
    "dn": {
      "uid": "rogerso",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roger",
    "uid": "rogerso",
    "sn": "So",
    "uidNumber": "2139",
    "ircNick": "spacehunt",
    "jabberJID": "debian@spacehunt.info",
    "labeledURI": "http://spacehunt.info",
    "gidNumber": "2139"
  },
  {
    "info": "# rogoyski, users, debian.org",
    "dn": {
      "uid": "rogoyski",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "rogoyski",
    "sn": "Rogoyski",
    "labeledURI": "http://www.rogoyski.com/adam/",
    "uidNumber": "1405",
    "ircNick": "Temperance",
    "gidNumber": "1405"
  },
  {
    "info": "# roktas, users, debian.org",
    "dn": {
      "uid": "roktas",
      "ou": "users",
      "dc": "org"
    },
    "uid": "roktas",
    "objectClass": "debianDeveloper",
    "uidNumber": "2773",
    "keyFingerPrint": "2061020BA49CB3F7810815D09C0E389B3FD25C84",
    "cn": "Recai",
    "sn": "Oktas",
    "ircNick": "pasha",
    "jabberJID": "roktas@jabber.org",
    "labeledURI": "http://roktas.me",
    "gidNumber": "2773"
  },
  {
    "info": "z80EYvw7GE7MzOKbrmAEt+ZbZq8QIDAQAB",
    "dn": {
      "uid": "roland",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roland",
    "uid": "roland",
    "sn": "Rosenfeld",
    "uidNumber": "1271",
    "keyFingerPrint": "AC2FBDB104943D8A44A0211D02713BCFCA5410B2",
    "gidNumber": "1271",
    "ircNick": "RoRo",
    "labeledURI": "https://www.spinnaker.de/"
  },
  {
    "info": "# romain, users, debian.org",
    "dn": {
      "uid": "romain",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Romain",
    "uid": "romain",
    "uidNumber": "2396",
    "sn": "Lerallut",
    "ircNick": "romain",
    "labeledURI": "http://rlerallut.free.fr",
    "gidNumber": "2396"
  },
  {
    "info": "# roman, users, debian.org",
    "dn": {
      "uid": "roman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roman",
    "uid": "roman",
    "sn": "Hodek",
    "uidNumber": "972",
    "ircNick": "Bombenleger",
    "labeledURI": "http://www.hodek.net/",
    "gidNumber": "972"
  },
  {
    "info": "# romosan, users, debian.org",
    "dn": {
      "uid": "romosan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://caliban.lbl.gov/",
    "cn": "Alex",
    "uid": "romosan",
    "uidNumber": "1196",
    "sn": "Romosan",
    "gidNumber": "1684"
  },
  {
    "info": "# ron, users, debian.org",
    "dn": {
      "uid": "ron",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ron",
    "uid": "ron",
    "uidNumber": "1369",
    "sn": "Lee",
    "keyFingerPrint": "52744009F2C743EDA2CC69524F346E47A5805D48",
    "gidNumber": "1369"
  },
  {
    "info": "# rosh, users, debian.org",
    "dn": {
      "uid": "rosh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rosh",
    "objectClass": "debianDeveloper",
    "uidNumber": "3421",
    "gidNumber": "3421",
    "keyFingerPrint": "2FAFEBB52F5F5DCE5570D2E66C6ACD6417B3ACB1",
    "cn": "Roger",
    "sn": "Shimizu"
  },
  {
    "info": "# ross, users, debian.org",
    "dn": {
      "uid": "ross",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ross",
    "keyFingerPrint": "1A21F5B0D8D0CFE381D4E25A2D09E447D0B433DF",
    "uid": "ross",
    "uidNumber": "2583",
    "sn": "Burton",
    "ircNick": "ross",
    "labeledURI": "http://www.burtonini.com/",
    "gidNumber": "2583"
  },
  {
    "info": "# rossgammon, users, debian.org",
    "dn": {
      "uid": "rossgammon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rossgammon",
    "objectClass": "debianDeveloper",
    "uidNumber": "3323",
    "keyFingerPrint": "FBEE0190904F1EA0BA6A300E53FE7BBDA68910FC",
    "cn": "Ross",
    "sn": "Gammon",
    "gidNumber": "3323",
    "ircNick": "Rosco2",
    "jabberJID": "gooby@jabber.dk",
    "labeledURI": "http://www.the-gammons.net/"
  },
  {
    "info": "# rotty, users, debian.org",
    "dn": {
      "uid": "rotty",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "rotty",
    "sn": "Rottmann",
    "uidNumber": "2199",
    "ircNick": "Rotty",
    "labeledURI": "http://yi.org/rotty/",
    "gidNumber": "2199"
  },
  {
    "info": "# rouca, users, debian.org",
    "dn": {
      "uid": "rouca",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rouca",
    "objectClass": "debianDeveloper",
    "uidNumber": "3466",
    "gidNumber": "3466",
    "keyFingerPrint": "5D0187B940A245BAD7B0F56A003A1A2DAA41085F",
    "cn": "Bastien",
    "sn": ": Um91Y2FyacOocw==",
    "ircNick": "rouca"
  },
  {
    "info": "# rousseau, users, debian.org",
    "dn": {
      "uid": "rousseau",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ludovic",
    "uid": "rousseau",
    "uidNumber": "2410",
    "sn": "Rousseau",
    "keyFingerPrint": "F5E11B9FFE911146F41D953D78A1B4DFE8F9C57E",
    "gidNumber": "2410",
    "labeledURI": "http://ludovic.rousseau.free.fr/"
  },
  {
    "info": "# rover, users, debian.org",
    "dn": {
      "uid": "rover",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roberto",
    "uid": "rover",
    "sn": "Lumbreras",
    "uidNumber": "1116",
    "keyFingerPrint": "D0FF1317762D5F3C80A5C740B67EE21012290B4D",
    "gidNumber": "1116"
  },
  {
    "info": "# rperrot, users, debian.org",
    "dn": {
      "uid": "rperrot",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Remi",
    "uid": "rperrot",
    "uidNumber": "2291",
    "ircNick": "jetto",
    "sn": "Perrot",
    "gidNumber": "2291"
  },
  {
    "info": "# rra, users, debian.org",
    "dn": {
      "uid": "rra",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rra",
    "objectClass": "debianDeveloper",
    "uidNumber": "2757",
    "cn": "Russ",
    "sn": "Allbery",
    "keyFingerPrint": "E784364E8DDE7BB370FBD9EAD15D313882004173",
    "labeledURI": "http://www.eyrie.org/~eagle/",
    "gidNumber": "2757"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "rread",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rread",
    "uidNumber": "866",
    "sn": "Read",
    "gidNumber": "866"
  },
  {
    "info": "# rrs, users, debian.org",
    "dn": {
      "uid": "rrs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rrs",
    "objectClass": "debianDeveloper",
    "uidNumber": "3071",
    "keyFingerPrint": "43DEF582F9E67111CE008917F2F11C23F00A2BE6",
    "cn": "Ritesh",
    "sn": "Sarraf",
    "gidNumber": "3071",
    "ircNick": "RickXy",
    "jabberJID": "rrs@researchut.com",
    "labeledURI": "http://www.researchut.com/blog"
  },
  {
    "info": "# rsahlen, users, debian.org",
    "dn": {
      "uid": "rsahlen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "rsahlen",
    "sn": "Sahlender",
    "uidNumber": "1165",
    "ircNick": "Klatu",
    "gidNumber": "1165"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "rSanders",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rSanders",
    "uidNumber": "815",
    "sn": "Sanders",
    "gidNumber": "815"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "rsl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Randall",
    "uid": "rsl",
    "uidNumber": "1139",
    "sn": "Loomis",
    "gidNumber": "1139"
  },
  {
    "info": "# rtandy, users, debian.org",
    "dn": {
      "uid": "rtandy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rtandy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3403",
    "gidNumber": "3403",
    "keyFingerPrint": "FC9322B6E5504CB86D9D86A4CABE1E9E2EBA364F",
    "cn": "Ryan",
    "sn": "Tandy"
  },
  {
    "info": "# rubund, users, debian.org",
    "dn": {
      "uid": "rubund",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rubund",
    "objectClass": "debianDeveloper",
    "uidNumber": "3531",
    "gidNumber": "3531",
    "keyFingerPrint": "3474C4096729ED0C51807D3CE69822C7E02958CD",
    "cn": "Ruben",
    "sn": "Undheim"
  },
  {
    "info": "# rudi, users, debian.org",
    "dn": {
      "uid": "rudi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roland",
    "keyFingerPrint": "0FDF0F1869C2AA24B45F1691A84F709AEDD3D779",
    "uid": "rudi",
    "uidNumber": "2678",
    "sn": "Rutschmann",
    "gidNumber": "2678",
    "labeledURI": "http://www.psychologie.uni-regensburg.de/Rutschmann"
  },
  {
    "info": "# rudy, users, debian.org",
    "dn": {
      "uid": "rudy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rudy",
    "objectClass": "debianDeveloper",
    "uidNumber": "2792",
    "cn": "Rudy",
    "sn": "Godoy",
    "ircNick": "stone-head",
    "jabberJID": "stone_head@jabber.org",
    "labeledURI": "http://stone-head.org",
    "gidNumber": "2792"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "rui",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ueyama",
    "uid": "rui",
    "uidNumber": "1386",
    "sn": "Rui",
    "gidNumber": "1386"
  },
  {
    "info": "# rul, users, debian.org",
    "dn": {
      "uid": "rul",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rul",
    "objectClass": "debianDeveloper",
    "uidNumber": "3662",
    "gidNumber": "3662",
    "keyFingerPrint": "16AD29CF574AD0206F9961E02CF0F17C43474B6F",
    "cn": ": UmHDumw=",
    "sn": "Benencia",
    "ircNick": "rul",
    "labeledURI": "https://rbenencia.name/"
  },
  {
    "info": "# ruoso, users, debian.org",
    "dn": {
      "uid": "ruoso",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "ruoso",
    "sn": "Ruoso",
    "uidNumber": "2661",
    "ircNick": "ruoso",
    "labeledURI": "http://daniel.ruoso.com",
    "gidNumber": "2661"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "ruud",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ruud",
    "uid": "ruud",
    "sn": "de Rooij",
    "labeledURI": "http://ruud.org",
    "uidNumber": "1191",
    "ircNick": "ruud",
    "gidNumber": "1628"
  },
  {
    "info": "# rvandegrift, users, debian.org",
    "dn": {
      "uid": "rvandegrift",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rvandegrift",
    "objectClass": "debianDeveloper",
    "uidNumber": "3559",
    "gidNumber": "3559",
    "keyFingerPrint": "B008D750B6B78361ED5356F0DAB389329A4CFA16",
    "cn": "Ross",
    "sn": "Vandegrift",
    "ircNick": "rvandegrift"
  },
  {
    "info": "# rvb, users, debian.org",
    "dn": {
      "uid": "rvb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "rvb",
    "objectClass": "debianDeveloper",
    "uidNumber": "2789",
    "sn": "van Bevern",
    "gidNumber": "2789",
    "cn": ": UmVuw6k="
  },
  {
    "info": "# rvdm, users, debian.org",
    "dn": {
      "uid": "rvdm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "rvdm",
    "uidNumber": "2072",
    "sn": "van der Meulen",
    "ircNick": "Emphyrio",
    "gidNumber": "2072"
  },
  {
    "info": "# rvr, users, debian.org",
    "dn": {
      "uid": "rvr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rogerio",
    "uid": "rvr",
    "sn": "Reis",
    "labeledURI": "http://www.ncc.up.pt/~rvr",
    "uidNumber": "2453",
    "ircNick": "khaos",
    "gidNumber": "2453"
  },
  {
    "info": "# rwalker, users, debian.org",
    "dn": {
      "uid": "rwalker",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rob",
    "uid": "rwalker",
    "uidNumber": "1251",
    "ircNick": "biffhero",
    "sn": "Walker",
    "gidNumber": "1699"
  },
  {
    "info": "# rweber, users, debian.org",
    "dn": {
      "uid": "rweber",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rene",
    "uid": "rweber",
    "uidNumber": "2151",
    "sn": "Weber",
    "keyFingerPrint": "98AA9AE3439BDA66E1AB3621E60BE35A13F71D36",
    "gidNumber": "2151"
  },
  {
    "info": "# ryan, users, debian.org",
    "dn": {
      "uid": "ryan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ryan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3024",
    "cn": "Ryan",
    "sn": "Niebur",
    "ircNick": "Ryan52",
    "gidNumber": "3024"
  },
  {
    "info": "# sacha, users, debian.org",
    "dn": {
      "uid": "sacha",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Alexander",
    "uid": "sacha",
    "sn": "Kotelnikov",
    "labeledURI": "http://www.myxomop.com/~sacha/",
    "uidNumber": "2060",
    "ircNick": "sacha",
    "gidNumber": "2060"
  },
  {
    "info": "# sahil, users, debian.org",
    "dn": {
      "uid": "sahil",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sahil",
    "objectClass": "debianDeveloper",
    "uidNumber": "3663",
    "gidNumber": "3663",
    "keyFingerPrint": "21A4A44B5BA4962F3ACFB89616B9F687A32466F9",
    "cn": "Sahil",
    "sn": "Dhiman",
    "ircNick": "sahilister",
    "jabberJID": "sahil@chat.sahilister.in",
    "labeledURI": "https://sahilister.in"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "sailer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tim",
    "uid": "sailer",
    "uidNumber": "893",
    "sn": "Sailer",
    "gidNumber": "893"
  },
  {
    "info": "# sakirnth, users, debian.org",
    "dn": {
      "uid": "sakirnth",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sakirnth",
    "objectClass": "debianDeveloper",
    "uidNumber": "3650",
    "gidNumber": "3650",
    "keyFingerPrint": "900F6DCAD20AD1819F9E8A6AD64A2FAE095EF799",
    "cn": "Sakirnth",
    "sn": "Nagarasa"
  },
  {
    "info": "1IcAy5X9TeMwIDAQAB",
    "dn": {
      "uid": "salve",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Davide",
    "uid": "salve",
    "sn": "Salvetti",
    "uidNumber": "1098",
    "keyFingerPrint": "8DE38CCA640823EB343C96B52EC615B2E8D3A535",
    "gidNumber": "1098",
    "ircNick": "salve",
    "labeledURI": "http://www.linux.it/%7Esalve/"
  },
  {
    "info": "91208]",
    "dn": {
      "uid": "sam",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Samuel",
    "uid": "sam",
    "sn": "Tardieu",
    "labeledURI": "http://www.rfc1149.net/sam",
    "uidNumber": "1272",
    "gidNumber": "1272"
  },
  {
    "info": "# sama, users, debian.org",
    "dn": {
      "uid": "sama",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andrea",
    "uid": "sama",
    "uidNumber": "2305",
    "ircNick": "sama",
    "sn": "Glorioso",
    "gidNumber": "2305"
  },
  {
    "info": "# samj, users, debian.org",
    "dn": {
      "uid": "samj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sam",
    "keyFingerPrint": "BD486B9E4BFAA792CEC5FD7D0EBD65E13C093EEF",
    "uid": "samj",
    "uidNumber": "2299",
    "sn": "Johnston",
    "ircNick": "samj",
    "labeledURI": "http://samj.net",
    "gidNumber": "2299"
  },
  {
    "info": "# samo, users, debian.org",
    "dn": {
      "uid": "samo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://superduper.net/",
    "cn": "Sam",
    "keyFingerPrint": "DDF8B22B71418194DB94817A2CEBF138D91EE369",
    "uid": "samo",
    "uidNumber": "2607",
    "ircNick": "samo",
    "sn": "Clegg",
    "gidNumber": "2607"
  },
  {
    "info": "# samu, users, debian.org",
    "dn": {
      "uid": "samu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Samuele",
    "uid": "samu",
    "sn": "Tonon",
    "labeledURI": "http://www.students.cs.unibo.it/~tonon/",
    "uidNumber": "2445",
    "ircNick": "samu",
    "gidNumber": "2445"
  },
  {
    "info": "# samueloph, users, debian.org",
    "dn": {
      "uid": "samueloph",
      "ou": "users",
      "dc": "org"
    },
    "uid": "samueloph",
    "objectClass": "debianDeveloper",
    "uidNumber": "3522",
    "gidNumber": "3522",
    "cn": "Samuel",
    "sn": "Henrique",
    "keyFingerPrint": "BFAE9E331A867A7C80D8EB78F4E4ACDBB8D08BE0",
    "ircNick": "samueloph"
  },
  {
    "info": "# sano, users, debian.org",
    "dn": {
      "uid": "sano",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Taketoshi",
    "uid": "sano",
    "sn": "Sano",
    "uidNumber": "1396",
    "ircNick": {},
    "labeledURI": "http://homepage3.nifty.com/taketoshi_sano/",
    "gidNumber": "1396"
  },
  {
    "info": "ac10",
    "dn": {
      "uid": "santiago",
      "ou": "users",
      "dc": "org"
    },
    "uid": "santiago",
    "objectClass": "debianDeveloper",
    "uidNumber": "2777",
    "cn": "Santiago",
    "gidNumber": "2777",
    "sn": ": UmluY8Ozbg==",
    "keyFingerPrint": "A2FD948D3153DC841A3E6C8922AFD1C0DAC8BC06",
    "ircNick": "santiago"
  },
  {
    "info": "# sanvila, users, debian.org",
    "dn": {
      "uid": "sanvila",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Santiago",
    "keyFingerPrint": "D54C3BFAFFB042DE382DA5D741CE7F0B9F1B8B32",
    "uid": "sanvila",
    "uidNumber": "968",
    "sn": "Vila",
    "gidNumber": "968"
  },
  {
    "info": "# sasa, users, debian.org",
    "dn": {
      "uid": "sasa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "sasa",
    "uidNumber": "1314",
    "keyFingerPrint": "0CBC5D95331B55C176C558E4F80D211A99FDEEF8",
    "cn": "Attila",
    "sn": "Szalay",
    "gidNumber": "1314"
  },
  {
    "info": "# sathieu, users, debian.org",
    "dn": {
      "uid": "sathieu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sathieu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3022",
    "cn": "Mathieu",
    "sn": "Parent",
    "keyFingerPrint": "AAA58B842E882CF414E0BAB1A7C72A1C782B8C3F",
    "gidNumber": "3022",
    "ircNick": "sathieu"
  },
  {
    "info": "# satta, users, debian.org",
    "dn": {
      "uid": "satta",
      "ou": "users",
      "dc": "org"
    },
    "uid": "satta",
    "objectClass": "debianDeveloper",
    "uidNumber": "3416",
    "gidNumber": "3416",
    "keyFingerPrint": "0EED77DC41D760FDE44035FF5556A34E04A3610B",
    "cn": "Sascha",
    "sn": "Steinbiss",
    "ircNick": "satta",
    "jabberJID": "satta@jabber.ccc.de",
    "labeledURI": "https://steinbiss.name"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "sauter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lonnie",
    "uid": "sauter",
    "uidNumber": "1316",
    "ircNick": "lebesgue",
    "sn": "Sauter",
    "gidNumber": "1316"
  },
  {
    "info": "# sax, users, debian.org",
    "dn": {
      "uid": "sax",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wouter",
    "uid": "sax",
    "sn": "de Vries",
    "labeledURI": "http://www.kermy.nl",
    "uidNumber": "2273",
    "ircNick": "kermy",
    "gidNumber": "2273"
  },
  {
    "info": "# sbadia, users, debian.org",
    "dn": {
      "uid": "sbadia",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sbadia",
    "objectClass": "debianDeveloper",
    "uidNumber": "3445",
    "gidNumber": "3445",
    "keyFingerPrint": "7E43E9ACBF727AB3CF0885338716CE4614A452D8",
    "cn": "Sebastien",
    "sn": "Badia",
    "ircNick": "sbadia",
    "labeledURI": "http://sebian.fr/"
  },
  {
    "info": "# sblondon, users, debian.org",
    "dn": {
      "uid": "sblondon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sblondon",
    "objectClass": "debianDeveloper",
    "uidNumber": "3432",
    "gidNumber": "3432",
    "keyFingerPrint": "15745B863B57D6928F858EEBA2B91BC6CD4EBDDC",
    "sn": "Blondon",
    "cn": ": U3TDqXBoYW5l"
  },
  {
    "info": "# sbronson, users, debian.org",
    "dn": {
      "uid": "sbronson",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sbronson",
    "objectClass": "debianDeveloper",
    "uidNumber": "3251",
    "keyFingerPrint": "C199D161F056EF615D115983BA9996E1D7CB9D6F",
    "cn": "Samuel",
    "sn": "Bronson",
    "ircNick": "SamB",
    "gidNumber": "3251"
  },
  {
    "info": "# schaffri, users, debian.org",
    "dn": {
      "uid": "schaffri",
      "ou": "users",
      "dc": "org"
    },
    "uid": "schaffri",
    "objectClass": "debianDeveloper",
    "uidNumber": "3308",
    "cn": "Waldemar",
    "sn": "Schaffrinna",
    "gidNumber": "3308"
  },
  {
    "info": "# schaumat, users, debian.org",
    "dn": {
      "uid": "schaumat",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "schaumat",
    "uidNumber": "2351",
    "sn": "Chaumat",
    "gidNumber": "2351",
    "cn": ": U8OpYmFzdGllbg=="
  },
  {
    "info": "# scheffczyk, users, debian.org",
    "dn": {
      "uid": "scheffczyk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "scheffczyk",
    "uidNumber": "2623",
    "sn": "Scheffczyk",
    "gidNumber": "2623"
  },
  {
    "info": "ouncing",
    "dn": {
      "uid": "schepler",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "keyFingerPrint": "83C28932B64F1A4EE8A2D7C9342DCB0320021490",
    "uid": "schepler",
    "uidNumber": "2225",
    "sn": "Schepler",
    "ircNick": "frob",
    "gidNumber": "2225"
  },
  {
    "info": "# schimmi, users, debian.org",
    "dn": {
      "uid": "schimmi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "schimmi",
    "sn": "Schimanski",
    "uidNumber": "2564",
    "ircNick": "schimmi",
    "jabberJID": "sts@1stein.org",
    "labeledURI": "http://1stein.org",
    "gidNumber": "2564"
  },
  {
    "info": "# schizo, users, debian.org",
    "dn": {
      "uid": "schizo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Clint",
    "uid": "schizo",
    "sn": "Adams",
    "uidNumber": "1022",
    "ircNick": "Clint",
    "gidNumber": "1022"
  },
  {
    "info": "# schlue, users, debian.org",
    "dn": {
      "uid": "schlue",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "schlue",
    "sn": "Schlueter",
    "uidNumber": "1046",
    "ircNick": "schlue",
    "jabberJID": "michael@jabber.johalla.de",
    "labeledURI": "http://people.debian.org/~schlue/",
    "gidNumber": "1046"
  },
  {
    "info": "# schmitz, users, debian.org",
    "dn": {
      "uid": "schmitz",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "schmitz",
    "uidNumber": "900",
    "sn": "Schmitz",
    "keyFingerPrint": "3954417E4E59A2D8D73B92D63AD82CD4B4C4FB4C",
    "labeledURI": "http://waratah.linuxd.org",
    "gidNumber": "900"
  },
  {
    "info": "# schoenfeld, users, debian.org",
    "dn": {
      "uid": "schoenfeld",
      "ou": "users",
      "dc": "org"
    },
    "uid": "schoenfeld",
    "objectClass": "debianDeveloper",
    "uidNumber": "2937",
    "cn": "Patrick",
    "sn": "Schoenfeld",
    "ircNick": "aptituz",
    "jabberJID": "patrick.schoenfeld@googlemail.com",
    "gidNumber": "2937"
  },
  {
    "info": "# schoepf, users, debian.org",
    "dn": {
      "uid": "schoepf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "schoepf",
    "uidNumber": "1353",
    "sn": "Schoepf",
    "gidNumber": "1353"
  },
  {
    "info": "# schuller, users, debian.org",
    "dn": {
      "uid": "schuller",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bart",
    "uid": "schuller",
    "uidNumber": "1232",
    "ircNick": "Arrowhead",
    "sn": "Schuller",
    "gidNumber": "1712"
  },
  {
    "info": "# schultmc, users, debian.org",
    "dn": {
      "uid": "schultmc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "keyFingerPrint": "BAE0340127D536AD2CB12FE09EC002FE1C9CA517",
    "uid": "schultmc",
    "sn": "Schultheiss",
    "uidNumber": "2479",
    "gidNumber": "2479",
    "ircNick": "schultmc",
    "jabberJID": "schultmc@jabber.org",
    "labeledURI": "http://people.debian.org/~schultmc/"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "schwab",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "schwab",
    "uidNumber": "880",
    "sn": "Schwab",
    "gidNumber": "880"
  },
  {
    "info": "# scott, users, debian.org",
    "dn": {
      "uid": "scott",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Scott",
    "uid": "scott",
    "sn": "Ellis",
    "labeledURI": "http://www.stormcrow.org/",
    "uidNumber": "997",
    "ircNick": "StormCrow",
    "gidNumber": "997"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "sde1000",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sde1000",
    "uidNumber": "867",
    "sn": "Early",
    "gidNumber": "867"
  },
  {
    "info": "# sdier, users, debian.org",
    "dn": {
      "uid": "sdier",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Scott",
    "uid": "sdier",
    "sn": "Dier",
    "uidNumber": "2212",
    "ircNick": "dieman",
    "jabberJID": "scott@dier.name",
    "labeledURI": "http://scott.dier.name/",
    "gidNumber": "2212"
  },
  {
    "info": "# sdt, users, debian.org",
    "dn": {
      "uid": "sdt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sdt",
    "objectClass": "debianDeveloper",
    "uidNumber": "2780",
    "cn": "Stuart",
    "sn": "Teasdale",
    "gidNumber": "2780"
  },
  {
    "info": "# seamlik, users, debian.org",
    "dn": {
      "uid": "seamlik",
      "ou": "users",
      "dc": "org"
    },
    "uid": "seamlik",
    "objectClass": "debianDeveloper",
    "uidNumber": "3551",
    "gidNumber": "3551",
    "cn": "Kai-Chung",
    "sn": "Yan",
    "labeledURI": "https://seamlik.github.io"
  },
  {
    "info": "# seamus, users, debian.org",
    "dn": {
      "uid": "seamus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Angel",
    "uid": "seamus",
    "uidNumber": "2390",
    "sn": "Ramos",
    "ircNick": "Seamus",
    "labeledURI": "http://www.atem-nt.com",
    "gidNumber": "2390"
  },
  {
    "info": "# seanius, users, debian.org",
    "dn": {
      "uid": "seanius",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sean",
    "uid": "seanius",
    "sn": "Finney",
    "uidNumber": "2576",
    "ircNick": "seanius",
    "labeledURI": "http://www.seanius.net",
    "gidNumber": "2576"
  },
  {
    "info": "# seb, users, debian.org",
    "dn": {
      "uid": "seb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "seb",
    "objectClass": "debianDeveloper",
    "uidNumber": "2760",
    "cn": "Sebastien",
    "sn": "Delafond",
    "keyFingerPrint": "E40F8DC40800ACA52562BBE491AD581ADAF6CE93",
    "gidNumber": "2760",
    "ircNick": "Seb"
  },
  {
    "info": "# seb128, users, debian.org",
    "dn": {
      "uid": "seb128",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sebastien",
    "uid": "seb128",
    "uidNumber": "2517",
    "sn": "Bacher",
    "keyFingerPrint": "4724B86F47E02915DEEA49E23EBD44903EDB0496",
    "gidNumber": "2517",
    "ircNick": "seb128"
  },
  {
    "info": "# sebastic, users, debian.org",
    "dn": {
      "uid": "sebastic",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sebastic",
    "objectClass": "debianDeveloper",
    "uidNumber": "3277",
    "keyFingerPrint": "8182DE417056408D614650D16750F10AE88D4AF1",
    "cn": "Sebastiaan",
    "sn": "Couwenberg",
    "ircNick": "sebastic",
    "gidNumber": "3277"
  },
  {
    "info": "288B7ITLV3t+2TieyEPahBy81KbhxrMwIDAQAB",
    "dn": {
      "uid": "sebastien",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sebastien",
    "objectClass": "debianDeveloper",
    "uidNumber": "3227",
    "sn": "Villemot",
    "keyFingerPrint": "20691DFCC2C98C47952984EE00018C22381A7594",
    "gidNumber": "3227",
    "cn": ": U8OpYmFzdGllbg==",
    "ircNick": "sebastien",
    "labeledURI": "https://sebastien.villemot.name"
  },
  {
    "info": "# segre, users, debian.org",
    "dn": {
      "uid": "segre",
      "ou": "users",
      "dc": "org"
    },
    "uid": "segre",
    "objectClass": "debianDeveloper",
    "uidNumber": "2794",
    "cn": "Carlo",
    "sn": "Segre",
    "keyFingerPrint": "1F9FA4CA6FFB59779BBCE14B58A922CDDB5DB08E",
    "gidNumber": "2794",
    "ircNick": "xray",
    "labeledURI": "http://phys.iit.edu/~segre"
  },
  {
    "info": "# seppy, users, debian.org",
    "dn": {
      "uid": "seppy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dennis",
    "uid": "seppy",
    "sn": "Stampfer",
    "uidNumber": "2581",
    "ircNick": "seppy",
    "gidNumber": "2581"
  },
  {
    "info": "6bWY4iOV/G2XwIDAQAB",
    "dn": {
      "uid": "sergiodj",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sergiodj",
    "objectClass": "debianDeveloper",
    "uidNumber": "3422",
    "gidNumber": "3422",
    "keyFingerPrint": "237A54B1028728BF00EF31F4D0EB762865FC5E36",
    "cn": "Sergio",
    "sn": "Durigan Junior",
    "ircNick": "sergiodj",
    "jabberJID": "sergiodj@sergiodj.net",
    "labeledURI": "https://sergiodj.net"
  },
  {
    "info": "# serpent, users, debian.org",
    "dn": {
      "uid": "serpent",
      "ou": "users",
      "dc": "org"
    },
    "uid": "serpent",
    "objectClass": "debianDeveloper",
    "uidNumber": "3512",
    "gidNumber": "3512",
    "keyFingerPrint": "A565CE64F866A2584DDCF9C7ECB73E37E887AA8C",
    "cn": "Tomasz",
    "sn": "Rybak",
    "ircNick": "serpent"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "servo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "servo",
    "uidNumber": "979",
    "sn": "Stringfield",
    "gidNumber": "979"
  },
  {
    "info": "# sesse, users, debian.org",
    "dn": {
      "uid": "sesse",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steinar",
    "uid": "sesse",
    "sn": "Gunderson",
    "uidNumber": "2646",
    "keyFingerPrint": "C2E9004FF028C18E4EADDB837F61756177978F76",
    "gidNumber": "2646",
    "ircNick": "Sesse",
    "labeledURI": "https://www.sesse.net/"
  },
  {
    "info": "# sez, users, debian.org",
    "dn": {
      "uid": "sez",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sez",
    "objectClass": "debianDeveloper",
    "uidNumber": "3063",
    "keyFingerPrint": "036456AA8EC8C0B08B48561B4F9F6D550ED6122A",
    "cn": "Serafeim",
    "sn": "Zanikolas",
    "ircNick": "sez",
    "labeledURI": "http://wiki.debian.org/SerafeimZanikolas",
    "gidNumber": "3063"
  },
  {
    "info": "# sf, users, debian.org",
    "dn": {
      "uid": "sf",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sf",
    "objectClass": "debianDeveloper",
    "uidNumber": "2844",
    "cn": "Stefan",
    "sn": "Fritsch",
    "keyFingerPrint": "3A988DCDAF09A81CB2618B31C6875F3541CEFDE0",
    "ircNick": "sf",
    "labeledURI": "http://www.sfritsch.de",
    "gidNumber": "2844"
  },
  {
    "info": "# sfllaw, users, debian.org",
    "dn": {
      "uid": "sfllaw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Simon",
    "uid": "sfllaw",
    "sn": "Law",
    "uidNumber": "2534",
    "ircNick": "coleSLAW",
    "jabberJID": "sfllaw@jabber.com",
    "labeledURI": "http://www.law.yi.org/~sfllaw/",
    "gidNumber": "2534"
  },
  {
    "info": "# sfrost, users, debian.org",
    "dn": {
      "uid": "sfrost",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sfrost",
    "sn": "Frost",
    "uidNumber": "2177",
    "keyFingerPrint": "C1FEA06F12A10FCEB7CEBC7FED6C8A3883476455",
    "gidNumber": "2177",
    "ircNick": "Snow-Man",
    "labeledURI": "http://www.snowman.net"
  },
  {
    "info": "# sgevatter, users, debian.org",
    "dn": {
      "uid": "sgevatter",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sgevatter",
    "objectClass": "debianDeveloper",
    "uidNumber": "3108",
    "keyFingerPrint": "7C0FF088052C082B442454171CFC22F3363DEAE3",
    "cn": "Siegfried-Angel",
    "sn": "Gevatter Pujals",
    "ircNick": "RainCT",
    "jabberJID": "siggi.gevatter@gmail.com",
    "labeledURI": "http://gevatter.com",
    "gidNumber": "3108"
  },
  {
    "info": "# sgk, users, debian.org",
    "dn": {
      "uid": "sgk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Susan",
    "uid": "sgk",
    "sn": "Kleinmann",
    "uidNumber": "885",
    "gidNumber": "885"
  },
  {
    "info": "# sgmoore, users, debian.org",
    "dn": {
      "uid": "sgmoore",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sgmoore",
    "objectClass": "debianDeveloper",
    "uidNumber": "3621",
    "gidNumber": "3621",
    "keyFingerPrint": "7C35920F1CE2899E8EA9AAD02E7C0367B9BFA089",
    "cn": "Scarlett",
    "sn": "Moore",
    "ircNick": "sgmoore",
    "labeledURI": "https://scarlettgatelymoore.dev"
  },
  {
    "info": "# sgolovan, users, debian.org",
    "dn": {
      "uid": "sgolovan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sgolovan",
    "objectClass": "debianDeveloper",
    "uidNumber": "2872",
    "cn": "Sergei",
    "sn": "Golovan",
    "keyFingerPrint": "59E895A48D09D6C8EBB94CE857E7C6324F81391D",
    "gidNumber": "2872",
    "jabberJID": "sgolovan@nes.ru"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "sgore",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sgore",
    "uidNumber": "2017",
    "ircNick": "sgore",
    "sn": "Gore",
    "gidNumber": "2017"
  },
  {
    "info": "# sgran, users, debian.org",
    "dn": {
      "uid": "sgran",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sgran",
    "sn": "Gran",
    "uidNumber": "2575",
    "ircNick": "sgran",
    "labeledURI": "http://www.lobefin.net/~steve",
    "gidNumber": "2575"
  },
  {
    "info": "# sgybas, users, debian.org",
    "dn": {
      "uid": "sgybas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "sgybas",
    "sn": "Gybas",
    "uidNumber": "1101",
    "gidNumber": "1101"
  },
  {
    "info": "# shachar, users, debian.org",
    "dn": {
      "uid": "shachar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "shachar",
    "objectClass": "debianDeveloper",
    "uidNumber": "2817",
    "cn": "Shachar",
    "sn": "Shemesh",
    "keyFingerPrint": "F2D643A558B9924C0649207BA0C5AD1359CD3653",
    "gidNumber": "2817",
    "ircNick": "Shachar",
    "labeledURI": "http://practical-pl.org"
  },
  {
    "info": "# shadur, users, debian.org",
    "dn": {
      "uid": "shadur",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rens",
    "uid": "shadur",
    "uidNumber": "2425",
    "ircNick": "Shadur",
    "sn": "Houben",
    "gidNumber": "2425"
  },
  {
    "info": "# shaka, users, debian.org",
    "dn": {
      "uid": "shaka",
      "ou": "users",
      "dc": "org"
    },
    "uid": "shaka",
    "objectClass": "debianDeveloper",
    "uidNumber": "2870",
    "ircNick": "shaka",
    "jabberJID": "oysteigi@jabber.no",
    "labeledURI": "http://oystein.gisnas.net/",
    "gidNumber": "2870",
    "cn": ": w5h5c3RlaW4=",
    "sn": ": R2lzbsOlcw=="
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "shaleh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sean",
    "uid": "shaleh",
    "sn": "Perry",
    "uidNumber": "1166",
    "ircNick": "Shaleh",
    "gidNumber": "1166"
  },
  {
    "info": "# shane, users, debian.org",
    "dn": {
      "uid": "shane",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shane",
    "uid": "shane",
    "sn": "Wegner",
    "uidNumber": "2297",
    "keyFingerPrint": "F099A5F4B77CD6F66A992E6C7B9DBAD42A7E0F3D",
    "gidNumber": "2297",
    "ircNick": "sauderite"
  },
  {
    "info": "# shanson, users, debian.org",
    "dn": {
      "uid": "shanson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Scott",
    "uid": "shanson",
    "uidNumber": "831",
    "sn": "Hanson",
    "gidNumber": "831"
  },
  {
    "info": "# sharkey, users, debian.org",
    "dn": {
      "uid": "sharkey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "sharkey",
    "uidNumber": "2174",
    "sn": "Sharkey",
    "ircNick": "sharkey",
    "keyFingerPrint": "2E98D4D0F8B723A1FF326B5B45E2CDA5A7FD90F9",
    "gidNumber": "2174"
  },
  {
    "info": "# sharky, users, debian.org",
    "dn": {
      "uid": "sharky",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeremy",
    "uid": "sharky",
    "uidNumber": "2717",
    "ircNick": "jeremy_laine",
    "labeledURI": "http://www.jerryweb.org/",
    "keyFingerPrint": "C8845CF9E83F50114D07A264D2CF64921ACE2687",
    "gidNumber": "2717",
    "sn": ": TGFpbsOp"
  },
  {
    "info": "# sharpone, users, debian.org",
    "dn": {
      "uid": "sharpone",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jamey",
    "uid": "sharpone",
    "sn": "Sharp",
    "uidNumber": "1283",
    "jabberJID": "jamey@minilop.net",
    "gidNumber": "1283"
  },
  {
    "info": "# shaul, users, debian.org",
    "dn": {
      "uid": "shaul",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shaul",
    "uid": "shaul",
    "uidNumber": "2251",
    "sn": "Karl",
    "gidNumber": "2251"
  },
  {
    "info": "# shayan_doust, users, debian.org",
    "dn": {
      "uid": "shayan_doust",
      "ou": "users",
      "dc": "org"
    },
    "uid": "shayan_doust",
    "objectClass": "debianDeveloper",
    "uidNumber": "3609",
    "gidNumber": "3609",
    "keyFingerPrint": "0401A810A6F243031EA397596D7D441919D02395",
    "cn": "Shayan",
    "sn": "Doust"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "shell",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hin-lik",
    "uid": "shell",
    "sn": "Hung",
    "labeledURI": "http://www.shellhung.org/",
    "uidNumber": "2446",
    "ircNick": "shell",
    "gidNumber": "2446"
  },
  {
    "info": "# shensche, users, debian.org",
    "dn": {
      "uid": "shensche",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.kodeaffe.de",
    "cn": "Sebastian",
    "uid": "shensche",
    "uidNumber": "2667",
    "sn": "Henschel",
    "gidNumber": "2667"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "shields",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "shields",
    "uidNumber": "886",
    "sn": "Shields",
    "gidNumber": "886"
  },
  {
    "info": "# shlomme, users, debian.org",
    "dn": {
      "uid": "shlomme",
      "ou": "users",
      "dc": "org"
    },
    "uid": "shlomme",
    "objectClass": "debianDeveloper",
    "uidNumber": "2784",
    "keyFingerPrint": "190200025DFC856BF146894C7CC5451EA244C858",
    "cn": "Torsten",
    "sn": "Marek",
    "gidNumber": "2784",
    "ircNick": "shlomme"
  },
  {
    "info": "# sho, users, debian.org",
    "dn": {
      "uid": "sho",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Samuel",
    "uid": "sho",
    "sn": "Hocevar",
    "labeledURI": "http://sam.zoy.org/",
    "uidNumber": "2049",
    "ircNick": "sam",
    "gidNumber": "2049"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "shorty",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Christian",
    "uid": "shorty",
    "uidNumber": "1352",
    "sn": "Kurz",
    "gidNumber": "1352"
  },
  {
    "info": "# showard, users, debian.org",
    "dn": {
      "uid": "showard",
      "ou": "users",
      "dc": "org"
    },
    "uid": "showard",
    "objectClass": "debianDeveloper",
    "uidNumber": "3098",
    "cn": "Scott",
    "sn": "Howard",
    "keyFingerPrint": "5E9E9A7BDBB9DC61B7D14D358ED06069037E24F2",
    "gidNumber": "3098"
  },
  {
    "info": "# shugo, users, debian.org",
    "dn": {
      "uid": "shugo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shugo",
    "uid": "shugo",
    "uidNumber": "2224",
    "sn": "Maeda",
    "ircNick": "shugo",
    "labeledURI": "http://www.modruby.net/",
    "gidNumber": "2224"
  },
  {
    "info": "# siggi, users, debian.org",
    "dn": {
      "uid": "siggi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Siggi",
    "uid": "siggi",
    "uidNumber": "2210",
    "sn": "Langauf",
    "gidNumber": "2210"
  },
  {
    "info": "# sim590, users, debian.org",
    "dn": {
      "uid": "sim590",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sim590",
    "objectClass": "debianDeveloper",
    "uidNumber": "3423",
    "gidNumber": "3423",
    "keyFingerPrint": "D2C650270E18F258C6AC2BFF9626CC09A9C3251F",
    "cn": "Simon",
    "sn": ": RMOpc2F1bG5pZXJz"
  },
  {
    "info": "# siretart, users, debian.org",
    "dn": {
      "uid": "siretart",
      "ou": "users",
      "dc": "org"
    },
    "uid": "siretart",
    "objectClass": "debianDeveloper",
    "uidNumber": "2834",
    "cn": "Reinhard",
    "sn": "Tartler",
    "gidNumber": "2834",
    "keyFingerPrint": "EA7E6B724BC9FBF2D171EB726B720BE9C5CF6D9E",
    "ircNick": "siretart",
    "labeledURI": "http://tauware.de"
  },
  {
    "info": "# sjackman, users, debian.org",
    "dn": {
      "uid": "sjackman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shaun",
    "uid": "sjackman",
    "uidNumber": "2531",
    "sn": "Jackman",
    "keyFingerPrint": "FEDD8BFA71ED33F4F8F4F469D4ED5223F4E57996",
    "gidNumber": "2531",
    "labeledURI": "https://sjackman.ca"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "sjc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sjc",
    "sn": "Carpenter",
    "labeledURI": "http://www.carpanet.net",
    "uidNumber": "1211",
    "gidNumber": "1620"
  },
  {
    "info": "# sjg, users, debian.org",
    "dn": {
      "uid": "sjg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sebastien",
    "uid": "sjg",
    "sn": "Gross",
    "uidNumber": "2438",
    "ircNick": "sjg",
    "gidNumber": "2438"
  },
  {
    "info": "# sjoerd, users, debian.org",
    "dn": {
      "uid": "sjoerd",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sjoerd",
    "uid": "sjoerd",
    "sn": "Simons",
    "uidNumber": "2359",
    "ircNick": "sjoerd",
    "jabberJID": "sjoerd@luon.net",
    "keyFingerPrint": "2870A31BEA9DBCF274723108C274DB64C2300F7B",
    "gidNumber": "2359"
  },
  {
    "info": "# sjogren, users, debian.org",
    "dn": {
      "uid": "sjogren",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "sjogren",
    "uidNumber": "2554",
    "ircNick": "Marvin--",
    "gidNumber": "2554",
    "sn": ": U2rDtmdyZW4="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "sjp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steven",
    "uid": "sjp",
    "uidNumber": "868",
    "sn": "Phillips",
    "gidNumber": "868"
  },
  {
    "info": "# sjq, users, debian.org",
    "dn": {
      "uid": "sjq",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sjq",
    "uidNumber": "2540",
    "sn": "Quinney",
    "ircNick": "steveq",
    "labeledURI": "http://jadevine.org.uk/",
    "gidNumber": "2540"
  },
  {
    "info": "# sjr, users, debian.org",
    "dn": {
      "uid": "sjr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Simon",
    "uid": "sjr",
    "sn": "Richter",
    "uidNumber": "2326",
    "keyFingerPrint": "9C43253495E4DCA837945F5BEBF67A846AABE354",
    "gidNumber": "2326",
    "ircNick": "GyrosGeier",
    "labeledURI": "http://people.debian.org/~sjr/"
  },
  {
    "info": "# skainz, users, debian.org",
    "dn": {
      "uid": "skainz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "skainz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3296",
    "cn": "Simon",
    "sn": "Kainz",
    "gidNumber": "3296"
  },
  {
    "info": "# skitt, users, debian.org",
    "dn": {
      "uid": "skitt",
      "ou": "users",
      "dc": "org"
    },
    "uid": "skitt",
    "objectClass": "debianDeveloper",
    "uidNumber": "3245",
    "keyFingerPrint": "79D9C58C50D6B5AA65D530C1759778A9A36B494F",
    "cn": "Stephen",
    "sn": "Kitt",
    "gidNumber": "3245",
    "ircNick": "thekittster"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "skuli",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Skuli",
    "uid": "skuli",
    "uidNumber": "1049",
    "sn": "Davidsson",
    "gidNumber": "1049"
  },
  {
    "info": "# skx, users, debian.org",
    "dn": {
      "uid": "skx",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "uid": "skx",
    "sn": "Kemp",
    "uidNumber": "2536",
    "gidNumber": "2536",
    "keyFingerPrint": "D516C42B1D0E3F854CAB97231909D4080C626242",
    "labeledURI": "https://steve.fi/"
  },
  {
    "info": "# slomo, users, debian.org",
    "dn": {
      "uid": "slomo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "slomo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2836",
    "cn": "Sebastian",
    "keyFingerPrint": "7F4BC7CC3CA06F97336BBFEB0668CC1486C2D7B5",
    "ircNick": "slomo",
    "jabberJID": "slomo@coaxion.net",
    "gidNumber": "2836",
    "sn": ": RHLDtmdl"
  },
  {
    "info": "# smarenka, users, debian.org",
    "dn": {
      "uid": "smarenka",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "smarenka",
    "sn": "Marenka",
    "uidNumber": "2450",
    "gidNumber": "2450",
    "ircNick": "smarenka"
  },
  {
    "info": "# smcv, users, debian.org",
    "dn": {
      "uid": "smcv",
      "ou": "users",
      "dc": "org"
    },
    "uid": "smcv",
    "objectClass": "debianDeveloper",
    "uidNumber": "2912",
    "keyFingerPrint": "DA98F25C0871C49A59EAFF2C4DE8FF2A63C7CC90",
    "cn": "Simon",
    "sn": "McVittie",
    "gidNumber": "2912",
    "ircNick": "smcv",
    "labeledURI": "http://www.pseudorandom.co.uk/"
  },
  {
    "info": "20091208] Retired 2005-05-25",
    "dn": {
      "uid": "smig",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "smig",
    "uidNumber": "2654",
    "ircNick": "smig",
    "sn": "Gava",
    "gidNumber": "2654"
  },
  {
    "info": "# smimram, users, debian.org",
    "dn": {
      "uid": "smimram",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Samuel",
    "keyFingerPrint": "52F6FEBCA7B91895F1B6D35A21A7B53B800969EF",
    "uid": "smimram",
    "sn": "Mimram",
    "labeledURI": "http://perso.ens-lyon.fr/samuel.mimram/",
    "uidNumber": "2680",
    "ircNick": "smimou",
    "gidNumber": "2680"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "smooge",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "smooge",
    "uidNumber": "945",
    "sn": "Smoogen",
    "gidNumber": "945"
  },
  {
    "info": "20091208] Resigned 2006-09-04",
    "dn": {
      "uid": "smp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sam",
    "uid": "smp",
    "uidNumber": "2344",
    "sn": "Powers",
    "ircNick": "spowers",
    "gidNumber": "2344"
  },
  {
    "info": "# smr, users, debian.org",
    "dn": {
      "uid": "smr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steven",
    "uid": "smr",
    "uidNumber": "2163",
    "sn": "Robbins",
    "keyFingerPrint": "CBCF64F1F6B7ADC94D8F2A24C9E55E2FADC8F4B9",
    "gidNumber": "2163"
  },
  {
    "info": "# smurf, users, debian.org",
    "dn": {
      "uid": "smurf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthias",
    "uid": "smurf",
    "sn": "Urlichs",
    "uidNumber": "2613",
    "keyFingerPrint": "AFD79782F3BAEC020B28A19F72CF8E5E25B4C293",
    "gidNumber": "2613",
    "ircNick": "smurfix",
    "labeledURI": "http://matthias.urlichs.de"
  },
  {
    "info": "# snd, users, debian.org",
    "dn": {
      "uid": "snd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "snd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3660",
    "gidNumber": "3660",
    "keyFingerPrint": "3CB7C302AFB57E61A8C617DD634EA55D902B9836",
    "cn": "Dennis",
    "sn": "Braun",
    "ircNick": "snd",
    "labeledURI": "https://y0o.de"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "soenke",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Soenke",
    "uid": "soenke",
    "uidNumber": "921",
    "sn": "Lange",
    "gidNumber": "921"
  },
  {
    "info": "20091208] Retired",
    "dn": {
      "uid": "solomon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jared",
    "uid": "solomon",
    "uidNumber": "2253",
    "sn": "Johnson",
    "gidNumber": "2253"
  },
  {
    "info": "# sonne, users, debian.org",
    "dn": {
      "uid": "sonne",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sonne",
    "objectClass": "debianDeveloper",
    "uidNumber": "2936",
    "cn": "Soeren",
    "sn": "Sonnenburg",
    "ircNick": "sonne",
    "jabberJID": "sonney2k@jabber.ccc.de",
    "labeledURI": "http://sonnenburgs.de/soeren",
    "gidNumber": "2936"
  },
  {
    "info": "# sophieb, users, debian.org",
    "dn": {
      "uid": "sophieb",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sophieb",
    "objectClass": "debianDeveloper",
    "uidNumber": "3634",
    "gidNumber": "3634",
    "keyFingerPrint": "3B21B8E68AE5C16F87F5322D5792783B206FEE30",
    "cn": "Sophie",
    "sn": "Brun"
  },
  {
    "info": "# sophiejjj, users, debian.org",
    "dn": {
      "uid": "sophiejjj",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sophiejjj",
    "objectClass": "debianDeveloper",
    "uidNumber": "3338",
    "keyFingerPrint": "85934C614F9A551FAD5694F64D89467027A75C80",
    "cn": "Jingjie",
    "sn": "Jiang",
    "gidNumber": "3338"
  },
  {
    "info": "# sopwith, users, debian.org",
    "dn": {
      "uid": "sopwith",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Elliot",
    "uid": "sopwith",
    "uidNumber": "1106",
    "sn": "Lee",
    "gidNumber": "1106"
  },
  {
    "info": "# sosborn, users, debian.org",
    "dn": {
      "uid": "sosborn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "uid": "sosborn",
    "uidNumber": "802",
    "sn": "Osborn",
    "gidNumber": "1681"
  },
  {
    "info": "# spa, users, debian.org",
    "dn": {
      "uid": "spa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Salvador",
    "uid": "spa",
    "sn": "Abreu",
    "uidNumber": "2401",
    "keyFingerPrint": "E3B9148FEB95F79876F52599A2B73E0DD4DDDC4E",
    "gidNumber": "2401",
    "ircNick": "Rodalvas",
    "labeledURI": "http://www.di.uevora.pt/~spa/"
  },
  {
    "info": "# spaillard, users, debian.org",
    "dn": {
      "uid": "spaillard",
      "ou": "users",
      "dc": "org"
    },
    "uid": "spaillard",
    "objectClass": "debianDeveloper",
    "uidNumber": "2993",
    "cn": "Simon",
    "sn": "Paillard",
    "ircNick": "symoon",
    "jabberJID": "symoon@jabber.org",
    "gidNumber": "2993"
  },
  {
    "info": "# spamaps, users, debian.org",
    "dn": {
      "uid": "spamaps",
      "ou": "users",
      "dc": "org"
    },
    "uid": "spamaps",
    "objectClass": "debianDeveloper",
    "uidNumber": "3194",
    "keyFingerPrint": "F00C9F2F4F7EB714AFAD1A42538C0766F4BCB38E",
    "cn": "Clint",
    "sn": "Byrum",
    "gidNumber": "3194",
    "ircNick": "SpamapS"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "spatka",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Petr",
    "uid": "spatka",
    "uidNumber": "1364",
    "sn": "Spatka",
    "gidNumber": "1364"
  },
  {
    "info": "# spectra, users, debian.org",
    "dn": {
      "uid": "spectra",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pablo",
    "uid": "spectra",
    "sn": "Lorenzzoni",
    "uidNumber": "2369",
    "keyFingerPrint": "20A741B470D74B2077D029E86C7BFF9E99746B65",
    "gidNumber": "2369",
    "ircNick": "spectra",
    "jabberJID": "spectra@jabber.org",
    "labeledURI": "http://lorenzzoni.pl/"
  },
  {
    "info": "20091208] Retired [2007-03-31 - JT]",
    "dn": {
      "uid": "speedblue",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Julien",
    "uid": "speedblue",
    "sn": "Lemoine",
    "labeledURI": "http://www.speedblue.org",
    "uidNumber": "2469",
    "ircNick": "speedblue",
    "gidNumber": "2469"
  },
  {
    "info": "# sph, users, debian.org",
    "dn": {
      "uid": "sph",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephan",
    "uid": "sph",
    "uidNumber": "2296",
    "sn": "Helma",
    "gidNumber": "2296"
  },
  {
    "info": "# spwhitton, users, debian.org",
    "dn": {
      "uid": "spwhitton",
      "ou": "users",
      "dc": "org"
    },
    "uid": "spwhitton",
    "objectClass": "debianDeveloper",
    "uidNumber": "3453",
    "gidNumber": "3453",
    "keyFingerPrint": "8DC2487E51ABDD90B5C4753F0F56D0553B6D411B",
    "cn": "Sean",
    "sn": "Whitton",
    "ircNick": "spwhitton",
    "jabberJID": "spwhitton@debian.org",
    "labeledURI": "https://spwhitton.name/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "sr1",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.sax.de/~sr1/",
    "cn": "Sven",
    "uid": "sr1",
    "uidNumber": "807",
    "sn": "Rudolph",
    "gidNumber": "1618"
  },
  {
    "info": "# sramacher, users, debian.org",
    "dn": {
      "uid": "sramacher",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sramacher",
    "objectClass": "debianDeveloper",
    "uidNumber": "3235",
    "keyFingerPrint": "F78CBA07817BB149A11D339069F2FC516EA71993",
    "cn": "Sebastian",
    "sn": "Ramacher",
    "gidNumber": "3235",
    "ircNick": "Sebastinas",
    "jabberJID": "sebastian@ramacher.at"
  },
  {
    "info": "# srbaker, users, debian.org",
    "dn": {
      "uid": "srbaker",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steven",
    "uid": "srbaker",
    "uidNumber": "1368",
    "sn": "Baker",
    "ircNick": "srbaker",
    "gidNumber": "1368"
  },
  {
    "info": "# sre, users, debian.org",
    "dn": {
      "uid": "sre",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sre",
    "objectClass": "debianDeveloper",
    "uidNumber": "3107",
    "keyFingerPrint": "EF660D07463F8B726A795413D8EED7F3C83BFA9A",
    "cn": "Sebastian",
    "sn": "Reichel",
    "gidNumber": "3107",
    "jabberJID": "sre@xmpp.ring0.de"
  },
  {
    "info": "# srittau, users, debian.org",
    "dn": {
      "uid": "srittau",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sebastian",
    "keyFingerPrint": "B4BC4D674EDE9802F9A74C17714E66B4496A1827",
    "uid": "srittau",
    "sn": "Rittau",
    "uidNumber": "2363",
    "ircNick": "jroger",
    "labeledURI": "http://www.rittau.org/",
    "gidNumber": "2363"
  },
  {
    "info": "# srivasta, users, debian.org",
    "dn": {
      "uid": "srivasta",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Manoj",
    "keyFingerPrint": "E37E5EC52A01DA25AD2005B6CF489438C5779A1C",
    "uid": "srivasta",
    "sn": "Srivastava",
    "uidNumber": "869",
    "gidNumber": "869",
    "ircNick": "Manoj",
    "jabberJID": "srivasta@jabber.org",
    "labeledURI": "http://www.golden-gryphon.com"
  },
  {
    "info": "# srk, users, debian.org",
    "dn": {
      "uid": "srk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Simon",
    "uid": "srk",
    "uidNumber": "2485",
    "sn": "Kelley",
    "keyFingerPrint": "D6EACBD6EE46B834248D111215CDDA6AE19135A2",
    "gidNumber": "2485"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "srua",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sergio",
    "uid": "srua",
    "uidNumber": "2050",
    "ircNick": "pattryn",
    "sn": "Rua",
    "gidNumber": "2050"
  },
  {
    "info": "# srud, users, debian.org",
    "dn": {
      "uid": "srud",
      "ou": "users",
      "dc": "org"
    },
    "uid": "srud",
    "objectClass": "debianDeveloper",
    "uidNumber": "3570",
    "gidNumber": "3570",
    "cn": "Sruthi",
    "sn": "Chandran",
    "keyFingerPrint": "71DDC5C6BE869A4641D779FEC7EA1BE1574DED5D",
    "ircNick": "srud"
  },
  {
    "info": "# ssgelm, users, debian.org",
    "dn": {
      "uid": "ssgelm",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ssgelm",
    "objectClass": "debianDeveloper",
    "uidNumber": "3541",
    "gidNumber": "3541",
    "keyFingerPrint": "0BBEEEF064E94ABE843258B47541CFAAFC35EACF",
    "cn": "Stephen",
    "sn": "Gelman",
    "ircNick": "ssgelm"
  },
  {
    "info": "# ssm, users, debian.org",
    "dn": {
      "uid": "ssm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stig",
    "uid": "ssm",
    "uidNumber": "1157",
    "sn": "Mathisen",
    "ircNick": "ssm",
    "labeledURI": "http://fnord.no",
    "keyFingerPrint": "799E67EEE22748031E05A2DC7DBA958C1C055538",
    "gidNumber": "1157"
  },
  {
    "info": "# ssmeenk, users, debian.org",
    "dn": {
      "uid": "ssmeenk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sander",
    "uid": "ssmeenk",
    "sn": "Smeenk",
    "uidNumber": "2114",
    "ircNick": "Fluor",
    "labeledURI": "http://www.freshdot.net/",
    "gidNumber": "2114"
  },
  {
    "info": "# sstrick, users, debian.org",
    "dn": {
      "uid": "sstrick",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stevie",
    "uid": "sstrick",
    "sn": "Strickland",
    "uidNumber": "1332",
    "ircNick": "Cyberlink",
    "jabberJID": "sstrickl@gmail.com",
    "labeledURI": "http://www.ccs.neu.edu/home/sstrickl",
    "gidNumber": "1332"
  },
  {
    "info": "# stapelberg, users, debian.org",
    "dn": {
      "uid": "stapelberg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stapelberg",
    "objectClass": "debianDeveloper",
    "uidNumber": "3189",
    "keyFingerPrint": "424E14D703E7C6D43D9D6F364E7160ED4AC8EE1D",
    "cn": "Michael",
    "sn": "Stapelberg",
    "gidNumber": "3189"
  },
  {
    "info": "# stappers, users, debian.org",
    "dn": {
      "uid": "stappers",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Geert",
    "uid": "stappers",
    "uidNumber": "2681",
    "sn": "Stappers",
    "keyFingerPrint": "8A7F208C6D9E73291657414D2135D123D8C19BEC",
    "gidNumber": "2681",
    "ircNick": "stappers",
    "labeledURI": "http://www.stappers.nl/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "stark",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gregory",
    "uid": "stark",
    "uidNumber": "1171",
    "sn": "Stark",
    "gidNumber": "1171"
  },
  {
    "info": "# steele, users, debian.org",
    "dn": {
      "uid": "steele",
      "ou": "users",
      "dc": "org"
    },
    "uid": "steele",
    "objectClass": "debianDeveloper",
    "uidNumber": "3412",
    "gidNumber": "3412",
    "keyFingerPrint": "AE0DBF5A92A5ADE49481BA6F8A3171EF366150CE",
    "cn": "David",
    "sn": "Steele",
    "ircNick": "steele"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "stefan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "stefan",
    "uidNumber": "1237",
    "sn": "Berndtsson",
    "gidNumber": "1680"
  },
  {
    "info": "# stefanb, users, debian.org",
    "dn": {
      "uid": "stefanb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefan",
    "uid": "stefanb",
    "uidNumber": "1204",
    "sn": "Bjornelund",
    "gidNumber": "1686"
  },
  {
    "info": "tp-ed25519.stefanor.user._domainkey.debian.org.",
    "dn": {
      "uid": "stefanor",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stefanor",
    "objectClass": "debianDeveloper",
    "uidNumber": "3120",
    "cn": "Stefano",
    "sn": "Rivera",
    "gidNumber": "3120",
    "keyFingerPrint": "F2FAAC0D44C32D8B98539B9297A0FA0FC8F2DE45",
    "ircNick": "tumbleweed",
    "jabberJID": "stefano@rivera.za.net",
    "labeledURI": "http://tumbleweed.org.za/"
  },
  {
    "info": "# steinm, users, debian.org",
    "dn": {
      "uid": "steinm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Uwe",
    "uid": "steinm",
    "uidNumber": "2649",
    "sn": "Steinmann",
    "keyFingerPrint": "DC2BE5A635D6DCF094A1BD3037C2B4F8642139FF",
    "gidNumber": "2649",
    "jabberJID": "uwe@steinmann.cx"
  },
  {
    "info": "# sten, users, debian.org",
    "dn": {
      "uid": "sten",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sten",
    "objectClass": "debianDeveloper",
    "uidNumber": "3617",
    "gidNumber": "3617",
    "keyFingerPrint": "E2A6261E3900AED7CDC667085A8830475F7D1061",
    "cn": "Nicholas",
    "sn": "Steeves",
    "ircNick": "sten0"
  },
  {
    "info": "# stender, users, debian.org",
    "dn": {
      "uid": "stender",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stender",
    "objectClass": "debianDeveloper",
    "uidNumber": "3400",
    "gidNumber": "3400",
    "cn": "Daniel",
    "sn": "Stender",
    "labeledURI": "https://danielstender.com"
  },
  {
    "info": "# stephanlachnit, users, debian.org",
    "dn": {
      "uid": "stephanlachnit",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stephanlachnit",
    "objectClass": "debianDeveloper",
    "uidNumber": "3623",
    "gidNumber": "3623",
    "keyFingerPrint": "BB45B0B3FF561BDBD45EE8A9B35B49EA5D563EFE",
    "cn": "Stephan",
    "sn": "Lachnit",
    "ircNick": "stephanlachnit"
  },
  {
    "info": "# stephe, users, debian.org",
    "dn": {
      "uid": "stephe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stephe",
    "objectClass": "debianDeveloper",
    "uidNumber": "3155",
    "keyFingerPrint": "5B8ADB53B6B308B4CF6CF46CECC1D5F5A1FC6CFA",
    "cn": "Stephen",
    "sn": "Leake",
    "gidNumber": "3155"
  },
  {
    "info": "# stephen, users, debian.org",
    "dn": {
      "uid": "stephen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "stephen",
    "sn": "Moraco",
    "uidNumber": "1320",
    "ircNick": "atc",
    "labeledURI": "http://www.debian.org/~stephen",
    "gidNumber": "1320"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "stetzer",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Olaf",
    "uid": "stetzer",
    "uidNumber": "1354",
    "sn": "Stetzer",
    "gidNumber": "1354"
  },
  {
    "info": "# steve, users, debian.org",
    "dn": {
      "uid": "steve",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "uid": "steve",
    "sn": "Kostecke",
    "uidNumber": "1160",
    "ircNick": "[steve]",
    "labeledURI": "http://kostecke.net:70",
    "keyFingerPrint": "187DAC2552E81F4C2F335B46420A4295E9DBDDBC",
    "gidNumber": "1160"
  },
  {
    "info": "org>",
    "dn": {
      "uid": "stevegr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.moregruel.net/",
    "cn": "Steve",
    "uid": "stevegr",
    "uidNumber": "870",
    "sn": "Greenland",
    "gidNumber": "870"
  },
  {
    "info": "# stevenc, users, debian.org",
    "dn": {
      "uid": "stevenc",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stevenc",
    "objectClass": "debianDeveloper",
    "uidNumber": "3349",
    "keyFingerPrint": "DF4717BA98C27F299BA7183369F30763DFEFC134",
    "cn": "Steven",
    "sn": "Chamberlain",
    "gidNumber": "3349"
  },
  {
    "info": "# stevenk, users, debian.org",
    "dn": {
      "uid": "stevenk",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "uid": "stevenk",
    "sn": "Kowalik",
    "uidNumber": "2376",
    "keyFingerPrint": "65574BDD9F3C76D0808CBF36FDD63BAF588A553F",
    "gidNumber": "2376",
    "ircNick": "StevenK",
    "labeledURI": "http://people.debian.org/~stevenk"
  },
  {
    "info": "# stew, users, debian.org",
    "dn": {
      "uid": "stew",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stew",
    "objectClass": "debianDeveloper",
    "uidNumber": "2914",
    "cn": "Mike",
    "sn": "O'Connor",
    "ircNick": "stew",
    "gidNumber": "2914"
  },
  {
    "info": "=",
    "dn": {
      "uid": "sthibault",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sthibault",
    "objectClass": "debianDeveloper",
    "uidNumber": "2976",
    "cn": "Samuel",
    "sn": "Thibault",
    "keyFingerPrint": "900CB024B67931D40F82304BD0178C767D069EE6",
    "gidNumber": "2976",
    "ircNick": "youpi",
    "jabberJID": "youpi@jabber.org",
    "labeledURI": "http://dept-info.labri.fr/~thibault/"
  },
  {
    "info": "# stigge, users, debian.org",
    "dn": {
      "uid": "stigge",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roland",
    "uid": "stigge",
    "sn": "Stigge",
    "uidNumber": "2604",
    "labeledURI": "http://www.rolandstigge.de/",
    "gidNumber": "2604"
  },
  {
    "info": "# stijn, users, debian.org",
    "dn": {
      "uid": "stijn",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stijn",
    "uid": "stijn",
    "sn": "de Bekker",
    "uidNumber": "2189",
    "ircNick": "stijn",
    "labeledURI": "http://stijn.debekker.org",
    "gidNumber": "2189"
  },
  {
    "info": "# sto, users, debian.org",
    "dn": {
      "uid": "sto",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sergio",
    "uid": "sto",
    "sn": "Talens-Oliag",
    "uidNumber": "2320",
    "keyFingerPrint": "FA908E471AD37D7F2363D78F821AEE0FD167FBDF",
    "gidNumber": "2320",
    "ircNick": "sto",
    "labeledURI": "http://people.debian.org/~sto/"
  },
  {
    "info": "# stone, users, debian.org",
    "dn": {
      "uid": "stone",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fredrik",
    "uid": "stone",
    "sn": "Steen",
    "uidNumber": "2242",
    "gidNumber": "2242",
    "ircNick": "stone",
    "jabberJID": "stone4x4@gmail.com"
  },
  {
    "info": "# storm, users, debian.org",
    "dn": {
      "uid": "storm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.tux.org/~storm/",
    "cn": "Bradley",
    "uid": "storm",
    "uidNumber": "2165",
    "ircNick": {},
    "sn": "Alexander",
    "gidNumber": "2165"
  },
  {
    "info": "# stramiello, users, debian.org",
    "dn": {
      "uid": "stramiello",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Warren",
    "uid": "stramiello",
    "uidNumber": "2343",
    "ircNick": "Uebergeek",
    "sn": "Stramiello",
    "gidNumber": "2343"
  },
  {
    "info": "# stratus, users, debian.org",
    "dn": {
      "uid": "stratus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Gustavo",
    "keyFingerPrint": "190852B94A166EC274D1C03BEDFB700537155778",
    "uid": "stratus",
    "uidNumber": "2686",
    "sn": "Franco",
    "ircNick": "stratus",
    "gidNumber": "2686"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "streeter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Carl",
    "uid": "streeter",
    "uidNumber": "806",
    "sn": "Streeter",
    "gidNumber": "1701"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "strombrg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dan",
    "uid": "strombrg",
    "uidNumber": "995",
    "sn": "Stromberg",
    "gidNumber": "995"
  },
  {
    "info": "# stroucki, users, debian.org",
    "dn": {
      "uid": "stroucki",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michael",
    "uid": "stroucki",
    "uidNumber": "1315",
    "sn": "Stroucken",
    "jabberJID": "stroucki@club.cc.cmu.edu",
    "gidNumber": "1315"
  },
  {
    "info": "# stuart, users, debian.org",
    "dn": {
      "uid": "stuart",
      "ou": "users",
      "dc": "org"
    },
    "uid": "stuart",
    "objectClass": "debianDeveloper",
    "uidNumber": "3208",
    "cn": "Stuart",
    "sn": "Prescott",
    "keyFingerPrint": "90E2D2C1AD146A1B7EBB891DBBC17EBB1396F2F7",
    "gidNumber": "3208",
    "ircNick": "themill"
  },
  {
    "info": "# sudip, users, debian.org",
    "dn": {
      "uid": "sudip",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sudip",
    "objectClass": "debianDeveloper",
    "uidNumber": "3586",
    "gidNumber": "3586",
    "keyFingerPrint": "B8340990283D8D9BC1949AC74799A35146D12B35",
    "cn": "Sudip",
    "sn": "Mukherjee",
    "ircNick": "sudip"
  },
  {
    "info": "iB0aGUgMjIvMDcgdXBsb2FkLiAgKFJUIzE2NjQp",
    "dn": {
      "uid": "sukria",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sukria",
    "objectClass": "debianDeveloper",
    "uidNumber": "2811",
    "cn": "Alexis",
    "sn": "Sukrieh",
    "ircNick": "sukria",
    "jabberJID": "dasukria@jabber.org",
    "labeledURI": "http://www.sukria.net/en",
    "gidNumber": "2811"
  },
  {
    "info": "# sune, users, debian.org",
    "dn": {
      "uid": "sune",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sune",
    "objectClass": "debianDeveloper",
    "uidNumber": "2886",
    "cn": "Sune",
    "sn": "Vuorela",
    "keyFingerPrint": "8514067B1F1F5DF4E79DE0801A30765DF1F0D3ED",
    "gidNumber": "2886"
  },
  {
    "info": "# sunweaver, users, debian.org",
    "dn": {
      "uid": "sunweaver",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sunweaver",
    "objectClass": "debianDeveloper",
    "uidNumber": "3246",
    "keyFingerPrint": "9BFBAEE86C0AA5FFBF2207829AF46B3025771B31",
    "cn": "Mike",
    "sn": "Gabriel",
    "gidNumber": "3246",
    "ircNick": "sunweaver",
    "jabberJID": "mike.gabriel@jabber.ccc.de",
    "labeledURI": "http://sunweavers.net"
  },
  {
    "info": "# sur5r, users, debian.org",
    "dn": {
      "uid": "sur5r",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sur5r",
    "objectClass": "debianDeveloper",
    "uidNumber": "3567",
    "gidNumber": "3567",
    "keyFingerPrint": "7BF5F6AC36431F5D40DC137A4CF2B218F54DAE3D",
    "cn": "Jakob",
    "sn": "Haufe",
    "ircNick": "sur5r",
    "jabberJID": "sur5r@sur5r.net"
  },
  {
    "info": "# susumuo, users, debian.org",
    "dn": {
      "uid": "susumuo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Susumu",
    "uid": "susumuo",
    "uidNumber": "1366",
    "sn": "Osawa",
    "ircNick": "susumuo",
    "gidNumber": "1366"
  },
  {
    "info": "# sven, users, debian.org",
    "dn": {
      "uid": "sven",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sven",
    "objectClass": "debianDeveloper",
    "uidNumber": "2787",
    "cn": "Sven",
    "sn": "Mueller",
    "keyFingerPrint": "0101241DE4C8565F3D8B3B19478112D54B1AC295",
    "gidNumber": "2787",
    "ircNick": "incase",
    "labeledURI": "http://incase.de"
  },
  {
    "info": "91208]",
    "dn": {
      "uid": "swan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.shockfrosted.org",
    "cn": "Stefan",
    "uid": "swan",
    "uidNumber": "2525",
    "ircNick": "krogalon",
    "sn": "Schwandter",
    "gidNumber": "2525"
  },
  {
    "info": "# sweeks, users, debian.org",
    "dn": {
      "uid": "sweeks",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stephen",
    "uid": "sweeks",
    "uidNumber": "2599",
    "sn": "Weeks",
    "labeledURI": "http://sweeks.com",
    "gidNumber": "2599"
  },
  {
    "info": "# swt2c, users, debian.org",
    "dn": {
      "uid": "swt2c",
      "ou": "users",
      "dc": "org"
    },
    "uid": "swt2c",
    "objectClass": "debianDeveloper",
    "uidNumber": "3590",
    "gidNumber": "3590",
    "keyFingerPrint": "6E7434F5897D43B17FCD57B753D5BC64B52378A2",
    "cn": "Scott",
    "sn": "Talbert"
  },
  {
    "info": "# sylvestre, users, debian.org",
    "dn": {
      "uid": "sylvestre",
      "ou": "users",
      "dc": "org"
    },
    "uid": "sylvestre",
    "objectClass": "debianDeveloper",
    "uidNumber": "2991",
    "cn": "Sylvestre",
    "sn": "Ledru",
    "keyFingerPrint": "B60DB5994D39BEC4D1A95CCF7E6528DA752F1BE1",
    "gidNumber": "2991",
    "ircNick": "sylvestre",
    "labeledURI": "https://sylvestre.ledru.info/"
  },
  {
    "info": "# synrg, users, debian.org",
    "dn": {
      "uid": "synrg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ben",
    "uid": "synrg",
    "sn": "Armstrong",
    "uidNumber": "1102",
    "gidNumber": "1102",
    "keyFingerPrint": "5887C8D4012BF84A53E5F9DA4330169952D556DB",
    "ircNick": "SynrG",
    "jabberJID": "synrg@jabber.org",
    "labeledURI": "http://syn.theti.ca"
  },
  {
    "info": "# syq, users, debian.org",
    "dn": {
      "uid": "syq",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3313",
    "keyFingerPrint": "816790FE0A75677E2A6C22C814135D277B88D7E5",
    "sn": "Su",
    "cn": "YunQiang",
    "uid": "syq",
    "gidNumber": "3313",
    "ircNick": "syq"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "sysv",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Sylvain",
    "uid": "sysv",
    "uidNumber": "2325",
    "ircNick": "sysv",
    "sn": "de Crom",
    "gidNumber": "2325"
  },
  {
    "info": "# szlin, users, debian.org",
    "dn": {
      "uid": "szlin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "szlin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3465",
    "gidNumber": "3465",
    "keyFingerPrint": "178F8338B31401E304FC44BAA959B38A9561F3F9",
    "cn": "SZ",
    "sn": "Lin",
    "ircNick": "szlin",
    "labeledURI": "https://szlin.me/"
  },
  {
    "info": "# tach, users, debian.org",
    "dn": {
      "uid": "tach",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Taku",
    "keyFingerPrint": "94769E52D436D58B260CA7DD40D716CFE909CDEE",
    "uid": "tach",
    "uidNumber": "2362",
    "sn": "Yasui",
    "gidNumber": "2362",
    "ircNick": "gtach",
    "labeledURI": "https://tach.arege.net/"
  },
  {
    "info": "# tachi, users, debian.org",
    "dn": {
      "uid": "tachi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tachi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3661",
    "gidNumber": "3661",
    "keyFingerPrint": "66DEF15282990C2199EFA801A8A128A8AB1CEE49",
    "cn": "Andrea",
    "sn": "Pappacoda",
    "ircNick": "tachi",
    "labeledURI": "https://andrea.pappacoda.it"
  },
  {
    "info": "++7R7ZJ0g4PI8eR3FbQTil70NcCSIP6wIDAQAB",
    "dn": {
      "uid": "taffit",
      "ou": "users",
      "dc": "org"
    },
    "uid": "taffit",
    "objectClass": "debianDeveloper",
    "uidNumber": "3121",
    "keyFingerPrint": "AE14AD01426D2BFB82EF7E1EB82A217AFDFE09F2",
    "cn": "David",
    "gidNumber": "3121",
    "sn": ": UHLDqXZvdA==",
    "ircNick": "taffit",
    "jabberJID": "taffit@im.apinc.org"
  },
  {
    "info": "# taggart, users, debian.org",
    "dn": {
      "uid": "taggart",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matt",
    "uid": "taggart",
    "sn": "Taggart",
    "uidNumber": "2372",
    "ircNick": "taggart",
    "keyFingerPrint": "00E7CB9E10210383D2FD2459AA68ECC8E9800953",
    "gidNumber": "2372"
  },
  {
    "info": "# tagoh, users, debian.org",
    "dn": {
      "uid": "tagoh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Akira",
    "uid": "tagoh",
    "uidNumber": "2409",
    "sn": "TAGOH",
    "ircNick": "tagoh",
    "labeledURI": "http://tagoh.jp/",
    "gidNumber": "2409"
  },
  {
    "info": "# takaki, users, debian.org",
    "dn": {
      "uid": "takaki",
      "ou": "users",
      "dc": "org"
    },
    "uid": "takaki",
    "objectClass": "debianDeveloper",
    "uidNumber": "3030",
    "cn": "Takaki",
    "sn": "Taniguchi",
    "keyFingerPrint": "D24ABFD127CD2556A13C6C79CC149F6C28A5E257",
    "gidNumber": "3030",
    "labeledURI": "http://takaki-web.media-as.org/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "take",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yasuhiro",
    "uid": "take",
    "uidNumber": "2079",
    "ircNick": "hirot",
    "sn": "Take",
    "gidNumber": "2079"
  },
  {
    "info": "# talau, users, debian.org",
    "dn": {
      "uid": "talau",
      "ou": "users",
      "dc": "org"
    },
    "uid": "talau",
    "objectClass": "debianDeveloper",
    "uidNumber": "3652",
    "gidNumber": "3652",
    "keyFingerPrint": "B522F39159DA07DD39DC0B11F4BAAA80DB28BA4C",
    "cn": "Marcos",
    "sn": "Talau",
    "ircNick": "talau",
    "labeledURI": "https://people.debian.org/~talau"
  },
  {
    "info": "# talby, users, debian.org",
    "dn": {
      "uid": "talby",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Robert",
    "uid": "talby",
    "sn": "Stone",
    "uidNumber": "1130",
    "ircNick": "talby",
    "labeledURI": "http://trap.mtview.ca.us/~talby/",
    "gidNumber": "1654"
  },
  {
    "info": "# tale, users, debian.org",
    "dn": {
      "uid": "tale",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tapio",
    "uid": "tale",
    "sn": "Lehtonen",
    "uidNumber": "1376",
    "gidNumber": "1376",
    "ircNick": "tale",
    "labeledURI": "http://www.iki.fi/tapio.lehtonen"
  },
  {
    "info": "# tali, users, debian.org",
    "dn": {
      "uid": "tali",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "tali",
    "sn": "Waitz",
    "uidNumber": "2346",
    "ircNick": "Tali",
    "jabberJID": "martin.waitz@googletalk.com",
    "labeledURI": "http://tali.admingilde.org",
    "gidNumber": "2346"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "talon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "talon",
    "uidNumber": "1334",
    "ircNick": "talon",
    "sn": "Ermovick",
    "gidNumber": "1334"
  },
  {
    "info": "# tamiko, users, debian.org",
    "dn": {
      "uid": "tamiko",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tamiko",
    "objectClass": "debianDeveloper",
    "uidNumber": "3305",
    "keyFingerPrint": "1EE97BF66221FC3069B5CF90B5A4E06FBD3A97A3",
    "cn": "Matthias",
    "sn": "Maier",
    "gidNumber": "3305"
  },
  {
    "info": "# tanguy, users, debian.org",
    "dn": {
      "uid": "tanguy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tanguy",
    "objectClass": "debianDeveloper",
    "uidNumber": "3170",
    "keyFingerPrint": "240BBA15B694DD00E38030D8D6EFA6AC4B10D847",
    "cn": "Tanguy",
    "sn": "Ortolo",
    "gidNumber": "3170",
    "ircNick": "Tanguy",
    "jabberJID": "tanguy@ortolo.eu",
    "labeledURI": "http://tanguy.ortolo.eu/"
  },
  {
    "info": "# tannoiser, users, debian.org",
    "dn": {
      "uid": "tannoiser",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tannoiser",
    "objectClass": "debianDeveloper",
    "uidNumber": "2852",
    "cn": "Maurizio",
    "sn": "Lemmo",
    "jabberJID": "Tannoiser@jabber.linux.it",
    "gidNumber": "2852"
  },
  {
    "info": "# tao, users, debian.org",
    "dn": {
      "uid": "tao",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "tao",
    "uidNumber": "2558",
    "sn": "Weinehall",
    "keyFingerPrint": "ED698CF5610221B9AFD19DB70B2796FD15DAA404",
    "gidNumber": "2558",
    "ircNick": "Tao"
  },
  {
    "info": "# taowa, users, debian.org",
    "dn": {
      "uid": "taowa",
      "ou": "users",
      "dc": "org"
    },
    "uid": "taowa",
    "objectClass": "debianDeveloper",
    "uidNumber": "3563",
    "gidNumber": "3563",
    "sn": "Taowa",
    "keyFingerPrint": "6CF439E66C1DA71247E6196FE744197CEAAF75CA",
    "cn": "-",
    "ircNick": "taowa",
    "jabberJID": "taowa@rosenzweig.io",
    "labeledURI": "https://people.debian.org/~taowa"
  },
  {
    "info": "# tar, users, debian.org",
    "dn": {
      "uid": "tar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3637",
    "gidNumber": "3637",
    "keyFingerPrint": "B60A1BF363DC1319FF0A8E89116852BCDF7515C0",
    "cn": ": R8O8cmthbg==",
    "sn": "Myczko",
    "ircNick": "tarzeau"
  },
  {
    "info": "91208]",
    "dn": {
      "uid": "taral",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jean-Philippe",
    "uid": "taral",
    "sn": "Sugarbroad",
    "labeledURI": "http://www.taral.net/",
    "uidNumber": "2284",
    "ircNick": "Taral",
    "gidNumber": "2284"
  },
  {
    "info": "# taru, users, debian.org",
    "dn": {
      "uid": "taru",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Masato",
    "uid": "taru",
    "uidNumber": "1254",
    "ircNick": "taru",
    "sn": "Taruishi",
    "gidNumber": "1624"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "tas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "tas",
    "uidNumber": "1380",
    "sn": "Schlegel",
    "gidNumber": "1380"
  },
  {
    "info": "# tassia, users, debian.org",
    "dn": {
      "uid": "tassia",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tassia",
    "objectClass": "debianDeveloper",
    "uidNumber": "3335",
    "keyFingerPrint": "EE8A7D484BF40152AB1F3708F5B8B2E02AF31308",
    "gidNumber": "3335",
    "cn": ": VMOhc3NpYQ==",
    "sn": ": Q2Ftw7VlcyBBcmHDumpv",
    "ircNick": "tassia"
  },
  {
    "info": "# tats, users, debian.org",
    "dn": {
      "uid": "tats",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tats",
    "objectClass": "debianDeveloper",
    "uidNumber": "2768",
    "cn": "Tatsuya",
    "sn": "Kinoshita",
    "keyFingerPrint": "031C623D94EF1F3EF17B1C84E5EFAB90080EA63C",
    "gidNumber": "2768"
  },
  {
    "info": "# tausq, users, debian.org",
    "dn": {
      "uid": "tausq",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Randolph",
    "uid": "tausq",
    "sn": "Chung",
    "uidNumber": "1341",
    "ircNick": "tausq",
    "labeledURI": "http://www.tausq.org/",
    "gidNumber": "1341"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "taylor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Taylor",
    "uid": "taylor",
    "uidNumber": "2137",
    "ircNick": "ixx",
    "sn": "Carpenter",
    "gidNumber": "2137"
  },
  {
    "info": "# tb, users, debian.org",
    "dn": {
      "uid": "tb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "tb",
    "uidNumber": "2064",
    "sn": "Bushnell",
    "gidNumber": "2064",
    "labeledURI": "http://www.mit.edu/~tb"
  },
  {
    "info": "# tbarreno, users, debian.org",
    "dn": {
      "uid": "tbarreno",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tinguaro",
    "uid": "tbarreno",
    "sn": "Delgado",
    "uidNumber": "1259",
    "gidNumber": "1704"
  },
  {
    "info": "# tbm, users, debian.org",
    "dn": {
      "uid": "tbm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "tbm",
    "sn": "Michlmayr",
    "uidNumber": "2087",
    "keyFingerPrint": "D866749DF7FCD903C32D01893D0787AAAF6C61DD",
    "gidNumber": "2087",
    "ircNick": "tbm",
    "labeledURI": "http://www.cyrius.com/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "tbourrillon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thierry",
    "uid": "tbourrillon",
    "uidNumber": "2321",
    "sn": "Bourrillon",
    "gidNumber": "2321"
  },
  {
    "info": "# tca, users, debian.org",
    "dn": {
      "uid": "tca",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tom",
    "uid": "tca",
    "sn": "Amundsen",
    "uidNumber": "2106",
    "ircNick": "tca",
    "gidNumber": "2106"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "tdale",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dale",
    "uid": "tdale",
    "uidNumber": "1119",
    "sn": "Thompson",
    "gidNumber": "1119"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "tedhajek",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ted",
    "uid": "tedhajek",
    "uidNumber": "871",
    "sn": "Hajek",
    "gidNumber": "871"
  },
  {
    "info": "# teh, users, debian.org",
    "dn": {
      "uid": "teh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "teh",
    "objectClass": "debianDeveloper",
    "uidNumber": "3461",
    "gidNumber": "3461",
    "keyFingerPrint": "67E7117DB4BC68DF4402FAB37D9F29BAE9DAC43A",
    "cn": "Tom",
    "sn": "Hill"
  },
  {
    "info": "# tehnick, users, debian.org",
    "dn": {
      "uid": "tehnick",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tehnick",
    "objectClass": "debianDeveloper",
    "uidNumber": "3256",
    "keyFingerPrint": "EF8359781239A9194D2D7F59D0015BD128F39278",
    "cn": "Boris",
    "sn": "Pek",
    "gidNumber": "3256",
    "jabberJID": "tehnick-8@jabber.ru",
    "labeledURI": "https://wiki.debian.org/BorisPek"
  },
  {
    "info": "20091208] resigned in 3A8D1756.75277CFC@telmer.com to keyring-maint@",
    "dn": {
      "uid": "telmerco",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Colin",
    "uid": "telmerco",
    "uidNumber": "1026",
    "sn": "Telmer",
    "gidNumber": "1026"
  },
  {
    "info": "# templin, users, debian.org",
    "dn": {
      "uid": "templin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Pete",
    "uid": "templin",
    "uidNumber": "1005",
    "sn": "Templin",
    "gidNumber": "1005"
  },
  {
    "info": "# teo, users, debian.org",
    "dn": {
      "uid": "teo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "teo",
    "uidNumber": "2585",
    "ircNick": "teo",
    "labeledURI": "http://www.teoruiz.com",
    "gidNumber": "2585",
    "cn": ": VGXDs2ZpbG8=",
    "sn": ": UnVpeiBTdcOhcmV6"
  },
  {
    "info": "# terceiro, users, debian.org",
    "dn": {
      "uid": "terceiro",
      "ou": "users",
      "dc": "org"
    },
    "uid": "terceiro",
    "objectClass": "debianDeveloper",
    "uidNumber": "3142",
    "cn": "Antonio",
    "sn": "Terceiro",
    "keyFingerPrint": "B2DEE66036C40829FCD0F10CFC0DB1BBCD460BDE",
    "gidNumber": "3142",
    "ircNick": "terceiro",
    "labeledURI": "https://terceiro.xyz"
  },
  {
    "info": "# terminus, users, debian.org",
    "dn": {
      "uid": "terminus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jeremy",
    "uid": "terminus",
    "sn": "Malcolm",
    "uidNumber": "2457",
    "ircNick": "terminus",
    "jabberJID": "Jeremy@Malcolm.id.au",
    "labeledURI": "http://jere.my",
    "gidNumber": "2457"
  },
  {
    "info": "# terpstra, users, debian.org",
    "dn": {
      "uid": "terpstra",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wesley",
    "uid": "terpstra",
    "sn": "Terpstra",
    "uidNumber": "2461",
    "gidNumber": "2461",
    "ircNick": "terpstra",
    "keyFingerPrint": "B68CD164CB8A86399F1D150BE6475D030A9BF164"
  },
  {
    "info": "# terry, users, debian.org",
    "dn": {
      "uid": "terry",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Terry",
    "uid": "terry",
    "sn": "Dawson",
    "uidNumber": "1120",
    "ircNick": "LabRat",
    "labeledURI": "http://www.animats.net/~tjd/",
    "gidNumber": "1120"
  },
  {
    "info": "# teusbenschop, users, debian.org",
    "dn": {
      "uid": "teusbenschop",
      "ou": "users",
      "dc": "org"
    },
    "uid": "teusbenschop",
    "objectClass": "debianDeveloper",
    "uidNumber": "3581",
    "gidNumber": "3581",
    "keyFingerPrint": "85EAFEE5CF2CD500736BD8DE93022BAD0563A51D",
    "cn": "Teus",
    "sn": "Benschop"
  },
  {
    "info": "20091208] retired 2006-08-01",
    "dn": {
      "uid": "tew",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.enac.northwestern.edu/~tew/",
    "cn": "Ted",
    "uid": "tew",
    "uidNumber": "1179",
    "ircNick": "tew",
    "sn": "Whalen",
    "gidNumber": "1605"
  },
  {
    "info": "# tfheen, users, debian.org",
    "dn": {
      "uid": "tfheen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tollef",
    "keyFingerPrint": "A28411A596193171331802C0B65A4871CA19D717",
    "uid": "tfheen",
    "uidNumber": "2233",
    "sn": "Fog Heen",
    "gidNumber": "2233",
    "ircNick": "Mithrandir",
    "jabberJID": "tfheen@err.no"
  },
  {
    "info": "# tg, users, debian.org",
    "dn": {
      "uid": "tg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tg",
    "objectClass": "debianDeveloper",
    "uidNumber": "2968",
    "cn": "Thorsten",
    "sn": "Glaser",
    "keyFingerPrint": "9031955E7A97A4FDA32B2B8676B534B2E99007E0",
    "gidNumber": "2968",
    "ircNick": "mirabilos",
    "labeledURI": "http://www.mirbsd.org/"
  },
  {
    "info": "# tgg, users, debian.org",
    "dn": {
      "uid": "tgg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tgg",
    "objectClass": "debianDeveloper",
    "uidNumber": "2849",
    "cn": "Thomas",
    "sn": "Girard",
    "ircNick": "tomtomnana",
    "keyFingerPrint": "3C08DE837B5EFA2461417F5B16A588942D510B52",
    "gidNumber": "2849"
  },
  {
    "info": "# tgs, users, debian.org",
    "dn": {
      "uid": "tgs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "tgs",
    "uidNumber": "2292",
    "sn": "Smith",
    "ircNick": "tgs",
    "jabberJID": "thomathom@gmail.com",
    "labeledURI": "http://finbar.dyndns.org/",
    "gidNumber": "2292"
  },
  {
    "info": "# thansen, users, debian.org",
    "dn": {
      "uid": "thansen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thansen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3221",
    "keyFingerPrint": "A07E3AA25DCCDAEDA6628D248C82169D8ECECD2A",
    "cn": "Tobias",
    "sn": "Hansen",
    "gidNumber": "3221"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "thaths",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.aunet.org/thaths/",
    "cn": "Sudhakar",
    "uid": "thaths",
    "uidNumber": "1184",
    "ircNick": "thaths",
    "sn": "Chandrasekharan",
    "gidNumber": "1677"
  },
  {
    "info": "# thb, users, debian.org",
    "dn": {
      "uid": "thb",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thaddeus",
    "uid": "thb",
    "sn": "Black",
    "uidNumber": "2726",
    "keyFingerPrint": "33500F0D4FA9C0C867B85E06706326432D8522BC",
    "gidNumber": "2726",
    "ircNick": "THBlack",
    "labeledURI": "http://www.derivations.org/"
  },
  {
    "info": "# thep, users, debian.org",
    "dn": {
      "uid": "thep",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thep",
    "objectClass": "debianDeveloper",
    "uidNumber": "2996",
    "cn": "Theppitak",
    "sn": "Karoonboonyanan",
    "keyFingerPrint": "32CC490E5A70B23DD6AA5AB2A2EBAED1B6F90241",
    "ircNick": "thep",
    "jabberJID": "theppitak@gmail.com",
    "labeledURI": "http://linux.thai.net/~thep/",
    "gidNumber": "2996"
  },
  {
    "info": "# thibaut, users, debian.org",
    "dn": {
      "uid": "thibaut",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thibaut",
    "objectClass": "debianDeveloper",
    "uidNumber": "3198",
    "cn": "Thibaut",
    "sn": "Paumard",
    "keyFingerPrint": "BCADFB52B41998D74D99D98E93945348E0DC2840",
    "jabberJID": "lot@jabber.fr",
    "gidNumber": "3198"
  },
  {
    "info": "# thierry, users, debian.org",
    "dn": {
      "uid": "thierry",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thierry",
    "objectClass": "debianDeveloper",
    "uidNumber": "3070",
    "keyFingerPrint": "C643C2338B7D7A1742877D03F892981B6258629A",
    "cn": "Thierry",
    "sn": "Randrianiriana",
    "ircNick": "thierry",
    "gidNumber": "3070"
  },
  {
    "info": "# thijs, users, debian.org",
    "dn": {
      "uid": "thijs",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thijs",
    "objectClass": "debianDeveloper",
    "uidNumber": "2802",
    "keyFingerPrint": "E0D3FAAA6F50A5DA9D5B293833961588E1C21845",
    "cn": "Thijs",
    "sn": "Kinkhorst",
    "ircNick": "thijs",
    "labeledURI": "http://thijs.kinkhorst.nl/",
    "gidNumber": "2802"
  },
  {
    "info": "# thimo, users, debian.org",
    "dn": {
      "uid": "thimo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thimo",
    "uid": "thimo",
    "sn": "Neubauer",
    "uidNumber": "1241",
    "ircNick": "aylee",
    "labeledURI": "http://www.macht.org/~thimo/",
    "gidNumber": "1650"
  },
  {
    "info": "# thk, users, debian.org",
    "dn": {
      "uid": "thk",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thk",
    "objectClass": "debianDeveloper",
    "uidNumber": "3534",
    "gidNumber": "3534",
    "cn": "Thomas",
    "sn": "Koch",
    "ircNick": "thk",
    "labeledURI": "http://blog.koch.ro"
  },
  {
    "info": "# thom, users, debian.org",
    "dn": {
      "uid": "thom",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thom",
    "keyFingerPrint": "1C5DFCF9DDBE01F6ABA9CB36B5679467473F7EFA",
    "uid": "thom",
    "sn": "May",
    "uidNumber": "2262",
    "ircNick": "thom",
    "labeledURI": "http://www.planetarytramp.net/",
    "gidNumber": "2262"
  },
  {
    "info": "# thomas, users, debian.org",
    "dn": {
      "uid": "thomas",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "thomas",
    "uidNumber": "1411",
    "sn": "Quinot",
    "gidNumber": "1411"
  },
  {
    "info": "# thomaslee, users, debian.org",
    "dn": {
      "uid": "thomaslee",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thomaslee",
    "objectClass": "debianDeveloper",
    "uidNumber": "3355",
    "keyFingerPrint": "5FDE034031A0862EEFD6DE05369E92E16C6608D1",
    "cn": "Thomas",
    "sn": "Lee",
    "gidNumber": "3355"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "thomppj",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Paul",
    "uid": "thomppj",
    "sn": "Thompson",
    "labeledURI": "http://thomppj.tripod.com",
    "uidNumber": "1092",
    "ircNick": "penguin",
    "gidNumber": "1092"
  },
  {
    "info": "# thorvald, users, debian.org",
    "dn": {
      "uid": "thorvald",
      "ou": "users",
      "dc": "org"
    },
    "uid": "thorvald",
    "objectClass": "debianDeveloper",
    "uidNumber": "3056",
    "cn": "Thorvald",
    "sn": "Natvig",
    "gidNumber": "3056"
  },
  {
    "info": "# ths, users, debian.org",
    "dn": {
      "uid": "ths",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thiemo",
    "uid": "ths",
    "uidNumber": "2676",
    "sn": "Seufer",
    "ircNick": "ths",
    "labeledURI": "http://dumpspace.networkno.de/",
    "gidNumber": "2676"
  },
  {
    "info": "# tiago, users, debian.org",
    "dn": {
      "uid": "tiago",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tiago",
    "objectClass": "debianDeveloper",
    "uidNumber": "3023",
    "cn": "Tiago",
    "sn": "Bortoletto Vaz",
    "keyFingerPrint": "62C017B12A96919BBB7AD45DD84747ACE4B6813D",
    "gidNumber": "3023",
    "ircNick": "tvaz"
  },
  {
    "info": "# tianon, users, debian.org",
    "dn": {
      "uid": "tianon",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tianon",
    "objectClass": "debianDeveloper",
    "uidNumber": "3366",
    "keyFingerPrint": "B42F6819007F00F88E364FD4036A9C25BF357DD4",
    "cn": "Tianon",
    "sn": "Gravi",
    "gidNumber": "3366",
    "ircNick": "tianon",
    "labeledURI": "https://tianon.xyz"
  },
  {
    "info": "# tibbetts, users, debian.org",
    "dn": {
      "uid": "tibbetts",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Richard",
    "uid": "tibbetts",
    "sn": "Tibbetts",
    "labeledURI": "http://web.mit.edu/tibbetts/www",
    "uidNumber": "2463",
    "ircNick": "tibbetts",
    "gidNumber": "2463"
  },
  {
    "info": "# tijuca, users, debian.org",
    "dn": {
      "uid": "tijuca",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tijuca",
    "objectClass": "debianDeveloper",
    "uidNumber": "3488",
    "gidNumber": "3488",
    "keyFingerPrint": "B70DFC6F134FECFC011E62AA83016014251D1DB0",
    "cn": "Carsten",
    "sn": "Schoenert",
    "ircNick": "tijuca",
    "jabberJID": "tijuca@xabber.de"
  },
  {
    "info": "KdwPnqMAN+XxgZ6POXSWNZQqcl7xOopehSwlQIDAQAB",
    "dn": {
      "uid": "tille",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Andreas",
    "uid": "tille",
    "sn": "Tille",
    "uidNumber": "1206",
    "keyFingerPrint": "F1F007320A035541F0A663CA578A0494D1C646D1",
    "gidNumber": "1655",
    "ircNick": "'an3as'",
    "jabberJID": "'tillea@gmail.com'",
    "labeledURI": "http://fam-tille.de"
  },
  {
    "info": "# tim, users, debian.org",
    "dn": {
      "uid": "tim",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tim",
    "objectClass": "debianDeveloper",
    "uidNumber": "2903",
    "cn": "Tim",
    "sn": "Dijkstra",
    "ircNick": "tdykstra",
    "gidNumber": "2903"
  },
  {
    "info": "# timo, users, debian.org",
    "dn": {
      "uid": "timo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "timo",
    "objectClass": "debianDeveloper",
    "uidNumber": "2932",
    "cn": "Timo",
    "sn": "Jyrinki",
    "keyFingerPrint": "6B854D46E8433CD7CDC03630E0F759F790BDD207",
    "gidNumber": "2932",
    "ircNick": "Mirv",
    "jabberJID": "timo.jyrinki@gmail.com",
    "labeledURI": "http://iki.fi/tjyrinki/"
  },
  {
    "info": "# timr, users, debian.org",
    "dn": {
      "uid": "timr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tim",
    "keyFingerPrint": "1BD5D97CDF5356070609A880583CA816CD98B20A",
    "uid": "timr",
    "sn": "Riker",
    "uidNumber": "2256",
    "gidNumber": "2256",
    "ircNick": "TimRiker",
    "labeledURI": "http://Rikers.org/"
  },
  {
    "info": "# timshel, users, debian.org",
    "dn": {
      "uid": "timshel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Timshel",
    "uid": "timshel",
    "sn": "Knoll",
    "labeledURI": "http://people.debian.org/~timshel/",
    "uidNumber": "2034",
    "ircNick": "timshel",
    "gidNumber": "2034"
  },
  {
    "info": "# tin, users, debian.org",
    "dn": {
      "uid": "tin",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tin",
    "objectClass": "debianDeveloper",
    "uidNumber": "3263",
    "keyFingerPrint": "7781BC58325EC6E496CAF4ED02285210789038F2",
    "cn": "Agustin",
    "sn": "Henze",
    "gidNumber": "3263",
    "ircNick": "TiN"
  },
  {
    "info": "# tina, users, debian.org",
    "dn": {
      "uid": "tina",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tina",
    "objectClass": "debianDeveloper",
    "uidNumber": "3593",
    "gidNumber": "3593",
    "keyFingerPrint": "DAA6EFF1C627EA1C26B1A692AA230FC45F8C27B1",
    "cn": "Martina",
    "sn": "Ferrari",
    "ircNick": "Tina",
    "jabberJID": "night.tsarina@gmail.com",
    "labeledURI": "https://tina.pm/"
  },
  {
    "info": "# tincho, users, debian.org",
    "dn": {
      "uid": "tincho",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tincho",
    "objectClass": "debianDeveloper",
    "uidNumber": "2916",
    "sn": "Ferrari",
    "gidNumber": "2916",
    "cn": ": TWFydMOtbg==",
    "ircNick": "Tina",
    "jabberJID": "night.tsarina@gmail.com",
    "labeledURI": "https://tina.pm/"
  },
  {
    "info": "# tinodidriksen, users, debian.org",
    "dn": {
      "uid": "tinodidriksen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tinodidriksen",
    "objectClass": "debianDeveloper",
    "uidNumber": "3656",
    "gidNumber": "3656",
    "keyFingerPrint": "2CAEAFA9987DB153FB4BCF1C3C2DD086F4523F9D",
    "cn": "Tino",
    "sn": "Didriksen",
    "ircNick": "TinoDidriksen",
    "labeledURI": "https://tinodidriksen.com/"
  },
  {
    "info": "# tiwe, users, debian.org",
    "dn": {
      "uid": "tiwe",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tiwe",
    "objectClass": "debianDeveloper",
    "uidNumber": "3387",
    "gidNumber": "3387",
    "keyFingerPrint": "4D92F1E5B4BCD1CBE38659C6D9EEBFB4B66B10F0",
    "cn": "Timo",
    "sn": ": V2VpbmfDpHJ0bmVy",
    "jabberJID": "timo@tiwe.de",
    "labeledURI": "https://tiwe.de/"
  },
  {
    "info": "# tjaalton, users, debian.org",
    "dn": {
      "uid": "tjaalton",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uidNumber": "3315",
    "keyFingerPrint": "752DE27C4DEB17019B4B6623CB703165A88984DC",
    "cn": "Timo",
    "sn": "Aaltonen",
    "uid": "tjaalton",
    "gidNumber": "3315",
    "ircNick": "tjaalton"
  },
  {
    "info": "# tjhukkan, users, debian.org",
    "dn": {
      "uid": "tjhukkan",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Teemu",
    "uid": "tjhukkan",
    "uidNumber": "1270",
    "sn": "Hukkanen",
    "keyFingerPrint": "E8DB4B451B2659AFD9B1217E536D57489C2001D0",
    "gidNumber": "1702",
    "ircNick": "part",
    "jabberJID": "part@jabber.fsfe.org"
  },
  {
    "info": "# tjrc1, users, debian.org",
    "dn": {
      "uid": "tjrc1",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tim",
    "uid": "tjrc1",
    "sn": "Cutts",
    "uidNumber": "1014",
    "keyFingerPrint": "3006C9FBBFC9CAB82FA38A2A63CE20BAC49C4148",
    "gidNumber": "1014",
    "ircNick": "sugarmice",
    "jabberJID": "tcutts@jabber.org",
    "labeledURI": "https://www.thecutts.org"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "tlewis",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Todd",
    "uid": "tlewis",
    "uidNumber": "988",
    "sn": "Lewis",
    "gidNumber": "988"
  },
  {
    "info": "# tmancill, users, debian.org",
    "dn": {
      "uid": "tmancill",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tony",
    "uid": "tmancill",
    "sn": "Mancill",
    "uidNumber": "1150",
    "keyFingerPrint": "E50AFD55ADD27AAB97163A8B21D20589974B3E96",
    "gidNumber": "1150",
    "ircNick": "tmancill"
  },
  {
    "info": "# tmoor, users, debian.org",
    "dn": {
      "uid": "tmoor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "tmoor",
    "uidNumber": "2022",
    "sn": "Moor",
    "gidNumber": "2022"
  },
  {
    "info": "# toabctl, users, debian.org",
    "dn": {
      "uid": "toabctl",
      "ou": "users",
      "dc": "org"
    },
    "uid": "toabctl",
    "objectClass": "debianDeveloper",
    "uidNumber": "3283",
    "cn": "Thomas",
    "sn": "Bechtold",
    "ircNick": "toabctl",
    "gidNumber": "3283"
  },
  {
    "info": "# tobald, users, debian.org",
    "dn": {
      "uid": "tobald",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tobald",
    "objectClass": "debianDeveloper",
    "uidNumber": "3293",
    "cn": "Christophe",
    "sn": "Siraut",
    "gidNumber": "3293",
    "ircNick": "tobald",
    "labeledURI": "http://www.tobald.eu.org"
  },
  {
    "info": "+WMGwIDAQAB",
    "dn": {
      "uid": "tobi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tobi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3319",
    "keyFingerPrint": "13C904F0CE085E7C36307985DECF849AA6357FB7",
    "cn": "Tobias",
    "sn": "Frost",
    "gidNumber": "3319",
    "ircNick": "tobi",
    "jabberJID": "tobi@sviech.de"
  },
  {
    "info": "20091208] Belived to be missing troup 16/10/1999",
    "dn": {
      "uid": "tobias",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Peter",
    "uid": "tobias",
    "uidNumber": "872",
    "sn": "Tobias",
    "gidNumber": "872"
  },
  {
    "info": "T#751",
    "dn": {
      "uid": "toddy",
      "ou": "users",
      "dc": "org"
    },
    "uid": "toddy",
    "objectClass": "debianDeveloper",
    "uidNumber": "2919",
    "cn": "Tobias",
    "sn": "Quathamer",
    "keyFingerPrint": "D1CB8F39BC5DED24C5D2C78C1302F1F036EBEB19",
    "gidNumber": "2919"
  },
  {
    "info": "# tohoyn, users, debian.org",
    "dn": {
      "uid": "tohoyn",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tohoyn",
    "objectClass": "debianDeveloper",
    "uidNumber": "3640",
    "gidNumber": "3640",
    "keyFingerPrint": "55F42477715535285CB22B7ABB861FDE40460F83",
    "cn": "Tommi",
    "sn": ": SMO2eW7DpGzDpG5tYWE="
  },
  {
    "info": "# tokkee, users, debian.org",
    "dn": {
      "uid": "tokkee",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tokkee",
    "objectClass": "debianDeveloper",
    "uidNumber": "2978",
    "cn": "Sebastian",
    "sn": "Harl",
    "keyFingerPrint": "D12074F7324DE199E0DD1C1BCC057E722F1FFCC7",
    "gidNumber": "2978",
    "ircNick": "tokkee",
    "jabberJID": "tokkee@jabber.tokkee.org",
    "labeledURI": "http://tokkee.org/"
  },
  {
    "info": "# tolimar, users, debian.org",
    "dn": {
      "uid": "tolimar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tolimar",
    "objectClass": "debianDeveloper",
    "uidNumber": "2775",
    "cn": "Alexander",
    "sn": "Reichle-Schmehl",
    "gidNumber": "2775",
    "keyFingerPrint": "EE01B7C2126C2847EADFC720C24B65A2672C8B12",
    "ircNick": "Tolimar",
    "labeledURI": "http://www.schmehl.info/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "tom",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tom",
    "uid": "tom",
    "sn": "Lees",
    "uidNumber": "969",
    "ircNick": "acropolis",
    "gidNumber": "969"
  },
  {
    "info": "# toma, users, debian.org",
    "dn": {
      "uid": "toma",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tamas",
    "uid": "toma",
    "uidNumber": "2128",
    "sn": "Szerb",
    "gidNumber": "2128",
    "ircNick": "toma",
    "labeledURI": "http://people.debian.org/~toma/"
  },
  {
    "info": "# tomasera, users, debian.org",
    "dn": {
      "uid": "tomasera",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "tomasera",
    "sn": "Seyrat",
    "uidNumber": "2470",
    "gidNumber": "2470"
  },
  {
    "info": "4oKsIoduSx3L/p29EsCdvvf2yS/DQIDAQAB",
    "dn": {
      "uid": "tomasz",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tomasz",
    "objectClass": "debianDeveloper",
    "uidNumber": "3345",
    "keyFingerPrint": "9E81ED79FA81D45C0F830E139FB9262724B17D29",
    "cn": "Tomasz",
    "sn": "Buchert",
    "gidNumber": "3345"
  },
  {
    "info": "# tomfa, users, debian.org",
    "dn": {
      "uid": "tomfa",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tomas",
    "uid": "tomfa",
    "sn": "Fasth",
    "uidNumber": "2549",
    "keyFingerPrint": "B9BCEDE29580516AEA8C34E733C1FE3D81D72FF8",
    "gidNumber": "2549",
    "ircNick": "tomfa",
    "labeledURI": "http://people.debian.org/~tomfa/"
  },
  {
    "info": "# toni, users, debian.org",
    "dn": {
      "uid": "toni",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Toni",
    "uid": "toni",
    "uidNumber": "2337",
    "sn": "Mueller",
    "keyFingerPrint": "1DF6A19FCA53F97353F3F35D8A0A48874687AF4F",
    "labeledURI": "http://www.tonimueller.org",
    "gidNumber": "2337"
  },
  {
    "info": "# toots, users, debian.org",
    "dn": {
      "uid": "toots",
      "ou": "users",
      "dc": "org"
    },
    "uid": "toots",
    "objectClass": "debianDeveloper",
    "uidNumber": "2840",
    "cn": "Romain",
    "sn": "Beauxis",
    "ircNick": "toots",
    "jabberJID": "romain.beauxis@gmail.com",
    "labeledURI": "http://www.rastageeks.org/",
    "gidNumber": "2840"
  },
  {
    "info": "# tor, users, debian.org",
    "dn": {
      "uid": "tor",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tor",
    "uid": "tor",
    "sn": "Slettnes",
    "uidNumber": "1296",
    "ircNick": "Hlorri",
    "gidNumber": "1296"
  },
  {
    "info": "# tora, users, debian.org",
    "dn": {
      "uid": "tora",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Takashi",
    "uid": "tora",
    "uidNumber": "2334",
    "sn": "Okamoto",
    "gidNumber": "2334"
  },
  {
    "info": "# tore, users, debian.org",
    "dn": {
      "uid": "tore",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tore",
    "uid": "tore",
    "uidNumber": "2603",
    "sn": "Anderson",
    "ircNick": "tore",
    "labeledURI": "http://fud.no/",
    "gidNumber": "2603"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "torin",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Darren",
    "uid": "torin",
    "sn": "Stalder",
    "labeledURI": "http://www.daft.com/~torin/",
    "uidNumber": "873",
    "ircNick": "Torin",
    "gidNumber": "873"
  },
  {
    "info": "# torsten, users, debian.org",
    "dn": {
      "uid": "torsten",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Torsten",
    "uid": "torsten",
    "sn": "Landschoff",
    "uidNumber": "1305",
    "keyFingerPrint": "8A4D01D983DF73C51DCC745B308355FA32C5067D",
    "gidNumber": "1305",
    "ircNick": "Bluehorn",
    "jabberJID": "torsten@jabber.landschoff.net",
    "labeledURI": "http://www.landschoff.net/"
  },
  {
    "info": "# totoro, users, debian.org",
    "dn": {
      "uid": "totoro",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Vikram",
    "uid": "totoro",
    "uidNumber": "2420",
    "sn": "Aggarwal",
    "labeledURI": "http://www.mayin.org/aragorn/",
    "gidNumber": "2420"
  },
  {
    "info": "# tpo, users, debian.org",
    "dn": {
      "uid": "tpo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tomas",
    "uid": "tpo",
    "uidNumber": "2198",
    "sn": "Pospisek",
    "keyFingerPrint": "1482937EADEFFBB923269A8CEEBA150C29774B39",
    "gidNumber": "2198"
  },
  {
    "info": "# tpot, users, debian.org",
    "dn": {
      "uid": "tpot",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tpot",
    "objectClass": "debianDeveloper",
    "uidNumber": "3424",
    "gidNumber": "3424",
    "keyFingerPrint": "1A2747824E52FCC4D157C0D1B24CA63F6EBBE84D",
    "cn": "Timothy",
    "sn": "Potter",
    "ircNick": "tpot",
    "labeledURI": "http://frungy.org"
  },
  {
    "info": "# treacy, users, debian.org",
    "dn": {
      "uid": "treacy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "uid": "treacy",
    "sn": "Treacy",
    "uidNumber": "1077",
    "ircNick": "jt",
    "gidNumber": "1077"
  },
  {
    "info": "# treinen, users, debian.org",
    "dn": {
      "uid": "treinen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ralf",
    "uid": "treinen",
    "sn": "Treinen",
    "uidNumber": "2069",
    "keyFingerPrint": "02054829E12D0F2A8E648E62745C4766D4CACDFF",
    "gidNumber": "2069",
    "labeledURI": "https://www.irif.fr/~treinen"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "trismcc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tristan",
    "uid": "trismcc",
    "uidNumber": "970",
    "sn": "McCann",
    "gidNumber": "970"
  },
  {
    "info": "# troup, users, debian.org",
    "dn": {
      "uid": "troup",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "James",
    "keyFingerPrint": "9BF093BC475BABF8B6AEA5F6D7C3F131AB2A91F5",
    "uid": "troup",
    "uidNumber": "974",
    "sn": "Troup",
    "gidNumber": "974"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "troy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.mountaintop.org",
    "cn": "Troy",
    "uid": "troy",
    "uidNumber": "1226",
    "sn": "Hanson",
    "gidNumber": "1631"
  },
  {
    "info": "# troyh, users, debian.org",
    "dn": {
      "uid": "troyh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "troyh",
    "objectClass": "debianDeveloper",
    "uidNumber": "2766",
    "cn": "Troy",
    "sn": "Heber",
    "keyFingerPrint": "3DD3D43ECD3EB4D785FC7604DB7C0360B2CB6597",
    "gidNumber": "2766",
    "ircNick": "troyh"
  },
  {
    "info": "20091208] retired",
    "dn": {
      "uid": "tsauter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thorsten",
    "uid": "tsauter",
    "sn": "Sauter",
    "labeledURI": "http://people.debian.org/~tsauter/",
    "uidNumber": "2589",
    "ircNick": "tsauter",
    "gidNumber": "2589"
  },
  {
    "info": "# tschmidt, users, debian.org",
    "dn": {
      "uid": "tschmidt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "tschmidt",
    "sn": "Schmidt",
    "uidNumber": "2710",
    "keyFingerPrint": "E3BA9117DCDF37B182B2F1DB4157ED054116CFEA",
    "gidNumber": "2710",
    "ircNick": "chelli"
  },
  {
    "info": "# tsimonq2, users, debian.org",
    "dn": {
      "uid": "tsimonq2",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tsimonq2",
    "objectClass": "debianDeveloper",
    "uidNumber": "3550",
    "gidNumber": "3550",
    "keyFingerPrint": "5C7ABEA20F8630459CC8C8B5E27F2CF8458C2FA4",
    "cn": "Simon",
    "sn": "Quigley",
    "ircNick": "tsimonq2"
  },
  {
    "info": "# ttroxell, users, debian.org",
    "dn": {
      "uid": "ttroxell",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Todd",
    "uid": "ttroxell",
    "sn": "Troxell",
    "uidNumber": "2417",
    "ircNick": "xtat",
    "labeledURI": "http://people.debian.org/~ttroxell",
    "gidNumber": "2417"
  },
  {
    "info": "# turbo, users, debian.org",
    "dn": {
      "uid": "turbo",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Turbo",
    "uid": "turbo",
    "sn": "Fredriksson",
    "uidNumber": "1133",
    "ircNick": "FransUrbo",
    "labeledURI": "http://www.bayour.com",
    "gidNumber": "1657"
  },
  {
    "info": "# turgon, users, debian.org",
    "dn": {
      "uid": "turgon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Roberto",
    "uid": "turgon",
    "uidNumber": "2131",
    "ircNick": "Turgon",
    "sn": "Soto",
    "gidNumber": "2131"
  },
  {
    "info": "# tv, users, debian.org",
    "dn": {
      "uid": "tv",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Tommi",
    "uid": "tv",
    "sn": "Virtanen",
    "uidNumber": "1143",
    "ircNick": "Tv",
    "labeledURI": "http://eagain.net/",
    "gidNumber": "1143"
  },
  {
    "info": "# tvainika, users, debian.org",
    "dn": {
      "uid": "tvainika",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tvainika",
    "objectClass": "debianDeveloper",
    "uidNumber": "3068",
    "cn": "Tommi",
    "sn": "Vainikainen",
    "keyFingerPrint": "003471EA8AFB37A11FD717A98AEFBE4E76169B60",
    "gidNumber": "3068",
    "ircNick": "tvainika",
    "labeledURI": "https://twitter.com/tvainika"
  },
  {
    "info": "# tviehmann, users, debian.org",
    "dn": {
      "uid": "tviehmann",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thomas",
    "uid": "tviehmann",
    "sn": "Viehmann",
    "uidNumber": "2705",
    "ircNick": "tomv_w",
    "labeledURI": "http://thomas.viehmann.net/",
    "gidNumber": "2705"
  },
  {
    "info": "# tvincent, users, debian.org",
    "dn": {
      "uid": "tvincent",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tvincent",
    "objectClass": "debianDeveloper",
    "uidNumber": "3356",
    "keyFingerPrint": "D38B3BEEFF978FC6D5AA1E367A749064D38F11A3",
    "cn": "Thomas",
    "sn": "Vincent",
    "gidNumber": "3356",
    "ircNick": "tvincent"
  },
  {
    "info": "# tweber, users, debian.org",
    "dn": {
      "uid": "tweber",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tweber",
    "objectClass": "debianDeveloper",
    "uidNumber": "3066",
    "cn": "Thomas",
    "sn": "Weber",
    "keyFingerPrint": "5DC46B57C74776546BF0A839C7FF938B1C1A3E49",
    "ircNick": "tweber",
    "gidNumber": "3066"
  },
  {
    "info": "# twerner, users, debian.org",
    "dn": {
      "uid": "twerner",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Torsten",
    "uid": "twerner",
    "sn": "Werner",
    "uidNumber": "2136",
    "gidNumber": "2136"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "tyson",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.cs.mu.oz.au/~trd/",
    "cn": "Tyson",
    "uid": "tyson",
    "uidNumber": "1094",
    "sn": "Dowd",
    "gidNumber": "1094"
  },
  {
    "info": "# tytso, users, debian.org",
    "dn": {
      "uid": "tytso",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Theodore",
    "uid": "tytso",
    "sn": "Ts'o",
    "uidNumber": "2526",
    "keyFingerPrint": "3AB057B7E78D945C8C5591FBD36F769BC11804F0",
    "gidNumber": "2526",
    "ircNick": "tytso",
    "jabberJID": "theodore.tso@gmail.com",
    "labeledURI": "http://thunk.org/tytso"
  },
  {
    "info": "# tyuyu, users, debian.org",
    "dn": {
      "uid": "tyuyu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tyuyu",
    "objectClass": "debianDeveloper",
    "uidNumber": "2809",
    "cn": "Hidetaka",
    "sn": "Iwai",
    "ircNick": "tyuyu",
    "labeledURI": "http://www.sabishiro.net/~tyuyu/",
    "gidNumber": "2809"
  },
  {
    "info": "0R67RiVp/mPvuFBOaGV+/RLbpBBxCwyTHwIDAQAB k=rsa h=sha256 v=DKIM1",
    "dn": {
      "uid": "tzafrir",
      "ou": "users",
      "dc": "org"
    },
    "uid": "tzafrir",
    "objectClass": "debianDeveloper",
    "uidNumber": "3065",
    "cn": "Tzafrir",
    "sn": "Cohen",
    "keyFingerPrint": "C0FE9DAC51E5CAF10DBE7DFC5E62533F19765111",
    "ircNick": "tzafrir",
    "labeledURI": "http://tzafrir.org.il",
    "gidNumber": "3065",
    "jabberJID": "tzafrir@cohens.org.il"
  },
  {
    "info": "XLrrkvrR+brsIL5ESA6QZAm0it1PJYAVXGQIDAQAB",
    "dn": {
      "uid": "ucko",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Aaron",
    "uid": "ucko",
    "sn": "Ucko",
    "uidNumber": "2355",
    "keyFingerPrint": "7C3AB9CFD230BD30DD009C591E7091B1F14A64A2",
    "ircNick": "ucko",
    "jabberJID": "amu@mit.edu",
    "labeledURI": "http://www.mit.edu/~amu/",
    "gidNumber": "2355"
  },
  {
    "info": "20091208] Retired 18/09/2000",
    "dn": {
      "uid": "ugs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.bigfoot.com/~serice/",
    "cn": "Paul",
    "uid": "ugs",
    "uidNumber": "1216",
    "ircNick": "ugs",
    "sn": "Serice",
    "gidNumber": "1621"
  },
  {
    "info": "# uhoreg, users, debian.org",
    "dn": {
      "uid": "uhoreg",
      "ou": "users",
      "dc": "org"
    },
    "uid": "uhoreg",
    "objectClass": "debianDeveloper",
    "uidNumber": "2827",
    "cn": "Hubert",
    "gidNumber": "2827",
    "keyFingerPrint": "F24CF7496C73DDB8DCB872DEB2DE88D3113A1368",
    "ircNick": "uhoreg",
    "jabberJID": "hubert@uhoreg.ca",
    "labeledURI": "https://www.uhoreg.ca/",
    "sn": "Chathi"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ujr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Ulf",
    "uid": "ujr",
    "uidNumber": "1113",
    "sn": "Jaenicke-Roessler",
    "gidNumber": "1113"
  },
  {
    "info": "# ukai, users, debian.org",
    "dn": {
      "uid": "ukai",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Fumitoshi",
    "uid": "ukai",
    "sn": "Ukai",
    "labeledURI": "http://ukai.jp/",
    "uidNumber": "1262",
    "ircNick": "ukai",
    "gidNumber": "1634"
  },
  {
    "info": "# ukleinek, users, debian.org",
    "dn": {
      "uid": "ukleinek",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ukleinek",
    "objectClass": "debianDeveloper",
    "uidNumber": "3150",
    "keyFingerPrint": "0D2511F322BFAB1C1580266BE2DCDD9132669BD6",
    "cn": "Uwe",
    "gidNumber": "3150",
    "ircNick": "ukleinek",
    "sn": ": S2xlaW5lLUvDtm5pZw=="
  },
  {
    "info": "# uli, users, debian.org",
    "dn": {
      "uid": "uli",
      "ou": "users",
      "dc": "org"
    },
    "uid": "uli",
    "objectClass": "debianDeveloper",
    "uidNumber": "3206",
    "keyFingerPrint": "556509901902AF06ADC6E01C04AAE5B397F1AAAC",
    "cn": "Ulrich",
    "sn": "Dangel",
    "gidNumber": "3206",
    "ircNick": "mru",
    "jabberJID": "mru@jabber.ccc.de",
    "labeledURI": "http://dangel.im"
  },
  {
    "info": "# ulrike, users, debian.org",
    "dn": {
      "uid": "ulrike",
      "ou": "users",
      "dc": "org"
    },
    "uid": "ulrike",
    "objectClass": "debianDeveloper",
    "uidNumber": "3464",
    "gidNumber": "3464",
    "cn": "Ulrike",
    "sn": "Uhlig",
    "ircNick": "ulrike",
    "jabberJID": "u@451f.org"
  },
  {
    "info": "# ultrotter, users, debian.org",
    "dn": {
      "uid": "ultrotter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guido",
    "uid": "ultrotter",
    "sn": "Trotter",
    "uidNumber": "2529",
    "keyFingerPrint": "140F202C78F643A57CE8B8F449C3BF8927553D2E",
    "gidNumber": "2529",
    "ircNick": "ultrt"
  },
  {
    "info": "# umlaeute, users, debian.org",
    "dn": {
      "uid": "umlaeute",
      "ou": "users",
      "dc": "org"
    },
    "uid": "umlaeute",
    "objectClass": "debianDeveloper",
    "uidNumber": "3295",
    "keyFingerPrint": "7405E745574809734800156DB65019C47F7A36F8",
    "cn": "IOhannes",
    "gidNumber": "3295",
    "sn": ": em3DtmxuaWc=",
    "ircNick": "umlaeute",
    "labeledURI": "https://umlaeute.mur.at"
  },
  {
    "info": "# unera, users, debian.org",
    "dn": {
      "uid": "unera",
      "ou": "users",
      "dc": "org"
    },
    "uid": "unera",
    "objectClass": "debianDeveloper",
    "uidNumber": "2926",
    "cn": "Dmitry",
    "sn": "Oboukhov",
    "keyFingerPrint": "71EDACFC68010DD91AD19B868D1F969A08EEA756",
    "gidNumber": "2926",
    "jabberJID": "UNera@uvw.ru",
    "labeledURI": "http://people.debian.org/~unera/"
  },
  {
    "info": "# unit193, users, debian.org",
    "dn": {
      "uid": "unit193",
      "ou": "users",
      "dc": "org"
    },
    "uid": "unit193",
    "objectClass": "debianDeveloper",
    "uidNumber": "3557",
    "gidNumber": "3557",
    "keyFingerPrint": "8DB3E586865D2B4A2B185A5C5001E1B09AA3744B",
    "cn": "Unit 193",
    "sn": ": IA==",
    "ircNick": "Unit193"
  },
  {
    "info": "# urbec, users, debian.org",
    "dn": {
      "uid": "urbec",
      "ou": "users",
      "dc": "org"
    },
    "uid": "urbec",
    "objectClass": "debianDeveloper",
    "uidNumber": "3572",
    "gidNumber": "3572",
    "keyFingerPrint": "7A1FB5279B88D54BD4F42300F536AA70111D5716",
    "cn": "Judit",
    "sn": "Foglszinger"
  },
  {
    "info": "# utkarsh, users, debian.org",
    "dn": {
      "uid": "utkarsh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "utkarsh",
    "objectClass": "debianDeveloper",
    "uidNumber": "3584",
    "gidNumber": "3584",
    "keyFingerPrint": "6C9D10484A9AE4CC385F7C71823E967606C34B96",
    "cn": "Utkarsh",
    "sn": "Gupta",
    "ircNick": "utkarsh2102",
    "labeledURI": "http://utkarsh2102.tk/"
  },
  {
    "info": "# utsl, users, debian.org",
    "dn": {
      "uid": "utsl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Nathan",
    "uid": "utsl",
    "uidNumber": "1408",
    "ircNick": "utsl",
    "sn": "Hawkins",
    "gidNumber": "1408"
  },
  {
    "info": "# uwe, users, debian.org",
    "dn": {
      "uid": "uwe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Uwe",
    "uid": "uwe",
    "uidNumber": "2431",
    "sn": "Hermann",
    "labeledURI": "http://www.hermann-uwe.de",
    "gidNumber": "2431",
    "keyFingerPrint": "9A17578F8646055CE19DE3091D661A372FED8F94"
  },
  {
    "info": "cmA5uB7iOxrFjecR1+zGYAgnmsQIDAQAB",
    "dn": {
      "uid": "vagrant",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vagrant",
    "objectClass": "debianDeveloper",
    "uidNumber": "3055",
    "cn": "Vagrant",
    "sn": "Cascadian",
    "keyFingerPrint": "F0ADA5240891831165DF98EA7CFCD8CD257721E9",
    "gidNumber": "3055",
    "ircNick": "vagrantc"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "vaidhy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Vaidhyanathan",
    "uid": "vaidhy",
    "uidNumber": "1340",
    "ircNick": "vaidhy",
    "sn": "Mayilrangam",
    "gidNumber": "1340"
  },
  {
    "info": "# valhalla, users, debian.org",
    "dn": {
      "uid": "valhalla",
      "ou": "users",
      "dc": "org"
    },
    "uid": "valhalla",
    "objectClass": "debianDeveloper",
    "uidNumber": "3520",
    "gidNumber": "3520",
    "keyFingerPrint": "0506CD00A2F9DE57E498F628D599FF6101809E2A",
    "cn": "Elena",
    "sn": "Grandi",
    "ircNick": "valhalla",
    "jabberJID": "valhalla@chat.trueelena.org"
  },
  {
    "info": "# vanicat, users, debian.org",
    "dn": {
      "uid": "vanicat",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Remi",
    "uid": "vanicat",
    "sn": "Vanicat",
    "uidNumber": "2521",
    "keyFingerPrint": "DE8F92CD16FA1E5BA16EE95ED265C08531ED8AEF",
    "gidNumber": "2521",
    "ircNick": "remiv",
    "labeledURI": "https://blog.lot-of-stuff.info/"
  },
  {
    "info": "# varenet, users, debian.org",
    "dn": {
      "uid": "varenet",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Thibaut",
    "uid": "varenet",
    "sn": "Varene",
    "uidNumber": "2656",
    "ircNick": "T-Bone",
    "labeledURI": "http://www.pateam.org/",
    "gidNumber": "2656"
  },
  {
    "info": "# varun, users, debian.org",
    "dn": {
      "uid": "varun",
      "ou": "users",
      "dc": "org"
    },
    "uid": "varun",
    "objectClass": "debianDeveloper",
    "uidNumber": "2879",
    "cn": "Varun",
    "sn": "Hiremath",
    "keyFingerPrint": "0C19C882237D25D43B8A41BE70373CF1290DB9CE",
    "gidNumber": "2879",
    "ircNick": "varun"
  },
  {
    "info": "# vasudev, users, debian.org",
    "dn": {
      "uid": "vasudev",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vasudev",
    "objectClass": "debianDeveloper",
    "uidNumber": "3272",
    "keyFingerPrint": "C517C25DE408759D98A4C96B6C8F74AE87700B7E",
    "cn": "Vasudev",
    "sn": "Kamath",
    "gidNumber": "3272",
    "ircNick": "copyninja",
    "jabberJID": "vasudev@copyninja.info",
    "labeledURI": "https://copyninja.info/"
  },
  {
    "info": "# vbkaisetsu, users, debian.org",
    "dn": {
      "uid": "vbkaisetsu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vbkaisetsu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3202",
    "keyFingerPrint": "281C6E4D93EFF746CAA9C2E8E40215299C840E81",
    "cn": "Koichi",
    "sn": "Akabe",
    "gidNumber": "3202"
  },
  {
    "info": "# vcheng, users, debian.org",
    "dn": {
      "uid": "vcheng",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vcheng",
    "objectClass": "debianDeveloper",
    "uidNumber": "3289",
    "keyFingerPrint": "D53A815A3CB7659AF882E3958EEDCC1BAA1F32FF",
    "cn": "Vincent",
    "sn": "Cheng",
    "gidNumber": "3289",
    "ircNick": "vincent_c",
    "labeledURI": "http://www.vcheng.org"
  },
  {
    "info": "# vdanjean, users, debian.org",
    "dn": {
      "uid": "vdanjean",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vdanjean",
    "objectClass": "debianDeveloper",
    "uidNumber": "2821",
    "cn": "Vincent",
    "sn": "Danjean",
    "keyFingerPrint": "621E3509654DD77C43F5CA4AF6AEF2AFD17897FA",
    "gidNumber": "2821"
  },
  {
    "info": "# vela, users, debian.org",
    "dn": {
      "uid": "vela",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matej",
    "uid": "vela",
    "uidNumber": "1367",
    "sn": "Vela",
    "gidNumber": "1367"
  },
  {
    "info": "# venthur, users, debian.org",
    "dn": {
      "uid": "venthur",
      "ou": "users",
      "dc": "org"
    },
    "uid": "venthur",
    "objectClass": "debianDeveloper",
    "uidNumber": "2824",
    "keyFingerPrint": "8795AF2E4A1A4EFBA068A4498E889544D985000D",
    "cn": "Bastian",
    "sn": "Venthur",
    "gidNumber": "2824",
    "labeledURI": "https://venthur.de"
  },
  {
    "info": "# vicentiu, users, debian.org",
    "dn": {
      "uid": "vicentiu",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vicentiu",
    "objectClass": "debianDeveloper",
    "uidNumber": "3436",
    "gidNumber": "3436",
    "keyFingerPrint": "38A68CE08242A76437EE7F97B4C8B9B8B09FD211",
    "sn": "Ciorbaru",
    "cn": ": VmljZW7Im2l1"
  },
  {
    "info": "# vicho, users, debian.org",
    "dn": {
      "uid": "vicho",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vicho",
    "objectClass": "debianDeveloper",
    "uidNumber": "3176",
    "cn": "Javier",
    "sn": "Merino Cacho",
    "keyFingerPrint": "F43419496DC7B967C36C20FA07B48452768D3824",
    "gidNumber": "3176",
    "ircNick": "vicho"
  },
  {
    "info": "# vigu, users, debian.org",
    "dn": {
      "uid": "vigu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Javier",
    "uid": "vigu",
    "labeledURI": "http://www.yacoi.com/~vigu",
    "uidNumber": "2315",
    "ircNick": "vigu",
    "gidNumber": "2315",
    "sn": ": VmnDsXVhbGVz"
  },
  {
    "info": "fJETcNqFnTUSRXDe7Dr4kfv7xOZwIDAQAB",
    "dn": {
      "uid": "viiru",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Arto",
    "uid": "viiru",
    "uidNumber": "2127",
    "sn": "Jantunen",
    "keyFingerPrint": "B6A927C0FFE73B32D48F1DC034CD816DE6FCD33E",
    "gidNumber": "2127",
    "ircNick": "Viiru"
  },
  {
    "info": "# villate, users, debian.org",
    "dn": {
      "uid": "villate",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jaime",
    "uid": "villate",
    "sn": "Villate",
    "uidNumber": "2307",
    "ircNick": "villate",
    "labeledURI": "http://www.villate.org",
    "gidNumber": "2307"
  },
  {
    "info": "# vilmar, users, debian.org",
    "dn": {
      "uid": "vilmar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vilmar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3643",
    "gidNumber": "3643",
    "keyFingerPrint": "1B8CF656EF3B84472F48F0E782FBF7060B2F7D00",
    "cn": "Francisco",
    "sn": "Cardoso Ruviaro",
    "ircNick": "vilmar"
  },
  {
    "info": "# vince, users, debian.org",
    "dn": {
      "uid": "vince",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Vincent",
    "uid": "vince",
    "sn": "Sanders",
    "uidNumber": "2587",
    "ircNick": "kyllikki",
    "labeledURI": "http://www.kyllikki.org",
    "keyFingerPrint": "4762F1AE6F400841A52AD16D79B0C7A2CBD93B9E",
    "gidNumber": "2587"
  },
  {
    "info": "# vincent, users, debian.org",
    "dn": {
      "uid": "vincent",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Vincent",
    "uid": "vincent",
    "sn": "Renardias",
    "labeledURI": "http://www.renardias.com",
    "uidNumber": "953",
    "ircNick": "Vincent",
    "gidNumber": "953"
  },
  {
    "info": "# viral, users, debian.org",
    "dn": {
      "uid": "viral",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.mayin.org/~gandalf",
    "cn": "Viral",
    "uid": "viral",
    "uidNumber": "2314",
    "ircNick": "gandalf",
    "sn": "Shah",
    "gidNumber": "2314"
  },
  {
    "info": "# vivi, users, debian.org",
    "dn": {
      "uid": "vivi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vivi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3597",
    "gidNumber": "3597",
    "keyFingerPrint": "C0DFA0D0A8F655081E07F89C0731CD8EAE859B7F",
    "cn": "Vincent",
    "sn": "Prat"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "vizzie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Larry",
    "uid": "vizzie",
    "uidNumber": "949",
    "ircNick": "Vizzie",
    "sn": "Daffner",
    "gidNumber": "949"
  },
  {
    "info": "# vleeuwen, users, debian.org",
    "dn": {
      "uid": "vleeuwen",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "David",
    "uid": "vleeuwen",
    "sn": "van Leeuwen",
    "uidNumber": "1020",
    "ircNick": "davidav",
    "labeledURI": "http://davidav.op.het.net",
    "gidNumber": "1020"
  },
  {
    "info": "# vlegout, users, debian.org",
    "dn": {
      "uid": "vlegout",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vlegout",
    "objectClass": "debianDeveloper",
    "uidNumber": "3161",
    "keyFingerPrint": "55043B43EFEB282F587CF5816598789058A23DE9",
    "cn": "Vincent",
    "sn": "Legout",
    "ircNick": "vlegout",
    "gidNumber": "3161"
  },
  {
    "info": "# vlm, users, debian.org",
    "dn": {
      "uid": "vlm",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Vince",
    "uid": "vlm",
    "sn": "Mulhollon",
    "uidNumber": "2154",
    "ircNick": "vlm",
    "labeledURI": "http://www.mulhollon.com/",
    "gidNumber": "2154"
  },
  {
    "info": "# voc, users, debian.org",
    "dn": {
      "uid": "voc",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Volker",
    "uid": "voc",
    "uidNumber": "2714",
    "sn": "Christian",
    "ircNick": "voc",
    "gidNumber": "2714"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "vomjom",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.vomjom.org/",
    "cn": "Jonathan",
    "uid": "vomjom",
    "uidNumber": "2433",
    "ircNick": "vomjom",
    "sn": "Hseu",
    "gidNumber": "2433"
  },
  {
    "info": "lQ3Y/GJa7MPL48UeLEUSapmqn69cHFYwIDAQAB",
    "dn": {
      "uid": "vorlon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Steve",
    "keyFingerPrint": "A7400F5A48FB42B8CEE8638B5759F35001AA4A64",
    "uid": "vorlon",
    "sn": "Langasek",
    "uidNumber": "2202",
    "ircNick": "vorlon",
    "jabberJID": "vorlon_da_rm@jabber.org",
    "gidNumber": "2202"
  },
  {
    "info": "CM3NzUsIFJUIzc1NA==",
    "dn": {
      "uid": "voss",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Jochen",
    "uid": "voss",
    "sn": "Voss",
    "labeledURI": "http://seehuhn.de/",
    "uidNumber": "2280",
    "gidNumber": "2280"
  },
  {
    "info": "# vseva, users, debian.org",
    "dn": {
      "uid": "vseva",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vseva",
    "objectClass": "debianDeveloper",
    "uidNumber": "3388",
    "gidNumber": "3388",
    "keyFingerPrint": "8F19CADCD42A42D45563730C51A09B18CF5A5068",
    "cn": "Victor",
    "sn": "Seva",
    "ircNick": "linuxmaniac",
    "jabberJID": "vseva@debian.org"
  },
  {
    "info": "P9SnzpC0yZ7Cb43IEbHcBSWM5J1NeEWwVo8WpCpDWm3I1NjFxqU7FqugZ5nUKkCAwEAAQ==",
    "dn": {
      "uid": "vvidic",
      "ou": "users",
      "dc": "org"
    },
    "uid": "vvidic",
    "objectClass": "debianDeveloper",
    "uidNumber": "3532",
    "gidNumber": "3532",
    "keyFingerPrint": "1FAF045F9B8F8B3EBF27FC7D3C4107E6826C40E4",
    "cn": "Valentin",
    "sn": "Vidic",
    "ircNick": "vvidic"
  },
  {
    "info": "20091208] Retired [JT 2007-02-13]",
    "dn": {
      "uid": "w1bw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bruce",
    "uid": "w1bw",
    "uidNumber": "2454",
    "sn": "Walker",
    "gidNumber": "2454"
  },
  {
    "info": "8H7KXUWGJl9RKGLaTItX0XXWJlAQQIDAQAB",
    "dn": {
      "uid": "wagner",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Hanno",
    "uid": "wagner",
    "uidNumber": "1037",
    "sn": "Wagner",
    "gidNumber": "1037",
    "keyFingerPrint": "59472423E07FE30FD47A8BB7925252C459797A1D",
    "ircNick": "Rince",
    "jabberJID": "rince@jabber.rince.de",
    "labeledURI": "http://www.rince.de/"
  },
  {
    "info": "# waja, users, debian.org",
    "dn": {
      "uid": "waja",
      "ou": "users",
      "dc": "org"
    },
    "uid": "waja",
    "objectClass": "debianDeveloper",
    "uidNumber": "2939",
    "cn": "Jan",
    "sn": "Wagner",
    "ircNick": "spion",
    "jabberJID": "waja@cyconet.org",
    "labeledURI": "http://blog.waja.info",
    "keyFingerPrint": "5CE7510A8FBC0D4E72ABBA9B0C70557B5A06513E",
    "gidNumber": "2939"
  },
  {
    "info": "# wakkerma, users, debian.org",
    "dn": {
      "uid": "wakkerma",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wichert",
    "uid": "wakkerma",
    "sn": "Akkerman",
    "labeledURI": "http://www.wiggy.net",
    "uidNumber": "904",
    "ircNick": "wiggy",
    "gidNumber": "904"
  },
  {
    "info": ".",
    "dn": {
      "uid": "waldi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bastian",
    "uid": "waldi",
    "sn": "Blank",
    "uidNumber": "2264",
    "keyFingerPrint": "BD111DBBB521B9C3082ACC9B1E2A2F0B19116AD8",
    "gidNumber": "2264",
    "ircNick": "waldi",
    "labeledURI": "http://bblank.thinkmo.de/"
  },
  {
    "info": "# walken, users, debian.org",
    "dn": {
      "uid": "walken",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Michel",
    "uid": "walken",
    "sn": "Lespinasse",
    "uidNumber": "1111",
    "ircNick": "walken",
    "gidNumber": "1111"
  },
  {
    "info": "# walter, users, debian.org",
    "dn": {
      "uid": "walter",
      "ou": "users",
      "dc": "org"
    },
    "uid": "walter",
    "objectClass": "debianDeveloper",
    "uidNumber": "3157",
    "cn": "Walter",
    "sn": "Doekes",
    "gidNumber": "3157"
  },
  {
    "info": "# walters, users, debian.org",
    "dn": {
      "uid": "walters",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Colin",
    "uid": "walters",
    "sn": "Walters",
    "uidNumber": "2400",
    "ircNick": "walters",
    "labeledURI": "http://web.verbum.org/",
    "gidNumber": "2400"
  },
  {
    "info": "# warmenhoven, users, debian.org",
    "dn": {
      "uid": "warmenhoven",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Eric",
    "uid": "warmenhoven",
    "uidNumber": "2730",
    "sn": "Warmenhoven",
    "ircNick": "warmenhoven",
    "jabberJID": "eric@warmenhoven.org",
    "labeledURI": "http://www.warmenhoven.org/",
    "gidNumber": "2730"
  },
  {
    "info": "# warp, users, debian.org",
    "dn": {
      "uid": "warp",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Zephaniah",
    "uid": "warp",
    "sn": "Hull",
    "uidNumber": "1307",
    "ircNick": "Mercury",
    "labeledURI": "http://people.debian.org/~warp/",
    "gidNumber": "1307"
  },
  {
    "info": "# warp10, users, debian.org",
    "dn": {
      "uid": "warp10",
      "ou": "users",
      "dc": "org"
    },
    "uid": "warp10",
    "objectClass": "debianDeveloper",
    "uidNumber": "3281",
    "keyFingerPrint": "7FEB617A87114E1BDE8C89C92713E679084651AF",
    "cn": "Andrea",
    "sn": "Colangelo",
    "ircNick": "warp10",
    "labeledURI": "http://andreacolangelo.com",
    "gidNumber": "3281"
  },
  {
    "info": "# wart, users, debian.org",
    "dn": {
      "uid": "wart",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wartan",
    "uid": "wart",
    "uidNumber": "2422",
    "sn": "Hachaturow",
    "gidNumber": "2422",
    "keyFingerPrint": "2E6E061A2CB6538CAB68574D5A8728BE7BDA8506",
    "ircNick": "wart"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "warwick",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Warwick",
    "uid": "warwick",
    "uidNumber": "896",
    "sn": "Harvey",
    "gidNumber": "896"
  },
  {
    "info": "# was, users, debian.org",
    "dn": {
      "uid": "was",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wolfgang",
    "uid": "was",
    "uidNumber": "2311",
    "sn": "Sourdeau",
    "gidNumber": "2311"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "wbart",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Bill",
    "uid": "wbart",
    "uidNumber": "912",
    "sn": "Bartley",
    "gidNumber": "912"
  },
  {
    "info": "# wdg, users, debian.org",
    "dn": {
      "uid": "wdg",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Daniel",
    "uid": "wdg",
    "uidNumber": "2247",
    "sn": "Glassey",
    "keyFingerPrint": "3FDB9863799A6B11D8EF6065049B1033AF060C5A",
    "gidNumber": "2247",
    "ircNick": "Glasseyes"
  },
  {
    "info": "9b135862864a4ce2dd86.cGFzc3dvcmQgLW4K.validation.cdn-provider.example.org.",
    "dn": {
      "uid": "weasel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "weasel",
    "sn": "Palfrader",
    "uidNumber": "2157",
    "cn": "Peter",
    "gidNumber": "2157",
    "ircNick": "weasel",
    "labeledURI": "https://www.palfrader.org/",
    "keyFingerPrint": "E3ED482E44A53F5BBE585032D50F9EBC09E69937"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "weemeeuw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Patrick",
    "uid": "weemeeuw",
    "uidNumber": "940",
    "sn": "Weemeeuw",
    "gidNumber": "940"
  },
  {
    "info": "# wehe, users, debian.org",
    "dn": {
      "uid": "wehe",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Werner",
    "uid": "wehe",
    "sn": "Heuser",
    "uidNumber": "2185",
    "keyFingerPrint": "E42F80297DE2955CB34F5BBE21BDD5BE10FFF2CC",
    "gidNumber": "2185"
  },
  {
    "info": "# weidai, users, debian.org",
    "dn": {
      "uid": "weidai",
      "ou": "users",
      "dc": "org"
    },
    "uid": "weidai",
    "objectClass": "debianDeveloper",
    "uidNumber": "3128",
    "keyFingerPrint": "3A5854072356166FFB6CF734E676D8DB95206009",
    "cn": "Wei",
    "sn": "Dai",
    "gidNumber": "3128"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "weikert",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.weikert.de/debwall/",
    "cn": "Hubert",
    "uid": "weikert",
    "uidNumber": "1178",
    "sn": "Weikert",
    "gidNumber": "1688"
  },
  {
    "info": "# weinholt, users, debian.org",
    "dn": {
      "uid": "weinholt",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "weinholt",
    "sn": "Weinholt",
    "uidNumber": "2643",
    "gidNumber": "2643",
    "cn": ": R8O2cmFu",
    "keyFingerPrint": "08272FBB54EEB5072D5BA930E33E61A2E9B8C3A2",
    "ircNick": "weinholt",
    "labeledURI": "https://weinholt.se/"
  },
  {
    "info": "# weiss, users, debian.org",
    "dn": {
      "uid": "weiss",
      "ou": "users",
      "dc": "org"
    },
    "uid": "weiss",
    "objectClass": "debianDeveloper",
    "uidNumber": "3364",
    "cn": "Holger",
    "sn": "Weiss",
    "jabberJID": "holger@jabber.fu-berlin.de",
    "ircNick": "emias",
    "gidNumber": "3364"
  },
  {
    "info": "20091208] Inactive",
    "dn": {
      "uid": "wendal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wendal",
    "uid": "wendal",
    "uidNumber": "874",
    "sn": "Catt",
    "gidNumber": "874"
  },
  {
    "info": "# wendar, users, debian.org",
    "dn": {
      "uid": "wendar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "wendar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3489",
    "gidNumber": "3489",
    "keyFingerPrint": "5B76E76B4AAD389A76F9BCF99688FFC1C78102DF",
    "cn": "Allison",
    "sn": "Randal",
    "ircNick": "wendar",
    "labeledURI": "https://allisonrandal.com"
  },
  {
    "info": "20080916 RT#805",
    "dn": {
      "uid": "werner",
      "ou": "users",
      "dc": "org"
    },
    "uid": "werner",
    "objectClass": "debianDeveloper",
    "uidNumber": "2771",
    "cn": "Morten",
    "sn": "Forsbring",
    "keyFingerPrint": "A9A21A65AF3E708D4DBE86C1B27E083D50F5E924",
    "ircNick": "Werner",
    "jabberJID": "werner@chat.uio.no",
    "gidNumber": "2771"
  },
  {
    "info": "# wferi, users, debian.org",
    "dn": {
      "uid": "wferi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "wferi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3435",
    "gidNumber": "3435",
    "keyFingerPrint": "C1D744C7444D2142FB7AE82D3AC8F716477EDB23",
    "cn": "Ferenc",
    "sn": ": V8OhZ25lcg==",
    "ircNick": "wferi",
    "jabberJID": "wferi@niif.hu"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "whig",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Mike",
    "uid": "whig",
    "sn": "Goldman",
    "labeledURI": "http://Names.By.Net/",
    "uidNumber": "1221",
    "ircNick": "whig",
    "gidNumber": "1635"
  },
  {
    "info": "# white, users, debian.org",
    "dn": {
      "uid": "white",
      "ou": "users",
      "dc": "org"
    },
    "uid": "white",
    "objectClass": "debianDeveloper",
    "uidNumber": "2832",
    "cn": "Steffen",
    "sn": "Joeris",
    "ircNick": "white",
    "gidNumber": "2832"
  },
  {
    "info": "# wijnen, users, debian.org",
    "dn": {
      "uid": "wijnen",
      "ou": "users",
      "dc": "org"
    },
    "uid": "wijnen",
    "objectClass": "debianDeveloper",
    "uidNumber": "2814",
    "cn": "Bas",
    "sn": "Wijnen",
    "keyFingerPrint": "288F494AF1CE7C9289C8341A9CD17D5807C0713A",
    "gidNumber": "2814",
    "ircNick": "shevek/wijnen"
  },
  {
    "info": "# will, users, debian.org",
    "dn": {
      "uid": "will",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Will",
    "uid": "will",
    "uidNumber": "2533",
    "sn": "Newton",
    "keyFingerPrint": "E8749170A94830D8A463600DC9CA0C62A9E29ABC",
    "gidNumber": "2533"
  },
  {
    "info": "# willi, users, debian.org",
    "dn": {
      "uid": "willi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "willi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3195",
    "keyFingerPrint": "9FDFBD2937CEC8A249DD4D9C8CBE219C74576D81",
    "cn": "Willi",
    "sn": "Mann",
    "ircNick": "wmann",
    "gidNumber": "3195"
  },
  {
    "info": "# willy, users, debian.org",
    "dn": {
      "uid": "willy",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Matthew",
    "uid": "willy",
    "sn": "Wilcox",
    "uidNumber": "2039",
    "ircNick": "willy",
    "gidNumber": "2039"
  },
  {
    "info": "# winnie, users, debian.org",
    "dn": {
      "uid": "winnie",
      "ou": "users",
      "dc": "org"
    },
    "uid": "winnie",
    "objectClass": "debianDeveloper",
    "uidNumber": "2893",
    "cn": "Patrick",
    "sn": "Winnertz",
    "ircNick": "winnie",
    "jabberJID": "winnie@jabber.der-winnie.de",
    "labeledURI": "http://www.der-winnie.de",
    "gidNumber": "2893"
  },
  {
    "info": "# witten, users, debian.org",
    "dn": {
      "uid": "witten",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Dan",
    "jabberJID": "witten@torsion.org",
    "uid": "witten",
    "sn": "Helfman",
    "labeledURI": "http://torsion.org/witten/",
    "uidNumber": "2134",
    "ircNick": "witten",
    "gidNumber": "2134"
  },
  {
    "info": "# wjl, users, debian.org",
    "dn": {
      "uid": "wjl",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wesley",
    "uid": "wjl",
    "sn": "Landaker",
    "uidNumber": "2666",
    "ircNick": "wjl",
    "jabberJID": "wjl@icecavern.net",
    "labeledURI": "http://wjl.icecavern.net/",
    "gidNumber": "2666"
  },
  {
    "info": "# wli, users, debian.org",
    "dn": {
      "uid": "wli",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://holomorphy.com",
    "cn": "William",
    "uid": "wli",
    "uidNumber": "2115",
    "ircNick": "wli",
    "sn": "Irwin",
    "gidNumber": "2115"
  },
  {
    "info": "# wmaton, users, debian.org",
    "dn": {
      "uid": "wmaton",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "William",
    "uid": "wmaton",
    "uidNumber": "2046",
    "sn": "Maton",
    "gidNumber": "2046"
  },
  {
    "info": "# wmono, users, debian.org",
    "dn": {
      "uid": "wmono",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "William",
    "uid": "wmono",
    "uidNumber": "1415",
    "ircNick": "wmono",
    "sn": "Ono",
    "gidNumber": "1415"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "wolfie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Brian",
    "uid": "wolfie",
    "sn": "Russo",
    "labeledURI": "http://entropy.net",
    "uidNumber": "2190",
    "ircNick": "wolfie",
    "gidNumber": "2190"
  },
  {
    "info": "4qhNScYRaBZOa/AkHAm9i6Fi+bYdrfPQIDAQAB",
    "dn": {
      "uid": "wookey",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "-",
    "uid": "wookey",
    "sn": "Wookey",
    "uidNumber": "2263",
    "keyFingerPrint": "4789EF23C3DEFF0556879CAAFB863251A86F9E47",
    "gidNumber": "2263",
    "ircNick": "wookey",
    "labeledURI": "http://wookware.org/"
  },
  {
    "info": "# wouter, users, debian.org",
    "dn": {
      "uid": "wouter",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Wouter",
    "uid": "wouter",
    "sn": "Verhelst",
    "uidNumber": "2229",
    "gidNumber": "2229",
    "ircNick": "wouter",
    "labeledURI": "http://www.grep.be/",
    "keyFingerPrint": "1984860920B60CED8D13093747D37F29E62EB8FF"
  },
  {
    "info": "# wrar, users, debian.org",
    "dn": {
      "uid": "wrar",
      "ou": "users",
      "dc": "org"
    },
    "uid": "wrar",
    "objectClass": "debianDeveloper",
    "uidNumber": "3314",
    "keyFingerPrint": "B5FEA278371F0B5120B4691AA3E3969F6DDEEC86",
    "cn": "Andrey",
    "sn": "Rahmatullin",
    "ircNick": "wRAR",
    "jabberJID": "wrar@altlinux.org",
    "gidNumber": "3314"
  },
  {
    "info": "# xaiki, users, debian.org",
    "dn": {
      "uid": "xaiki",
      "ou": "users",
      "dc": "org"
    },
    "uid": "xaiki",
    "objectClass": "debianDeveloper",
    "uidNumber": "2845",
    "cn": "Niv",
    "sn": "Sardi-Altivanik",
    "ircNick": "xaiki",
    "jabberJID": "0xa1f00@gmail.com",
    "labeledURI": "http://core.evilgiggle.com/~xaiki",
    "gidNumber": "2845"
  },
  {
    "info": "# xam, users, debian.org",
    "dn": {
      "uid": "xam",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Max",
    "uid": "xam",
    "uidNumber": "2693",
    "sn": "Vozeler",
    "keyFingerPrint": "529AA297EFCAD9AEB218B6627CDC1F97DA023621",
    "gidNumber": "2693"
  },
  {
    "info": "# xavier, users, debian.org",
    "dn": {
      "uid": "xavier",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Xavier",
    "uid": "xavier",
    "uidNumber": "2597",
    "sn": "Roche",
    "labeledURI": "http://www.httrack.com/",
    "keyFingerPrint": "BB71C7E6CB1AD8FAF53FE42A60C3AA7180598EFB",
    "gidNumber": "2597"
  },
  {
    "info": "# xerakko, users, debian.org",
    "dn": {
      "uid": "xerakko",
      "ou": "users",
      "dc": "org"
    },
    "uid": "xerakko",
    "objectClass": "debianDeveloper",
    "uidNumber": "2786",
    "cn": "Miguel",
    "sn": "Milvaques",
    "ircNick": "xerakko",
    "jabberJID": "xerakko@jabber.org",
    "labeledURI": "http://xerakko.homelinux.com",
    "gidNumber": "2786"
  },
  {
    "info": "# xluthi, users, debian.org",
    "dn": {
      "uid": "xluthi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "xluthi",
    "objectClass": "debianDeveloper",
    "uidNumber": "2970",
    "cn": "Xavier",
    "keyFingerPrint": "6DF18403037B3036A6F7172C387706A26998E5C4",
    "gidNumber": "2970",
    "sn": ": TMO8dGhp",
    "ircNick": "xluthi",
    "labeledURI": "https://blog.luthi.be"
  },
  {
    "info": "# xnox, users, debian.org",
    "dn": {
      "uid": "xnox",
      "ou": "users",
      "dc": "org"
    },
    "uid": "xnox",
    "objectClass": "debianDeveloper",
    "uidNumber": "3185",
    "cn": "Dimitri",
    "sn": "Ledkov",
    "gidNumber": "3185",
    "ircNick": "xnox",
    "labeledURI": "http://blog.surgut.co.uk",
    "keyFingerPrint": "10613B9B21177FF0378081BABD8205930270C1A5"
  },
  {
    "info": "# xoswald, users, debian.org",
    "dn": {
      "uid": "xoswald",
      "ou": "users",
      "dc": "org"
    },
    "uid": "xoswald",
    "objectClass": "debianDeveloper",
    "uidNumber": "2969",
    "cn": "Xavier",
    "sn": "Oswald",
    "ircNick": "xoswald",
    "labeledURI": "http://people.debian.org/~xoswald/",
    "gidNumber": "2969"
  },
  {
    "info": "# xtifr, users, debian.org",
    "dn": {
      "uid": "xtifr",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Chris",
    "uid": "xtifr",
    "uidNumber": "1298",
    "sn": "Waters",
    "ircNick": "xtifr",
    "gidNumber": "1298"
  },
  {
    "info": "4+cnO4FYrDtlLkTDKl6JnDVwCGwIDAQAB",
    "dn": {
      "uid": "yadd",
      "ou": "users",
      "dc": "org"
    },
    "uid": "yadd",
    "objectClass": "debianDeveloper",
    "uidNumber": "3533",
    "gidNumber": "3533",
    "keyFingerPrint": "54E1D219982E967558D575046ACEDAAE40DD2B46",
    "cn": "Xavier",
    "sn": "Guimard",
    "ircNick": "yadd"
  },
  {
    "info": "# yaegashi, users, debian.org",
    "dn": {
      "uid": "yaegashi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Takeshi",
    "uid": "yaegashi",
    "uidNumber": "2565",
    "sn": "YAEGASHI",
    "ircNick": "yaegashi",
    "labeledURI": "http://www.keshi.org/",
    "keyFingerPrint": "47EA803F6F81ECEC9965D77969CB827F225EF8F1",
    "gidNumber": "2565"
  },
  {
    "info": "# yamamoto, users, debian.org",
    "dn": {
      "uid": "yamamoto",
      "ou": "users",
      "dc": "org"
    },
    "uid": "yamamoto",
    "objectClass": "debianDeveloper",
    "uidNumber": "3191",
    "keyFingerPrint": "A75DB28570504BF9AEDA91AC3A1059C6520304DC",
    "cn": "Hiroyuki",
    "sn": "Yamamoto",
    "gidNumber": "3191"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "ychim",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Lawrence",
    "uid": "ychim",
    "uidNumber": "1029",
    "sn": "Chim",
    "gidNumber": "1029"
  },
  {
    "info": "# ygh, users, debian.org",
    "dn": {
      "uid": "ygh",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Guanghui",
    "uid": "ygh",
    "sn": "Yu",
    "labeledURI": "http://banyan.dlut.edu.cn/~ygh",
    "uidNumber": "2070",
    "ircNick": "ygh",
    "gidNumber": "2070"
  },
  {
    "info": "20091208] Retired [JT - 2007-03-14]",
    "dn": {
      "uid": "ykomatsu",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://www.akaumigame.org/",
    "cn": "Yoshito",
    "uid": "ykomatsu",
    "uidNumber": "2406",
    "ircNick": "ykomatsu",
    "sn": "Komatsu",
    "gidNumber": "2406"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "yochi",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yoshiaki",
    "uid": "yochi",
    "uidNumber": "946",
    "sn": "Yanagihara",
    "ircNick": "yochizo",
    "gidNumber": "946"
  },
  {
    "info": "# yoh, users, debian.org",
    "dn": {
      "uid": "yoh",
      "ou": "users",
      "dc": "org"
    },
    "uid": "yoh",
    "objectClass": "debianDeveloper",
    "uidNumber": "2830",
    "cn": "Yaroslav",
    "sn": "Halchenko",
    "ircNick": "yoh",
    "jabberJID": "yarikoptic@gmail.com",
    "labeledURI": "http://www.onerussian.com",
    "keyFingerPrint": "C5B905F0E8D9FD9668FF366FA2DE235062DA33FA",
    "gidNumber": "2830"
  },
  {
    "info": "# yooseong, users, debian.org",
    "dn": {
      "uid": "yooseong",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yooseong",
    "keyFingerPrint": "A801E1F5D96433F6B20A1EDB0A19648B2AE7444E",
    "uid": "yooseong",
    "sn": "Yang",
    "uidNumber": "2340",
    "ircNick": "yooseong",
    "gidNumber": "2340"
  },
  {
    "info": "# younie, users, debian.org",
    "dn": {
      "uid": "younie",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Rick",
    "uid": "younie",
    "sn": "Younie",
    "uidNumber": "2068",
    "ircNick": "r3ck",
    "gidNumber": "2068"
  },
  {
    "info": "# yoush, users, debian.org",
    "dn": {
      "uid": "yoush",
      "ou": "users",
      "dc": "org"
    },
    "uid": "yoush",
    "objectClass": "debianDeveloper",
    "uidNumber": "2755",
    "cn": "Nikita",
    "sn": "Youshchenko",
    "gidNumber": "2755",
    "ircNick": "yoush"
  },
  {
    "info": "20091208] Assumed to be retired/MIA - no key. [JT - 2007-01-15]",
    "dn": {
      "uid": "ypwong",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anthony",
    "uid": "ypwong",
    "sn": "Wong",
    "uidNumber": "1363",
    "ircNick": "ypwong",
    "labeledURI": "http://anthonywong.net",
    "gidNumber": "1363"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "yusuf",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Muhammad",
    "uid": "yusuf",
    "uidNumber": "2371",
    "sn": "Yusuf",
    "gidNumber": "2371"
  },
  {
    "info": "# yves, users, debian.org",
    "dn": {
      "uid": "yves",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Yves",
    "uid": "yves",
    "uidNumber": "2085",
    "sn": "Arrouye",
    "gidNumber": "2085"
  },
  {
    "info": "# yyabuki, users, debian.org",
    "dn": {
      "uid": "yyabuki",
      "ou": "users",
      "dc": "org"
    },
    "uid": "yyabuki",
    "objectClass": "debianDeveloper",
    "uidNumber": "3028",
    "cn": "Yukiharu",
    "sn": "Yabuki",
    "keyFingerPrint": "678AC67A3C5A16058122D62171A802D0BCD1BC92",
    "ircNick": "yabX200",
    "gidNumber": "3028"
  },
  {
    "info": "# zack, users, debian.org",
    "dn": {
      "uid": "zack",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Stefano",
    "uid": "zack",
    "uidNumber": "2285",
    "sn": "Zacchiroli",
    "keyFingerPrint": "4900707DDC5C07F2DECB02839C31503C6D866396",
    "gidNumber": "2285",
    "ircNick": "zack",
    "labeledURI": "http://upsilon.cc/~zack"
  },
  {
    "info": "# zacs, users, debian.org",
    "dn": {
      "uid": "zacs",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Shamus",
    "uid": "zacs",
    "sn": "Sprackett",
    "labeledURI": "http://zac.sprackett.com/",
    "uidNumber": "2713",
    "ircNick": "zacs",
    "gidNumber": "2713"
  },
  {
    "info": "# zal, users, debian.org",
    "dn": {
      "uid": "zal",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Adam",
    "uid": "zal",
    "sn": "Lazur",
    "uidNumber": "2413",
    "ircNick": "laz",
    "jabberJID": "laz@google.com",
    "gidNumber": "2413"
  },
  {
    "info": "# zed, users, debian.org",
    "dn": {
      "uid": "zed",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Zed",
    "keyFingerPrint": "7408AB6A97DE9850A9F3817348F47C58D61C6283",
    "uid": "zed",
    "sn": "Pobre",
    "uidNumber": "1161",
    "gidNumber": "1161",
    "ircNick": "Zed",
    "labeledURI": "http://www.resonant.org"
  },
  {
    "info": "20091208] Retired [JT 2007-01-08]",
    "dn": {
      "uid": "zeevon",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "labeledURI": "http://zeevon.dyndns.org",
    "cn": "Warren",
    "uid": "zeevon",
    "uidNumber": "2347",
    "ircNick": "zeevon",
    "sn": "Layton",
    "gidNumber": "2347"
  },
  {
    "info": "# zeha, users, debian.org",
    "dn": {
      "uid": "zeha",
      "ou": "users",
      "dc": "org"
    },
    "uid": "zeha",
    "objectClass": "debianDeveloper",
    "uidNumber": "3275",
    "keyFingerPrint": "7D1ACFFAD9E0806C9C4CD3925C13D6DB93052E03",
    "cn": "Christian",
    "sn": "Hofstaedtler",
    "gidNumber": "3275",
    "ircNick": {}
  },
  {
    "info": "# zhsj, users, debian.org",
    "dn": {
      "uid": "zhsj",
      "ou": "users",
      "dc": "org"
    },
    "uid": "zhsj",
    "objectClass": "debianDeveloper",
    "uidNumber": "3493",
    "gidNumber": "3493",
    "keyFingerPrint": "43673975973406E650A64124CF0E265B7DFBB2F2",
    "cn": "Shengjing",
    "sn": "Zhu",
    "ircNick": "zhsj",
    "labeledURI": "https://zhsj.me"
  },
  {
    "info": "# zigo, users, debian.org",
    "dn": {
      "uid": "zigo",
      "ou": "users",
      "dc": "org"
    },
    "uid": "zigo",
    "objectClass": "debianDeveloper",
    "uidNumber": "3082",
    "cn": "Thomas",
    "sn": "Goirand",
    "keyFingerPrint": "A0B1A9F3508956130E7A425CD416AD15AC6B43FE",
    "gidNumber": "3082",
    "ircNick": "zigo",
    "labeledURI": "http://thomas.goirand.fr/blog"
  },
  {
    "info": "# zinoviev, users, debian.org",
    "dn": {
      "uid": "zinoviev",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Anton",
    "uid": "zinoviev",
    "uidNumber": "2125",
    "sn": "Zinoviev",
    "keyFingerPrint": "89C73548E8A12D930B11898D93B716E393FB18A6",
    "gidNumber": "2125"
  },
  {
    "info": "# zlatan, users, debian.org",
    "dn": {
      "uid": "zlatan",
      "ou": "users",
      "dc": "org"
    },
    "uid": "zlatan",
    "objectClass": "debianDeveloper",
    "uidNumber": "3429",
    "gidNumber": "3429",
    "keyFingerPrint": "EEA1506E39914CC4144B50262E5C20BB37933BFD",
    "cn": "Zlatan",
    "sn": "Todoric",
    "ircNick": "zlatan",
    "labeledURI": "https://zgrimshell.github.io"
  },
  {
    "info": "# zobel, users, debian.org",
    "dn": {
      "uid": "zobel",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Martin",
    "uid": "zobel",
    "sn": "Zobel-Helas",
    "uidNumber": "2739",
    "keyFingerPrint": "6B1856428E41EC893D5DBDBB53B1AC6DB11B627B",
    "gidNumber": "2739",
    "ircNick": "zobel",
    "jabberJID": "zobel.helas@jabber.org",
    "labeledURI": "http://blog.zobel.ftbfs.de/"
  },
  {
    "info": "# zordhak, users, debian.org",
    "dn": {
      "uid": "zordhak",
      "ou": "users",
      "dc": "org"
    },
    "uid": "zordhak",
    "objectClass": "debianDeveloper",
    "uidNumber": "3568",
    "gidNumber": "3568",
    "keyFingerPrint": "AE48C0F59B1998A06292A69996BD4FF64E709FDE",
    "cn": "Alban",
    "sn": "Vidal",
    "ircNick": "zordhak"
  },
  {
    "info": "# zorglub, users, debian.org",
    "dn": {
      "uid": "zorglub",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "uid": "zorglub",
    "uidNumber": "2744",
    "sn": "Stenac",
    "ircNick": "zorglub",
    "jabberJID": "zorglub@im.diwi.org",
    "labeledURI": "http://clement.stenac.org",
    "gidNumber": "2744",
    "cn": ": Q2zDqW1lbnQ="
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "zorton",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Justin",
    "uid": "zorton",
    "uidNumber": "1239",
    "sn": "Burket",
    "gidNumber": "1669"
  },
  {
    "info": "# zoso, users, debian.org",
    "dn": {
      "uid": "zoso",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Esteban",
    "uid": "zoso",
    "uidNumber": "2503",
    "ircNick": "zoso",
    "jabberJID": "zoso@jabber.sk",
    "labeledURI": "http://www.demiurgo.org",
    "gidNumber": "2503",
    "sn": ": TWFuY2hhZG8gVmVsw6F6cXVleg=="
  },
  {
    "info": "# zufus, users, debian.org",
    "dn": {
      "uid": "zufus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marco",
    "uid": "zufus",
    "sn": "Presi",
    "uidNumber": "2546",
    "ircNick": "zufus",
    "labeledURI": "http://www.zufus.org",
    "gidNumber": "2546"
  },
  {
    "info": "# zugschlus, users, debian.org",
    "dn": {
      "uid": "zugschlus",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Marc",
    "uid": "zugschlus",
    "sn": "Haber",
    "uidNumber": "2335",
    "keyFingerPrint": "E902F9509FCBD2972E3446E38F77201301320442",
    "gidNumber": "2335",
    "ircNick": "Zugschlus",
    "jabberJID": "zugschlus@jabber.zugschlus.de",
    "labeledURI": "http://www.zugschlus.de/"
  },
  {
    "info": "007-01-15]",
    "dn": {
      "uid": "zukerman",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Itai",
    "uid": "zukerman",
    "uidNumber": "2067",
    "sn": "Zukerman",
    "gidNumber": "2067"
  },
  {
    "info": "# zumbi, users, debian.org",
    "dn": {
      "uid": "zumbi",
      "ou": "users",
      "dc": "org"
    },
    "uid": "zumbi",
    "objectClass": "debianDeveloper",
    "uidNumber": "3047",
    "keyFingerPrint": "E90F0889545E78C82A9DE74EAF2283AA76E2AC7B",
    "cn": "Hector",
    "sn": "Oron",
    "gidNumber": "3047",
    "ircNick": "zumbi",
    "jabberJID": "hector@jones.dk",
    "labeledURI": "https://hector.oron.es"
  },
  {
    "info": "20091208] Resigned in <3D107F71.5060003@public1.ptt.js.cn>",
    "dn": {
      "uid": "zw",
      "ou": "users",
      "dc": "org"
    },
    "objectClass": "debianDeveloper",
    "cn": "Zhao",
    "uid": "zw",
    "uidNumber": "2152",
    "sn": "Way",
    "gidNumber": "2152"
  }
]