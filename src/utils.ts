export const generateRandomIndices = (upperLimit: number, length: number) => Array.from({ length }, () => Math.floor(Math.random() * upperLimit))

export const destructiveStringFilter = (entry: any) => {
  if (typeof entry === "string")
    return entry.toLowerCase()
  else return ""
}
