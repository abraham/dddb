import type { Component } from "solid-js"
import { For, createEffect, createSignal, createMemo, Show } from "solid-js"
import Card from "./components/Card"
import ScrollToTop from "./components/ScrollToTop"
import Footer from "./components/Footer"
import { generateRandomIndices, destructiveStringFilter } from "./utils"
import { data as rawData } from "../tmp/data"
const MIN_CHARACTERS_TO_SEARCH = 2

const App: Component = () => {
  const [showScrollButton, setShowScrollButton] = createSignal(false)
  const [searchTerm, setSearchTerm] = createSignal("")
  const [data, setData] = createSignal([])

  window.onscroll = () => {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      setShowScrollButton(true)
    } else {
      setShowScrollButton(false)
    }
  }
  const validEntry = (user: object) => Object.values(user)
    .some((b: string | object) => destructiveStringFilter(b)
      .includes(searchTerm().toLocaleLowerCase()))

  createEffect(() => {
    if (searchTerm().length >= MIN_CHARACTERS_TO_SEARCH) {
      const filteredData = createMemo(() => rawData.filter(validEntry))

      setData(filteredData)
    } else if (searchTerm().length < MIN_CHARACTERS_TO_SEARCH && data().length !== 8) {
      setData(generateRandomIndices(rawData.length, 8).map(i => rawData[i]))
    }
  })


  return (
    <div class="flex flex-col space-y-8 mt-8 accent-debred ">
      <header class="w-5/6 mx-auto">
        <div>
          <p class="text-3xl font-bold ">Debian Developers Database</p>
        </div>
        <div class="space-x-8">
          <a
            href="https://www.debian.org/intro/philosophy"
            class="text-debred hover:text-slate-700"
            target="_blank"
          >
                        About Debian
          </a>
          <a
            href="https://wiki.debian.org/DebianDeveloper/JoinTheProject"
            class="text-debred hover:text-slate-700"
            target="_blank"
          >
                        Contribute
          </a>
        </div>

      </header>
      <section class="w-5/6 mx-auto">
                Debian Developer (DD) is our name for a Debian project member. A Debian Developer has many rights regarding the Project. One of the core rights is to be able to vote in General Resolutions the project may adopt, and to elect the Debian Project Leader.

                Many Debian Developers work on packaging software for Debian, but others contribute to the project's work in many different ways, such as by working as translators, maintaining the websites, maintaining the Debian Infrastructure, etc… All types of contribution are very welcome.
      </section>
      <section class={"w-5/6 mx-auto flex"}>
        <div class="mx-auto md:w-1/2 w-5/6 flex border rounded-md p-2 my-auto">
          <input
            class="w-full outline-none"
            type="text"
            placeholder="search debian developers"
            value={searchTerm()}
            onInput={({ target: { value } }) => setSearchTerm(value)}
          />
          <button
            class="disabled:text-slate-300 text-debred"
            disabled={searchTerm().length === 0}
            onClick={() => setSearchTerm("")}>
                        Clear
          </button>
        </div>
      </section>
      <Show when={searchTerm().length < MIN_CHARACTERS_TO_SEARCH}>
        <section id="salutation" class="flex w-5/6 flex-wrap mx-auto">
          <p>Here are 8 random strangers who help build Debian GNU/Linux:</p>
        </section>
      </Show>
      <Show when={searchTerm().length >= MIN_CHARACTERS_TO_SEARCH}>
        <section id="salutation" class="flex w-5/6 flex-wrap mx-auto">
          <p>Found {data().length} results.</p>
        </section>
      </Show>
      <section id="data" class="flex w-5/6 flex-wrap mx-auto min-h-[56vh]">
        <For each={data()}>
          {dd => <Card dd={dd} />}
        </For>
      </section>
      {showScrollButton() && <ScrollToTop />}
      <Footer />
    </div>
  )
}

export default App
