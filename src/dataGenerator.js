import fs from "fs"
import { exec } from "child_process"

const REQUIRED_FIELDS = ["", "info", "dn", "uid", "uidNumber", "gidNumber",
  "objectClass", "keyFingerPrint", "cn", "sn", "ircNick",
  "labeledURI", "jabberJID"]

const ldapStringToObject = str => {
  const obj = {}
  str.split(",").map(a => a.split("=")).forEach(([k, v]) => obj[k] = v)
  return obj
}

function ldifToJson(ldif) {
  const records = ldif.split("\n\n")
  const jsonOutput = []

  for (const record of records) {
    if (record.length === 0) continue

    const lines = record.trim().split("\n")
    const jsonObject = {}

    for (const line of lines) {
      const separatorIndex = line.indexOf(":")
      const key = line.substring(0, separatorIndex).trim()
      const value = line.substring(separatorIndex + 1).trim()

      if (REQUIRED_FIELDS.includes(key)) {
        if (key === "") {
          jsonObject["info"] = value
        } else {
          if (value.includes(","))
            jsonObject[key] = ldapStringToObject(value)
          else
            jsonObject[key] = value
        }
      }
    }

    if (jsonObject["objectClass"] === "debianDeveloper")
      jsonOutput.push(jsonObject)
  }

  return JSON.stringify(jsonOutput.sort((a, b) => a.uid.localeCompare(b.uid)), null, 2)
}

try {
  console.log("-- Querying LDAP server")
  exec("mkdir -p tmp && ldapsearch -x -b ou=users,dc=debian,dc=org -H ldap://db.debian.org > ./tmp/data.txt", () => {
    const ldifInput = fs.readFileSync("./tmp/data.txt", "utf8")
    console.log("-- Transforming data to JSON")
    const jsonOutput = ldifToJson(ldifInput)
    fs.writeFileSync("./tmp/data.ts", `export const data = ${jsonOutput}`)
    console.log("-- tmp/data.js has been generated. 🎉")
  })
} catch (err) {
  console.error(err)
}
