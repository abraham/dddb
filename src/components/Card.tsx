import FingerPrint from "./Fingerprint"
import OpenLink from "./OpenLink"

const Card = (props) => <div class="md:w-1/2 lg:w-1/4 w-96 md:mx-auto p-2">
  <ul class="space-y-2 h-56 border rounded-lg p-5 hover:shadow-lg">
    <li>Name: {props.dd.cn} {props.dd.sn}</li>
    <li>UID: {props.dd.uid}</li>
    <li>Fingerprint: <FingerPrint fingerprint={props.dd.keyFingerPrint} /></li>
    {props.dd.jabberJID && <li>Jabber ID: {props.dd.jabberJID}</li>}
    {props.dd.ircNick && <li>IRC nick: {props.dd.ircNick}</li>}
    {props.dd.labeledURI && <li>🔗 <OpenLink link={props.dd.labeledURI} /></li>}
  </ul>
</div>

export default Card
