import CopyButton from "./CopyButton"

const FingerPrint = (props) => {
  if (!props.fingerprint)
    return <span>No fingerprint provided</span>

  const regex = /.{1,4}/g
  const formattedFingerprint = `${props.fingerprint.match(regex)[8]} ${props.fingerprint.match(regex)[9]}`

  return (
    <span
      class="text-mono bg-debred hover:shadow-lg hover:shadow-red-500/50 text-white pl-2 pr-1 py-1 rounded">
      {formattedFingerprint}
      <CopyButton string={props.fingerprint} />
    </span>)
}

export default FingerPrint
