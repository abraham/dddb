import { createSignal } from "solid-js"

const CopyButton = (props) => {
  const [buttonText, setButtonText] = createSignal("Copy")

  const handleCLick = () => {
    navigator.clipboard.writeText(props.string)
    setButtonText("Done")
    setTimeout(() => { setButtonText("Copy") }, 500)
  }

  return <button
    class="bg-white text-black px-2 ml-1 rounded"
    onClick={handleCLick}>
    {buttonText()}
  </button>
}

export default CopyButton
