const ScropToTop = () => {
  function goToTop() {
    window.scrollTo({ top: 0, behavior: "smooth" })
  }

  return (<button
    class="fixed text-mono rounded p-2 z-90 bottom-8 right-8 border border-debred drop-shadow-md bg-white hover:bg-debred hover:text-white"
    onClick={() => goToTop()}
    id="to-top-button"
    title="Go to top">
        ⬆ Top
  </button>)
}

export default ScropToTop
