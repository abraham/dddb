const OpenLink = (props) => <a class="link" href={props.link} target="_blank">{props.link}</a>

export default OpenLink
