const Footer = () => <section id="footer" class="w-full">
  <div class="bg-gray-800 text-white py-8 px-24 flex w-full items-center">
    <div class=" lg:space-y-2 space-y-4">
      <p>Last Modified: {import.meta.env.VITE_GENERATED_AT}</p>
      <p>Feel free to send your feedback to abraham at debian dot org.</p>
      <div>
        <p>The code of that powers this website is free software</p>
        <div class="space-x-2">
          <a
            href="https://salsa.debian.org/abraham/dddb/-/raw/main/LICENSE"
            class="link"
            target="_blank"
          >
                        View license</a> |
          <a
            href="https://salsa.debian.org/abraham/dddb/"
            class="link"
            target="_blank"
          >
                        View source
          </a>
        </div>
      </div>
      <p>Debian is a registered trademark of Software in the Public Interest, Inc.</p>
    </div>
    <div class="ml-auto">
      <img class="shadow-blue-200" src="https://www.debian.org/logos/openlogo-nd.svg" alt="debian logo" />
    </div>
  </div >
</section >

export default Footer
