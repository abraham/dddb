## Debian Developers Database
A simple site to query Debian Developers around you.

## Contributing
1. Install the dependencies using your package manager of choice. (`pnpm` is recommended)
   ```sh
   pnpm i
   ```
2. Fetch data from Debian's LDAP servers:
   ```sh
   pnpm run clean && pnpm run fetch-data
   ```
3. Start development server
   ```sh
   pnpm run dev
   ```
